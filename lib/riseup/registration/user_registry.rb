require 'digest'
require 'singleton'

class UserRegistry
  include Singleton

  def can_authenticate?(user, password)
    user.pwd_hash == hash_password(password, user.reg_dt.to_s)
  end

  def authenticated?(token_str)
    tokens = AuthToken.find_by_sql ['SELECT * FROM auth_tokens WHERE token = ?', token_str]
    if 1 == tokens.count
      tokens.first
    elsif 0 == tokens.count
      return false
    else
      raise("AuthTokens Storage Inconsistency. More than one same tokens occurred: #{tokens.to_s}")
    end
  end

  def register(email, password)
    unless /^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i.match(email)
      raise Riseup::Exceptions::BadEmailException.new()
    end

    user = User.new
    user.email = email
    user.nickname = email
    user.reg_dt = Time.now
    user.pwd_hash = hash_password(password, user.reg_dt.to_s)
    user.save
    user
  end

  def create_token(user)
    token = AuthToken.new
    token.user_id = user.id
    token.token = Digest::MD5.hexdigest(user.email + Time.now.to_s)
    token.cdate = Time.now
    token.save
    token
  end

  def load_token(token_str)
    AuthToken.find_by_token(token_str)
  end

  private

  def hash_password(password, salt)
    Digest::SHA1.hexdigest(password + salt)
  end
end