require File.expand_path('../../../../app/models/user', __FILE__)

module Riseup
  module Rest

    class User < BaseResource
      CRED_PUBLIC = 2
      CRED_FOR_FRIENDS = 1
      CRED_INVISIBLE = 0

      @fields_names = [:id, :nickname, :name, :lastName, :sex, :credentials, :email, :friendshipStatus, :avatarUrl, :stat, :online]

      # @param user User|Hash
      def initialize(user, friendship_status = nil)
        super()

        if user.instance_of? ::User
          set_field('id', user.id)
          set_field('nickname', user.nickname)
          set_field('name', user.name)
          set_field('lastName', user.lastname)
          set_field('sex', user.sex)
          set_field('credentials', user.credentials)
          set_field('email', user.email)
          set_field('friendshipStatus', friendship_status)
          set_field('avatarUrl', user.avatar_url == '' ? APP_CONFIG.images['noimg'].to_s : user.avatar_url)
          set_field('stat', nil)
          set_field('online', nil)
        elsif
          from_attributes(user)
        end
      end
    end

  end
end