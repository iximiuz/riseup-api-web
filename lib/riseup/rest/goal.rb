require File.expand_path('../../../../app/models/goal', __FILE__)

module Riseup
  module Rest

    class Goal < BaseResource
      @fields_names = [:id, :userId, :description, :state, :endDate, :createdAt, :stat, :img]

      # @param [Hash] params
      def initialize(params)
        assert(
            params.has_key?(:attrs) || (params.has_key?(:id) && params.has_key?(:data)),
            'Missed required params keys: [attrs] or [id && data]'
        )

        super()

        if params.has_key?(:attrs)
          from_attributes(params[:attrs])
        else
          goal = params[:data]
          self.id = params[:id]
          set_field('userId', goal.user_id)
          set_field('description', goal.description)
          set_field('state', goal.state)
          set_field('endDate', goal.end_dt)
          set_field('createdAt', goal.created_at)
          set_field('img', goal.images.nil? ? '' : goal.images)

          if params.has_key?(:stat)
            set_field('stat', params[:stat])
          else
            set_field('stat', nil)
          end

          if params.has_key?(:votes)
            set_field('votes', params[:votes])
          else
            set_field('votes', nil)
          end
        end
      end

      # Определяет, принадлежит ли цель субъекту с id.
      def belongs?(id)
        self.userId == id
      end

      def id=(id)
        @fields[:id] = _check_id(id)
      end

      ##################################################################################################################

      private

      def _check_id(id)
        assert nil == id || id.is_a?(String) || id.is_a?(Riseup::Rest::Id::GoalId), 'Wrong id: ' + id.inspect

        if id.is_a?(String)
          id = Riseup::Rest::Id::GoalId.new(id)
        end

        id
      end
    end

  end
end