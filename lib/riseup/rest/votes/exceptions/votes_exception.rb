module Riseup
  module Rest
    module Votes
      module Exceptions

        class VotesException < Riseup::Rest::Exceptions::RestException

        end

      end
    end
  end
end