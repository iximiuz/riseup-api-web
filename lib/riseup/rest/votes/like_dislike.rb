module Riseup
  module Rest
    module Votes

      class LikeDislike < AbstractVote
        def initialize(attrs)
          super()
          from_attributes(attrs)
        end

        def dislike?
          -1 == self.value.to_i
        end

        def like?
          1 == self.value.to_i
        end

        def get_value
          value.to_i
        end

        protected

        def self.fields_names
          super() << :value
        end
      end

    end
  end
end