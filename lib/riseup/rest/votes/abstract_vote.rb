module Riseup
  module Rest
    module Votes

      class AbstractVote < Riseup::Rest::BaseResource
        @votes_map = {:likedislike => LikeDislike}

        # @param [Hash] attrs
        def self.create(attrs)
          attrs = attrs.deep_symbolize_keys
          assert attrs.has_key?(:type), 'Missed key: [type] ' + attrs.inspect

          type = attrs[:type]
          if type.is_a?(String)
            type = type.to_sym
          end

          unless @votes_map.has_key?(type)
            raise Riseup::Rest::Exceptions::RestException.new('Unknown vote type [' + type + ']')
          end

          @votes_map[type].new(attrs)
        end

        def get_value
          raise Riseup::Rest::Votes::Exceptions::VotesException.new('Not implemented!')
        end

        protected

        def self.fields_names
          [:subjectId, :objectId, :objectType, :type, :createdAt]
        end
      end

    end
  end
end