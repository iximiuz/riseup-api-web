require File.expand_path('../../../../app/models/activity', __FILE__)

module Riseup
  module Rest

    class Activity < BaseResource
      PRIVACY_INVISIBLE = -1
      PRIVACY_INHERIT = 0
      PRIVACY_FOR_FRIENDS = 1
      PRIVACY_PUBLIC = 2

      # @todo: !refactor вынести :votes из списка полей в миксин
      @fields_names = [:id, :nextId, :prevId, :userId, :goalId, :description, :privacyLevel, :startDate, :startTime, :state, :cdate, :votes, :comments_count]

      attr_accessor :startDt

      # @param activity Activity|Hash
      def initialize(activity, next_id = nil, prev_id = nil, user = nil)
        super()

        set_field('user', user)

        if activity.instance_of? ::Activity
          self.id = "#{activity.user_id}_#{activity.id}"
          self.nextId = next_id
          self.prevId = prev_id
          self.goalId = activity.goal_id ? "#{activity.user_id}_#{activity.goal_id}" : nil
          self.privacyLevel = activity.privacy_level
          set_field('userId', activity.user_id)
          set_field('description', activity.description)
          set_field('startTime', Riseup::Utils::TimeUtils.extract_time(activity.start_dt))
          set_field('startDate', Riseup::Utils::TimeUtils.extract_date(activity.start_dt))
          set_field('state', activity.state)
          set_field('cdate', activity.cdate)
          set_field('comments_count', (activity.comments_count) ? activity.comments_count.to_i : 0)

          @startDt = activity.start_dt
        else
          activity = activity.deep_symbolize_keys
          unless activity.has_key?(:privacyLevel)
            activity[:privacyLevel] = PRIVACY_INHERIT
          end

          @startDt = activity[:startDate] +
              'T' + (activity.has_key?(:startTime) && activity[:startTime] ? activity[:startTime] : '00:00:00') + 'Z'

          from_attributes(activity)
        end

        # @todo: hack! fix && remove it!
        self.votes = {'-1' => 0, '1' => 0}
      end

      # Определяет, принадлежит ли активность субъекту с id.
      def belongs?(id)
        self.userId == id
      end

      def completed?
        1 == state
      end

      # @param [bool] dispense если true и не удается найти в хранилище день создает в памяти новый.
      def get_day(dispense = false)
        if dispense
          _get_days_storage.find_or_dispense(self.userId, Riseup::Utils::TimeUtils.date_to_timestamp(self.startDate))
        else
          _get_days_storage.find(self.userId, Riseup::Utils::TimeUtils.date_to_timestamp(self.startDate))
        end
      end

      def goalId=(id)
        set_field(:goalId, _check_goal_id(id))
      end

      def id=(id)
        set_field(:id, _check_id(id))
      end

      def new?
        nil == self.id
      end

      def nextId=(id)
        set_field(:nextId, _check_id(id))
      end

      def prevId=(id)
        set_field(:prevId, _check_id(id))
      end

      def privacyLevel=(level)
        set_field(:privacyLevel, level)
      end

      def set_days_storage(storage)
        @days_storage = storage
      end

      def to_json
        result = super()

        result.delete(:nextId)
        result.delete(:prevId)

        result
      end

      private ############################################################################################################

      def _get_days_storage
        unless @days_storage
          @days_storage = Riseup::Storages::DaysStorage.new
        end

        @days_storage
      end

      def _check_id(id)
        assert nil == id || id.is_a?(String) || id.is_a?(Riseup::Rest::Id::ActivityId), 'Wrong activity id: ' + id.inspect

        if id.is_a?(String)
          id = Riseup::Rest::Id::ActivityId.new(id)
        end

        id
      end

      def _check_goal_id(id)
        assert nil == id || id.is_a?(String) || id.is_a?(Riseup::Rest::Id::GoalId), 'Wrong goal id: ' + id.inspect

        if id.is_a?(String)
          id = Riseup::Rest::Id::GoalId.new(id)
        end

        id
      end
    end

  end
end