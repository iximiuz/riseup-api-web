module Riseup
  module Rest

    class RestLiveHubEvent < BaseResource
      @fields_names = [:id, :type, :generator, :subject, :createdAt]

      def initialize(id, type, generator, subject, created_at)
        super()

        set_field(:id, id)
        set_field(:type, type)
        set_field(:generator, generator)
        set_field(:subject, subject)
        set_field(:createdAt, created_at)
      end
    end

  end
end