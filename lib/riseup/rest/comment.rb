require File.expand_path('../../../../app/models/comment', __FILE__)

module Riseup
  module Rest

    class Comment < BaseResource
      @fields_names = [:id, :msg, :author, :to_user, :create_dt, :is_moderated]

      # @param comment Comment
      def initialize(comment, author, to_user = nil)
        super()

        if comment.instance_of? ::Comment
          set_field('id', comment.id)
          set_field('msg', comment.msg)
          set_field('author', author)
          set_field('to_user', to_user)
          set_field('create_dt', comment.create_dt)
          set_field('is_moderated', comment.is_moderated)
        elsif
          from_attributes(comment)
        end
      end
    end

  end
end