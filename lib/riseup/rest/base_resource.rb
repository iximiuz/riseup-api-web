module Riseup
  module Rest

    class BaseResource
      class << self
        attr_reader :fields_names
      end

      @fields_names = []


      def initialize
        @fields = {}
      end

      def self.get_type_by_resource_class(klass)
        # @todo: !defect check klass uniqueness in map!
        _get_resources_map().key(klass)
      end

      def self.get_resource_class_by_type(type)
        if type.is_a?(String)
          type = type.to_sym
        end

        res_map = _get_resources_map()
        unless res_map.has_key?(type)
          raise Riseup::Rest::Exceptions::RestException.new('Unknown resource type [' + type.inspect + ']')
        end

        res_map[type].to_s
      end

      def from_attributes(attrs)
        attrs = attrs.deep_symbolize_keys

        self.class.fields_names.each do |field_name|
          if attrs.has_key?(field_name)
            self.send("#{field_name}=".to_sym, attrs[field_name])
          else
            self.send("#{field_name}=".to_sym, nil)
          end
        end
      end

      def set_field(name, value)
        @fields[name.to_sym] = value
        self
      end

      def get_field(name)
        unless @fields.has_key?(name)
          raise "Resource has not attribute [#{name}]."
        end

        @fields[name]
      end

      def method_missing(name, *arguments)
        value = arguments[0]
        name = name.to_s

        if name[-1, 1] == '='
          field_name = name[0..-2]
          set_field(field_name.to_sym, value)
        else
          get_field(name.to_sym)
        end
      end

      # todo: !improve если поле - дата или дата-время, конвертируем во время пользователя, если это явно указано.
      def to_json
        result = {}
        self.class.fields_names.each do |field_name|
          value = get_field(field_name)
          result[field_name] =
              (value.is_a?(Riseup::Rest::BaseResource) || value.is_a?(Riseup::Rest::Id::Id)) ? value.to_json : value
        end

        if self.is_a?(Riseup::Services::Voting::Votable)
          result['votes'] = self.votes
        end

        result
      end

      private

      def self._get_resources_map()
        @resources_map ||= {:activity => Riseup::Rest::Activity, :goal => Riseup::Rest::Goal}
      end
    end

  end
end