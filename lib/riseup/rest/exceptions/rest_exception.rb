module Riseup
  module Rest
    module Exceptions

      class RestException < Riseup::Exceptions::RiseupException

      end

    end
  end
end