module Riseup
  module Rest

    class ActivitiesRegistry < BaseResource
      @fields_names = [:userId, :records]

      def initialize(user_id, records)
        super()

        self.userId = user_id
        self.records = records.map { |el| {:date => el.date, :count => el.count} }
      end
    end

  end
end