module Riseup
  module Rest

    class SearchItem < BaseResource
      USER = 1
      ACTIVITY = 2
      GOAL = 3


      @fields_names = [:id, :type, :type_text, :text, :img]

      def initialize(item)
        super()
        type = get_type(item)
        type_text = get_type_text(type)
        if item.instance_of? ::User
          set_field('id', item.id)
          set_field('type', type)
          set_field('type_text', type_text)
          set_field('text', "User #{item.lastname} #{item.name}")
          set_field('img', item.avatar_url)
        elsif item.instance_of? ::Activity
          set_field('id', item.id)
          set_field('type', type)
          set_field('type_text', type_text)
          set_field('text', item.description)
          set_field('img', '__stub__')
        elsif item.instance_of? ::Goal
          set_field('id', item.id)
          set_field('type', type)
          set_field('type_text', type_text)
          set_field('text', item.description)
          set_field('img', '__stub__')
        end
      end

      def get_type (item)
        if item.instance_of?(::User)
          return USER
        elsif item.instance_of?(::Activity)
          return ACTIVITY
        elsif item.instance_of?(::Goal)
          return GOAL
        end

        raise('Not supported search type')
      end

      def get_type_text(type)
        type = type.to_i
        if type == USER
          return 'User'
        elsif type == ACTIVITY
          return 'Activity'
        elsif type == GOAL
          return 'Goal'
        end
        raise('Not supported search type text')
      end
    end

  end
end