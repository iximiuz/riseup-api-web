module Riseup
  module Rest
    module Id

      class FriendshipId

        def self.from_string(string_key)
          key_parts = string_key.split('_')

          FriendshipId.new(key_parts[0], key_parts[1])
        end

        def initialize(one_user_id, another_user_id)
          @one_user_id = one_user_id.to_i
          @another_user_id = another_user_id.to_i
        end

        def get_mate(user_id)
          if @one_user_id == user_id.to_i
            return @another_user_id
          end

          if @another_user_id == user_id.to_i
            return @one_user_id
          end

          nil
        end

        def to_s
          (@one_user_id > @another_user_id) ? @another_user_id.to_s + '_' + @one_user_id.to_s
            : @one_user_id.to_s + '_' + @another_user_id.to_s
        end
      end

    end
  end
end