module Riseup
  module Rest
    module Id

      class Id
        def initialize(str_id)
          assert str_id.is_a?(String), 'Param must be a string.'

          @parts = str_id.split(/_/)
        end

        def ==(other_id)
          self.to_s == other_id.to_s
        end

        # @param [String|Symbol] type
        # @param [Riseup::Rest:Id:Id] id
        def self.create(type, id_val)
          @ids_map ||= {:activity => ActivityId, :goal => GoalId}

          if type.is_a?(String)
            type = type.to_sym
          end

          unless @ids_map.has_key?(type)
            raise Rest::Id::Exceptions::IdException.new('Missed id type for: ' + type.to_s)
          end

          @ids_map[type].new(id_val)
        end

        def to_json
          to_s
        end

        def to_s
          @parts.join('_')
        end
      end

    end
  end
end
