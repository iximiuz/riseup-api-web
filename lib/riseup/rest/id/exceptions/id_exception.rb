module Riseup
  module Rest
    module Id
      module Exceptions

        class IdException < Rest::Exceptions::RestException

        end

      end
    end
  end
end