module Riseup
  module Rest
    module Id

      class GoalId < Riseup::Rest::Id::Id
        def item_id
          @parts[1].to_i
        end

        def user_id
          @parts[0].to_i
        end
      end

    end
  end
end
