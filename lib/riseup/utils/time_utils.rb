module Riseup
  module Utils

    class TimeUtils
      def self.date_to_timestamp(date)
        DateTime.strptime((date.is_a?(String) ? date : date.to_s), '%Y-%m-%d').to_time.to_i
      end

      #
      # Преобразовывает строку, содержающую дату и время в YYYY:MM:DD в той же временной зоне.
      #
      def self.extract_date(dt)
        DateTime.parse((dt.is_a?(String) ? dt : dt.to_s)).strftime('%Y-%m-%d')
      end

      #
      # Преобразовывает строку, содержающую дату и время в HH:MM:SS в той же временной зоне.
      #
      def self.extract_time(dt)
        DateTime.parse((dt.is_a?(String) ? dt : dt.to_s)).strftime('%H:%M:%S')
      end

      def self.to_timestamp(dt)
        is_string = dt.is_a?(String)

        if !is_string && !dt.respond_to?(to_time)
          raise Riseup::Exceptions::RiseupException.new(
                    'Unexpected argument type: ' + dt.inspect + ' Expected formatted String or Date/Time/DateTime.'
                )
        end

        is_string ? DateTime.parse(dt).to_time.to_i : dt.to_time.to_i
      end

      # dimka3210
      # Возвращает нужный формат времени. Пояс UTC +0
      def self.get_now_utc
        format = '%Y-%m-%d %H:%M:%S'
        timestamp = Time.new.getutc().to_s
        DateTime.parse(timestamp).strftime(format)
      end
    end

  end
end