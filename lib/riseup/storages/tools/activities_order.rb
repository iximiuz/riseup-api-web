module Riseup
  module Storages
    module Tools

      class ActivitiesOrder
        def initialize(days_storage = DaysStorage.new)
          @days_storage = days_storage
        end

        # Добавляет новую активность в последовательность существующих. Нужно вызывать только
        # для только что созданных активностей.
        #
        # @param [Riseup::Rest::Id:ActivityId] id
        # @param [Riseup::Rest::Activity] activity
        def insert(id, activity)
          _check_insert_params(id, activity)

          activity_start_date = Riseup::Utils::TimeUtils.date_to_timestamp(activity.startDt)
          day = @days_storage.find_or_dispense(activity.userId, activity_start_date)

          if _need_auto_determine_order(activity)
            # нет пожелания по точному положению новой активности на дне => просто добавляем в конец
            assert(nil == activity.nextId && nil == activity.prevId)

            day.append_activity(id)
          else
            # есть пожелание по точному положению активности
            assert(nil != activity.nextId || nil != activity.prevId)

            _insert_by_sibling_id(day, id, activity, activity_start_date)
          end

          @days_storage.save(day)
        end

        def remove(activity)
          day = @days_storage.find(activity.userId, Riseup::Utils::TimeUtils.date_to_timestamp(activity.startDt))
          assert day, 'Integrity constrain failed: missed day for activity: ' + activity.inspect
          assert(
              day.has_activity(activity.id),
              'Integrity constrain failed: day [' + day.inspect + '] does not contain activity ' + activity.inspect
          )

          if day
            day.remove_activity(activity.id)
            @days_storage.save_or_remove_if_empty(day)
          end
        end

        def update(activity, old_activity)
          _check_update_params(activity)

          if _need_update_order(activity, old_activity)
            if _need_auto_determine_order(activity)
              _update_order_auto(activity, old_activity)
            else
              _update_order_explicit(activity, old_activity)
            end
          end
        end

        private ########################################################################################################

        def _ensure_next_day_state(day, next_day, next_id)
          _ensure_day_existence(day, next_day, 'next')

          if day.get_date(true) >= next_day.get_date(true)
            raise Riseup::Storages::Tools::Exceptions::OrderException.new(
                      'Next day can\'t has date less than cur day. Next [' + next_day.to_s + '] Cur [' + day.to_s + ']'
                  )
          end

          if next_id != next_day.get_first_activity
            raise Riseup::Storages::Tools::Exceptions::OrderException.new(
                      'Inconsistent order. Expected nextId [' +
                          next_day.get_first_activity.inspect + '] Given [' + next_id.inspect + ']'
                  )
          end
        end

        def _ensure_prev_day_state(day, prev_day, prev_id)
          _ensure_day_existence(day, prev_day, 'previous')

          if day.get_date(true) <= prev_day.get_date(true)
            raise Riseup::Storages::Tools::Exceptions::OrderException.new(
                      'Prev day can\'t has date greater than cur day. Prev [' + prev_day.to_s + '] Cur [' + day.to_s + ']'
                  )
          end

          if prev_id != prev_day.get_last_activity
            raise Riseup::Storages::Tools::Exceptions::OrderException.new(
                      'Inconsistent order. Expected prevId [' +
                          prev_day.get_last_activity.inspect + '] Given [' + prev_id.inspect + ']'
                  )
          end
        end

        def _ensure_day_existence(day, checking_day, direction)
          if nil == checking_day || checking_day.empty?
            raise Riseup::Storages::Tools::Exceptions::OrderException.new(
                      'Missed ' + direction + ' day for day ' + day.to_s
                  )
          end
        end

        def _check_insert_params(id, activity)
          unless id
            raise Riseup::Storages::Tools::Exceptions::OrderException.new(
                      'Logic error: can not insert not saved activity'
                  )
          end

          unless activity.new?
            raise Riseup::Storages::Tools::Exceptions::OrderException.new(
                      'Logic error: can not insert not new activity'
                  )
          end
        end

        def _check_update_params(activity)
          if activity.new?
            raise Riseup::Storages::Tools::Exceptions::OrderException.new(
                      'Logic error: can not update order for new activity'
                  )
          end
        end

        def _insert_by_next_id(day, id, activity, activity_start_date)
          if day.has_activity(activity.nextId)
            day.insert_before(activity.nextId, id)
          else
            next_day = @days_storage.find_next(activity.userId, activity_start_date)
            _ensure_next_day_state(day, next_day, activity.nextId)

            day.append_activity(id)
          end
        end

        def _insert_by_prev_id(day, id, activity, activity_start_date)
          if day.has_activity(activity.prevId)
            day.insert_after(activity.prevId, id)
          else
            prev_day = @days_storage.find_prev(activity.userId, activity_start_date)
            _ensure_prev_day_state(day, prev_day, activity.prevId)

            day.prepend_activity(id)
          end
        end

        # Вставляет активность в последовательность по заданному prevId (в приоритете ) или nextId.
        #
        # @param [Riseup::Domain::Activities::Day] day
        # @param [Riseup::Rest::Id::ActivityId] id
        # @param [Riseup::Rest::Activity] activity
        # @param [Integer] activity_start_date
        def _insert_by_sibling_id(day, id, activity, activity_start_date)
          if nil != activity.prevId
            _insert_by_prev_id(day, id, activity, activity_start_date)
          else # nil != activity.nextId
            _insert_by_next_id(day, id, activity, activity_start_date)
          end
        end

        # Определяет, нужно ли изменить порядок активностей при обновлении существующей активности.
        #
        # Порядок необходимо изменять, если явно передан nextId и/или prevId отличающийся от старых
        # значений этих параметров или же, если изменилась дата, на которую запланирована активность.
        #
        # @param [Riseup::Rest::Activity] new_activity
        # @param [Riseup::Rest::Activity] old_activity
        def _need_update_order(new_activity, old_activity)
          (nil != new_activity.nextId && old_activity.nextId != new_activity.nextId) ||
              (nil != new_activity.prevId && old_activity.prevId != new_activity.prevId) ||
              new_activity.startDate != old_activity.startDate
        end

        # Определяет, нужно серверу самостоятельно определить положение создаваемой/обновляемой активности.
        #
        # Возвращает true, если нужно определить положение активности самостоятельно,
        # так как явно не указаны ни предшествующая (prevId) этой активности ни следующая
        # (nextId) за этой активностью и false в противном случае.
        #
        def _need_auto_determine_order(activity)
          nil == activity.prevId && nil == activity.nextId
        end

        def _remove_from_old_position(old_day, id)
          assert old_day, 'Data integrity constraint failed: empty old day'

          if old_day
            old_day.remove_activity(id)
            @days_storage.save_or_remove_if_empty(old_day)
          end
        end

        # Удаляет активность с ее прошлого дня и добавляет в конец нового (возможно создавая его).
        # Дни обязательно должны быть разными, т.е. этот метод должен вызываться, если у активности
        # была изменена дата начала.
        def _update_order_auto(activity, old_activity)
          old_day = old_activity.get_day
          _remove_from_old_position(old_day, old_activity.id)

          new_day = activity.get_day(true)
          assert old_day.get_date(true) != new_day.get_date(true), 'Move activity from day to same day?'

          new_day.append_activity(activity.id)
          @days_storage.save(new_day)
        end

        def _update_order_explicit(activity, old_activity)
          # При перемещении одной активности изменяется еще до 5 активностей (если и на старом и на новом месте у искомой
          # активности есть по 2 соседа). Изменения могут затронуть от 1 до 6 дней (до 4 дней на старых и новых соседей
          # перемещаемой активности и до 2 дней на саму активность).

          #
          # TODO: assert that DATE(Activity.find(activity.nextId).startDt) >= DATE(activity.startDt) >= DATE(Activity.find(activity.prevId).startDt) ?
          #

          old_activity_start_date = Riseup::Utils::TimeUtils.date_to_timestamp(old_activity.startDt)
          new_activity_start_date = Riseup::Utils::TimeUtils.date_to_timestamp(activity.startDt)
          if old_activity_start_date == new_activity_start_date
            old_day = new_day = @days_storage.find_or_dispense(old_activity.userId, new_activity_start_date)
          else
            old_day = @days_storage.find(old_activity.userId, old_activity_start_date)
            new_day = @days_storage.find_or_dispense(activity.userId, new_activity_start_date)
          end

          _remove_from_old_position(old_day, activity.id)

          _insert_by_sibling_id(new_day, activity.id, activity, new_activity_start_date)
          @days_storage.save(new_day)
        end
      end

    end
  end
end