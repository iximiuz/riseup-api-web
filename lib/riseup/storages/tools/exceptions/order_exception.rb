module Riseup
  module Storages
    module Tools
      module Exceptions

        class OrderException < Riseup::Storages::Exceptions::StorageException

        end

      end
    end
  end
end