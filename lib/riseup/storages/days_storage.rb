module Riseup
  module Storages

    # TODO: !defect! Обернуть все методы, использующие несколько вызовов redis в транзакции
    class DaysStorage

      public
      def initialize(days_registry = Services::Nest.new('days'), days = Services::Nest.new('day'))
        @days_registry = days_registry
        @days = days
      end

      def find(user_id, timestamp)
        registry_entry = @days_registry[user_id].zrangebyscore(timestamp, timestamp, :limit => [0, 1], :with_scores => true)
        return nil if registry_entry.empty?

        date_str = registry_entry.first.first
        date = registry_entry.first.second
        Riseup::Domain::Activities::Day.new(
            user_id,
            date,
            _get_day_holder(user_id, date_str).lrange(0, -1).map { |el| Riseup::Rest::Id::ActivityId.new(el) }
        )
      end

      def find_next(user_id, timestamp)
        registry_entry = @days_registry[user_id].zrangebyscore(timestamp + 1, '+inf', :limit => [0, 1], :with_scores => true)
        return nil if registry_entry.empty?

        date_str = registry_entry.first.first
        date = registry_entry.first.second
        Riseup::Domain::Activities::Day.new(
            user_id,
            date,
            _get_day_holder(user_id, date_str).lrange(0, -1).map { |el| Riseup::Rest::Id::ActivityId.new(el) }
        )
      end

      #
      # Пытается найти день по заданным параметрам, если не удается, создает новый пустой день.
      # Если возвращается новый день, он не сохраняется в хранилище.
      #
      def find_or_dispense(user_id, timestamp)
        result = find(user_id, timestamp)
        result ? result : Riseup::Domain::Activities::Day.new(user_id, timestamp)
      end

      def find_prev(user_id, timestamp)
        registry_entry = @days_registry[user_id].zrevrangebyscore(timestamp - 1, '-inf', :limit => [0, 1], :with_scores => true)
        return nil if registry_entry.empty?

        date_str = registry_entry.first.first
        date = registry_entry.first.second
        Riseup::Domain::Activities::Day.new(
            user_id,
            date,
            _get_day_holder(user_id, date_str).lrange(0, -1).map { |el| Riseup::Rest::Id::ActivityId.new(el) }
        )
      end

      def find_range(user_id, timestamp_from, timestamp_to)
        registry_entry = @days_registry[user_id].zrangebyscore(timestamp_from, timestamp_to, :with_scores => true)

        result = []
        registry_entry.map do |day_data|
          date_str = day_data.first
          date = day_data.second

          result << Riseup::Domain::Activities::Day.new(
              user_id,
              date,
              _get_day_holder(user_id, date_str).lrange(0, -1).map { |a_id| Riseup::Rest::Id::ActivityId.new(a_id) }
          )
        end

        result
      end

      def remove(day)
        date = day.get_date
        @days_registry[day.user_id].zrem(date)
        day_holder = _get_day_holder(day.user_id, date)
        day_holder.del
      end

      def save(day)
        assert 0 < day.activities_count, 'Logic error: you should not save day without activities: ' + day.inspect

        date = day.get_date
        @days_registry[day.user_id].zadd(day.get_date(true), date)
        day_holder = _get_day_holder(day.user_id, date)
        day_holder.del
        unless day.get_activities.empty?
          day_holder.rpush(day.get_activities.map {|el| el.to_s})
        end
      end

      def save_or_remove_if_empty(day)
        if day.empty?
          remove(day)
        else
          save(day)
        end
      end

      private ##########################################################################################################
      
      def _get_day_holder(user_id, date)
        @days[date][user_id]
      end
    end

  end
end