require 'observer'

module Riseup
  module Storages
    class CommentsStorage < ResourcesStorage
      attr_accessor :user
      @user

      def fetch(entity_type, entity_id, offset=0)
        #Comment.select(%w(comments.* users.*)).where({:entity_type => entity_type, :entity_id => entity_id}).limit(20).offset(offset).joins('LEFT JOIN users ON comments.user_id=users.id')
        comments = Comment.where({:entity_type => entity_type, :entity_id => entity_id}).limit(20).offset(offset)
        comments.map { |comment|
          if comment.user_id == @user[:id]
            criteria = {:entity_type => entity_type, :entity_id => entity_id}
            CommentsDelivery.where(criteria).destroy_all
          end
        }

        comments #return
      end

      def find_by_id(id)
        Comment.find(id)
      end

      def save(params)
        comment = Comment.new
        comment.entity_id = params[:entity_id]
        comment.entity_type = params[:entity_type]
        comment.msg = params[:msg]
        comment.user_id = params[:user_id]
        comment.create_dt = Time.now.utc.iso8601
        comment.save

        criteria = {:entity_type => comment.entity_type, :entity_id => comment.entity_id}
        comment_delivery = CommentsDelivery.where(criteria)
        comment_delivery = (comment_delivery.size > 0) ? comment_delivery.first : CommentsDelivery.new
        comment_delivery[:count] = (comment_delivery.count.to_i + 1)
        comment_delivery.entity_type = comment.entity_type
        comment_delivery.entity_id = comment.entity_id
        comment_delivery.last_modify = Time.now
        comment_delivery.save

        result = comment.attributes
        result[:author] = User.find(comment.user_id)
        result #return
      end

      def del(comment_id)
        Comment.where(:id => comment_id.to_i).destroy_all()
      end


    end
  end
end