#require 'sphinx.rb'
require File.dirname(__FILE__) + '/../../../vendor/plugins/sphinx/lib/sphinx'
module Riseup
  module Storages
    class SearchStorage < ResourcesStorage
      attr_reader :total
      ALL = 0
      USER = 1
      ACTIVITY = 2
      GOAL = 3

      @total = 0


      def initialize
        @sphinx = Sphinx::Client.new
      end

      def find(search_query, filter)
        filter = valid_filter(filter)
        filters = []
        __result = []

        # Если ищем по одному типу
        if filter == '*'
          filters = %w(users activities goals)
        else
          filters.push(filter)
        end
        filters.map { |f|
          uids = []
          search_query.force_encoding('ASCII-8BIT')
          @sphinx.AddQuery(search_query, f)
          res = @sphinx.RunQueries
          res = res.first if res.size == 1 && res.instance_of?(Array)
          if res['total'].to_i > 0
            res['matches'].map { |item|
              uids.push(item['id'].to_i)
            }
            if f == 'users'
              User.find(uids).map { |user|
                __result.push(Riseup::Rest::SearchItem.new(user).to_json)
              }
            elsif f == 'activities'
              Activity.find(uids).map { |activity|
                __result.push(Riseup::Rest::SearchItem.new(activity).to_json)
              }
            elsif f == 'goals'
              Goal.find(uids).map { |goal|
                __result.push(Riseup::Rest::SearchItem.new(goal).to_json)
              }
            end
          end
        }

        return __result
      end

      private
      def valid_filter(filter)
        if filter == 'users'
          return filter
        elsif filter == 'goals'
          return filter
        elsif filter == 'activities'
          return filter
        else
          return '*'
        end
      end
    end
  end
end