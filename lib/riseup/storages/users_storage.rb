module Riseup
  module Storages
    class UsersStorage < ResourcesStorage
      def initialize(activities_storage)
        @activities_storage = activities_storage
      end

      def find_all(offset = 0, limit = 20, sort = 'ASC')
        User.all(
            :select => 'users.*, COUNT(activities.user_id) as activities_count',
            :joins => 'LEFT JOIN activities ON users.id = activities.user_id',
            :group => 'users.id',
            :order => 'users.credentials DESC, activities_count ' + sort + ', users.id DESC',
            :limit => limit,
            :offset => offset
        ).map { |user|
          Riseup::Rest::User.new(user)
        }
      end

      def find_all_with_stat(cur_user, offset = 0, limit = 20, sort = 'ASC')
        users = find_all(offset, limit, sort)
        stats = find_users_stat_multi(users.map { |record| record.id }, cur_user)
        users.each do |user|
          if user.credentials.to_i == 0
            user.stat = []
          elsif user.credentials.to_i == 1 && !Riseup::Services::ServiceProvider.instance.get_service(:friendship).friends?(user.id, cur_user[:id])
            user.stat = []
          else
            user.stat = stats[user.id]
          end
        end

        users
      end

      def find_users_stat_multi(ids, user)
        utc_now = Time.now.gmtime.to_i
        utc_seconds_offset_on_user = 60 * user.timezone
        user_local = utc_now - utc_seconds_offset_on_user

        @activities_storage.find_users_stat(ids, DateTime.strptime("#{user_local}", '%s').to_s(:db))
      end

    end
  end
end