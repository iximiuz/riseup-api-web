module Riseup
  module Storages

    class ActivitiesRegistryStorage < ResourcesStorage
      # @param [int] user_id
      def find(user_id)
        registry_records = Activities::ActivitiesRegistry.where('user_id = ?', user_id).order('date ASC')
        Riseup::Rest::ActivitiesRegistry.new(user_id, registry_records)
      end
    end

  end
end