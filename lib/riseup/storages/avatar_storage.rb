require 'RMagick'

module Riseup
  module Storages
    class AvatarStorage
      def save_avatar(tmp_file)
        if tmp_file == ''
          return ''
        end
        image = Magick::Image.read(tmp_file.tempfile.path).first
        image.format=APP_CONFIG.images['avatar']['format']
        image = image.scale(APP_CONFIG.images['avatar']['width'], APP_CONFIG.images['avatar']['height'])
        hash = filename_hash(tmp_file)
        image.write('jpeg:'+APP_CONFIG.images['path'].to_s + hash.to_s + '.' + APP_CONFIG.images['avatar']['format'])
        hash + '.' + APP_CONFIG.images['avatar']['format'] # return
      end

      def save_img(image, path)
        image.write('jpeg:'+path)
        path # return
      end

      def create_complect(tmp_file)
        if tmp_file == ''
          return ''
        end

        image = Magick::Image.read(tmp_file.tempfile.path).first
        image.format=APP_CONFIG.images['avatar']['format']
        variants = APP_CONFIG.images['avatar']['complect']
        hash = filename_hash(tmp_file)

        result={}

        variants.map { |key, variant|
          _tmp = image.clone

          if _tmp.columns>_tmp.rows
            k = (_tmp.rows.to_d / _tmp.columns.to_d)
            _tmp = _tmp.scale(variant['width'], (variant['width']*k).to_i)
          elsif _tmp.columns<_tmp.rows
            k = (_tmp.columns.to_d / _tmp.rows.to_d)
            _tmp = _tmp.scale((variant['height']*k).to_i, variant['height'])
          else
            _tmp = _tmp.scale(variant['width'], variant['height'])
          end

          _tmp.write('jpeg:'+APP_CONFIG.images['path'].to_s+hash.to_s+'-'+key+'.'+APP_CONFIG.images['avatar']['format'])
          result[key] = hash+'-'+key+'.' + APP_CONFIG.images['avatar']['format']
        }

        width = APP_CONFIG.images['avatar']['width']
        height = APP_CONFIG.images['avatar']['height']
        image = image.resize_to_fill(width, height)
        image.write('jpeg:'+APP_CONFIG.images['path'].to_s+hash.to_s+'-strict.'+APP_CONFIG.images['avatar']['format'])
        result['strict'] = hash+'-strict.' + APP_CONFIG.images['avatar']['format']

        result # return
      end

      def filename_hash(tmp_file)
        Digest::MD5.hexdigest(tmp_file.original_filename + Time.now().to_s)
      end
    end
  end
end

