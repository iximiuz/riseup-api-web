module Riseup
  module Storages
    module Exceptions

      class StorageException < Riseup::Exceptions::RiseupException

      end

    end
  end
end