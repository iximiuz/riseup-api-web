require Rails.root.join('lib', 'riseup', 'exceptions', 'missed_day_exception')
require Rails.root.join('lib', 'riseup', 'exceptions', 'missed_activities_in_db_exception')
require 'observer'

module Riseup
  module Storages

    class ActivitiesStorage < ResourcesStorage
      include Observable

      EV_PRIVACY_LEVEL_CHANGED = 'activity_privacy_level_changed'
      EV_ACTIVITY_COMPLETED = 'activity_completed'
      EV_ACTIVITY_CREATED = 'activity_created'
      EV_ACTIVITY_REMOVED = 'activity_removed'


      def initialize(days_storage = DaysStorage.new, activities_order = Riseup::Storages::Tools::ActivitiesOrder.new)
        @order = activities_order
        @days_storage = days_storage
      end

      def delete(activity)
        @order.remove(activity)
        Activity.delete(activity.id.item_id)

        changed
        notify_observers(EV_ACTIVITY_REMOVED, activity)
      end

      # @param [String|Riseup::Storages:Id:ActivityId[]] ids
      def find(ids, access_privileges = [])
        is_enumerable = ids.is_a?(Enumerable)
        unless is_enumerable
          ids = [ids]
        end

        ids = ids.map { |id| _check_id(id) }

        # @todo: !refactor db access code for sharding use and then remove assertion by user_id
        if access_privileges.empty?
          activity_records = ::Activity.where(id: ids.map { |id| id.item_id })
        else
          activity_records = ::Activity.where(id: ids.map { |id| id.item_id }, privacy_level: access_privileges)
        end

        return [] if activity_records.empty?

        ids = ids.index_by { |id| id.item_id }
        filtered_records = activity_records.select do |activity_record|
          activity_record.user_id == ids[activity_record.id].user_id
        end

        assert(
            activity_records.size == filtered_records.size,
            'Wrong activity id. User part not correspond activity part'
        )
        # end of todo

        result = filtered_records.map { |activity_record| Riseup::Rest::Activity.new(activity_record) }
        is_enumerable ? result : result.first
      end

      # todo: !refactor simplify me! =((
      # todo: !defect добавить коррекцию по user_time_zone, когда используется align_by_days
      #       (можно выбирать на 1 день больше, в зависимости от того, положительное или отрицательное смешение у
      #        user_time_zone и потом фильтровать при отдаче, отрезая часть данных с одной стороы и дополняя с другой).
      #
      # @param [Riseup::Rest::Id::ActivityId] activity_id
      # @param [bool] align_by_days дополнять ли результат до целых дней
      #
      # @return [Riseup::Rest::Activity[]]
      def find_by_activity(user_id, activity_id, a_count, b_count, align_by_days, goal_ids = [], access_privileges = [])
        activity_id = _check_id(activity_id)

        activity_record_id = activity_id.item_id

        activity_record = Activity.find_by_id(activity_record_id)
        return [] unless activity_record

        if activity_record.user_id != user_id
          raise Riseup::Storages::Exceptions::StorageException.new('Activity user_id mismatch requested user id.')
        end

        if activity_record.user_id != activity_id.user_id
          raise Riseup::Storages::Exceptions::StorageException.new('Activity user_id mismatch record user id.')
        end

        activity_ids = [activity_id]
        timestamp = Riseup::Utils::TimeUtils.date_to_timestamp(activity_record.start_dt)

        # Набираем активности из дня активности с id == параметру activity_id.
        day = @days_storage.find(user_id, timestamp)
        unless day
          raise Exceptions::ActivitiesStorage::MissedDayException.new(activity_record.user_id, timestamp)
        end

        unless day.has_activity(activity_id)
          assert false, "Missed activity [#{activity_id}] in day [#{day}]." # @todo: raise event
        end

        # Набираем активности из текущего дня, следующие за activity_id
        activity_ids += day.get_activities_after(activity_id, align_by_days ? nil : a_count)

        # Набираем активности из следущих за date дней, пока кол-во таких активностей не превысит a_count,
        # либо не закончатся дни
        next_day_timestamp = timestamp
        while a_count > activity_ids.count
          next_day = @days_storage.find_next(user_id, next_day_timestamp)
          unless next_day
            break
          end

          next_day_timestamp = next_day.get_date(true)

          first_activity = next_day.get_first_activity
          activity_ids << first_activity

          activity_ids += next_day.get_activities_after(
              first_activity,
              align_by_days ? nil : (a_count - activity_ids.count)
          )
        end

        # Набираем активности из текущего дня, предшествующие activity_id
        existing_a_count = activity_ids.count
        activity_ids = day.get_activities_before(activity_id, align_by_days ? nil : b_count) + activity_ids
        existing_b_count = activity_ids.count - existing_a_count

        # Набираем активности из дней, предшествующих date, пока кол-во таких активностей не превысит b_count
        # либо не закончатся дни
        prev_day_timestamp = timestamp
        while b_count > existing_b_count
          prev_day = @days_storage.find_prev(user_id, prev_day_timestamp)
          unless prev_day
            break
          end

          prev_day_timestamp = prev_day.get_date(true)

          last_activity = prev_day.get_last_activity
          activity_ids.unshift(last_activity)

          existing_b_count = activity_ids.count - existing_a_count
          activity_ids = prev_day.get_activities_before(
              last_activity,
              align_by_days ? nil : b_count - existing_b_count
          ) + activity_ids
        end

        # По сформированному массиву активностей, соотв. критериям поиска, достаем из базы сами активности и формируем
        # результат выполнения функции
        _find_multi(activity_ids, goal_ids, access_privileges)
      end

      def find_by_date(user_id, date, a_count, b_count, align_by_days, goal_ids = [], access_privileges = [])
        activity_ids, a_activity_ids, b_activity_ids = [], [], []

        date_timestamp = Riseup::Utils::TimeUtils.date_to_timestamp(date)
        day = @days_storage.find(user_id, date_timestamp)
        if day
          activity_ids += day.get_activities
        end


        # Набираем активности из следущих за date дней, пока кол-во таких активностей не превысит a_count,
        # либо не закончатся дни
        next_day_timestamp = date_timestamp
        while a_count > a_activity_ids.count
          next_day = @days_storage.find_next(user_id, next_day_timestamp)
          unless next_day
            break
          end

          next_day_timestamp = next_day.get_date(true)

          first_activity = next_day.get_first_activity
          a_activity_ids << first_activity

          a_activity_ids += next_day.get_activities_after(
              first_activity,
              align_by_days ? nil : (a_count - activity_ids.count)
          )
        end

        # Набираем активности из дней, предшествующих date, пока кол-во таких активностей не превысит b_count
        # либо не закончатся дни
        prev_day_timestamp = date_timestamp
        while b_count > b_activity_ids.count
          prev_day = @days_storage.find_prev(user_id, prev_day_timestamp)
          unless prev_day
            break
          end

          prev_day_timestamp = prev_day.get_date(true)

          last_activity = prev_day.get_last_activity
          b_activity_ids.unshift(last_activity)

          b_activity_ids = prev_day.get_activities_before(
              last_activity,
              align_by_days ? nil : b_count - b_activity_ids.count
          ) + b_activity_ids
        end

        _find_multi(b_activity_ids + activity_ids + a_activity_ids, goal_ids, access_privileges)
      end

      # @return [Rest::Activity[]]
      def find_by_date_range(user_id, date_from, date_to, goal_ids = [], access_privileges = [])
        days = @days_storage.find_range(
            user_id,
            Riseup::Utils::TimeUtils.date_to_timestamp(date_from),
            Riseup::Utils::TimeUtils.date_to_timestamp(date_to)
        )

        activity_ids = []
        days.map { |day| activity_ids += day.get_activities }

        _find_multi(activity_ids, goal_ids, access_privileges)
      end

      # @todo: add sharding by user_id support
      #
      # @param [int] goal_id
      # @param [String] user_utc_time YYYY-MM-DD HH:MM:SS в UTC
      #
      # @return [GoalActivitiesStat]
      def find_goal_stat(goal_id, user_utc_time)
        result = find_goals_stat([goal_id], user_utc_time)
        result[goal_id.item_id]
      end

      # @todo: add sharding by user_id support
      def find_goals_stat(goal_ids, user_utc_time)
        _check_datetime_string_format(user_utc_time)

        goals_records_ids = goal_ids.map { |el| el.item_id }

        #
        # SELECT goal_id, CASE WHEN (state = 1) THEN 1 WHEN start_dt > '2013-03-15' THEN 0 ELSE -1 END, COUNT(*) FROM activities
        # WHERE goal_id IN (0)
        # GROUP BY goal_id, CASE WHEN (state = 1) THEN 1 WHEN start_dt > '2013-03-15' THEN 0 ELSE -1 END;
        #
        stat_records = Activity.select(
            "goal_id, CASE WHEN (state = 1) THEN 'completed' WHEN start_dt > '#{user_utc_time}' THEN 'planned' ELSE 'overdued' END as status, COUNT(id) as cnt",
        ).where('goal_id IN(?)', goals_records_ids)
        .group("goal_id, CASE WHEN (state = 1) THEN 'completed' WHEN start_dt > '#{user_utc_time}' THEN 'planned' ELSE 'overdued' END")

        stat_data = {}
        goals_records_ids.each{ |id| stat_data[id] = {'completed' => 0, 'overdued' => 0, 'planned' => 0} }
        stat_records.each do |entry|
          stat_data[entry.goal_id][entry.status] = entry.cnt.to_i
        end

        result = {}
        stat_data.each do |goal_id, stat|
          result[goal_id] =
              Domain::Goals::GoalActivitiesStat.new(
                  goal_id,
                  stat['completed'],
                  stat['overdued'],
                  stat['completed'] + stat['overdued'] + stat['planned']
              )
        end

        result
      end

      def find_users_stat(ids, user_utc_time)
        stat_records = Activity.select(
            "user_id, CASE WHEN (state = 1) THEN 'completed' WHEN start_dt > '#{user_utc_time}' THEN 'planned' ELSE 'overdued' END as status, COUNT(id) as cnt",
        ).where('user_id IN(?)', ids)
        .group("user_id, CASE WHEN (state = 1) THEN 'completed' WHEN start_dt > '#{user_utc_time}' THEN 'planned' ELSE 'overdued' END")

        stat_data = {}
        ids.each{ |id| stat_data[id] = {'completed' => 0, 'overdued' => 0, 'planned' => 0} }
        stat_records.each do |entry|
          stat_data[entry.user_id][entry.status] = entry.cnt.to_i
        end
        result = {}
        stat_data.each do |id, stat|
          result[id] =
              Domain::Users::UserActivitiesStat.new(
                  id,
                  stat['completed'],
                  stat['overdued'],
                  stat['completed'] + stat['overdued'] + stat['planned']
              )
        end

        result
      end

      # Не все параметры ранее созданной активности можно изменить!
      # Если будут изменены параметры, запрещенные для изменения,
      # сохранения не произойдет и будет выброшено исключение.
      #
      # @param [Rest::Activity] activity
      def save(activity)
        # hack :(
        if activity.nextId && activity.prevId
          activity.nextId = nil
        end

        if activity.new?
          _create_activity(activity)

          changed
          notify_observers(EV_ACTIVITY_CREATED, activity)
        else
          _update_activity(activity)
        end

        activity
      end

      private ##########################################################################################################

      # TODO: !refactor! move it to TimeUtils
      def _check_datetime_string_format(dt)
        begin
          DateTime.strptime(dt, '%Y-%m-%d %H:%M:%S')
        rescue ArgumentError
          raise "Invalid DateTime formatted string #{dt}"
        end
      end

      def _check_fields_invariance(old_activity, new_activity)
        raise 'Invariant field(s) changed.' if old_activity.userId != new_activity.userId
        #TODO: !!! || old_activity.cdate.strftime('%Y-%m-%d %H:%M:%S') != new_activity.cdate fix here!
      end

      # create and save new activity
      def _create_activity(activity)
        id = _save_activity_data(activity)
        @order.insert(id, activity)

        activity.id = id
      end

      def _find_multi(ids, goal_ids = [], access_privileges)
        if ids.empty?
          return []
        end

        if 0 < goal_ids.count
          goal_ids = goal_ids.map { |goal_id| Riseup::Rest::Id::GoalId.new(goal_id).item_id }

          if access_privileges.empty?
            acty_records = Activity.where(
                id: ids.map { |el| el.item_id }, goal_id: goal_ids
            )
          else
            acty_records = Activity.where(
                id: ids.map { |el| el.item_id }, goal_id: goal_ids, privacy_level: access_privileges
            )
          end
        else
          if access_privileges.empty?
            acty_records = Activity.find(ids.map { |el| el.item_id })
          else
            acty_records = Activity.where(id: ids.map { |el| el.item_id }, privacy_level: access_privileges)
          end
        end

        return [] if acty_records.empty?

        acty_records = acty_records.index_by { |record| record.id }

        result = []
        ids.each do |id|
          if acty_records.has_key?(id.item_id)
            result << Riseup::Rest::Activity.new(acty_records[id.item_id])
          end
        end

        result
      end

      # @return [Riseup::Rest::Id::ActivityId] id новой активности (не записи в БД, а REST-объекта)
      def _save_activity_data(activity)
        # Добавляем в базу новую запись
        activity_record = Activity.new
        activity_record.user_id = activity.userId
        activity_record.goal_id = (activity.goalId) ? activity.goalId.item_id : nil
        activity_record.description = activity.description
        activity_record.privacy_level = activity.privacyLevel
        activity_record.start_dt = activity.startDt
        activity_record.state = activity.state
        activity_record.cdate = activity.cdate
        activity_record.save

        _check_id(activity_record.user_id.to_s + '_' + activity_record.id.to_s)
      end

      def _need_update_data(old_activity, new_activity)
        old_activity.goalId != new_activity.goalId ||  old_activity.description != new_activity.description ||
            old_activity.startDt != new_activity.startDt || old_activity.state != new_activity.state ||
            old_activity.privacyLevel != new_activity.privacyLevel
      end

      # update existing activity
      def _update_activity(activity)
        old_activity = find(activity.id)
        assert old_activity, 'Integrity constraint failed: missed activity ' + old_activity.inspect

        _check_fields_invariance(old_activity, activity)

        @order.update(activity, old_activity)
        if _need_update_data(old_activity, activity)
          _update_activity_data(activity)

          if !old_activity.completed? && activity.completed?
            changed
            notify_observers(EV_ACTIVITY_COMPLETED, activity)
          end

          if old_activity.privacyLevel != activity.privacyLevel
            changed
            notify_observers(EV_PRIVACY_LEVEL_CHANGED, activity)
          end
        end
      end

      def _update_activity_data(activity)
        # Просто обновляем данные в базе
        activity_record = Activity.allocate
        activity_record.init_with('attributes' => {})
        activity_record.id = activity.id.item_id
        activity_record.goal_id = (activity.goalId) ? activity.goalId.item_id : nil
        activity_record.description = activity.description
        activity_record.privacy_level = activity.privacyLevel
        activity_record.start_dt = activity.startDt
        activity_record.state = activity.state
        activity_record.save
      end

      # Проверяем id, и, если он строковый, создает объект Id.
      #
      # @param [String|Riseup::Rest::Id::ActivityId] id
      def _check_id(id)
        assert id.is_a?(String) || id.is_a?(Riseup::Rest::Id::ActivityId)

        if id.is_a?(String)
          id = Riseup::Rest::Id::ActivityId.new(id)
        end

        id
      end
    end

  end
end