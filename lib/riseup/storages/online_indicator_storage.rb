module Riseup
  module Storages

    class OnlineIndicatorStorage
      def set_online(user_id)
        Riseup::Services::Nest.new(_make_redis_key(user_id)).set(Time.now.utc.to_i)
      end

      def get_stats(user_id)
        Riseup::Services::Nest.new(_make_redis_key(user_id)).get()
      end

      def load_by_ids(ids)
        result = {}
        timestamp_list = Services::Providers::RedisProvider.get_service.mget(
            ids.map { |user_id| Riseup::Services::Nest.new(_make_redis_key(user_id)) }
        )

        for i in 0..timestamp_list.count
          unless timestamp_list[i].nil?
            result[ids[i]] = timestamp_list[i]
          end
        end

        result
      end

      private ##########################################################################################################

      def _make_redis_key(user_id)
        "user:#{user_id}:last_action"
      end
    end

  end
end

