module Riseup
  module Storages

    class ResourcesStorage
      # @param [String|Symbol] type
      # @param [Riseup::Rest:Id:Id] id
      def self.find(type, id)
        # @todo: !refactor to DI
        storage = _get_storage_by_type(type)
        storage.find(id)
      end

      private ##########################################################################################################

      def self._get_storage_by_type(type)
        if type.is_a?(String)
          type = type.to_sym
        end

        klass = Riseup::Rest::BaseResource.get_resource_class_by_type(type).to_s
        storages_map = _get_storages_map()
        unless storages_map.has_key?(klass)
          raise Riseup::Storages::Exceptions::StorageException.new(
                    'Unknown resource type [' + type.inspect + '] (class [' + klass.to_s + ']'
                )
        end

        Riseup::Services::ServiceProvider.instance.get_service(storages_map[klass])
      end

      # @todo: maybe useful?
      #def _get_storage_by_class(klass)
      #
      #end

      def self._get_storages_map()
        @resources_map ||= {Riseup::Rest::Activity.to_s => :activities, Riseup::Rest::Goal.to_s => :goals}
      end
    end

  end
end