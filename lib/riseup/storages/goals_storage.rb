module Riseup
  module Storages

    class GoalsStorage < ResourcesStorage

      # @param [ActivitiesStorage] activities_storage:
      def initialize(activities_storage)
        @activities_storage = activities_storage
      end

      def delete(goal)
        return if nil == goal.id

        Goal.delete(goal.id.item_id)
      end

      # @param [Riseup::Rest:Id:GoalId] id
      def find(id)
        goal_record = Goal.find_by_id(id.item_id)

        return nil until goal_record

        Rest::Goal.new(:id => id, :data => goal_record)
      end

      #
      # @todo: find_all
      #

      def find_all_with_stat(user, limit, offset, sort, filter)
        now = Time.now.utc.to_i
        local_now = Time.at(now - user.timezone * 60 * 60)

        if 'completed' == filter
          goal_records = Goal.where("user_id = #{user.id} and state = 1").order("id #{sort}").limit(limit).offset(offset)
        elsif 'active' == filter
          goal_records = Goal.where("user_id = #{user.id} and state = 0 and end_dt > '#{local_now}'").order("id #{sort}").limit(limit).offset(offset)
        elsif 'failed' == filter
          goal_records = Goal.where("user_id = #{user.id} and state = 0 and end_dt <= '#{local_now}'").order("id #{sort}").limit(limit).offset(offset)
        else
          goal_records = Goal.where("user_id = #{user.id}").order("id #{sort}").limit(limit).offset(offset)
        end

        return [] if goal_records.empty?

        stats = find_activities_stat_multi(goal_records.map { |record| Rest::Id::GoalId.new("#{user.id}_#{record.id}") }, user)

        result = []
        goal_records.each do |goal_record|
          result.push(
              Rest::Goal.new(
                  :id => "#{goal_record.user_id}_#{goal_record.id}",
                  :data => goal_record,
                  :stat => stats[goal_record.id]
              )
          )
        end

        result
      end

      # @param [Riseup::Rest:Id:GoalId] id
      # @param [User] user
      def find_with_stat(id, user)
        goal = find(id)

        return nil until goal

        goal.stat = find_activities_stat(id, user)
        goal
      end

      def save(goal)
        if nil != goal.id
          # update existing goal
          goal_record = Goal.allocate
          goal_record.init_with('attributes' => {})
          goal_record.id = goal.id.item_id
          goal_record.description = goal.description
          goal_record.state = goal.state
          goal_record.end_dt = goal.endDate
          goal_record.images = goal.img
          goal_record.save
        else
          # insert new activity
          goal_record = Goal.new
          goal_record.user_id = goal.userId
          goal_record.description = goal.description
          goal_record.state = goal.state
          goal_record.end_dt = goal.endDate
          goal_record.created_at = goal.createdAt
          goal_record.save

          goal.id = "#{goal_record.user_id}_#{goal_record.id}"
        end
      end

      private

      # @param [int] id
      # @param [User] user
      # @return [GoalActivitiesStat]
      def find_activities_stat(id, user)
        utc_now = Time.now.gmtime.to_i
        utc_seconds_offset_on_user = 60 * user.timezone
        user_local = utc_now - utc_seconds_offset_on_user

        @activities_storage.find_goal_stat(id, DateTime.strptime("#{user_local}", '%s').to_s(:db))
      end

      # @param [User] user
      # @return [GoalActivitiesStat]
      def find_activities_stat_multi(ids, user)
        utc_now = Time.now.gmtime.to_i
        utc_seconds_offset_on_user = 60 * user.timezone
        user_local = utc_now - utc_seconds_offset_on_user

        @activities_storage.find_goals_stat(ids, DateTime.strptime("#{user_local}", '%s').to_s(:db))
      end
    end

  end
end