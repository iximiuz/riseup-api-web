module Riseup
  module Storages
    class StatisticStorage < ResourcesStorage
      def self.save(key, json)
        p json
        Riseup::Services::Nest.new('statistic:'+key).lpush(json)
      end
    end
  end
end