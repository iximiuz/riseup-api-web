module Riseup
  module Services

    class BusinessObserver

      # @param [Riseup::Services::LiveHub::LiveHubService] live_hub_service
      def initialize(live_hub_service)
        @live_hub_service = live_hub_service
      end

      def update(event, *args)
        if Riseup::Storages::ActivitiesStorage::EV_ACTIVITY_CREATED == event
          activity = args[0]
          @live_hub_service.pub_event(
              Riseup::Services::LiveHub::Events::ActivityCreatedEvent.new(activity)
          )
          return
        end

        if Riseup::Storages::ActivitiesStorage::EV_ACTIVITY_COMPLETED == event
          activity = args[0]
          @live_hub_service.pub_event(
              Riseup::Services::LiveHub::Events::ActivityCompletedEvent.new(activity)
          )
          return
        end

        if Riseup::Storages::ActivitiesStorage::EV_ACTIVITY_REMOVED == event
          activity = args[0]
          @live_hub_service.remove_events(
              Riseup::Services::LiveHub::Generators::GeneratorUser.new(activity.user),
              activity.id,
              Riseup::Services::LiveHub::Subjects::TYPE_ACTIVITY
          )
          return
        end

        if Riseup::Storages::ActivitiesStorage::EV_PRIVACY_LEVEL_CHANGED == event
          activity = args[0]
          @live_hub_service.change_events_privacy_level(
              Riseup::Services::LiveHub::Generators::GeneratorUser.new(activity.user),
              activity.id,
              Riseup::Services::LiveHub::Subjects::TYPE_ACTIVITY,
              activity.privacyLevel
          )
          return
        end

        if Riseup::Services::Friendship::FriendshipService::EV_NEW_INVOCATION == event ||
           Riseup::Services::Friendship::FriendshipService::EV_INVOCATION_ACCEPTED == event
          @live_hub_service.subscribe_user_to_generator(
              args[0],
              Riseup::Services::LiveHub::Generators::GeneratorUser.new(User.find(args[1]))
          )
        end

        if Riseup::Services::Friendship::FriendshipService::EV_CONNECTION_DESTROYED == event
          @live_hub_service.unsubscribe_user_from_generator(
              args[0],
              Riseup::Services::LiveHub::Generators::GeneratorUser.new(User.find(args[1]))
          )
        end
      end
    end

  end
end