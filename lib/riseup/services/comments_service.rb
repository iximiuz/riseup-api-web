class Riseup::Services::CommentsService
  ACTIVITIES_ENTITY=1
  GOALS_ENTITY=2

  # @param entity_type
  # @param entity_id
  # @return true
  def counter_change(entity_type, entity_id, action='up')
    _id = (entity_id.to_i != entity_id) ? get_entity_id(entity_id, entity_type) : entity_id
    if entity_type.to_i == ACTIVITIES_ENTITY
      activity = Activity.where({:id => _id}).first
      unless activity
        return false
      end
      activity.comments_count = ((action == 'up') ? (activity.comments_count.to_i + 1) : (activity.comments_count.to_i - 1))
      activity.save()

      return true
    end
    raise Exception.new('Not supported type: '+type.to_s)
  end

  # @param id String
  # @param type String
  def get_entity_id(id, type)
    if type.to_i == ACTIVITIES_ENTITY
      # @user: dlarchikov
      # TODO: спросить у Ивана о сервисе.
      return id.split('_')[1].to_i
    end

    ## Расширить в случае необходимости
    raise Exception.new('Not supported type: '+type.to_s)
  end

  # @param entity_type
  # @param entity_id - clear id (YES: 123 or 124, NO: 123_124)
  def get_entity(entity_type, entity_id)

    if entity_type.to_i == ACTIVITIES_ENTITY
       return Activity.find(entity_id)
    end

    raise Exception.new('Not supported entity type ' + entity_type)
  end

end
