module Riseup
  module Services
    module Providers

      class VotingServiceProvider
        @vs = {}

        # @return [Riseup::Services::Voting::VotingService]
        def self.get_service(version)
          unless @vs[version]
            config = YAML.load_file("#{Rails.root}/config/voting_system.yml") || {}
            config = OpenStruct.new(config[Rails.env] || {})

            core = Riseup::Services::Voting::Core::VotingCore.new(
                version,
                Riseup::Services::Voting::Storages::RemoteStorage.new(
                    config.host,
                    config.port
                )
            )
            transport = Riseup::Services::Voting::Transports::LocalTransport.new(core)
            @vs[version] = Riseup::Services::Voting::VotingService.new(version, transport)
          end

          @vs[version]
        end
      end

    end
  end
end
