module Riseup
  module Services
    module Providers

      class RedisProvider
        extend Riseup::Services::Utils::RedisConf

        class << self
          attr_reader :redis
        end

        def self.get_service
          unless @redis
            @redis = Redis.new(self.read_conf)
          end

          @redis
        end
      end

    end
  end
end
