module Riseup
  module Services
    class StatisticService
      def self.save(key, json)
        Riseup::Storages::StatisticStorage.save(key, json)
      end
    end
  end
end