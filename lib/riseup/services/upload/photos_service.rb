require 'RMagick'

class Riseup::Services::Upload::PhotosService
  attr_accessor :tmp_file

  def initialize(tempfile)
    @tmp_file = tempfile
  end

  def save(cur_user)
    file_name = generate_filename(cur_user).to_s+'.'+APP_CONFIG.images['photos']['format']
    image = Magick::Image.read(@tmp_file.path).first

    width = image.columns
    height = image.rows

    if width > APP_CONFIG.images['photos']['max_width'] || height > APP_CONFIG.images['photos']['max_height']
      if image.columns>image.rows
        k = (image.rows.to_d / image.columns.to_d)
        image = image.scale(APP_CONFIG.images['photos']['max_width'], (APP_CONFIG.images['photos']['max_width']*k).to_i)
      elsif image.columns<image.rows
        k = (image.columns.to_d / image.rows.to_d)
        image = image.scale((APP_CONFIG.images['photos']['max_height']*k).to_i, APP_CONFIG.images['photos']['max_height'])
      else
        image = image.scale(APP_CONFIG.images['photos']['max_width'], APP_CONFIG.images['photos']['max_height'])
      end
    end
    image.write('jpeg:'+APP_CONFIG.images['photos']['path'].to_s+file_name)

    result = {:img => 'photos/'+file_name}
    # Генерируем превью
    if APP_CONFIG.images['photos'].include?('preview')
      preview = Magick::Image.read(@tmp_file.path).first
      preview = preview.resize_to_fit(APP_CONFIG.images['photos']['preview']['width'].to_i, APP_CONFIG.images['photos']['preview']['height'].to_i)
      preview.write('jpeg:'+APP_CONFIG.images['photos']['preview']['path'].to_s+file_name)
      result[:preview] = 'photos/preview/'+file_name
    end
    result[:pic_domain] = APP_CONFIG.images['']
    result # return
  end

  # Генерируем имя файла на основе имени пользователя
  def generate_filename(user_name='')
    result = ''
    username_hash = Digest::SHA1.hexdigest(user_name)
    username_hash_length = username_hash.length
    first_hash_length = rand(username_hash_length)
    first_hash = username_hash[(username_hash_length-first_hash_length), 6]
    p username_hash_length
    result << first_hash << '_' << Digest::MD5.hexdigest(user_name + Time.now.to_s)
    result
  end
end