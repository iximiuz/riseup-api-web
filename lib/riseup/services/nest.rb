module Riseup
  module Services
    class Nest < ::Nest
      extend Riseup::Services::Utils::RedisConf

      def initialize(key, redis = Services::Providers::RedisProvider.get_service)
        unless key.start_with?(_retrieve_namespace)
          key = _retrieve_namespace + ':' + key
        end

        super(key, redis)
      end

      private ##########################################################################################################

      def _retrieve_namespace
        until @namespace
          @namespace = self.class.read_conf[:namespace]
        end

        @namespace
      end
    end
  end
end