module Riseup
  module Services
    class ImgEditService
      def initialize(path_to_img)
        @image = Magick::Image.read(path_to_img).first
      end

      def rotate(direction)
        if direction == 'right'
          img = @image.rotate(90)
        else
          img = @image.rotate(270)
        end
        @image = img # return
      end
    end
  end
end