module Riseup
  module Services
    module Voting
      module Votes

        class LikeDislike < AbstractVote
          def dislike?
            assert(@rest_vote.instance_of?(Riseup::Rest::Votes::LikeDislike))

            @rest_vote.dislike?
          end

          def like?
            assert(@rest_vote.instance_of?(Riseup::Rest::Votes::LikeDislike))

            @rest_vote.like?
          end
        end

      end
    end
  end
end