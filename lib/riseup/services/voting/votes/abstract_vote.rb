module Riseup
  module Services
    module Voting
      module Votes

        class AbstractVote
          @votes_map = {:likedislike => LikeDislike}

          attr_reader :tag

          # @param [Riseup::Rest::Votes::AbstractVote] rest_vote
          def initialize(rest_vote, tag)
            @rest_vote = rest_vote

            @voter_subject_id = rest_vote.subjectId
            @vote_object_id = rest_vote.objectId
            @tag = tag
          end

          def self.create(vote, tag)
            assert nil != vote.type, 'Rest::Vote without type.'

            type = vote.type
            if type.is_a?(String)
              type = type.to_sym
            end

            @votes_map[type].new(vote, tag)
          end

          def created_at(timestamp = false)
            timestamp ? Riseup::Utils::TimeUtils.to_timestamp(@rest_vote.createdAt) : @rest_vote.createdAt
          end

          def value
            @rest_vote.get_value
          end

          def vote_object_id
            @rest_vote.objectId
          end

          def vote_object_type
            @rest_vote.objectType
          end

          def voter_subject_id
            @rest_vote.subjectId
          end
        end

      end
    end
  end
end