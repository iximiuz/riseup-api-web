module Riseup
  module Services
    module Voting
      module Transports

        class LocalTransport < AbstractTransport
          # @param [Riseup::Services::Voting::Core::VotingCore] core
          def initialize(core)
            @core = core
          end

          def vote(vote)
            @core.vote(vote)
          end

          def get_votes_count_multi(object_type, object_ids, tag)
            @core.get_votes_count_multi(object_type, object_ids, tag)
          end

          def votize(objects, obj_type, tags, user_id)
            @core.votize(objects, obj_type, tags, user_id)
          end
        end

      end
    end
  end
end