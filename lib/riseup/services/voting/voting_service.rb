require 'ostruct'
require 'yaml'

module Riseup
  module Services
    module Voting

      class VotingService
        def initialize(version, transport)
          @transport = transport
          @version = version
        end

        # @param [Riseup::Rest::Votes::AbstractVote] vote
        # @param [String|nil] tag
        def vote(vote, tag = nil)
          vote = Votes::AbstractVote.create(vote, tag)
          @transport.vote(vote)
        end

        # Проставляет объекту или коллекции объектов информацию о голосах за них (пока только кол-во).
        # Какой тип голосов соответствует объекту определяется по текущим настройкам (см. реализацию Core).
        #
        # @param [Riseup::Rest::BaseResource|Riseup::Rest::BaseResource[]] один ресурс или коллекция однотипных ресурсов
        def votize(obj, obj_type, tags = [], user_id = nil)
          assert obj.is_a?(Enumerable) || obj.is_a?(Riseup::Rest::BaseResource)

          objects_collection = obj.is_a?(Enumerable) ? obj : [obj]
          return if objects_collection.empty?

          @transport.votize(objects_collection, obj_type, tags, user_id)
        end
      end

    end
  end
end