module Riseup
  module Services
    module Voting
      module Exceptions

        class NotImplException < VotingException
          def initialize(method_name)
            super(method_name + 'is not implemented.')
          end
        end

      end
    end
  end
end