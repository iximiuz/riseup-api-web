module Riseup
  module Services
    module Voting
      module Exceptions

        class VotingException < Riseup::Exceptions::RiseupException

        end

      end
    end
  end
end