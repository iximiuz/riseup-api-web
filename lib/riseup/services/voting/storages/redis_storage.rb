module Riseup
  module Services
    module Voting
      module Storages

        class RedisStorage < Storage
          def initialize(nest = Riseup::Services::Nest.new('voting'))
            @base_nest = nest
          end

          def get_votes_count_multi(object_type, object_ids, tag, version)
            raise Voting::Exceptions::NotImplException.new(__method__)
          end

          # @param [Riseup::Services::Voting::Votes::AbstractVote] vote
          def save(vote, version)
            # raise Riseup::Services::Voting::Exceptions::ReVoteBanException.new()

            @base_nest.redis.eval(save_script, []) do
              _get_registry_nest(vote, version).hsetnx(vote.voter_subject_id, vote.to_storage)
              _get_order_nest(vote, version).zadd(vote.created_at(true), vote.voter_subject_id)
              _get_count_nest(vote, version).incr()

              # @todo: maybe add particular saving with call _do_save from subclasses?
            end
          end

          protected

          # @param [Riseup::Services::Voting::Votes::AbstractVote] vote
          def _get_count_nest(vote, version)
            raise Voting::Exceptions::NotImplException.new(__method__)
          end

          # @param [Riseup::Services::Voting::Votes::AbstractVote] vote
          def _get_iterable_list_nest(vote, version)
            raise Voting::Exceptions::NotImplException.new(__method__)
          end

          # @param [Riseup::Services::Voting::Votes::AbstractVote] vote
          def _get_registry_nest(vote, version)
            raise Voting::Exceptions::NotImplException.new(__method__)
          end
        end

      end
    end
  end
end