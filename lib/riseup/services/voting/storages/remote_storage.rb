require 'socket'

module Riseup
  module Services
    module Voting
      module Storages

        class RemoteStorage < Storage
          def initialize(host, port)
            @host = host
            @port = port
            @client = 42
          end

          def save(vt, version)
            response = _write(_make_vote_command(vt, version))
            json_response = JSON.parse(response)

            if !json_response || !json_response['result']
              if json_response && 10001 == json_response['code']
                raise Riseup::Services::Voting::Exceptions::ReVoteBanException.new(vt.inspect)
              else
                raise Exceptions::StorageException.new(
                          'Can\'t save vote [' + (response ? response.inspect : 'empty response') + ']'
                      )
              end
            end
          end

          # @return [Array]
          def get_homogen_votes_count(object_type, object_ids, tags, user_id, version)
            response = _write(_make_homogen_votes_count_command(object_type, object_ids, tags, user_id, version))
            response = JSON.parse(response)

            if !response || response['error']
              raise Exceptions::StorageException.new(
                        'Votes count request error [' + (response ? response.inspect : 'empty response') + ']'
                    )
            end

            response
          end

          private ######################################################################################################

          def _create_socket
            TCPSocket.new(@host, @port)
          end

          def _make_homogen_votes_count_command(object_type, object_ids, tags, user_id, version)
            body = {
                :voteeType => object_type,
                :voteeIds => object_ids.map { |id| id.to_s },
                :v => version,
                :client => @client
            }

            if tags && 0 < tags.length
              body[:tags] = tags
            end

            if user_id
              body[:voterId] = user_id
            end

            body = body.to_json

            header = {:c => 'get_votes_count', :bl => body.length}

            header.to_json + body
          end

          # @param [Riseup::Services::Voting::Votes::AbstractVote] vote
          def _make_vote_command(vote, version)
            body = {
                :voteeType => vote.vote_object_type,
                :voteeId => vote.vote_object_id,
                :voterId => vote.voter_subject_id,
                :value => vote.value,
                :createdAt => vote.created_at,
                :v => version,
                :client => @client
            }

            if vote.tag
              body[:tag] = vote.tag
            end

            body = body.to_json

            header = {:c => 'vote', :bl => body.length}

            header.to_json + body
          end

          def _write(data)
            socket = _create_socket
            socket.write(data)
            response = socket.read
            socket.close

            response
          end
        end

      end
    end
  end
end