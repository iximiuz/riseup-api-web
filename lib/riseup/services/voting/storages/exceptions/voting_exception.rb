module Riseup
  module Services
    module Voting
      module Storages
        module Exceptions

          class StorageException < Riseup::Exceptions::RiseupException

          end

        end
      end
    end
  end
end