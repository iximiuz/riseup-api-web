module Riseup
  module Services
    module Voting
      module Storages

        class LikeDislikeRedisStorage < RedisStorage
          # @return [Array]
          def get_votes_count_multi(object_type, object_ids, tag, version)
            like_keys = []
            dislike_keys = []
            object_ids.each do |id|
              like_keys << _get_count_nest_by_params('lk', object_type, id.to_s, tag, version).to_s
              dislike_keys << _get_count_nest_by_params('dslk', object_type, id.to_s, tag, version).to_s
            end


            likes = @base_nest.redis.mget(like_keys).map { |el| nil == el ? 0 : el }
            dislikes = @base_nest.redis.mget(dislike_keys).map { |el| nil == el ? 0 : el }

            result = []
            likes.each_with_index do |like_vote, idx|
              result << {:like => like_vote, :dislike => dislikes[idx]}
            end

            result
          end

          protected

          # @param [Riseup::Services::Voting::Votes::LikeDislike] vote
          def _get_count_nest(vote, version)
            _get_base_nest(vote, version)['a'][vote.like? ? 'lk' : 'dslk']
          end

          # @param [Riseup::Services::Voting::Vote] vote
          def _get_order_nest(vote, version)
            _get_base_nest(vote, version)['o'][vote.like? ? 'lk' : 'dslk']
          end

          # @param [Riseup::Services::Voting::Vote] vote
          def _get_registry_nest(vote, version)
            _get_base_nest(vote, version)['r']
          end

          private

          # @param [Riseup::Services::Voting::Vote] vote
          def _get_base_nest(vote, version)
            _get_base_nest_by_params(vote.vote_object_type, vote.vote_object_id, vote.tag, version)
          end

          def _get_base_nest_by_params(object_type, object_id, tag, version)
            result = @base_nest[version][object_type][object_id]
            tag ? result[tag] : result
          end

          def _get_count_nest_by_params(vote_type, object_type, object_id, tag, version)
            assert 'lk' == vote_type || 'dslk' == vote_type, 'Unexpected vote type: ' + vote_type.inspect

            _get_base_nest_by_params(object_type, object_id, tag, version)['a'][vote_type]
          end
        end

      end
    end
  end
end