module Riseup
  module Services
    module Voting
      module Core

        class VotingCore
          def initialize(version, storage)
            @version = version
            @storage = storage
          end

          def vote(vote)
            # switch by object_type, check rules and consistency and if ok pass to storage.
            # May be some consistency checking strategies should be passed directly to storage.
            # ...
            _get_storage.save(vote, @version)
          end

          def votize(objects, object_type, tags, user_id)
            # @todo: По произвольному объекту из коллекции и конфигу для соотв. версии выбираем текущий тип голосов,
            # по которому уже получаем storage.

            # формируем массив id объектов, так как единственный способ сопоставить объект и кол-во голосов за него -
            # это по порядковому номеру в возвращаемом массиве голосов.
            objects_by_ids = {}
            object_ids = []

            objects.map do |el|
              object_ids << el.id
              objects_by_ids[el.id.to_s] = el
            end

            votes = _get_storage.get_homogen_votes_count(
                object_type,
                object_ids,
                tags,
                user_id,
                @version
            )

            if tags && 0 < tags.length
              #tags.each
            else
              if votes['counters']
                votes['counters'].each do |object_id, counts|
                  assert(objects_by_ids[object_id])

                  if objects_by_ids[object_id]
                    objects_by_ids[object_id].votes = counts
                  end
                end
              end

              if votes['voted']
                votes['voted'].each do |object_id, value|
                  assert(objects_by_ids[object_id])
                  assert(objects_by_ids[object_id].votes)

                  objects_by_ids[object_id].votes ||= {}
                  objects_by_ids[object_id].votes['voted'] = value
                end
              end
            end
          end

          private

          def _get_storage
            @storage
          end
        end

      end
    end
  end
end