module Riseup
  module Services

    class OnlineIndicatorService

      def initialize
        @online_indicator_storage = Riseup::Storages::OnlineIndicatorStorage.new
      end

      def up_online(user_id)
        @online_indicator_storage.set_online(user_id)
      end

      def online?(last_timestamp)
        difference = APP_CONFIG.no_online_time_difference.to_i

        if last_timestamp.nil? || last_timestamp.to_s == ''
          return false
        end

        (last_timestamp.to_i > Time.now.to_i - difference)
      end

      def load_by_ids(user_ids)
        @online_indicator_storage.load_by_ids(user_ids)
      end
    end

  end
end