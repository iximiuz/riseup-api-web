require 'singleton'

module Riseup
  module Services

    class ServiceProvider
      include Singleton

      def self.i
        self.instance
      end

      def get_service(name)
        if @services.has_key?(name)
          unless @services[name][:service]
            _initialize_service(name)
          end

          return @services[name][:service]
        end

        raise Riseup::Services::Exceptions::UnregisteredServiceException.new(name)
      end

      def register_service(name, initializer)
        @services[name] = { :initializer => initializer, :service => nil }
      end

      private ##########################################################################################################

      def initialize
        @services = {}
      end

      def _initialize_service(name)
        @services[name][:service] = @services[name][:initializer].call(self)
      end
    end

    SP = ServiceProvider
  end
end