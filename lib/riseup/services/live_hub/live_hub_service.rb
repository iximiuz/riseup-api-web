module Riseup
  module Services
    module LiveHub

      module Events
        TYPE_NEW_ACTIVITY = 1
        TYPE_ACTIVITY_COMPLETED = 2
      end

      module Subjects
        TYPE_ACTIVITY = 1
      end

      class LiveHubService
        def initialize(
            events_storage = LiveHub::Storages::LiveHubEventsStorage.new,
            subscriptions_storage = LiveHub::Storages::LiveHubSubscriptionsStorage.new
        )
          @events_storage = events_storage
          @subscriptions_storage = subscriptions_storage
        end

        def change_events_privacy_level(generator, subject_id, subject_type, privacy_level)
          @events_storage.change_events_privacy_level(
              generator, subject_id, subject_type, privacy_level
          )
        end

        def remove_events(generator, subject_id, subject_type)
          @events_storage.remove_events(generator, subject_id, subject_type)
        end

        def retrieve_generators_for_user(user_id)
          @subscriptions_storage.retrieve_generators_for_user(user_id)
        end

        # @param [Privacy::User2GeneratorPrivilege[]] generators_criteria
        # @return [Riseup::Rest::RestLiveHubEvent[]]
        def retrieve_events_selection(
            generators_criteria,
            limit,
            from_timestamp = nil,
            generators_loader = LiveHub::Loaders::GeneratorsLoader.new,
            subjects_loader = LiveHub::Loaders::SubjectsLoader.new
        )
          return [] if generators_criteria.empty?

          event_rows = @events_storage.retrieve_events_selection(generators_criteria, limit, from_timestamp)

          generators = generators_loader.load(event_rows)
          subjects = subjects_loader.load(generators_criteria.first.user_id, event_rows)

          event_rows.map do |event|
            id = event.id
            Riseup::Rest::RestLiveHubEvent.new(id, event.event_type, generators[id], subjects[id], event.created_at)
          end
        end

        def subscribe_user_to_generator(user_id, generator)
          @subscriptions_storage.create_subscription(user_id, generator)
        end

        def pub_event(event)
          @events_storage.record_event(event)
        end

        def unsubscribe_user_from_generator(user_id, generator)
          @subscriptions_storage.remove_subscription(user_id, generator)
        end
      end
    end

  end
end