module Riseup
  module Services
    module LiveHub
      module Loaders

        class SubjectsLoader

          def initialize(
              activities_service = Services::SP.i.get_service(:activities),
              voting_service = Riseup::Services::SP.i.get_service(:voting),
              logger = Rails.logger
          )
            @activities_service = activities_service
            @voting_service = voting_service
            @logger = logger
          end

          def load(user_id, event_rows)
            ids = []

            event_rows.each do |row|
              if LiveHub::Subjects::TYPE_ACTIVITY != row.subject_type
                raise LiveHub::Exceptions::UnknownSubjectException.new
              end

              ids << row.subject_id
            end

            activities = @activities_service.find(ids)

            begin
              @voting_service.votize(
                  activities,
                  Riseup::Rest::BaseResource.get_type_by_resource_class(Riseup::Rest::Activity),
                  [],
                  user_id
              )
            rescue => e
              @logger.error("Exception: #{e.message}. Trace: \n" + e.backtrace.join("\n"))
            end

            activities = activities.index_by { |activity| activity.id.to_s }
            event_rows = event_rows.index_by { |event| event.id }

            result = {}
            event_rows.each do |k, v|
              result[k] = activities[v.subject_id]
            end

            result
          end

        end

      end
    end
  end
end