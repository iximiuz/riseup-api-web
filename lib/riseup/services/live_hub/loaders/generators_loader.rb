module Riseup
  module Services
    module LiveHub
      module Loaders

        class GeneratorsLoader

          def load(event_rows)
            ids = []

            event_rows.each do |row|
              if LiveHub::Generators::TYPE_USER != row.generator_type
                raise LiveHub::Exceptions::UnknownGeneratorException.new
              end

              ids << row.generator_id
            end

            users = User.find(ids).index_by { |user| user.id }
            event_rows = event_rows.index_by { |event| event.id }

            result = {}
            event_rows.each do |k, v|
              result[k] = Riseup::Rest::User.new(users[v.generator_id])
            end

            result
          end

        end

      end
    end
  end
end