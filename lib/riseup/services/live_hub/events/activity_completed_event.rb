module Riseup
  module Services
    module LiveHub
      module Events

        class ActivityCompletedEvent
          attr_reader :generator, :subject, :subject_id, :subject_type, :privacy_level

          def initialize(activity)
            @generator = LiveHub::Generators::GeneratorUser.new(activity.user)
            @subject = activity
            @subject_id = activity.id
            @subject_type = LiveHub::Subjects::TYPE_ACTIVITY
            @privacy_level = activity.privacyLevel
          end

          def type
            TYPE_ACTIVITY_COMPLETED
          end
        end

      end
    end
  end
end