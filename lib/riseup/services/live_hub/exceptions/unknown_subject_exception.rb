module Riseup
  module Services
    module LiveHub
      module Exceptions

        class UnknownSubjectException < LiveHubException

        end

      end
    end
  end
end
