module Riseup
  module Services
    module LiveHub
      module Exceptions

        class UnknownGeneratorException < LiveHubException

        end

      end
    end
  end
end
