module Riseup
  module Services
    module LiveHub
      module Exceptions

        class LiveHubException < Services::Exceptions::ServicesException

        end

      end
    end
  end
end
