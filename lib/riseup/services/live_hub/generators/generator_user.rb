module Riseup
  module Services
    module LiveHub
      module Generators

        class GeneratorUser < Generator

          def initialize(user)
            @user = user
          end

          def credentials
            @user.credentials
          end

          def id
            @user.id
          end

          def type
            TYPE_USER
          end
        end

      end
    end
  end
end