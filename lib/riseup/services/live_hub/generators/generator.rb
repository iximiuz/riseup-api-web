module Riseup
  module Services
    module LiveHub
      module Generators

        TYPE_USER = 1

        class Generator

          def self.create(generator_id, generator_type)
            self._check_allowed_types(generator_type)

            if TYPE_USER == generator_type
              return GeneratorUser.new(User.find(generator_id))
            end
          end

          def self.create_batch(generators_data)
            generators_data_by_type = {}

            generators_data.each do |datum|
              self._check_allowed_types(datum.generator_type)

              (generators_data_by_type[datum.generator_type] ||= []) << datum.generator_id
            end

            result = []
            generators_data_by_type.each do |type, ids|
              if TYPE_USER == type
                User.find(ids).each { |user| result << GeneratorUser.new(user) }
              end
            end

            result
          end

          private ######################################################################################################

          def self._check_allowed_types(type)
            if TYPE_USER != type
              raise LiveHub::Exceptions::UnknownGeneratorException.new('type ' + type.inspect)
            end
          end
        end

      end
    end
  end
end