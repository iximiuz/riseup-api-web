module Riseup
  module Services
    module LiveHub
      module Privacy

        class User2GeneratorPrivilege
          attr_reader :user_id, :generator, :privacy_levels

          def initialize(user_id, generator, privacy_levels)
            @user_id = user_id
            @generator = generator
            @privacy_levels = privacy_levels
          end

        end

      end
    end
  end
end