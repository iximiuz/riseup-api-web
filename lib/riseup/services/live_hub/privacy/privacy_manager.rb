module Riseup
  module Services
    module LiveHub
      module Privacy

        class PrivacyManager

          def initialize(friendship_service = Riseup::Services::SP.i.get_service(:friendship), logger = Rails.logger)
            @friendship_service = friendship_service
            @logger = logger
          end

          def calc_privileges(user_id, generators)
            generators_by_type = _group_generators_by_type(generators)

            # todo: заменить switch наследованием
            privacies = {}
            generators_by_type.map do |type, gens|
              case type
                when LiveHub::Generators::TYPE_USER
                  privacies[type] = _calc_privacy_levels_user_2_user(user_id, gens)
                else
                  raise LiveHub::Exceptions::UnknownGeneratorException.new
              end
            end

            generators.map do |generator|
              User2GeneratorPrivilege.new(user_id, generator, privacies[generator.type][generator.id])
            end
          end

          private ##########################################################################################################

          def _calc_privacy_levels_user_2_user(user_id, users_generators)
            generators_by_credentials = {}
            users_generators.each do |generator|
              (generators_by_credentials[generator.credentials] ||= []) << generator
            end

            result = {}
            generators_by_credentials.map do |credential, generators|
              case credential
                when Rest::User::CRED_PUBLIC
                  # full access
                  generators.each { |generator| result[generator.id] = [0, 1, 2] }
                when Rest::User::CRED_FOR_FRIENDS
                  friendship_statuses = @friendship_service.friendship_status(user_id, generators.map { |g| g.id } )

                  generators.each do |generator|
                    if Services::Friendship::FriendshipService::STATUS_IS_FRIEND == friendship_statuses[generator.id]
                      result[generator.id] = [0, 1, 2] # full access
                    else
                      # access to only explicitly shared content
                      result[generator.id] = [2]
                    end
                  end

                when Rest::User::CRED_INVISIBLE
                  # access to only explicitly shared content
                  generators.each { |generator| result[generator.id] = [2] }
                else
                  @logger.error('Unexpected user credentials: ' + credential.inspect)
              end
            end

            result
          end

          def _group_generators_by_type(generators)
            result = {}

            generators.each do |generator|
              (result[generator.type] ||= []) << generator
            end

            result
          end
        end

      end
    end
  end
end