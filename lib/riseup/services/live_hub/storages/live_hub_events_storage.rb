module Riseup
  module Services
    module LiveHub
      module Storages

        class LiveHubEventsStorage

          def change_events_privacy_level(generator, subject_id, subject_type, privacy_level)
            LiveHubEvent.update_all(
                {:privacy_level => privacy_level},
                {
                    :generator_id => generator.id,
                    :generator_type => generator.type,
                    :subject_id => subject_id,
                    :subject_type => subject_type
                }
            )
          end

          def record_event(event)
            live_hub_record = LiveHubEvent.new

            live_hub_record.event_type = event.type
            live_hub_record.generator_id = event.generator.id
            live_hub_record.generator_type = event.generator.type
            live_hub_record.subject_id = event.subject_id.to_s
            live_hub_record.subject_type = event.subject_type
            live_hub_record.privacy_level = event.privacy_level

            live_hub_record.save
          end

          def remove_events(generator, subject_id, subject_type)
              LiveHubEvent.delete_all(
                  [
                      'generator_id = ? AND generator_type = ? AND subject_id = ? AND subject_type = ?',
                      generator.id,
                      generator.type,
                      subject_id.to_s,
                      subject_type
                  ]
              )
          end

          # @return [LiveHubEvent[]]
          def retrieve_events_selection(generators_criteria, limit, from_timestamp = nil)
            if generators_criteria.empty?
              return []
            end

            query_string = 'FALSE'
            generators_criteria.each do |criterion|
              query_string += ' OR (generator_id = ' + criterion.generator.id.to_s
              query_string += ' AND generator_type = ' + criterion.generator.type.to_s
              query_string += ' AND privacy_level IN (' + criterion.privacy_levels.join(',') + '))'
            end

            if nil != from_timestamp
              query_string = "(#{query_string}) AND created_at <= '#{from_timestamp}'"
            end

            LiveHubEvent.where(query_string).limit(limit).order('created_at DESC')
          end
        end

      end
    end
  end
end