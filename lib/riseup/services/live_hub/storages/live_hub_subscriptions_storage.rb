module Riseup
  module Services
    module LiveHub
      module Storages

        class LiveHubSubscriptionsStorage
          def create_subscription(user_id, generator)
            subscription_record = LiveHubSubscription.new

            subscription_record.user_id = user_id
            subscription_record.generator_id = generator.id
            subscription_record.generator_type = generator.type

            subscription_record.save
          end

          def remove_subscription(user_id, generator)
            LiveHubSubscription.delete_all(
                ['user_id = ? AND generator_id = ? AND generator_type = ?', user_id, generator.id, generator.type]
            )
          end

          def retrieve_generators_for_user(user_id)
            subscriptions = LiveHubSubscription.where(user_id: user_id)

            generators_data = subscriptions.each do |s|
              {:generator_id => s.generator_id, :generator_type => s.generator_type }
            end

            LiveHub::Generators::Generator.create_batch(generators_data)
          end
        end

      end
    end
  end
end