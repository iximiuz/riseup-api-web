module Riseup
  module Services
    module Exceptions

      class UnregisteredServiceException < ServicesException

      end

    end
  end
end
