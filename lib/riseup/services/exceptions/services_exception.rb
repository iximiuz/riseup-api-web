module Riseup
  module Services
    module Exceptions

      class ServicesException < Riseup::Exceptions::RiseupException

      end

    end
  end
end
