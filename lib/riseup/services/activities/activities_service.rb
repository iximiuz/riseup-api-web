module Riseup
  module Services
    module Activities

      class ActivitiesService
        def initialize(activities_storage)
          @activities_storage = activities_storage
        end

        def create(user, activity)
          if activity.userId != user.id
            raise Riseup::Exceptions::PrivilegeException.new
          end

          unless activity.new?
            raise Riseup::Services::Activities::Exceptions::ActivitiesServiceException.new(
                      'Can\'t create new activity with predefined id. Maybe use HTTP PUT Method for update existing?',
                      1033
                  )
          end

          save(activity)
        end

        def delete(activity)
          @activities_storage.delete(activity)
        end

        def find(id, access_privileges = [])
          assert id.is_a?(Enumerable) || id.is_a?(String) || id.is_a?(Riseup::Rest::Id::ActivityId)

          return [] if !id || (id.is_a?(Enumerable) && id.empty?)

          @activities_storage.find(id, access_privileges)
        end

        def find_by_activity(user_id, activity_id, a_count, b_count, align_by_days, goal_ids = [], access_privileges = [])
          @activities_storage.find_by_activity(user_id, activity_id, a_count, b_count, align_by_days, goal_ids, access_privileges)
        end

        def find_by_date(user_id, date, a_count, b_count, align_by_days, goal_ids = [], access_privileges = [])
          @activities_storage.find_by_date(user_id, date, a_count, b_count, align_by_days, goal_ids, access_privileges)
        end

        def find_by_date_range(user_id, date_from, date_to, goal_ids = [], access_privileges = [])
          @activities_storage.find_by_date_range(user_id, date_from, date_to, goal_ids, access_privileges)
        end

        def find_goal_stat(goal_id, user_utc_time)
          @activities_storage.find_goal_stat(goal_id, user_utc_time)
        end

        def find_goals_stat(goal_ids, user_utc_time)
          @activities_storage.find_goals_stat(goal_ids, user_utc_time)
        end

        def find_users_stat(ids, user_utc_time)
          @activities_storage.find_users_stat(ids, user_utc_time)
        end

        def save(activity)
          @activities_storage.save(activity)
        end
      end

    end
  end
end