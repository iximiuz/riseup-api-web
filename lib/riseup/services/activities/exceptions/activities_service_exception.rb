module Riseup
  module Services
    module Activities
      module Exceptions

        class ActivitiesServiceException < Riseup::Services::Exceptions::ServicesException

        end

      end
    end
  end
end