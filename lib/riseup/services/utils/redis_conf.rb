module Riseup
  module Services
    module Utils
      module RedisConf
        private
        @@conf = nil

        public
        def read_conf
          unless @@conf
            conf = YAML.load(File.open(Rails.root.join('config/redis.yml'))).symbolize_keys
            defaults = conf[:default].symbolize_keys

            env = Rails.env.to_sym
            unless conf[env]
              raise "Environment #{env} does not set in redis conf file."
            end

            @@conf = defaults.merge(conf[env].symbolize_keys)
          end

          @@conf
        end
      end
    end
  end
end