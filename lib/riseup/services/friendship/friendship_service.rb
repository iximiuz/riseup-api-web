require 'observer'

module Riseup
  module Services
    module Friendship

      class FriendshipService
        include Observable

        STATUS_NOT_A_FRIEND = 0
        STATUS_INVOCATION_WAS_SENT = 1
        STATUS_HAS_INCOMING_INVOCATION = 2
        STATUS_IS_FRIEND = 3

        EV_NEW_INVOCATION = 'new_invocation'
        EV_CONNECTION_DESTROYED = 'connection_destroyed'
        EV_INVOCATION_ACCEPTED = 'invocation_accepted'


        def initialize(friendship_storage = Friendship::Storages::FriendshipStorage.new)
          @friendship_storage = friendship_storage
        end

        def accept_invocation(acceptor_user_id, friendship_id)
          @friendship_storage.accept_invocation(acceptor_user_id, friendship_id)

          changed
          notify_observers(EV_INVOCATION_ACCEPTED, acceptor_user_id, friendship_id.get_mate(acceptor_user_id))

          STATUS_IS_FRIEND
        end

        def friends?(one_user_id, another_user_id)
          @friendship_storage.friends?(one_user_id, another_user_id)
        end

        # @return [Hash] - хеш статусов {target_user_id_1: status, target_user_id_2: status, ...}
        def friendship_status(pivotal_user_id, target_user_id)
          assert target_user_id.is_a?(Enumerable) || target_user_id.is_a?(Integer)

          target_user_ids = target_user_id.is_a?(Enumerable) ? target_user_id : [target_user_id]

          @friendship_storage.retrieve_friendship_statuses(pivotal_user_id, target_user_ids)
        end

        def get_friends_for_user(user_id)
          _prepare_data_from_storage(@friendship_storage.get_friends_for_user(user_id))
        end

        def inbox_request(user_id)
          _prepare_data_from_storage(@friendship_storage.inbox_requests(user_id))
        end

        def outbox_request(user_id)
          _prepare_data_from_storage(@friendship_storage.outbox_requests(user_id))
        end

        def send_invocation(initiator_user_id, target_user_id)
          @friendship_storage.create_invocation(initiator_user_id, target_user_id)

          changed
          notify_observers(EV_NEW_INVOCATION, initiator_user_id, target_user_id)

          STATUS_INVOCATION_WAS_SENT
        end

        # Уничтожает входящую или исходящую заявки или же уже установленную дружбу
        def break_friendship(breaker_user_id, friendship_id)
          @friendship_storage.break_friendship(breaker_user_id, friendship_id)

          changed
          notify_observers(EV_CONNECTION_DESTROYED, breaker_user_id, friendship_id.get_mate(breaker_user_id))

          STATUS_NOT_A_FRIEND
        end

        private ########################################################################################################

        def _prepare_data_from_storage(data_from_storage)
          result = []
          data_from_storage.each do |request_data|
            result << {:user_id => request_data[0].to_i, :created_at => request_data[1] }
          end

          result
        end
      end

    end
  end
end