module Riseup
  module Services
    module Friendship
      module Exceptions

        class FriendshipException < Services::Exceptions::ServicesException

        end

      end
    end
  end
end
