module Riseup
  module Services
    module Friendship
      module Storages

        # Все операции являются атомарными.
        #
        # Хранилище гарантирует следующую согласованность данных:
        # - если есть хоть какая-то связь между двумя пользователями (заявка или дружба), то есть и fusion-ключ
        # - если нет никакой связи - то не и fusion-ключа
        # - если заявка попала в исходящие для пользователя-инициатора, то она
        #   будет и во входящих у пользователя-получателя
        # - если заявка была удалена из исходящих, то она была удалена и из входящих
        # - если заявка была принята, то была добавлена соотв. связь дружбы для обоих пользователей
        #   и удалены две заявки (входящая и исходящая)
        #
        class FriendshipStorage

          STATUS_PENDING = 1
          STATUS_ESTABLISHED = 2

          def initialize(base_nest = Services::Nest.new('friendship'), logger = Rails.logger)
            @base_nest = base_nest
            @logger = logger
          end

          def accept_invocation(acceptor_user_id, friendship_id)
            # Приглашение можно принять, только если оно есть у acceptor_user_id во входящих

            requester_user_id = friendship_id.get_mate(acceptor_user_id)
            if nil == requester_user_id
              raise Friendship::Exceptions::FriendshipException.new(
                        "Wrong friendship pair #{friendship_id.to_s} for acceptor #{acceptor_user_id}."
                    )
            end

            mutex_nest = _lock_mutex(friendship_id)

            acceptor_inbox_nest = _get_inbox_requests_nest(acceptor_user_id)
            if nil == acceptor_inbox_nest.zscore(requester_user_id)
              _unlock_mutex

              raise Friendship::Exceptions::FriendshipException.new(
                        "Can not accept invocation #{friendship_id.to_s} without inbox request."
                    )
            end

            multi_exec_result = @base_nest.redis.multi do
              acceptor_inbox_nest.zrem(requester_user_id)
              _get_outbox_requests_nest(requester_user_id).zrem(acceptor_user_id)

              now_timestamp = Time.now.utc.to_i
              _get_friends_nest(acceptor_user_id).zadd(now_timestamp, requester_user_id)
              _get_friends_nest(requester_user_id).zadd(now_timestamp, acceptor_user_id)
              _get_fusion_nest(friendship_id).set(
                  {
                      :initiator_id => requester_user_id,
                      :created_at => now_timestamp,
                      :status => STATUS_ESTABLISHED
                  }.to_json
              )
              mutex_nest.incr
            end

            _ensure_multi_exec(multi_exec_result)
          end

          def break_friendship(breaker_user_id, friendship_id)
            # дружбу можно разорвать только, если breaker_user_id является участником этой связи

            mate_user_id = friendship_id.get_mate(breaker_user_id)
            if nil == mate_user_id
              raise Friendship::Exceptions::FriendshipException.new(
                        "Wrong friendship pair #{friendship_id.to_s} for break by user #{breaker_user_id}."
                    )
            end

            multi_exec_result = @base_nest.redis.multi do
              #todo: можно бы и оптимизнуть кол-во запросов тут...
              _get_inbox_requests_nest(breaker_user_id).zrem(mate_user_id)
              _get_inbox_requests_nest(mate_user_id).zrem(breaker_user_id)
              _get_outbox_requests_nest(breaker_user_id).zrem(mate_user_id)
              _get_outbox_requests_nest(mate_user_id).zrem(breaker_user_id)
              _get_friends_nest(breaker_user_id).zrem(mate_user_id)
              _get_friends_nest(mate_user_id).zrem(breaker_user_id)
              _get_fusion_nest(friendship_id).del
              _get_mutex_nest(friendship_id).del
            end

            _ensure_multi_exec(multi_exec_result)
          end

          def create_invocation(initiator_user_id, target_user_id)
            # приглашение можно отправить только, если выполняются все условия:
            # - нет входящей заявки от этого пользователя
            # - нет исходящей заявки этому пользователю
            # - инициатор и целевой пользователь еще не дружат

            friendship_id = Riseup::Rest::Id::FriendshipId.new(initiator_user_id, target_user_id)
            mutex_nest = _lock_mutex(friendship_id)

            fusion_nest = _get_fusion_nest(friendship_id)
            fusion = _parse_fusion_data(fusion_nest.get)
            if nil != fusion
              _unlock_mutex

              raise Friendship::Exceptions::FriendshipException.new(
                        'Can not send invocation because friendship relation already exists.'
                    )
            end

            multi_exec_result = @base_nest.redis.multi do
              now_timestamp = Time.now.utc.to_i

              _get_inbox_requests_nest(target_user_id).zadd(now_timestamp, initiator_user_id)
              _get_outbox_requests_nest(initiator_user_id).zadd(now_timestamp, target_user_id)
              fusion_nest.set(
                  {
                      :initiator_id => initiator_user_id,
                      :created_at => now_timestamp,
                      :status => STATUS_PENDING
                  }.to_json
              )
              mutex_nest.incr
            end

            _ensure_multi_exec(multi_exec_result)
          end

          # todo: добавить limit + offset
          def inbox_requests(user_id)
            _get_inbox_requests_nest(user_id).zrange(0, -1, :with_scores => true)
          end

          def friends?(one_user_id, another_user_id)
            nil != _get_friends_nest(one_user_id).zscore(another_user_id)
          end

          def get_friends_for_user(user_id)
            _get_friends_nest(user_id).zrange(0, -1, :with_scores => true)
          end

          # todo: добавить limit + offset
          def outbox_requests(user_id)
            _get_outbox_requests_nest(user_id).zrange(0, -1, :with_scores => true)
          end

          def retrieve_friendship_statuses(pivotal_user_id, target_user_ids)
            fusion_keys = []

            target_user_ids.each do |target_user_id|
              fusion_keys << _get_fusion_nest(
                  Riseup::Rest::Id::FriendshipId.new(pivotal_user_id, target_user_id)
              ).to_s
            end

            fusions = @base_nest.redis.mget(fusion_keys)

            result = {}
            fusions.each_with_index do |fusion, idx|
              fusion = _parse_fusion_data(fusion)

              if fusion.nil?
                status = Friendship::FriendshipService::STATUS_NOT_A_FRIEND
              else
                if STATUS_ESTABLISHED == fusion[:status]
                  status = Friendship::FriendshipService::STATUS_IS_FRIEND
                elsif fusion[:initiator_id] == pivotal_user_id
                  status = Friendship::FriendshipService::STATUS_INVOCATION_WAS_SENT
                else
                  status = Friendship::FriendshipService::STATUS_HAS_INCOMING_INVOCATION
                end
              end

              result[target_user_ids[idx]] = status
            end

            result
          end

          private ######################################################################################################

          def _ensure_multi_exec(ret_val)
            # todo: добавить проверку возвращаемого multi массива значений
            if nil == ret_val
              raise Friendship::Exceptions::FriendshipException.new('Error during saving data.')
            end
          end

          # riseup:<env>:friendship:<user_id>:friends
          #
          # sorted set
          def _get_friends_nest(user_id)
            @base_nest[user_id]['friends']
          end

          # Нужна для хранения информации о связи и сокращения числа запросов
          # при определении, дуржит ли пользователь с несколькими другими пользователями
          # за счет MGET
          #
          # riseup:<env>:friendship:<less-user-id_larger-user-id>:fusion
          #
          # string
          def _get_fusion_nest(friendship_id)
            @base_nest[friendship_id.to_s]['fusion']
          end

          # riseup:<env>:friendship:<user_id>:inbox
          #
          # sorted set
          def _get_inbox_requests_nest(user_id)
            @base_nest[user_id]['inbox']
          end

          # Нужна для обеспечения атомарности операций хранилища. Каждая запись увеличивает счетчик на 1.
          #
          # riseup:<env>:friendship:<less-user-id_larger-user-id>:mutex
          #
          # string
          def _get_mutex_nest(friendship_id)
            @base_nest[friendship_id.to_s]['mutex']
          end

          # riseup:<env>:friendship:<user_id>:outbox
          #
          # sorted set
          def _get_outbox_requests_nest(user_id)
            @base_nest[user_id]['outbox']
          end

          def _lock_mutex(friendship_id)
            mutex_nest = _get_mutex_nest(friendship_id)

            # Создает новый ключ, если таковой еще не существует,
            # так как watch можно делать только на существующие ключи.
            # При этом если такой ключ уже существует, и где-то длится watch на него,
            # мы не нарушаем watch, так как отрабатывает условие NX =)
            mutex_nest.setnx(1)
            @base_nest.redis.watch(mutex_nest.to_s)

            mutex_nest
          end

          def _parse_fusion_data(fusion_data)
            return nil if fusion_data.nil?

            begin
              fusion = JSON::parse(fusion_data).deep_symbolize_keys

              assert(fusion.has_key?(:status))
              assert(STATUS_ESTABLISHED == fusion[:status] || STATUS_PENDING == fusion[:status])
              assert(fusion.has_key?(:initiator_id))

              return fusion
            rescue JSON::ParserError
              @logger.error('Bad JSON for friendship fusion data: ' + fusion_data.inspect)

              nil
            end
          end

          def _unlock_mutex
            @base_nest.redis.unwatch
          end
        end

      end
    end
  end
end

