module Riseup
  module Domain
    module Users

      class UserActivitiesStat
        def initialize(id, completed_count, failed_count, total_count)
          @id = id
          @completed_count = completed_count
          @failed_count = failed_count
          @total_count = total_count
        end

        def get_id
          @id
        end

        def get_completed_count
          @completed_count
        end

        def get_failed_count
          @failed_count
        end

        def get_total_count
          @total_count
        end
      end

    end
  end
end