module Riseup
  module Domain
    module Activities

      class Day
        attr_accessor :user_id

        def initialize(user_id, date_timestamp, activity_ids = [])
          if !date_timestamp.respond_to?('to_i') || date_timestamp.to_i != date_timestamp
            raise 'Param "date_timestamp" should by timestamp.'
          end

          @user_id = user_id
          @date = date_timestamp
          @activity_ids = activity_ids
        end

        def append_activity(activity_id)
          assert activity_id.is_a?(Riseup::Rest::Id::Id)

          @activity_ids.append(activity_id)
        end

        def prepend_activity(activity_id)
          assert activity_id.is_a?(Riseup::Rest::Id::Id)

          @activity_ids.prepend(activity_id)
        end

        def insert_after(whom_id, who_id)
          assert whom_id.is_a?(Riseup::Rest::Id::Id)
          assert who_id.is_a?(Riseup::Rest::Id::Id)

          whom_index = _index(whom_id)
          unless whom_index
            raise Riseup::Exceptions::RiseupException.new("Activity with id #{whom_id} not found in day #{@date}")
          end

          @activity_ids.insert(whom_index + 1, who_id.to_s)
        end

        def insert_before(whom_id, who_id)
          assert whom_id.is_a?(Riseup::Rest::Id::Id)
          assert who_id.is_a?(Riseup::Rest::Id::Id)

          whom_index = _index(whom_id)
          unless whom_index
            raise Riseup::Exceptions::RiseupException.new("Activity with id #{whom_id} not found in day #{self}")
          end

          @activity_ids.insert(whom_index, who_id)
        end

        # @deprecated
        # @todo: !refactor make private
        def get_activities
          @activity_ids
        end

        def get_activities_after(pivot, max_count = nil)
          unless has_activity(pivot)
            raise Riseup::Exceptions::RiseupException.new("Activity with id #{pivot} not found in day #{self}")
          end

          if is_last_activity(pivot) || (nil != max_count && 0 >= max_count)
            return []
          end

          pivot_idx = _index(pivot)
          if nil != max_count
              return get_activities.slice(pivot_idx + 1, [activities_count, max_count].min)
          end

          get_activities.slice(pivot_idx + 1..activities_count)
        end

        def get_activities_before(pivot, max_count = nil)
          unless has_activity(pivot)
            raise Riseup::Exceptions::RiseupException.new("Activity with id #{pivot} not found in day #{self}")
          end

          if is_first_activity(pivot) || (nil != max_count && 0 >= max_count)
            return []
          end

          pivot_idx = _index(pivot)

          start_pos = (nil == max_count) ? 0 : [0, pivot_idx - max_count].max

          count = pivot_idx - start_pos
          get_activities.slice(start_pos, count)
        end

        def remove_activity(activity_id)
          assert activity_id.is_a?(Riseup::Rest::Id::Id)

          get_activities.delete(activity_id.to_s)
        end

        def has_activity(activity_id)
          return false if nil == activity_id

          assert activity_id.is_a?(Riseup::Rest::Id::Id)

          nil != _index(activity_id)
        end

        def activities_count
          get_activities.count
        end

        def get_date(as_timestamp = false)
          return @date if as_timestamp
          Time.at(@date).to_date
        end

        def get_first_activity
          return nil if empty?

          get_activities[0]
        end

        def get_next_activity(id)
          if empty?
            raise Riseup::Exceptions::RiseupException.new("Day #{self} is empty.")
          end

          unless has_activity(id)
            raise Riseup::Exceptions::RiseupException.new("Day #{self} has not activity #{id}.")
          end

          idx = _index(id)
          (idx < activities_count - 1) ? get_activities[idx + 1] : nil
        end

        def get_last_activity
          return nil if empty?

          get_activities[activities_count - 1]
        end

        def get_prev_activity(id)
          if empty?
            raise Riseup::Exceptions::RiseupException.new("Day #{self} is empty.")
          end

          unless has_activity(id)
            raise Riseup::Exceptions::RiseupException.new("Day #{self} has not activity #{id}.")
          end

          idx = _index(id)
          (0 < idx) ? get_activities[idx - 1] : nil
        end

        def is_first_activity(activity_id)
          # @todo: raise ActivityNotInDay exceptions
          0 == _index(activity_id)
        end

        def is_last_activity(activity_id)
          # @todo: raise ActivityNotInDay exceptions
          activities_count - 1 == _index(activity_id)
        end

        def to_s
          "Day [#{get_date}] user_id=[#{user_id}] first act-y=[#{@activity_ids.first}] last act-y=[#{@activity_ids.last}]"
        end

        def empty?
          0 == activities_count
        end

        ################################################################################################################

        private

        def _index(id)
          @activity_ids.index { |el| el == id }
        end
      end

    end
  end
end