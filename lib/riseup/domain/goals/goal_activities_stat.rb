module Riseup
  module Domain
    module Goals

      class GoalActivitiesStat
        def initialize(goal_id, completed_count, failed_count, total_count)
          @goal_id = goal_id
          @completed_count = completed_count
          @failed_count = failed_count
          @total_count = total_count
        end

        def get_goal_id
          @goal_id
        end

        def get_completed_count
          @completed_count
        end

        def get_failed_count
          @failed_count
        end

        def get_total_count
          @total_count
        end
      end

    end
  end
end