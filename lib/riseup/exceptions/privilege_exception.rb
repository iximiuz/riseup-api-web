module Riseup
  module Exceptions

    class PrivilegeException < RiseupException
      def initialize(msg = 'Access denied.', code = 1009)
        super(msg, code)
      end
    end

  end
end