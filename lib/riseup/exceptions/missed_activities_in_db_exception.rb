module Exceptions
  module ActivitiesStorage
    class MissedActivitiesInDbException < Exception
      attr_accessor :missed_activities

      def initialize(missed_acty_ids)
        @missed_activities = missed_acty_ids
      end
    end
  end
end