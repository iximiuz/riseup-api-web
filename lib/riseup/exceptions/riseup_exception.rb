module Riseup
  module Exceptions

    class RiseupException < ::Exception
      attr_reader :code

      def initialize(msg = nil, code = 0)
        super(msg)
        @code = code
      end

    end

  end
end