module Exceptions
  module ActivitiesStorage
    class MissedDayException < Exception
      attr_accessor :user_id, :date

      def initialize(user_id, date)
        @user_id = user_id
        @date = date
      end
    end
  end
end