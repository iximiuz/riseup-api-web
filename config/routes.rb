Riseup::Application.routes.draw do
  constraints DomainConstraint.new(APP_CONFIG.domains['known_api']) do
    #
    resources :activities do
      resource :comments
    end
    resources :auth_tokens, :only => [:show, :create, :destroy]
    resources :comments, :only => [:create, :update, :delete, :index]
    resources :feedback, :only => [:create]
    resources :friendship, :only => [:index, :create, :update, :destroy]
    resources :friends, :only => [:index]
    resources :goals
    resources :live_hub_events, :only => [:index, :show]
    resources :registry, :controller => 'activities/registry', :path => 'activities/registry', :only => [:index]
    resources :search, :only => [:show]
    resources :statistic, :only => [:create]
    resources :subscriptions, :controller => 'users/subscriptions', :only => [:index, :update]
    resources :users do
      resources :activities, :only => [:index, :show], :controller => 'users/activities'
      resources :avatar, :controller => 'users/avatar'
      resources :awards, :controller => 'users/awards'
      resources :registry, :controller => 'users/activities/registry', :path => 'activities/registry', :only => [:index]
    end

    namespace :upload do
      resource :photo
    end


    scope '/voting/:version' do
      scope 'objects/:object/:obj_id(/:tag)' do
        resources :votes, :controller => 'voting/objects_votes'
      end
    end

    match '(*r)', :controller => 'api', :action => 'options', :constraints => {:method => 'OPTIONS'} , :via => [:options]
    #get '(*r)', to: 'api#options'
  end

  constraints DomainConstraint.new([APP_CONFIG.domains['app']]) do
    match '(*r)' => 'mobile#index', :via => [:options, :get]
    #get '(*r)', to: 'mobile#index'
  end

  # todo: !improve route to 404 page
  match '(*r)' => 'web#index', :via => [:options, :get]
  #get '(*r)', to: 'web#index'
end
