module MobileHelper
  def add_js_stack(*files)
    content_for(:head) { javascript_include_tag(*files) }
  end
end
