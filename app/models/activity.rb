class Activity < ActiveRecord::Base
  has_one :user
  attr_accessible :cdate, :description, :goal_id, :id, :start_dt, :state, :udate, :user_id, :comments_count
  before_save :set_udate
  before_create :set_udate

  private
  def set_udate
    self.udate = Time.now
  end
end
