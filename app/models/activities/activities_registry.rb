module Activities
  class ActivitiesRegistry < ActiveRecord::Base
    attr_accessible :id, :user_id, :date, :count
  end
end
