require File.dirname(__FILE__) + '/schemas/live_hub_schema'

class LiveHubSubscription < Schemas::LiveHubSchema
  self.table_name = 'subscriptions'

  attr_accessible :id, :user_id, :generator_id, :generator_type, :created_at
end