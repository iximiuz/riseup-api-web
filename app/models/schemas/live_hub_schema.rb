module Schemas

  class LiveHubSchema < ActiveRecord::Base
    self.abstract_class = true

    establish_connection "#{Rails.env}_live_hub"
  end

end
