class AuthToken < ActiveRecord::Base
  attr_accessible :cdate, :client_id, :id, :token, :user_id
end
