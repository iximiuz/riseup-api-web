require File.dirname(__FILE__) + '/schemas/live_hub_schema'

class ::LiveHubEvent < Schemas::LiveHubSchema
  self.table_name = 'events'

  attr_accessible :id, :generator_id, :generator_type, :event_id, :event_type, :event_created_at, :created_at
end