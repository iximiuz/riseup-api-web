class User < ActiveRecord::Base
  has_many :activities

  before_save :check_credential

  private

  def check_credential
    self.credentials ||= 0
  end
end
