(function(App, Backbone, _) {
	'use strict';
    
    App.View.Mobile.VActivityStream = App.View.BaseView.extend({
		tagName: 'div',
		className: 'activity-stream',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/ActivityStream'))(args); },

        events: {
            'click .activity-stream__error-msg-back': '_goBack'
        },

        initialize: function(attrs) {
            var self = this;

            attrs = attrs || {};
            this.m_nUserId = attrs.userId;
            this.m_mUser = attrs.user;
            this.m_bCanEdit = attrs.editable;
            this.m_tCurDate = attrs.date || App.getStartOfToday(true);
            this.m_sMode = attrs.mode;
            this.m_sBindActivityId = attrs.bindActivityId;


            this.m_oDays = {dates: [], days: {}};
            this.m_aActivities = {};
            this.m_aPendingActivities = {};


            App.M.installTo(this);
            this.m_sActivitiesChannel = 'acty-stream:data-loaded';
            this.sub(this.m_sActivitiesChannel, this.renderActivities);
            this.sub('activity:moved', this._moveActivity);
            this.sub('activity:removed', this._removeActivity);
            this.sub('activity-stream:new-days', this._addNewDays);
            this.sub('activity-stream:aux-days', this._loadAuxDaysExplicit);
            this.sub('day:need-empty-activity', this.addNewEmptyActivityToDay);
            this.sub('activity-stream:scroll-to-day', this.scrollToDay);
            this.sub('rise-binding-widget-bind', this._bindActivityToRise);

            // Реестр активностей
            this.m_bRegistryLoaded = false;
            this.m_aRegistryLoadDeffereds = [];
            this.m_mRegistry = new App.Model.RegistryActivities({
                userId: this.m_nUserId, isCurUser: this.m_nUserId == App.currentUser.id
            });

            this.m_mRegistry.fetch({success: function() {
                self._onRegistryLoad();
            }});


            this.m_bLoadingLock = false;

            App.View.Mobile.VAddDayWidget.resetWidgets();
            this.resetCache();
        },

        render: function() {
            var req = {
                acount: 20,
                bcount: 20,
                channel: this.m_sActivitiesChannel,
                dayAlign: true,
                mode: this.m_sMode,
                userId: this.m_nUserId
            }, self = this;

            if ('rise-binding' === this.m_sMode) {
                req.activityId = this.m_sBindActivityId;
            } else {
                req.date = this.m_tCurDate;
                this._registerOnRenderCompleteListener(function() {
                    this.scrollToDay(this.m_tCurDate);
                });
            }

            this._tryLockLoadingMutex();
            this._registerOnRenderCompleteListener(function() {
                self._initScrollDetection()
            });
            this._load(req);

            return App.View.BaseView.prototype.render.apply(this, arguments);
        },

        addNewSiblingActivity: function(vSiblingActivity, sPos) {
            if (!this.m_bCanEdit) {
                App.assert(false);
                return;
            }

            App.assert('after' === sPos || 'before' === sPos);
            sPos = sPos || 'after';

            var mActivity = new App.Model.Activity({
                userId: vSiblingActivity.model.get('userId'),
                startDate: vSiblingActivity.model.get('startDate'),
                cdate: moment(new Date())
            }),
                vNewActivity = this._createNewActivityView({model: mActivity, timeline: this}),
                vDay = this._getOrCreateDay(vSiblingActivity.model.get('startDate'));

            vNewActivity.model[{after: 'prevCid', before: 'nextCid'}[sPos]] = vSiblingActivity.model.cid;
            vDay.renderActivity(vNewActivity);
            vNewActivity.tryStartEdit();
        },

        addNewEmptyActivityToDay: function(vDay) {
            if (!this.m_bCanEdit) {
                App.assert(false);
                return;
            }

            var mActivity = new App.Model.Activity({
                    userId: this.m_nUserId,
                    startDate: vDay.getDate(),
                    cdate: moment(new Date())
                }),
                vNewActivity = this._createNewActivityView({model: mActivity, timeline: this});

            vDay.renderActivity(vNewActivity);
            vNewActivity.tryStartEdit();
        },

        findNext: function(vActivity, onlySaved) {
            var sDate = vActivity.model.get('startDate');
            App.assert(sDate);

            return this._getOrCreateDay(sDate).findNext(vActivity, onlySaved);
        },

        findPrev: function(vActivity, onlySaved) {
            var sDate = vActivity.model.get('startDate');
            App.assert(sDate);

            return this._getOrCreateDay(sDate).findPrev(vActivity, onlySaved);
        },

        /**
         * Вызывается из Activity View при сохранении новой модельки активности.
         * @param vActivity
         */
        registerCreatedActivity: function(vActivity) {
            App.assert(!vActivity.model.isNew());
            this.m_aActivities[vActivity.model.id] = vActivity;
            delete this.m_aPendingActivities[vActivity.model.cid];
        },

        renderActivities: function(aActivities) {
            if (this.m_bNeedRestoreScrollPos) {
                App.saveWindowScrollPos();
            }


            if (aActivities.error) {
                this._getDaysHolder().append(
                    '<div class="activity-stream__no-one-activity-msg"><span>'
                        + this._getErrorMsgByCode(aActivities.code) + '</span><br/>' +
                        '<div class="activity-stream__error-msg-back btn btn-primary">Назад</div></div>'
                );
            } else {
                this._renderActivities(aActivities);
                this._makeRegistryLoadedDeferredCall(this._renderAddDayWidgetsIfNeeded);

                if (0 === this.m_oDays.dates.length) {
                    App.assert('rise-binding' !== this.m_sMode); //TODO: показать предупреждение

                    if (this.m_nUserId == App.currentUser.id) {
                        this._addNewDays({from: this.m_tCurDate.clone().add('days', 1), to: this.m_tCurDate.clone().add('days', 2)});
                        this._addNewDay(this.m_tCurDate);
                    } else {
                        this._getDaysHolder().append(
                            '<div class="activity-stream__no-one-activity-msg"><span>Пока ни одной активности, а жаль...</span></div>'
                        );
                    }
                } else if (this.m_nUserId == App.currentUser.id && !this._getDay(this.m_tCurDate)) {
                    this._addNewDay(this.m_tCurDate);
                } else if ('rise-binding' === this.m_sMode) {
                    if (this.m_aActivities[this.m_sBindActivityId]) {
                        this.pub('rise-binding', this.m_sBindActivityId);
                    } else {
                        App.assert(this.m_aActivities[this.m_sBindActivityId]);
                        //TODO: показать предупреждение и App.navigate('timeline');
                        App.navigate('timeline');
                    }
                }
            }

            this._unlockLoadingMutex();
            this._notifyOnRenderComplete();

            if (this.m_bNeedRestoreScrollPos) {
                App.restoreWindowScrollPos();
                this.m_bNeedRestoreScrollPos = false;
            }
        },

        scrollToActivity: function(vActivity, fnCallback) {
            var vDay = this._getDay(vActivity.model.get('startDate'));

            if (!App.isMobile || App.isIphone() || App.isIpad()) {
                App.scrollWindowToAnimated(vDay.el.offsetTop + vActivity.el.offsetTop, 200, fnCallback);
            } else {
                window.scrollTo(0, vDay.el.offsetTop + vActivity.el.offsetTop);
                fnCallback();
            }
        },

        scrollToDay: function(tDate) {
            if (0 === this.m_oDays.dates.length) {
                return;
            }

            var sDate = tDate.format('YYYY-MM-DD'),
                oNearest,
                vDay = this._getDay(sDate);

            // #BEGIN_ASSERT
            App.assert(!vDay || (vDay.isRendered() && vDay.isRendered()));
            // #END_ASSERT

            if (vDay && vDay.isRendered()) {
                window.scrollTo(0, vDay.el.offsetTop);
            } else {
                oNearest = App.Utils.findOneOrNearest(this.m_oDays.dates, sDate, true, undefined, true)
                    || App.Utils.findOneOrNearest(this.m_oDays.dates, sDate, true);

                App.assert(oNearest && undefined !== oNearest.idx);
                if (oNearest && undefined !== oNearest.idx) {
                    if (oNearest.idx < this.m_oDays.dates.length) {
                        tDate = moment(this.m_oDays.dates[oNearest.idx]);
                        this.scrollToDay(tDate);
                    } else if (oNearest.idx >= 0) {
                        tDate = moment(this.m_oDays.dates[oNearest.idx]);
                        this.scrollToDay(tDate);
                    }
                }
            }
        },

        startScrollDetection: function() {
            this.m_bScrollDetectionStopped = false;
        },

        stopScrollDetection: function() {
            this.m_bScrollDetectionStopped = true;
        },

        subscriberId: function() {
            return 'view:activity-stream';
        },

        /**************************************************************************************************************/

        _addNewDay: function(mixDate) {
            var vDay = this._getOrCreateDay(mixDate);
            App.assert(vDay.isEmpty());
            App.assert(!vDay.isRendered());

            if (!vDay.isRendered()) {
                this._renderDay(vDay);
            }

            var mActivity = new App.Model.Activity({
                    userId: this.m_nUserId,
                    startDate: mixDate,
                    cdate: moment(new Date())
                }),
                vNewActivity = this._createNewActivityView({model: mActivity, timeline: this});

            vDay.renderActivity(vNewActivity);
            this._makeRegistryLoadedDeferredCall(this._renderAddDayWidgetsIfNeeded);
            // if days exists in registry activities - retrieve it from server
            this._makeRegistryLoadedDeferredCall(this._loadDailyActivitiesIfNeeded, [vDay]);

            vNewActivity.tryStartEdit();
        },

        _addNewDays: function(options) {
            options = options || {};
            options.from = options.from || moment();
            options.to = options.to || options.from.clone().add('days', 30);

            var oDateIterator = new App.Utils.DateIterator(options.from, options.to),
                tCurDate,
                counter = 0,
                vDay;

            App.saveWindowScrollPos();

            while (counter++ < 30) {
                tCurDate = oDateIterator.next();
                if (!tCurDate) {
                    break;
                }

                vDay = this._getOrCreateDay(tCurDate);
                App.assert(vDay.isEmpty());
                App.assert(!vDay.isRendered());

                if (!vDay.isRendered()) {
                    this._renderDay(vDay);
                }
            }

            App.restoreWindowScrollPos();

            this._makeRegistryLoadedDeferredCall(this._renderAddDayWidgetsIfNeeded);
        },

        _autoLoading: function(forward) {
            if (!this._tryLockLoadingMutex()) {
                return;
            }

            App.assert(this.m_oDays.dates.length);

            var sBoundaryDate = this.m_oDays.dates[forward ? this.m_oDays.dates.length - 1 : 0],
                req,
                self = this;

            if (
                (forward && this.m_mRegistry.hasDaysAfter(sBoundaryDate))
                || (!forward && this.m_mRegistry.hasDaysBefore(sBoundaryDate))
            ) {
                this._showPreloader(forward ? 'top' : 'bottom');
                this._registerOnRenderCompleteListener(function() { self._hidePreloader(forward ? 'top' : 'bottom'); });

                req = {
                    channel: this.m_sActivitiesChannel,
                    dayAlign: true,
                    userId: this.m_nUserId,
                    date: moment(sBoundaryDate).add('days', forward ? 1 : -1)
                };
                req[forward ? 'bcount' : 'acount'] = 20;

                if (forward) {
                    this.m_bNeedRestoreScrollPos = true;
                }
                this._load(req);
            }
        },

        _bindActivityToRise: function(attrs) {
            attrs = attrs || {};

            var vActivity = this.m_aActivities[attrs.activityId] || this.m_aPendingActivities[attrs.activityId];
            App.assert(vActivity);
            if (!vActivity) {
                return;
            }

            vActivity.model.set('goalId', attrs.goal.id);
            vActivity.model.goal = attrs.goal;

            if (!vActivity.model.isNew()) {
                vActivity.model.save(null, {success: function() {
                    vActivity.render();
                }});
            }
        },

        _createNewActivityView: function(options) {
            var vActivity = new App.View.Mobile.VActivityLite(options);
            this.m_aPendingActivities[options.model.cid] = vActivity;
            return vActivity;
        },

        _getErrorMsgByCode: function(code) {
            var errors = this.m_mUser.isFriend()
                ? {1028: 'Пользователь предпочел скрыть свои активности.'}
                : {1028: 'Доступ к активностям пользователя ограничен. Добавьте его в друзья и получите больше информации о его планах'};

            return errors[code] || 'Неожиданная ошибка';
        },

        _getDayByIdx: function(nIdx) {
            return this.m_oDays.days[this.m_oDays.dates[nIdx]];
        },

        _getDaysHolder: function() {
            return this.getElement('.activity-stream__days');
        },

        /**
         * Возвращает вьюху, соответствующую сохраненной ранее модели.
         * Если эта активность еще не была отрисована ранее, создается новая View.
         *
         * @param mActivity
         * @param forceCreate
         * @returns {App.View.Mobile.VActivityLite}
         * @private
         */
        _getOrCreateActivityViewByModel: function(mActivity, forceCreate) {
            App.assert(!mActivity.isNew());

            if (!this.m_aActivities[mActivity.id] || forceCreate) {
                this.m_aActivities[mActivity.id] = this._createNewActivityView({timeline: this, model: mActivity});
            }

            return this.m_aActivities[mActivity.id];
        },

        _getDay: function(mixDate) {
            var sDate = moment.isMoment(mixDate) ? mixDate.format('YYYY-MM-DD') : moment(mixDate).format('YYYY-MM-DD');
            return this.m_oDays.days[sDate] ? this.m_oDays.days[sDate] : null;
        },

        _getOrCreateDay: function(mixDate) {
            var sDate = moment.isMoment(mixDate) ? mixDate.format('YYYY-MM-DD') : moment(mixDate).format('YYYY-MM-DD');

            if (!this.m_oDays.days[sDate]) {
                this.m_oDays.days[sDate] = new App.View.Mobile.VActivityStreamDay({date: sDate});
                var idx = _.sortedIndex(this.m_oDays.dates, sDate);
                this.m_oDays.dates.splice(idx, 0, sDate);
            }

            return this.m_oDays.days[sDate];
        },

        _goBack: function() {
            window.history.back();
        },

        _initScrollDetection: function() {
            // initialize scroll detection

            this.sub('window:scroll', function(e) {
                if (!self.m_bScrollDetectionStopped) {
                    self.m_bScrolled = true;
                }
            });

            var nScrollTop, nWindowHeight, self = this;
            App.startMutexInterval('activity-stream-scroll', function() {
                if (!self.m_bScrolled) {
                    return;
                }

                self.m_bScrolled = false;

                nScrollTop = window.scrollY;
                nWindowHeight = App.getWindowHeight();

                if (nScrollTop <= 128) {
                    self._tryTriggerOnTop();
                } else if (nScrollTop + nWindowHeight + 128 >= document.documentElement.scrollHeight) {
                    self._tryTriggerOnBottom();
                }
            }, 250);
        },

        _load: function(req) {
            this.pub('acty-stream:load', req);
        },

        _loadAuxDaysExplicit: function(sFrom, sTo) {
            var req = {
                from: moment(sFrom),
                to: moment(sTo),
                channel: this.m_sActivitiesChannel,
                userId: this.m_nUserId
            };

            if (this._tryLockLoadingMutex()) {
                this._load(req);
            } else {
                // TODO: сообщение "Подождите, идет загрузка"
            }
        },

        _loadDailyActivitiesIfNeeded: function(vDay) {
            var sDate = vDay.getDateAsString(), oFrom, oTo;

            if (this.m_mRegistry.dayHasActivities(sDate)) {
                oFrom = this.m_mRegistry.getOldestDayFrom(sDate, 2);
                oTo = this.m_mRegistry.getEarliestDayAfter(sDate, 2);

                this._loadDailyActivitiesByRange(
                    oFrom ? moment(oFrom.date) : vDay.getDate().clone(),
                    oTo ? moment(oTo.date) : vDay.getDate().clone()
                );
            }
        },

        _loadDailyActivitiesByRange: function(tFrom, tTo) {
            var req = {
                from: tFrom,
                to: tTo,
                channel: this.m_sActivitiesChannel,
                dayAlign: true,
                userId: this.m_nUserId
            };

            if (this._tryLockLoadingMutex()) {
                this._load(req);
            } else {
                // TODO: поставить подгрузку в очередь
            }
        },

        _makeRegistryLoadedDeferredCall: function(callback, aArgs, ctx) {
            ctx = ctx || this;

            if (!this.m_bRegistryLoaded) {
                this.m_aRegistryLoadDeffereds.push({callback: callback, args: aArgs, ctx: ctx});
            } else {
                callback.apply(ctx, aArgs);
            }
        },

        _moveActivity: function(mActivity, tOldDate) {
            this._removeActivityFromDay(mActivity, tOldDate);

            if (this._renderActivity(this._getOrCreateActivityViewByModel(mActivity, true))) {
                this._makeRegistryLoadedDeferredCall(this._renderAddDayWidgetsIfNeeded);
            }
        },

        _notifyOnRenderComplete: function() {
            if (!this.m_aRenderCompletionListeners) {
                return;
            }

            var fnListener;
            while (fnListener = this.m_aRenderCompletionListeners.pop()) {
                fnListener.apply(this);
            }
        },

        _onRegistryLoad: function() {
            var deferredCall;

            this.m_bRegistryLoaded = true;

            // обрабатываем все вызовы, дожидающиеся загрузки registry, например подгрузку новых активностей.
            for (var i = 0, l = this.m_aRegistryLoadDeffereds.length; i < l; i++) {
                deferredCall = this.m_aRegistryLoadDeffereds[i];
                deferredCall.callback.apply(deferredCall.ctx, deferredCall.args);
            }
        },

        _placeDayAtTheBeginning: function(vRenderedDay) {
            var vDay = this._getDayByIdx(1), prependDayWidget, $pivot;
            App.assert(vDay);

            prependDayWidget = vDay.getPrependDayWidget();
            $pivot = prependDayWidget ? prependDayWidget.$el : vDay.$el;

            vRenderedDay.$el.insertAfter($pivot);
        },

        _placeFirstDay: function(vRenderedDay) {
            this._getDaysHolder().append(vRenderedDay.el);
        },

        _placeMiddleOrLastDay: function(vRenderedDay, nPos) {
            var pivot = this._getDayByIdx(nPos - 1);
            vRenderedDay.$el.insertBefore(pivot.$el);
        },

        _removeActivity: function(mActivity) {
            this._removeActivityFromDay(mActivity);
            delete this.m_aActivities[mActivity.id];

            if (0 === this.m_oDays.dates.length) {
                this._addNewDay(this.m_tCurDate);
            }
        },

        _registerOnRenderCompleteListener: function(fnCallback) {
            this.m_aRenderCompletionListeners = this.m_aRenderCompletionListeners || [];
            this.m_aRenderCompletionListeners.push(fnCallback);
        },

        _removeActivityFromDay: function(mActivity, mixOldDateDate) {
            App.assert(
                !mixOldDateDate
                    || moment.isMoment(mixOldDateDate)
                    || ('string' === typeof mixOldDateDate && 10 === mixOldDateDate.length)
            );

            var sDate, vDay;
            if (mixOldDateDate) {
                sDate = moment.isMoment(mixOldDateDate)
                    ? mixOldDateDate.format('YYYY-MM-DD')
                    : mixOldDateDate;
            } else {
                sDate = mActivity.get('startDate').format('YYYY-MM-DD');
            }

            vDay = this._getOrCreateDay(sDate);
            App.assert(vDay.getActivitiesCount() > 0);

            vDay.removeActivity(mActivity);
            vDay.getActivitiesCount();
        },

        /**
         * @param vActivity
         * @returns {boolean} true, если новый день был отрисован, false, если активность была добавлена на существующий день
         * @private
         */
        _renderActivity: function(vActivity) {
            var bResult = false, vDay = this._getOrCreateDay(vActivity.model.get('startDate'));
            if (!vDay.isRendered()) {
                this._renderDay(vDay);
                bResult = true;
            }

            vDay.renderActivity(vActivity);
            return bResult;
        },

        _renderActivities: function(aActivities) {
            aActivities = aActivities.reverse();

            _.each(aActivities, function(mActivity) {
                this._renderActivity(this._getOrCreateActivityViewByModel(mActivity));
            }, this);
        },

        _renderAddDayWidgetsIfNeeded: function() {
            if (this.m_bCanEdit) {
                App.View.Mobile.VAddDayWidget.fillStream(this.m_oDays, this.m_mRegistry, this.m_bCanEdit);
            }
        },

        _renderDay: function(vDay) {
            var nTargetDayIdx = _.indexOf(this.m_oDays.dates, vDay.getDateAsString(), true),
                nDaysCount = this.m_oDays.dates.length;

            // may be 0 and length = 1               => empty stream, simple append
            // may be 0 and length > 1               => non-empty stream, should be prepended to the begin
            // may be (length - 1 and length > 1) OR may be in the middle  => ...

            vDay.render();
            if (0 === nTargetDayIdx && 1 === nDaysCount) {
                this._placeFirstDay(vDay);
            } else if (0 === nTargetDayIdx) {
                this._placeDayAtTheBeginning(vDay);
            } else {
                this._placeMiddleOrLastDay(vDay, nTargetDayIdx);
            }
        },

        _showPreloader: function(sPos) {
            this.m_aPreloaders = this.m_aPreloaders || {};
            this.m_aPreloaders[sPos] = $('<div class="preloader"></div>');
            this._getDaysHolder()[{top: 'prepend', bottom: 'append'}[sPos]](this.m_aPreloaders[sPos]);
        },

        _hidePreloader: function(sPos) {
            this.m_aPreloaders = this.m_aPreloaders || [];
            if (this.m_aPreloaders[sPos]) {
                this.m_aPreloaders[sPos].remove();
                delete this.m_aPreloaders[sPos];
            }
        },

        _tryLockLoadingMutex: function() {
            if (this.m_bLoadingLock) {
                return false;
            }

            var self = this;
            // TODO: после истечения таймайта запустить оповестить всех, успевших подписаться на окончание блокировки
            setTimeout(function() { self.m_bLoadingLock = false; }, 5000);

            return this.m_bLoadingLock = true;
        },

        _tryTriggerOnBottom: function() {
            if (!this.m_bOnBottomProcessing) {
                this.m_bOnBottomProcessing = true;

                var self = this;
                this._registerOnRenderCompleteListener(function() { self.m_bOnBottomProcessing = false; });

                this._makeRegistryLoadedDeferredCall(this._autoLoading);
            }
        },

        _tryTriggerOnTop: function() {
            if (!this.m_bOnTopProcessing) {
                this.m_bOnTopProcessing = true;

                var self = this;
                this._registerOnRenderCompleteListener(function() { self.m_bOnTopProcessing = false; });

                this._makeRegistryLoadedDeferredCall(this._autoLoading, [true]);
            }
        },

        _unlockLoadingMutex: function() {
            this.m_bLoadingLock = false;
        }
	});
})(App, Backbone, _);