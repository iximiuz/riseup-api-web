(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VFriendsListItem = App.View.BaseView.extend({
		className: 'friends-list-item',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/FriendsListItem'))(args); },

        events: {
            'click .friends-list-item__click-catcher': '_goToUserTimeline',
            'click .friends-list-item__friendship-btn': '_friendshipAction',
            'click .friends-list-item__revoke-btn': '_revokeFriendshipOffer'
        },

        initialize: function(attrs) {
            App.assert(attrs);
            App.assert(attrs.list);

            this.m_vList = attrs.list;
        },

        render: function() {
            this.$el.html(this.template({
                name: this.model.getPrintableName(),
                avatarUrl: this.model.getAvatar(),
                friendshipButton: this._getFriendshipButtonTextAndClass(this.model.get('friendshipStatus'))
            }));

            if (2 == this.model.get('friendshipStatus')) {
                this._getRevokeButton().show();
            } else {
                this._getRevokeButton().hide();
            }

            return this;
        },

        /**************************************************************************************************************/

        //TODO: переделать
        setFriendshipStatus: function (status) {
            status = parseInt(status);

            switch (status) {
                case 0: // исходящая заявка удалена или друг удален
                    this.m_vList._removeFromList(this);
                    break;
                case 3: // превращение из предложения дружбы в друга - перемещаем из входящих заявок в друзья
                    this.m_vList._moveFromIncomingOffersToFriends(this);
                    break;
            }

            this.model.set('friendshipStatus', status);
            this._updateFriendshipButton(status);
        },

        _friendshipAction: function () {
            var friendship = new App.Model.Friendship();
            friendship.action(this.m_friendshipStatus, this.model.id, this);
        },

        _getFriendshipButton: function () {
            return this.getElement('.friends-list-item__friendship-btn');
        },

        _getFriendshipButtonTextAndClass: function (status) {
            this.m_friendshipStatus = parseInt(status);

            switch (this.m_friendshipStatus) {
                case 1:
                    return {text: 'Отписаться', cssClass: 'btn-none'};
                case 2:
                    return {text: 'Принять', cssClass: 'btn btn-success'};
                case 3:
                    return {text: 'Не дружить', cssClass: 'btn-none'};
                default :
                    return {text: 'Дружить', cssClass: 'btn btn-primary'};
            }
        },

        _getRevokeButton: function () {
            return this.getElement('.friends-list-item__revoke-btn');
        },

        _goToUserTimeline: function() {
            App.navigate('riseupers/' + this.model.id + '/timeline', true);
        },

        _revokeFriendshipOffer: function() {
            var friendship = new App.Model.Friendship();
            friendship.declineIncommingOffer(this.model.id, this.setFriendshipStatus, this);
        },

        _updateFriendshipButton: function(status) {
            status = parseInt(status);

            var buttonStatusData = this._getFriendshipButtonTextAndClass(status),
                $button = this._getFriendshipButton();

            $button.removeClass('btn btn-primary btn-success revoke');
            $button.text(buttonStatusData.text);
            $button.addClass(buttonStatusData.cssClass);

            if (2 == status) {
                this._getRevokeButton().show();
            } else {
                this._getRevokeButton().hide();
            }
        }
	});
})(App, Backbone, _);