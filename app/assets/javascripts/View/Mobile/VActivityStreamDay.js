(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VActivityStreamDay = App.View.BaseView.extend({
		tagName: 'div',
		className: 'activity-stream-day',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/ActivityStreamDay'))(args); },

        events: {
            'click .activity-stream-day__banana-header': '_toggleDay',
            'click .activity-stream-day__side-line': '_collapseDay'
        },

        initialize: function(attrs) {
            attrs = attrs || {};
            this.m_sDate = attrs.date;
            this.m_momentDate = moment(attrs.date);
            this.m_bRendered = false;
            this.m_oActivities = {views: {}, cids: []};
            this.m_oStat = {completed: 0, failed: 0, total: 0};

            App.M.installTo(this);
        },

		render: function() {
			this.$el.html(this.template({
                date: this.m_momentDate.format('D'),
                monthAndYear: this.m_momentDate.format('MMM[.] YY'),
                dow: this.m_momentDate.format('dddd'),
                stat: this.m_oStat
            }));

            if (this.isToday()) {
                this.$el.addClass('today');
            }

            this.m_bRendered = true;
			return this;
		},

        isToday: function() {
            return 0 === this.distance(App.getStartOfToday(true));
        },

        renderActivity: function(vActivity) {
            if (!vActivity.model.isNew()) {
                // TODO: добавить проверку, что такая моделька уже отрисована и просто обновлять вьюху в таком случае.
            }

            var prevCid = vActivity.model.prevCid,
                nextCid = vActivity.model.nextCid;

            if (prevCid) {
                this._insertBefore(vActivity, prevCid);
            } else if (nextCid) {
                this._insertAfter(vActivity, nextCid);
            } else {
                App.assert(this.isEmpty() || !(nextCid && prevCid));
                this._appendActivity(vActivity);
            }

            this._updateDailyStatOnAdd(vActivity.model);
            this.listenTo(vActivity.model, 'change:state', this._updateDailyStatOnStateChanged);
            vActivity.afterInsertEl();

            if (1 === this.getActivitiesCount()) {
                this.$el.addClass('non-empty');
            }
        },

        isRendered: function() {
            return this.m_bRendered;
        },

        getDateAsString: function() {
            return this.m_sDate;
        },

        getDate: function() {
            return moment(this.m_momentDate);
        },

        getPrependDayWidget: function() {
            return this.m_vPrependWidget || null;
        },

        removeActivity: function(mActivity) {
            App.assert(this.m_oActivities.views[mActivity.cid]);

            if (this.m_oActivities.views[mActivity.cid]) {
                this.m_oActivities.views[mActivity.cid].remove();
                this.m_oActivities.cids.splice(_.indexOf(this.m_oActivities.cids, mActivity.cid), 1);
                delete this.m_oActivities.views[mActivity.cid];
                this.stopListening(mActivity);
                this._updateDailyStatOnRemoved(mActivity);
            }

            if (0 >= this.getActivitiesCount()) {
                this.$el.removeClass('non-empty');
            }
        },

        setPrependDayWidget: function(vWidget) {
            this.m_vPrependWidget = vWidget;
            return this;
        },

        isEditable: function() {
            return this.m_momentDate.unix() >= App.getStartOfToday()
        },

        isEmpty: function() {
            return (0 === this.getActivitiesCount());
        },

        getActivitiesCount: function() {
            return this.m_oActivities.cids.length;
        },

        distance: function(tDate) {
            return this.m_momentDate.clone().diff(tDate, 'days');
        },

        findNext: function(vActivity, onlySaved) {
            var nTargetIdx = _.indexOf(this.m_oActivities.cids, vActivity.model.cid),
                result;
            if (-1 === nTargetIdx || 0 === nTargetIdx) {
                return null;
            }

            result = this.m_oActivities.views[this.m_oActivities.cids[nTargetIdx - 1]];
            return (onlySaved && result.model.isNew()) ? this.findNext(result, onlySaved) : result;
        },

        findPrev: function(vActivity, onlySaved) {
            var nTargetIdx = _.indexOf(this.m_oActivities.cids, vActivity.model.cid),
                result;
            if (-1 === nTargetIdx || this.getActivitiesCount() - 1 === nTargetIdx) {
                return null;
            }

            result = this.m_oActivities.views[this.m_oActivities.cids[nTargetIdx + 1]];
            return (onlySaved && result.model.isNew()) ? this.findPrev(result, onlySaved) : result;
        },

        /**************************************************************************************************************/

        _appendActivity: function(vActivity) {
            this._getActivitiesHolder().append(vActivity.render().el);

            this.m_oActivities.views[vActivity.model.cid] = vActivity;
            this.m_oActivities.cids.push(vActivity.model.cid);
        },

        _collapseDay: function() {
            this._getActivitiesHolder().hide();
            this.m_bCollapsed = true;

            if (this.el.offsetTop < window.scrollY) {
                this.pub('activity-stream:scroll-to-day', this.getDate());
            }
        },

        _getActivitiesHolder: function() {
            return this.getElement('.activity-stream-day__acty-holder');
        },

        _getStatCounterCompleted: function() {
            return this.getElement('.activity-stream-day__stat-cntr.completed');
        },

        _getStatCounterFailed: function() {
            return this.getElement('.activity-stream-day__stat-cntr.failed');
        },

        _getStatCounterSeparatorAfterTotal: function() {
            return this.getElement('.activity-stream-day__stat-cntr-sep.completed');
        },

        _getStatCounterSeparatorAfterCompleted: function() {
            return this.getElement('.activity-stream-day__stat-cntr-sep.failed');
        },

        _getStatCounterTotal: function() {
            return this.getElement('.activity-stream-day__stat-cntr.total');
        },

        _getStatHolder: function() {
            return this.getElement('.activity-stream-day__stat');
        },

        _insertAfter: function(vActivity, nextCid) {
            var nTargetIdx = _.indexOf(this.m_oActivities.cids, nextCid),
                $pivot;
            if (-1 === nTargetIdx) {
                this._prependActivity(vActivity);
            } else {
                $pivot = this.m_oActivities.views[this.m_oActivities.cids[nTargetIdx]].$el;
                $pivot.after(vActivity.render().el);

                this.m_oActivities.views[vActivity.model.cid] = vActivity;
                this.m_oActivities.cids.splice(nTargetIdx + 1, 0, vActivity.model.cid);
            }
        },

        _insertBefore: function(vActivity, prevCid) {
            var nTargetIdx = _.indexOf(this.m_oActivities.cids, prevCid),
                $pivot;
            if (-1 === nTargetIdx) {
                this._appendActivity(vActivity);
            } else {
                $pivot = this.m_oActivities.views[this.m_oActivities.cids[nTargetIdx]].$el;
                $pivot.before(vActivity.render().el);

                this.m_oActivities.views[vActivity.model.cid] = vActivity;
                this.m_oActivities.cids.splice(nTargetIdx, 0, vActivity.model.cid);
            }
        },

        _prependActivity: function(vActivity) {
            this._getActivitiesHolder().prepend(vActivity.render().el);

            this.m_oActivities.views[vActivity.model.cid] = vActivity;
            this.m_oActivities.cids.unshift(vActivity.model.cid);
        },

        _toggleDay: function() {
            if (0 === this.m_oActivities.cids.length) {
                this.pub('day:need-empty-activity', this);

                if (this.m_bCollapsed) {
                    this._getActivitiesHolder().show();
                    this.m_bCollapsed = false;
                }

                // stop event propagation
                return false;
            }

            if (this.m_bCollapsed) {
                this._getActivitiesHolder().show();
                this.m_bCollapsed = false;
            } else {
                this._getActivitiesHolder().hide();
                this.m_bCollapsed = true;
            }

            // stop event propagation
            return false;
        },

        _updateDailyStatOnAdd: function(mActivity) {
            if (mActivity.isNewState()) {
                return;
            }

            this._getStatCounterTotal().text(++this.m_oStat.total);
            if (1 === this.m_oStat.total) { // добавление первой активности на день
                this._getStatHolder().show();
            }

            if (mActivity.isCompleted()) {
                this._getStatCounterCompleted().text(++this.m_oStat.completed);
                this._getStatCounterSeparatorAfterTotal().show();
            } else if (mActivity.isFailed()) {
                this._getStatCounterFailed().text(++this.m_oStat.failed);
                this._getStatCounterSeparatorAfterCompleted().show();
            }
        },

        _updateDailyStatOnRemoved: function(mActivity) {
            if (mActivity.isNewState()) {
                return;
            }

            this._getStatCounterTotal().text(--this.m_oStat.total);

            if (mActivity.isCompleted()) {
                this._getStatCounterCompleted().text(--this.m_oStat.completed);
                if (!this.m_oStat.completed) {
                    this._getStatCounterSeparatorAfterTotal().hide();
                }
            } else if (mActivity.isFailed()) {
                this._getStatCounterFailed().text(--this.m_oStat.failed);
                if (!this.m_oStat.failed) {
                    this._getStatCounterSeparatorAfterCompleted().hide();
                }
            }
        },

        _updateDailyStatOnStateChanged: function(mActivity) {
            var nPrevState = mActivity.previousAttributes().state;

            if (App.Model.Activity.STATE_NEW === nPrevState) {
                this._getStatCounterTotal().text(++this.m_oStat.total);
                if (1 === this.m_oStat.total) { // добавление первой активности на день
                    this._getStatHolder().show();
                }
            }

            if (App.Model.Activity.STATE_COMPLETE !== nPrevState && mActivity.isCompleted()) {
                this._getStatCounterCompleted().text(++this.m_oStat.completed);
                this._getStatCounterSeparatorAfterTotal().show();
            }

            // TODO: Разобраться с STATE_FAILED
        }
	});
})(App, Backbone, _);