(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VCommentsListItemView = App.View.BaseView.extend({
		className: 'comments-list-item',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/ActivityCommentsItem'))(args); },

        events: {
            'click .comments-list-item__del-comments': 'deleteComment'
        },

        render: function() {
            var self = this;
            self.$el.html(self.template({
                dt: self.model.create_dt,
                userName: (self.model.author.name || self.model.author.lastName) ? self.model.author.name + ' ' + self.model.author.lastName : self.model.nickname,
                msg: self.model.msg,
                userDeleteComment: (self.model.author.id == App.currentUser.id || self.options.activityAuthor)
            }));

            return this;
        },

        deleteComment: function(){
            var self = this;
            var commentsStorage = new App.Storage.CommentStorage();
            commentsStorage.setUrl('/activities/' + self.options.activityId + '/comments');
            commentsStorage.del({comment_id: self.model.id}, function (data) {
                if (data.status && data.status != 'error') {
                    self.$el.html('');
                }
            });
        }
	});
})(App, Backbone, _);