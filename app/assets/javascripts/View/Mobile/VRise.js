(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VRise = App.View.BaseView.extend({
		className: 'rise',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/Rise'))(args); },

        events: {
            'click .rise__complete-button': '_complete',
            'click .rise__actions_delete': '_remove',
            'click .rise__expand-button': '_toggleActionsBlock',
            'click .rise__pic': '_uploadPhoto',
            'change input[name=rise__file]': '_sendFile'
        },

        initialize: function(attrs) {
            this.m_bActionsExpanded = false;

            this.model.on('change', this.render, this);
        },

        render: function() {
            var endDate = this.model.get('endDate'),
                stat = this.model.get('stat'),
                sDescription = this.model.get('description');

            if (140 < sDescription.length) {
                sDescription = sDescription.substr(0, 142) + '...';
            }

            this.$el.html(this.template({
                description: sDescription,
                date: endDate.format('dddd[,] D MMMM YYYY'),
                remainDays: endDate.diff(new Date(), 'days'),
                canComplete: this.model.isActive(),
                completedActyCount: stat ? stat.completed_count : 0,
                failedActyCount: stat ? stat.failed_count : 0,
                totalActyCount: stat ? stat.total_count : 0,
                isOwnedToCurUser: this.model.get('userId') == App.currentUser.id,
                img: this.model.get('img')
            }));

            if (this.model.isCompleted()) {
                this.$el.addClass('completed')
            } else if (this.model.isOverdued()) {
                this.$el.addClass('overdued')
            }

            return this;
        },

        /**************************************************************************************************************/

        _complete: function() {
            this.model.complete();
            this.model.save();
        },

        _remove: function() {
            var self = this;
            this.model.destroy({success: function () {
                self.remove();
            }});
        },

        _toggleActionsBlock: function() {
            if (this.m_bActionsExpanded) {
                this.$el.removeClass('expanded');
            } else {
                this.$el.addClass('expanded');
            }

            this.m_bActionsExpanded = !this.m_bActionsExpanded;
        },

        _uploadPhoto: function () {
            var _el = this.$el;
            var fileInput = _el.find('input[name=rise__file]');
            // Имитируем нажатие
            fileInput.trigger('click');
        },

        _sendFile: function(){
            var _el = this.$el, self = this;
            var fileInput = _el.find('input[name=rise__file]')[0];
            var fileStorage = new App.Storage.FileStorage();
            if (fileInput.files.length > 0) {
                var file = fileInput.files[0];
                fileStorage.save(file, function(data) {
                    data = JSON.parse(data.response);
                    if (data.error) {
                        console.log('ERROR!');
                        return;
                    }
                    self.model.set('img', data.path.preview);
                    _el.find('.rise__pic img').attr('src', data.path.preview);
                    self.model.save();
                });
            }
        }
	});
})(App, Backbone, _);