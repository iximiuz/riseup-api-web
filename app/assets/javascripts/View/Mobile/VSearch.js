(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.VSearch = App.View.BaseView.extend({
        tagName: 'div',
        className: 'search',
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/Search'))(args);
        },

        events: {
            'click .search__form-submit-button': '_sendQuery',
            'keydown .search__form-text-field': '_sendByEnter'
        },

        initialize: function (attrs) {
        },

        _sendQuery: function (e) {
            var self = this;
            var filter = self._getFilter(e);
            self._preloaderShow();

            var storage = new App.Storage.SearchStorage(filter);
            storage.fetch({
                'q': self._getQueryText()
            }, function (data) {
                self._preloaderHide();
                self._renderResult(data);
            });
        },

        _sendByEnter: function(e) {
            if (e.keyCode == 13) {
                $('.search__form-submit-button[filter=all]').trigger('click');
            }
        },

        _renderResult: function (data) {
            var self = this;
            data = JSON.parse(data.response);
            if (data) {
                self._getSearchResultBox().empty();
                $(data).each(function(){
                    var item = $(this)[0];
                    var resultTemplate = _.template(TplHelper.getTplHtml('mobile/SearchItem'))(item);
                    self._getSearchResultBox().append(resultTemplate);
                });
            } else {
                self._getSearchResultBox().empty().html('<ul><li>Ничего не найдено!</li></ul>');
            }
        },

        _getFilter: function (event) {
            return $(event.target).attr('filter');
        },

        _getPreloader: function () {
            return $('.search__form-preloader');
        },

        _preloaderShow: function () {
            this._getPreloader().show();
            $('.search__form-submit-controls').attr('disabled', 'disabled');
        },

        _preloaderHide: function () {
            this._getPreloader().hide();
            $('.search__form-submit-controls').removeAttr('disabled');
        },

        _getQueryText: function(){
            return $('.search__form-text-field').val();
        },

        _getSearchResultBox: function(){
            return $('.search__result');
        }
    });
})(App, Backbone, _);