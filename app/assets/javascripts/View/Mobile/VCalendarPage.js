(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.VCalendarPage = App.View.BaseView.extend({
        tagName: 'div',
        className: 'calendar-page',
        template: function () {
            return _.template(TplHelper.getTplHtml('mobile/CalendarPage'));
        },

        events: {

        },

        initialize: function (attrs) {
            attrs = attrs || {};
            App.M.installTo(this);
        },

        render: function () {
            this.$el.html(this.template());

            this.m_vDatePicker = new App.View.Common.VCellularDatePicker({startDate: moment(new Date())});

            this._getDpHolder().html(this.m_vDatePicker.render().el);
            this.m_vDatePicker.afterInsertEl();

            return this;
        },

        /**************************************************************************************************************/

        _getDpHolder: function() {
            return this.getElement('.calendar-page__dp-holder');
        }
    });
})(App, Backbone, _);