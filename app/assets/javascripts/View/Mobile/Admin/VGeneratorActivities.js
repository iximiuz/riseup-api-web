(function(App, Backbone, _) {
    'use strict';

    App.View.Mobile.Admin.VGeneratorActivities = App.View.BaseView.extend({
        tagName: 'div',
        className: 'generator-activities',
        template: function(args) { return _.template(TplHelper.getTplHtml('mobile/admin/GeneratorActivities'))(args); },

        events: {
            'click .generator-activities__button-generate': '_generate',
            'click .generator-activities__button-reset': '_reset'
        },

        initialize: function(attrs) {
            this.m_nInGeneration = 0;
            this.m_nDefaultDailyActivitiesCount = 15;
            this.m_aDays = {};
        },

        render: function() {
            this.$el.html(this.template({
                userId: App.currentUser.id,
                activitiesCount: this.m_nDefaultDailyActivitiesCount
            }));

            this.m_vDatePicker = new App.View.Common.VCellularDatePicker({startDate: moment(new Date())});
            this._getDatePickerHolder().append(this.m_vDatePicker.render().$el);

            this.m_vDatePicker.addOnSelectListener('generator-activities-day-selected', this._onDaySelected, this);
            this.m_vDatePicker.addOnDeselectListener('generator-activities-day-deselected', this._onDayDeselected, this);

            return this;
        },

        afterInsertEl: function() {
            this.m_vDatePicker.scrollToTop(moment(new Date));
        },

        /**************************************************************************************************************/

        _generate: function(e) {
            this._saveSelectedDayConfIfNeeded();

            var aDates = this.m_vDatePicker.getSelectedDates(),
                sDate,
                nUserId = parseInt(this._getUserIdInput().val()),
                nDailyActivitiesCount,
                mActivity,
                self = this,
                button = e.target,
                callback = function() {
                    self.m_nInGeneration--;
                    if (0 >= self.m_nInGeneration) {
                        self.m_nInGeneration = 0;
                        button.disabled = false;
                    }
                };

            for (var i = 0, l = aDates.length; i < l; i++) {
                sDate = aDates[i];

                nDailyActivitiesCount = this.m_aDays[sDate]
                    ? parseInt(this.m_aDays[sDate].activitiesCount)
                    : this.m_nDefaultDailyActivitiesCount;

                for (var j = 0; j < nDailyActivitiesCount; j++) {
                    mActivity = new App.Model.Activity({
                        state: 0,
                        userId: nUserId,
                        description: '~auto-generated: ' + sDate + '::' + j + '~',
                        startDate: sDate,
                        cdate: moment(new Date())
                    });

                    this.m_nInGeneration++;

                    mActivity.save(null, {success: callback, error: callback});
                }
            }

            if (this.m_nInGeneration) {
                button.disabled = true;
            }
        },

        _getActivitiesCountInput: function() {
            return this.getElement('.generator-activities__daily-activities-count');
        },

        _getDatePickerHolder: function() {
            return this.getElement('.generator-activities__date-picker-holder');
        },

        _getUserIdInput: function() {
            return this.getElement('.generator-activities__user-id');
        },

        _onDayDeselected: function(sDate) {
            delete this.m_aDays[sDate];
            delete this.m_sSelectedDay;
        },

        _onDaySelected: function(sDate) {
            this._saveSelectedDayConfIfNeeded();

            this.m_sSelectedDay = sDate;
            if (this.m_aDays[sDate]) {
                // если для дня были сохранены настройки - подгружаем их
                this._getActivitiesCountInput().val(this.m_aDays[sDate].activitiesCount);
            } else {
                this._getActivitiesCountInput().val(this.m_nDefaultDailyActivitiesCount);
            }
        },

        _saveSelectedDayConfIfNeeded: function() {
            if (this.m_sSelectedDay) {
                // если до этого был выбран другой день - сохраняем его кол-во активностей для генерации
                this.m_aDays[this.m_sSelectedDay] = this.m_aDays[this.m_sSelectedDay] || {};
                this.m_aDays[this.m_sSelectedDay].activitiesCount = parseInt(this._getActivitiesCountInput().val());
            }
        },

        _reset: function() {
            this.m_vDatePicker.deselectAll();
        }
    });
})(App, Backbone, _);
