(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VRisesFilter = App.View.BaseView.extend({
        tagName: 'ul',
		className: 'rises-filter',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/RisesFilter'))(args); },

        events: {
            'click [data-filter]': '_filter',
            'click .rises-filter__expand-right': '_expandRight',
            'click .rises-filter__expand-left': '_expandLeft'
        },

        initialize: function(attrs) {
            attrs = attrs || {};
            this.m_sMode = attrs.mode;

            App.M.installTo(this);
        },

        render: function() {
			this.$el.html(this.template());

            if (this.m_sMode) {
                this._selectTab(this.getElement('[data-filter="' + this.m_sMode + '"]'));
            }

			return this;
		},

        /**************************************************************************************************************/

        _expandLeft: function() {
            this.$el.removeClass('right-expanded');
        },

        _expandRight: function() {
            this.$el.addClass('right-expanded');
        },

        _filter: function(e) {
            this._selectTab($(e.currentTarget));

            var sFilterVal = this.m_$curTab.data('filter');
            this.pub('rises-filter-list', sFilterVal);

            App.navigate('rises/' + sFilterVal); // fix it
        },

        _selectTab: function($tab) {
            if (this.m_$curTab) {
                this.m_$curTab.removeClass('selected');
            }

            this.m_$curTab = $tab;
            this.m_$curTab.addClass('selected');
        }
	});
})(App, Backbone, _);