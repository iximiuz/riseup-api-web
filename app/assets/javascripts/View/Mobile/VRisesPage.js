(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VRisesPage = App.View.BaseView.extend({
		className: 'rises-page',
		template: function() { return _.template(TplHelper.getTplHtml('mobile/RisesPage')); },

        initialize: function(attrs) {
            attrs = attrs || {};

            this.m_sGoalId = attrs.goalId;
            this.m_sFilter = attrs.filter;
        },

        render: function() {
			this.$el.html(this.template());

            var vRisesFilter = new App.View.Mobile.VRisesFilter(this.m_sGoalId ? {} : {mode: this.m_sFilter}),
                vRisesAddWidget = new App.View.Mobile.VRisesAddWidget(),
                vRisesList = new App.View.Mobile.VRisesList({goalId: this.m_sGoalId, filter: this.m_sFilter});

            this._getFilterHolder().html(vRisesFilter.render().el);
            this._getAddWidgetHolder().html(vRisesAddWidget.render().el);
            this._getRisesListHolder().html(vRisesList.render().el);

			return this;
		},

        /**************************************************************************************************************/

        _getFilterHolder: function() {
            return this.getElement('.rises-page__filter-holder');
        },

        _getAddWidgetHolder: function() {
            return this.getElement('.rises-page__add-widget-holder');
        },

        _getRisesListHolder: function() {
            return this.getElement('.rises-page__list-holder');
        }
	});
})(App, Backbone, _);