(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.RegFormView = App.View.BaseView.extend({
		tagName: 'div',
		className: 'reg-form',
		template: function() { return _.template(TplHelper.getTplHtml('mobile/RegForm')); },

		events: {
			'click .registerButton' : 'register',
            'click .reg-form__toggle-registration': '_toggleLogin'
		},

		initialize: function(attributes) {
			this.m_oAuthService = attributes.authenticity;

            App.M.installTo(this);

            var self = this;
            this.sub('auth:fail', function(response) {
                self._getErrorMsgHolder().html(response);
            });
		},

		render: function() {
			this.$el.html(this.template());
			return this;
		},

		register: function() {
            this._getErrorMsgHolder().text('');

            this.m_oAuthService.register(
                this.getLoginHolder().val(),
                this.getPasswordHolder().val()
            );
		},

		getLoginHolder: function() {
			if (!this.m_$login) {
				this.m_$login = this.$('#login-input');
			}

			return this.m_$login;
		},

		getPasswordHolder: function() {
			if (!this.m_$password) {
				this.m_$password = this.$('#pwd-input');
			}

			return this.m_$password;
		},

        _toggleLogin: function() {
            this._getErrorMsgHolder().text('');
            App.navigate('login', true);
        },

        _getErrorMsgHolder: function() {
            return this.getElement('.reg-form__error');
        }
	});
})(App, Backbone, _);