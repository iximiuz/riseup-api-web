(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VRisesList = App.View.BaseView.extend({
		className: 'rises-list',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/RisesList'))(args); },

        initialize: function(attrs) {
            attrs = attrs || {};
            this.m_sGoalId = attrs.goalId;
            this.m_sFilter = attrs.filter;

            App.M.installTo(this);

            this.m_sCh = 'rises-list-ch';
            this.sub(this.m_sCh, this._renderRises);

            this.sub('rise-created', this.render);
            this.sub('rises-filter-list', this.render);
        },

        render: function(sFilter) {
            this.$el.html(this.template());

            if (!sFilter && this.m_sGoalId) {
                var self = this, mGoal = new App.Model.Goal({id: this.m_sGoalId}, true);
                mGoal.fetch({success: function(model) {
                    self._renderRises([model]);
                }});
            } else {
                this.pub('models:find', {
                    model: 'Goal',
                    channel: this.m_sCh,
                    criteria: {
                        sort: 'asc',
                        limit: 1000,
                        offset: 0,
                        filter: sFilter || this.m_sFilter
                    }
                });
            }

            return this;
        },

        /**************************************************************************************************************/

        _renderRises: function(aGoals) {
            this.$el.html('');

            var self = this, vRise;
            _.each(aGoals, function(mGoal) {
                vRise = new App.View.Mobile.VRise({model: mGoal});
                self.$el.append(vRise.render().el);
            });
        }
	});
})(App, Backbone, _);