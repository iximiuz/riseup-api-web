(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VActivityStreamPage = App.View.BaseView.extend({
		className: 'activity-stream-page',

		template: function() { return _.template(TplHelper.getTplHtml('mobile/ActivityStreamPage')); },

        events: {
            'click .sp-on .activity-stream-page__stream-holder__touch-helper': '_cancelSidePaneShowing'
        },

        initialize: function(attrs) {
            App.assert(attrs);
            App.assert(attrs.mode != 'rise-binding' || (attrs.mode == 'rise-binding' && attrs.bindActivityId));
            attrs = attrs || {};

            this.m_oAttrs = attrs;

            App.M.installTo(this);
            this.sub('side-bar-click:' + this.cid, this._toggleSidePanel);
            this.sub('date-picker:show', this._selectDate); // todo: пененсти на страницу каленадря
        },

        render: function() {
            this.$el.html(this.template());

            this.m_vStream = new App.View.Mobile.VActivityStream(this.m_oAttrs);
            this._getStreamHolder().prepend(this.m_vStream.render().el);

            var nHeight = App.getWindow().height();
            this.m_vStream.$el.css('min-height', nHeight);

            return this;
		},

        /**************************************************************************************************************/

        _cancelSidePaneShowing: function() {
            this._toggleSidePanel();

            // stop event propagation
            return false;
        },

        _getStreamHolder: function() {
            return this.getElement('.activity-stream-page__stream-holder');
        },

        _getSidePanelHolder: function() {
            return this.getElement('.activity-stream-page__side-panel-holder');
        },

        _restoreSidePanelHolderState: function() {
            this.$el.removeClass('sp-on');
            this._getStreamHolder().find('.activity-stream-page__stream-holder__touch-helper').remove();

            window.scrollTo(0, this.m_nScrollTopToRestore);
            this.m_vStream.startScrollDetection();

            this.pub('layout:restore-side-bar');
        },

        _renderSideMenu: function() {
            var bIsSelfTimeline = App.currentUser.id == this.m_oAttrs.userId;

            this.m_vSideMenu = new App.View.Mobile.VSideMenu({
                title: bIsSelfTimeline ? 'Активности' : 'Активности пользователя ' + this.m_oAttrs.user.getPrintableName(),
                showActivitiesItem: !bIsSelfTimeline
            });
            this._getSidePanelHolder().html(this.m_vSideMenu.render().el);
            this.m_vSideMenu.afterInsertEl();

            this.sub('side-menu:close', this._toggleSidePanel);
        },

        _selectDate: function(attrs) {
            this.m_sNewDayChannel = attrs.channel;

            // todo: go to calendar page

            return false;
        },

        _showSidePanel: function() {
            this.m_vStream.stopScrollDetection();
            this.m_nScrollTopToRestore = window.scrollY;

            this.pub('layout:hide-side-bar');

            this.$el.addClass('sp-on');
            this._getStreamHolder().append('<div class="activity-stream-page__stream-holder__touch-helper"></div>');
            this._getSidePanelHolder().css('min-height', App.getWindow().height());
            this._getSidePanelHolder().css('top', this.m_nScrollTopToRestore);
            this._getSidePanelHolder().show();

            this.$el.scrollTop(this.m_nScrollTopToRestore);
        },

        _toggleSidePanel: function() {
            if (this.m_bSideMenuShowed) {
                this._restoreSidePanelHolderState();
                this.m_bSideMenuShowed = false;
            } else {
                this._showSidePanel();
                this.m_bSideMenuShowed = true;

                if (!this.m_vSideMenu) {
                    this._renderSideMenu();
                }
            }            
        }
	});
})(App, Backbone, _);