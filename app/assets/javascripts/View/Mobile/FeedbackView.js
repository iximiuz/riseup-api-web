(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.FeedbackView = App.View.BaseView.extend({
        tagName: 'div',
        className: 'feedback',
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/FeedbackForm'))(args);
        },

        events: {
            'click #submit_feedback_form': '_save',
            'click #clear_feedback_form': '_clear',
            'click #message_feedback_form_plus': '_modifyFormPlus',
            'click #message_feedback_form_minus': '_modifyFormMinus'
        },

        initialize: function (attrs) {
//            this.model.on('change', this.render, this);
        },

        _save: function () {
            var text = this._getTextArea().val();
            var file = null;
            var token = new App.Authenticity().getAuthToken();
            var self = this;
            if (!text) {
                return;
            }
            var fData = new FormData();
            fData.append('text', text);

            if (document.getElementById("attach").files) {
                file = document.getElementById("attach").files[0];
                if (file) {
                    fData.append('attach', file);
                }
            }

            App.Server.defaultServer.POST("/feedback?token=" + token, {
                data: fData,
                processData: false,
                contentType: false,
                complete: function (data) {
                    if (!data.code) {
                        data = JSON.parse(data.response);
                    }

                    if (200 == data.code) {
                        alert("Сообщение отправлено!");
                        self._clear();
                    } else {
                        alert(data.error);
                    }
                },
                dataType: 'json'
            });
        },

        _clear: function () {
            this._getTextArea().val('');
            this._getFileInput().val('');
        },

        _getTextArea: function () {
            return this.getElement("#message_feedback_form");
        },

        _getFileInput: function () {
            return this.getElement("#attach");
        },

        _modifyFormPlus: function () {
            var curHeight = this._getTextArea().css('height');
            this._getTextArea().css('height', (parseInt(curHeight) + 100) + 'px');

        },

        _modifyFormMinus: function () {
            var curHeight = parseInt(this._getTextArea().css('height'));
            if (curHeight > 100) {
                this._getTextArea().css('height', (curHeight - 100) + 'px');
            }

        }
    });
})(App, Backbone, _);