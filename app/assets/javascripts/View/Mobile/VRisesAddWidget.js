(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VRisesAddWidget = App.View.BaseView.extend({
		className: 'rises-add-widget',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/RisesAddWidget'))(args); },

        events: {
            'click .rises-add-widget__show-button': '_toggleWidget',
            'click .rises-add-widget__add-button': '_addRise'
        },

        initialize: function(attrs) {
            this.m_bShowed = false;

            App.M.installTo(this);
        },

        render: function() {
            this.$el.html(this.template());

            this._renderDatePicker();

            return this;
		},

        /**************************************************************************************************************/

        _addRise: function() {
            var sDescription = this._getTextArea().val(),
                sEndDate = this.m_vDatePicker.getSelectedDates().pop();

            if (!sDescription) {
                this._getErrorMsgHolder().text('Необходимо заполнить описание цели');
                return;
            }

            if (!sEndDate) {
                this._getErrorMsgHolder().text('Необходимо установить срок достижения цели');
                return;
            }

            var oGoalAttrs = {
                userId: App.currentUser.id,
                endDate: sEndDate,
                description: sDescription,
                createdAt: new Date()
            }, mGoal, self = this;

            mGoal = new App.Model.Goal(oGoalAttrs);
            mGoal.save(null, {
                success: function() {
                    self.pub('rise-created');
                    self._collapseWidget();
                },
                error: function(args) {
                    console.log(arguments, args);
                }
            });
        },

        _collapseWidget: function() {
            this._getErrorMsgHolder().text('');
            this._getTextArea().val('');
            this.m_vDatePicker.reset();
            this._getShowButton()[0].click();
        },

        _getErrorMsgHolder: function() {
            return this.getElement('.rises-add-widget__error-msg');
        },

        _getDatePickerHolder: function() {
            return this.getElement('.rise-add-widget__date-picker-holder');
        },

        _getShowButton: function() {
            return this.getElement('.rises-add-widget__show-button');
        },

        _getTextArea: function() {
            return this.getElement('.rises-add-widget__rise-text');
        },

        _renderDatePicker: function() {
            var tToday = App.now();

            this.m_vDatePicker = new App.View.Common.VCellularDatePicker({
                startDate: tToday,
                mode: 'single',
                from: tToday
            });
            this._getDatePickerHolder().html(this.m_vDatePicker.render().el);
        },

        _toggleWidget: function() {
            this._getShowButton().text(this.m_bShowed ? '+ Добавить цель' : 'Отменить');
            this.m_bShowed = !this.m_bShowed;
        }
    });
})(App, Backbone, _);