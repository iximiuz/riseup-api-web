(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.AuthFormView = App.View.BaseView.extend({
        tagName: 'div',
        className: 'auth-form',
        template: function () {
            return _.template(TplHelper.getTplHtml('mobile/AuthForm'));
        },

        events: {
            'click .loginButton': 'login',
            'click .auth-form__toggle-registration': '_toggleRegistration',
            'keypress #login-input,#pwd-input': '_isEnterThatSend'
        },

        initialize: function (attributes) {
            this.m_oAuthService = attributes.authenticity;

            App.M.installTo(this);

            var self = this;
            this.sub('auth:fail', function (response) {
                self._getErrorMsgHolder().html(response);
            });
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        },

        login: function () {
            this._getErrorMsgHolder().text('');

            this.m_oAuthService.login(
                this.getLoginHolder().val(),
                this.getPasswordHolder().val()
            );
        },

        getLoginHolder: function () {
            if (!this.m_$login) {
                this.m_$login = this.$('#login-input');
            }

            return this.m_$login;
        },

        getPasswordHolder: function () {
            if (!this.m_$password) {
                this.m_$password = this.$('#pwd-input');
            }

            return this.m_$password;
        },

        _toggleRegistration: function () {
            this._getErrorMsgHolder().text('');
            App.navigate('register', true);
        },

        _getErrorMsgHolder: function () {
            return this.getElement('.auth-form__error');
        },

        _isEnterThatSend: function (event) {
            if (13 == event.keyCode) {
                this.login();
            }
        }
    });
})(App, Backbone, _);