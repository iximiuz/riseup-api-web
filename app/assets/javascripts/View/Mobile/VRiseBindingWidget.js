(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VRiseBindingWidget = App.View.BaseView.extend({
		className: 'rise-binding-widget',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/RiseBindingWidget'))(args); },

        events: {
            'click [data-action="bind"]': 'bindRise',
            'click [data-action="cancel"]': 'cancelBinding',
            'click [data-action="addRise"]': 'goToRiseAdding'
        },

        initialize: function(attrs) {
            attrs = attrs || {};

            if (!attrs.activityId) {
                throw new Error('No one activity to bind!');
            }

            this.m_sActivityId = attrs.activityId;

            App.M.installTo(this);
            this.sub('window:resize', this.resizeMe);
        },

        render: function() {
			this.$el.html(this.template());
            this.loadRises();

            this.disableBind();

			return this;
		},

        appendRiseToList: function(mGoal) {
            var vRiseListItem = new App.View.Mobile.VRiseBindingWidgetListItem({model: mGoal, widget: this});
            this._getList().append(vRiseListItem.render().el);
        },

        afterInsertEl: function() {
            this.resizeMe();
        },

        disableBind: function() {
            this._getBindButton().addClass('disabled');
        },

        enableBind: function() {
            this._getBindButton().removeClass('disabled');
        },

        bindRise: function() {
            if (this.m_vSelectedItem) {
                this.pub('rise-binding-widget-bind', {activityId: this.m_sActivityId, goal: this.m_vSelectedItem.model});
                App.navigate('timeline');
                this.remove();
            }
        },

        cancelBinding: function() {
            App.navigate('timeline');
            this.remove();
        },

        goToRiseAdding: function() {
            App.navigate('rises', true);
            this.remove();
        },

        hideListScroll: function() {
            var $list = this._getList(),
                listWidth = $list.width(),
                listWithoutScrollWidth = $list[0].scrollWidth,
                wrapperWidth = this._getListWrapper().width();

            $list.width(listWidth + wrapperWidth - listWithoutScrollWidth);
        },

        listItemSelected: function(vItem) {
            if (this.m_vSelectedItem) {
                this.m_vSelectedItem.deselectMe();
            }

            this.m_vSelectedItem = vItem;
            this.enableBind();
        },

        loadRises: function() {
            if (!this.m_sCh) {
                this.m_sCh = 'rise-binding-list-ch';
                this.sub(this.m_sCh, this.renderRises)
            }

            this.pub('models:find', {
                model: 'Goal',
                channel: this.m_sCh,
                criteria: {
                    sort: 'asc',
                    limit: 1000,
                    offset: 0,
                    filter: 'active'
                }
            });
        },

        renderRises: function(aGoals) {
            var self = this;

            if (_.isEmpty(aGoals)) {
                this._getHint().show();
            } else {
                _.each(aGoals, function(mGoal) {
                    self.appendRiseToList(mGoal);
                });
            }
        },

        resizeMe: function() {
            this.hideListScroll();
            this._getShadow().height(this.$el.parent().height());
            // hack
            this.getElement('.rise-binding-widget_content').css({top: (window.scrollY + $(window).height() * 0.1)});
        },

        _getBindButton: function() {
            return this.getElement('[data-action="bind"]');
        },

        _getHint: function() {
            return this.getElement('.rise-binding-widget_no-rises-hint');
        },

        _getList: function() {
            return this.getElement('.rise-binding-widget_rise-list');
        },

        _getListWrapper: function() {
            return this.getElement('.rise-binding-widget_rise-list-wrapper');
        },

        _getShadow: function() {
            return this.getElement('.rise-binding-widget_oblivion');
        }
	});
})(App, Backbone, _);