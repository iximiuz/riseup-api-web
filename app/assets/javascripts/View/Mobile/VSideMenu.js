(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VSideMenu = App.View.BaseView.extend({
        tagName: 'ul',
		className: 'side-menu',

		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/SideMenu'))(args); },

        events: {
            'click .side-menu__activities': '_goToTimeLine',
            'click .side-menu__friends': '_goToFriends',
            'click .side-menu__users': '_goToUsers',
            'click .side-menu__profile': '_goToProfile',
            'click .side-menu__live-hub': '_goToLiveHub',
            'click .side-menu__calendar': '_goToCalendar',
            'click .side-menu__rises': '_goToRises',
            'click .side-menu__today': '_goToToday',
            'click .side-menu__close': '_close',
            'click .side-menu__logout': '_logout'
        },

        initialize: function(attrs) {
            attrs = attrs || {};

            //attrs.title;
            //attrs.canGoBack;
            //attrs.items;
            //attrs.canClose;

            this.m_oAttrs = attrs;

            App.M.installTo(this);
        },

        render: function() {
            this.$el.html(this.template({
                title: this.m_oAttrs.title,
                showActivitiesItem: this.m_oAttrs.showActivitiesItem
            }));

            return this;
		},

        /**************************************************************************************************************/

        _close: function() {
            this.pub('side-menu:close');
        },

        _goToCalendar: function() {
            this._close();
            App.navigate('calendar', true, true);
        },

        _goToFriends: function() {
            this._close();
            App.navigate('friends', true, true);
        },

        _goToLiveHub: function() {
            this._close();
            App.navigate('live', true, true);
        },

        _goToProfile: function() {
            this._close();
            App.navigate('profile', true, true);
        },

        _goToRises: function() {
            this._close();
            App.navigate('rises', true, true);
        },

        _goToTimeLine: function() {
            this._close();
            App.navigate('timeline', true, true);
        },

        _goToToday: function() {
            this._close();
            this.pub('activity-stream:scroll-to-day', moment());
        },

        _goToUsers: function() {
            this._close();
            App.navigate('riseupers', true, true);
        },

        _logout: function() {
            (new App.Authenticity(App.Server.defaultServer, '/auth_tokens')).logout();
        }
	});
})(App, Backbone, _);