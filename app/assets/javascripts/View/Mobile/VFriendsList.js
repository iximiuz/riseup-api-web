(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VFriendsList = App.View.BaseView.extend({
		tagName: 'div',
		className: 'friends-list',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/FriendsList'))(args); },

        events: {
            'click [data-section="friends"]': '_showFriendsList',
            'click [data-section="inbox-offers"]': '_showInboxOffersList',
            'click [data-section="outbox-offers"]': '_showOutboxOffersList'
        },

        initialize: function(attrs) {
            App.assert(attrs);
            App.assert(attrs.user);

            this.m_mUser = attrs.user;
            this.m_sMode = attrs.mode || 'friends';
            this.m_sSubMode = attrs.subMode || 'inbox';

            App.assert({friends: 1, offers: 1}[this.m_sMode]);

            this.m_aModes = {friends: 'Friend', offers: 'Friendship'};

            App.M.installTo(this);
            this.m_sCh = 'friends-list-ch';
            this.sub(this.m_sCh, this._doRender);
            this.sub('window:resize', this._fitToScreen);
        },

        render: function() {
            this.$el.html(this.template());

            if ('friends' === this.m_sMode) {
                this._showFriendsList();
                this._makeDataRequest('offers');
            } else {
                this._showOffersList();
                this._makeDataRequest('friends');
            }

            return this;
        },

        afterInsertEl: function() {
            this._fitToScreen();
        },

        /**************************************************************************************************************/

        _fitToScreen: function() {
            this._getListsHolder().height(this.$el.height() - this._getSwitcherHolder().height());
        },

        _getFriendsCounter: function() {
            return this.getElement('.friends-list__friends-cntr');
        },

        _getFriendsHolder: function() {
            return this.getElement('.friends-list__friends');
        },

        _getInboxOffersCounter: function() {
            return this.getElement('.friends-list__inbox-offers-cntr');
        },

        _getInboxRequestsHolder: function() {
            return this.getElement('.friends-list__inbox-requests');
        },

        _getListsHolder: function() {
            return this.getElement('.friends-list__lists-holder');
        },

        _getOutboxOffersCounter: function() {
            return this.getElement('.friends-list__outbox-offers-cntr');
        },

        _getOutboxRequestsHolder: function() {
            return this.getElement('.friends-list__outbox-requests');
        },

        _getShowFriendsButton: function() {
            return this.getElement('[data-section="friends"]');
        },

        _getShowInboxOffersButton: function() {
            return this.getElement('[data-section="inbox-offers"]');
        },

        _getShowOutboxOffersButton: function() {
            return this.getElement('[data-section="outbox-offers"]');
        },

        _getSwitcherHolder: function() {
            return this.getElement('.friends-list__section-switcher');
        },

        _doRender: function(response) {
            if (response.inbox) {
                this.m_oOffers = this.m_oOffers || {};
                this.m_oOffers.inbox = response.inbox;

                // хак для проставления статуса дружбы на фронтэнде. По-хорошему эти данные должны приходить с бекенда
                _.each(response.inbox, function(mUser) {
                    mUser.set('friendshipStatus', 2);
                });

                this._renderSection(response.inbox, this._getInboxRequestsHolder());
                this._setInboxOffersCount(response.inbox.length);
            }

            if (response.outbox) {
                this.m_oOffers = this.m_oOffers || {};
                this.m_oOffers.oubbox = response.oubbox;

                // хак для проставления статуса дружбы на фронтэнде. По-хорошему эти данные должны приходить с бекенда
                _.each(response.outbox, function(mUser) {
                    mUser.set('friendshipStatus', 1);
                });

                this._renderSection(response.outbox, this._getOutboxRequestsHolder());
                this._setOutboxOffersCount(response.outbox.length);
            }

            if (_.isArray(response)) {
                this.m_aFriends = response;

                // хак для проставления статуса дружбы на фронтэнде. По-хорошему эти данные должны приходить с бекенда
                _.each(response, function(mUser) {
                    mUser.set('friendshipStatus', 3);
                });

                this._renderSection(response, this._getFriendsHolder());
                this._setFriendsCount(response.length);
            }
        },

        _makeDataRequest: function(sMode) {
            this.pub('models:find', {
                model: this.m_aModes[sMode],
                channel: this.m_sCh,
                criteria: {}
            });
        },

        _moveFromIncomingOffersToFriends: function(vListItem) {
            this._getFriendsHolder()[0].appendChild(vListItem.el);
            this.m_aFriends = this.m_aFriends || [];
            this.m_aFriends.push(vListItem.model);

            this._setFriendsCount(this.m_aFriends.length);

            this.m_oOffers.inbox = _.filter(this.m_oOffers.inbox, function(mUser) { return mUser.id != vListItem.model.id; });
            this._setInboxOffersCount(this.m_oOffers.inbox.length);
        },

        _removeFromList: function(vListItem) {
            vListItem.remove();

            if (vListItem.model.isFriend()) {
                this.m_aFriends = _.filter(this.m_aFriends, function(mUser) { return mUser.id != vListItem.model.id});
                this._setFriendsCount(this.m_aFriends.length);
            } else if (vListItem.model.wantToBeFriend()) {
                this.m_oOffers.inbox = _.filter(this.m_oOffers.inbox, function(mUser) { return mUser.id != vListItem.model.id; });
                this._setInboxOffersCount(this.m_oOffers.inbox.length);
            } else {
                this.m_oOffers.outbox = _.filter(this.m_oOffers.outbox, function(mUser) { return mUser.id != vListItem.model.id; });
                this._setOutboxOffersCount(this.m_oOffers.outbox.length);
            }
        },

        _renderSection: function(aUsers, $section) {
            var vUser;

            $section.html('');
            _.each(aUsers, function(mUser) {
                vUser = new App.View.Mobile.VFriendsListItem({model: mUser, list: this});
                $section.append(vUser.render().el);
            }, this);
        },

        _setFriendsCount: function(nCount) {
            this._getFriendsCounter().text('(' + nCount + ')');
        },

        _setInboxOffersCount: function(nCount) {
            this._getInboxOffersCounter().text('(' + (nCount > 0 ? '+' : '') + nCount + ')');
        },

        _setOutboxOffersCount: function(nCount) {
            this._getOutboxOffersCounter().text('(' + nCount + ')');
        },

        _showFriendsList: function() {
            this.m_sMode = 'friends';

            // show/hide sections
            this._getFriendsHolder().show();
            this._getInboxRequestsHolder().hide();
            this._getOutboxRequestsHolder().hide();

            // activate/deactivate buttons
            this._getShowInboxOffersButton()[0].disabled = false;
            this._getShowOutboxOffersButton()[0].disabled = false;
            this._getShowFriendsButton()[0].disabled = true;

            if (this.m_aFriends) {
                this._doRender(this.m_aFriends)
            } else {
                this._makeDataRequest('friends');
            }

            App.navigate('riseupers/' + this.m_mUser.id + '/friends');
        },

        _showInboxOffersList: function() {
            this.m_sSubMode = 'inbox';
            this._showOffersList();
        },

        _showOutboxOffersList: function() {
            this.m_sSubMode = 'outbox';
            this._showOffersList();
        },

        _showOffersList: function() {
            this.m_sMode = 'offers';

            // show/hide sections
            this._getFriendsHolder().hide();
            ('inbox' === this.m_sSubMode) ? this._getInboxRequestsHolder().show() : this._getInboxRequestsHolder().hide();
            ('outbox' === this.m_sSubMode) ? this._getOutboxRequestsHolder().show() : this._getOutboxRequestsHolder().hide();

            // activate/deactivate buttons
            this._getShowInboxOffersButton()[0].disabled = ('inbox' === this.m_sSubMode);
            this._getShowOutboxOffersButton()[0].disabled = ('outbox' === this.m_sSubMode);
            this._getShowFriendsButton()[0].disabled = false;

            if (this.m_oOffers) {
                this._doRender(this.m_oOffers)
            } else {
                this._makeDataRequest('offers');
            }

            App.navigate('riseupers/' + this.m_mUser.id + '/friends/offers/' + this.m_sSubMode);
        }
	});
})(App, Backbone, _);