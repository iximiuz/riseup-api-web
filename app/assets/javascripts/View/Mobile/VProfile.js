(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.VProfile = App.View.BaseView.extend({
        tagName: 'div',
        className: 'profile',
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/Profile'))(args);
        },

        events: {
            'click .profile__user-sex-male': '_toggleSexMale',
            'click .profile__user-sex-female': '_toggleSexFemale',
            'click .profile__user-credentials-only-i': '_toggleCredentialsOnlyI',
            'click .profile__user-credentials-friends': '_toggleCredentialsFriends',
            'click .profile__user-credentials-all': '_toggleCredentialsAll',
            'click .profile__avatar-update': '_updateAvatar',
            'click .profile__avatar-rotate-left': '_rotateAvatarLeft',
            'click .profile__avatar-rotate-right': '_rotateAvatarRight',
            'click .profile__avatar-delete': '_deleteAvatar',
            'click .profile__avatar-change': '_showChangeAvatarTools',
            'click .profile__save-button': '_save',
            'click .profile__subscriptions-manage-button': '_redirectToSubscriptions',
            'change input[type="file"]': '_onAvatarFileSelected'
        },

        initialize: function (attrs) {
            this.m_mUser = attrs.user;
            this.avatar = new App.Storage.AvatarStorage(this.m_mUser);
        },

        render: function () {
            this.$el.html(this.template({
                user: this.m_mUser.attributes,
                avatarUrl: this.m_mUser.getAvatar()
            }));

            return this;
        },

        _hideChangeAvatarTools: function () {
            this._getAvatarChangeTools().addClass('display-none');
            this._getAvatarChangeButton().show();
        },

        _getFileButton: function () {
            return this.getElement('.profile__avatar-changing-tools-file button');
        },

        _getAvatarChangeTools: function () {
            return this.getElement('.profile__avatar-changing-tools');
        },

        _getAvatarChangeButton: function () {
            return this.getElement('.profile__avatar-change');
        },

        _getAvatarFile: function () {
            return this.getElement('.profile__avatar-file');
        },

        _getAvatarPic: function () {
            return this.getElement('.profile__avatar-pic');
        },

        _getCredAllInput: function () {
            return this.getElement('.profile__user-credentials-all');
        },

        _getCredFriendsInput: function () {
            return this.getElement('.profile__user-credentials-friends');
        },

        _getCredOnlyIInput: function () {
            return this.getElement('.profile__user-credentials-only-i');
        },

        _getSexFemaleInput: function () {
            return this.getElement('.profile__user-sex-female');
        },

        _getSexMaleInput: function () {
            return this.getElement('.profile__user-sex-male');
        },

        _getUserPrimaryInfoVal: function (sKey) {
            return this.getElement('.profile__user-primary-info-val.' + sKey);
        },

        _onAvatarFileSelected: function (e) {
            var sFileName = e.target.value, sShortName = sFileName.substr(Math.min(0, 10 - sFileName.length));
            if (sFileName.length) {
                this._getFileButton().text((sShortName.length !== sFileName.length ? '...' : '') + sShortName);
            } else {
                // TODO: maybe e.target.value = this._getFileButton().text();
            }
        },

        _save: function () {
            var aFields = ['nickname', 'name', 'lastName', 'email'], sField;

            for (var i = 0, l = aFields.length; i < l; i++) {
                sField = aFields[i];
                this.m_mUser.set(sField, this._getUserPrimaryInfoVal(sField).val());
            }

            // TODO: preloader
            this.m_mUser.save();
        },

        _redirectToSubscriptions: function () {
                App.navigate('subscriptions', true, true);
        },

        _showChangeAvatarTools: function () {
            this._getAvatarChangeTools().removeClass('display-none');
            this._getAvatarChangeButton().hide();
            this._getAvatarFile().click();
        },

        _toggleCredentialsAll: function () {
            this._getCredFriendsInput()[0].checked = false;
            this._getCredOnlyIInput()[0].checked = false;
            this.m_mUser.set('credentials', 2);
        },

        _toggleCredentialsFriends: function () {
            this._getCredAllInput()[0].checked = false;
            this._getCredOnlyIInput()[0].checked = false;
            this.m_mUser.set('credentials', 1);
        },

        _toggleCredentialsOnlyI: function () {
            this._getCredAllInput()[0].checked = false;
            this._getCredFriendsInput()[0].checked = false;
            this.m_mUser.set('credentials', 0);
        },

        _toggleSexFemale: function () {
            this._getSexMaleInput()[0].checked = false;
            this.m_mUser.set('sex', 0);
        },

        _toggleSexMale: function () {
            this._getSexFemaleInput()[0].checked = false;
            this.m_mUser.set('sex', 1);
        },

        _updateAvatar: function () {
            var self = this;
            this.avatar.save(this._getAvatarFile()[0].files[0], function (response) {
                self._hideChangeAvatarTools();
                self.m_mUser.set('avatarUrl', response.file);
                self._getAvatarPic(true /* No cache */).attr('src', App.Server.picRootUrl + response.file);
            })
        },

        _rotateAvatar: function (direction) {
            var self = this;
            this.avatar.edit('rotate', direction, function (data) {
                var response = JSON.parse(data.response);
                if (!response.error) {
                    self.m_mUser.set('avatarUrl', response.file);
                    self._getAvatarPic(true /* No cache */).attr('src', App.Server.picRootUrl + 'no-avatar.jpg').attr('src', App.Server.picRootUrl + response.file + '?' + Math.random());
                }
            });
        },
        _rotateAvatarLeft: function () {
            this._rotateAvatar('left');
        },
        _rotateAvatarRight: function () {
            this._rotateAvatar('right');
        },
        _deleteAvatar: function () {
            var self = this;
            this.avatar.drop(function (data) {
                var response = JSON.parse(data.response);
                if (!response.error) {
                    self._hideChangeAvatarTools();
                    self.m_mUser.set('avatarUrl', response.file);
                    self._getAvatarPic().attr('src', App.Server.picRootUrl + 'no-avatar.jpg');
                }
            });
        }
    });
})(App, Backbone, _);
