(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.UsersListItemView = App.View.BaseView.extend({
		className: 'users-list-item',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/UsersListItem'))(args); },

        events: {
            'click .users-list-item__header': '_toggleActions',
            'click .users-list-item__action-timeline': '_goToUserTimeline',
            'click .users-list-item__action-friend': '_friendshipAction',
            'click .users-list-item__action-friend-revoke': '_revokeFriendshipOffer'
        },

        render: function() {
            this.$el.html(this.template({
                name: this.model.getPrintableName(),
                avatarUrl: this.model.getAvatar(),
                friendshipButton: this._getFriendshipButtonTextAndClass(
                    this.model.get('friendshipStatus')
                )
            }));

            if (2 == this.model.get('friendshipStatus')) {
                this._getRevokeButton().show();
            } else {
                this._getRevokeButton().hide();
            }

            return this;
        },

        setFriendshipStatus: function (status) {
            status = parseInt(status);

            this.model.set('friendshipStatus', status);
            this._updateFriendshipButton(status);
        },

        /**************************************************************************************************************/

        _getActions: function() {
            return this.getElement('.users-list-item__actions');
        },

        _friendshipAction: function () {
            var friendship = new App.Model.Friendship();
            friendship.action(this.m_friendshipStatus, this.model.id, this);
        },

        _getFriendshipButton: function () {
            return this.getElement('.users-list-item__action-friend');
        },

        _getFriendshipButtonTextAndClass: function (status) {
            this.m_friendshipStatus = parseInt(status);

            switch (this.m_friendshipStatus) {
                case 1:
                    return {text: 'Отписаться', cssClass: 'btn-none'};
                case 2:
                    return {text: 'Принять', cssClass: 'btn btn-success'};
                case 3:
                    return {text: 'Удалить из друзей', cssClass: 'btn-none'};
                default :
                    return {text: 'Дружить', cssClass: 'btn btn-primary'};
            }
        },

        _getRevokeButton: function () {
            return this.getElement('.users-list-item__action-friend-revoke');
        },

        _goToUserTimeline: function() {
            App.navigate('riseupers/' + this.model.id + '/timeline', true);
        },

        _revokeFriendshipOffer: function() {
            var friendship = new App.Model.Friendship();
            friendship.declineIncommingOffer(this.model.id, this.setFriendshipStatus, this);
        },

        _toggleActions: function() {
            if (this.m_bExpanded) {
                this._getActions().hide();
                this.m_bExpanded = false;
            } else {
                this._getActions().show();
                this.m_bExpanded = true;
            }
        },

        _updateFriendshipButton: function(status) {
            status = parseInt(status);

            var buttonStatusData = this._getFriendshipButtonTextAndClass(status),
                $button = this._getFriendshipButton();

            $button.removeClass('btn btn-primary btn-none');
            $button.text(buttonStatusData.text);
            $button.addClass(buttonStatusData.cssClass);

            if (2 == status) {
                this._getRevokeButton().show();
            } else {
                this._getRevokeButton().hide();
            }
        }
	});
})(App, Backbone, _);