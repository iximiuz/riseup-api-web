(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.VActivityLite = App.View.BaseView.extend({
        className: 'activity-lite',
        defaultDescription: 'Что вы собираетесь сделать?',
        renderComments: false,
        toggleComment: false,

        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/ActivityLite'))(args);
        },

        events: {
            'focus .activity-lite__text': '_prepareTextAreaToEdit',
            'blur .activity-lite__text': '_tryStopEdit',
            'click': '_handleClick',
            'click .activity-lite__text': 'tryStartEdit',
            'click .activity-lite__comments_count': '_toggleComments'
        },

        initialize: function (attrs) {
            attrs = attrs || {};
            this.m_vAcivityStream = attrs.timeline;

            this.m_bIsOwnedToCurUser = (this.model.get('userId') == App.currentUser.id);
            this.m_bIsActionsBlockShowed = false;

            // todo: сделать статическим свойством
            this.m_aStates = {};
            this.m_aStates[App.Model.Activity.STATE_NEW] = {
                cssClass: 'pending'
            };
            this.m_aStates[App.Model.Activity.STATE_COMPLETE] = {
                cssClass: 'completed'
            };
            this.m_aStates[App.Model.Activity.STATE_OVERDUE] = {
                cssClass: 'failed'
            };
            this.m_aStates[App.Model.Activity.STATE_IN_PROGRESS] = {
                cssClass: 'pending'
            };
            this.m_aStates[App.Model.Activity.STATE_IN_PLANS] = {
                cssClass: 'planned'
            };

            App.M.installTo(this);
        },

        render: function () {
            this.resetCache();

            var votes = this.model.get('votes') || null;
            if (votes) {
                if (this.m_bIsOwnedToCurUser) {
                    votes.voteUpClass = 'disabled';
                    votes.voteDownClass = 'disabled';
                } else {
                    if ('1' == votes.voted) {
                        votes.voteUpClass = 'voted';
                        votes.voteDownClass = 'disabled';
                    }

                    if ('-1' == votes.voted) {
                        votes.voteUpClass = 'disabled';
                        votes.voteDownClass = 'voted';
                    }
                }
            }

            var bIsNewActivity = this.model.isNewState(),
                sDescription = this.model.get('description');

            this.m_bHasDefaultDescr = (bIsNewActivity && !sDescription);
            this.$el.html(this.template({
                id: this.model.get('id'),
                descr: this.m_bHasDefaultDescr ? this.defaultDescription : sDescription,
                editable: this.m_bIsOwnedToCurUser && this.model.isEditable(),
                votes: votes,
                stateClass: this.m_aStates[this.model.getState()].cssClass,
                canBePrepended: this.model.canBePrepended(),
                isOwnedToCurUser: this.m_bIsOwnedToCurUser,
                hasRise: this.model.get('goalId'),
                commentsCount: this.model.get('comments_count'),
                commentsClickable: (this.model.get('comments_count') > 0) ? ' clickable' : ''
            }));

            if (bIsNewActivity) {
                this.$el.addClass('new');
            } else {
                this.$el.removeClass('has-rise new edit modified');
            }

            if (this.model.get('goalId')) {
                this.$el.addClass('has-rise');
            }

            return this;
        },

        afterInsertEl: function () {
            this._fitTextHolderHeight();
        },

        tryStartEdit: function () {
            if (!this.m_bIsOwnedToCurUser || !this.model.isEditable()) {
                return;
            }

            var self = this,
                $textArea = this._getTextHolder(),
                fnEditHandler = function () {
                    self._fitTextHolderHeight();

                    if ($textArea.val() != self.model.get('description')) {
                        self.$el.addClass('modified');
                    }
                };

            self._getTextHolder()[0].removeAttribute('readonly');
            self.$el.addClass('edit');
            App.startMutexInterval('edit-activity', fnEditHandler, 250);
        },


        /**************************************************************************************************************/

        _cancelEdit: function () {
            if (this.model.isNewState()) {
                this._removeActivity();
            } else {
                this.model.resetChanges();
                this.render();
            }
        },

        _complete: function () {
            var self = this;
            this.model.complete().save(null, {
                success: function () {
                    self.render();
                }
            });
        },

        _createVote: function () {
            return new App.Model.LikeDislike({
                objectId: this.model.id,
                objectType: 'activity',
                subjectId: App.currentUser.id
            });
        },

        _fitTextHolderHeight: function () {
            this._getTextHolder().height(this._getTextHolder()[0].scrollHeight);
        },

        _getActionsBlock: function () {
            return this.getElement('.activity-lite__actions');
        },

        _getPrivacyLevelCheckbox: function () {
            return this.getElement('.activity-lite__main-span__privacy-level input');
        },

        _getTextHolder: function () {
            return this.getElement('.activity-lite__text');
        },

        _getExpandButton: function () {
            return this.getElement('.activity-lite__expand-button');
        },

        _getVotesCounter: function (sDirection) {
            return this.getElement('.activity-lite__vote.' + sDirection);
        },

        _handleClick: function (e) {
            var $target = $(e.target), sClass = $target.attr('class');
            if (!sClass) {
                return;
            }

            if (-1 !== sClass.indexOf('activity-lite__rise-binding')) {
                this._handleClickOnRise();
                return;
            }

            if (this.m_bIsOwnedToCurUser) {
                if (-1 !== sClass.indexOf('activity-lite__prepend-button')) {
                    this.m_vAcivityStream.addNewSiblingActivity(this, 'before');
                    return;
                }

                if (-1 !== sClass.indexOf('activity-lite__cancel-button')) {
                    this._cancelEdit();
                    return;
                }

                if (-1 !== sClass.indexOf('activity-lite__save-button')) {
                    this._saveActivity();
                    return;
                }

                if (-1 !== sClass.indexOf('activity-lite__status') && -1 !== sClass.indexOf('pending')) {
                    this._complete();
                    return;
                }

                if (-1 !== sClass.indexOf('activity-lite__actions_delete')) {
                    this._removeActivity();
                }

                if (-1 !== sClass.indexOf('activity-lite__actions_unbind-rise')) {
                    this.model.set('goalId', null);
                    this._saveActivity();
                }
            }

            if (!this.m_bIsOwnedToCurUser) {
                if (-1 !== sClass.indexOf('activity-lite__vote')) {
                    var votes = this.model.get('votes') || {};
                    if (votes.voted) {
                        return;
                    }

                    if (-1 !== sClass.indexOf('down')) {
                        this._voteDown();
                    } else {
                        this._voteUp();
                    }
                }
            }

            if (-1 !== sClass.indexOf('__expand-button')) {
                this._toggleActionsBlock();
                return;
            }
        },

        _handleClickOnRise: function () {
            var sGoalId = this.model.get('goalId');
            if (sGoalId) {
                App.navigate('rises/' + sGoalId, true, true);
                return;
            }

            this.pub('rise-binding', this.model.id || this.model.cid);
        },

        _prepareTextAreaToEdit: function () {
            if (this.m_bHasDefaultDescr) {
                this._getTextHolder().val('');
            }
        },

        _removeActivity: function () {
            var self = this;
            this.model.destroy({success: function () {
                self.pub('activity:removed', self.model);
            }});
        },

        _saveActivity: function () {
            var self = this, vSibling, bNeedRegisterCreatedModelInStream = false;

            this.model.set('description', this._getTextHolder().val());

            if (this.model.isNewState()) {
                if (this._getPrivacyLevelCheckbox().attr('checked')) {
                    this.model.set('privacyLevel', -1);
                }

                this.model.set('state', 0);
                bNeedRegisterCreatedModelInStream = true;

                vSibling = this.m_vAcivityStream.findPrev(this, true);
                if (vSibling) {
                    this.model.set('prevId', vSibling.model.id);
                } else {
                    vSibling = this.m_vAcivityStream.findNext(this, true);
                    if (vSibling) {
                        this.model.set('nextId', vSibling.model.id);
                    }
                }
            }

            this.model.save(null, {
                success: function () {
                    //self.hidePreloader();
                    //TODO: адский хак, обернуть в метод как минимум!
                    if (bNeedRegisterCreatedModelInStream) {
                        self.m_vAcivityStream.registerCreatedActivity(self);
                    }

                    self.model.changesSaved();
                    self.render();
                    self._fitTextHolderHeight();
                },
                error: function (args) {
                    //self.hidePreloader();
                    console.log(arguments);
                    //TODO:
                }
            });

            //this.showPreloader();
        },

        _toggleActionsBlock: function () {
            if (this.m_bIsActionsBlockShowed) {
                this._getActionsBlock().hide();
                this._getExpandButton().removeClass('constrict');
                this.m_bIsActionsBlockShowed = false;
            } else {
                this._getActionsBlock().show();
                this._getExpandButton().addClass('constrict');
                this.m_bIsActionsBlockShowed = true;
            }
        },

        _tryStopEdit: function () {
            App.stopMutexInterval('edit-activity');
            this._getTextHolder().attr('readonly', 'readonly');
            this.$el.removeClass('edit');

            if (!this._getTextHolder().val()) {
                this._getTextHolder().val(this.defaultDescription);
                this.m_bHasDefaultDescr = true;
            } else {
                this.m_bHasDefaultDescr = false;
            }
        },

        _updateVotes: function (mVote) {
            var votes = this.model.get('votes') || {'-1': 0, '1': 0};
            votes.voted = mVote.get('value');

            var $votesCounter = this._getVotesCounter(mVote.isLike() ? 'up' : 'down'),
                nVotesCount = parseInt($votesCounter.text()) || 0;

            $votesCounter.addClass('voted');
            if (mVote.isLike()) {
                votes['1'] = votes['1'] + 1;
                $votesCounter.text('+' + (nVotesCount + 1));
            } else {
                App.assert(mVote.isDislike());
                votes['-1'] = votes['-1'] - 1;
                $votesCounter.text(nVotesCount - 1);
            }
        },

        _voteDown: function () {
            var self = this;
            this._createVote().dislike({
                success: function (response) {
                    self._updateVotes(response);
                },
                error: function (response) {
                    // TODO: show error
                }
            });
        },

        _voteUp: function () {
            var self = this;
            this._createVote().like({
                success: function (response) {
                    self._updateVotes(response);
                },
                error: function (response) {
                    // TODO: show error
                }
            });
        },

        _toggleComments: function(){
            var self = this;
            var commentList =$('.activity-lite__comments', self.$el);
            if (self.toggleComment) {
                commentList.hide();
                self.toggleComment = false;
            } else {
                self._getComments();
                commentList.show();
                self.toggleComment = true;
            }
        },

        _getComments: function () {
            var self = this;
            //var commentsCount = self.model.get('comments_count');
            //if (!commentsCount) {
            //    return;
            //}
            if (!self.renderComments) {
                var vCommentsList = new App.View.Mobile.VCommentsList({el: $('.activity-lite__comments', self.$el), model: self.model});
                vCommentsList.render();
                self.renderComments = true;
                this.sub('countComments-' + self.model.get('id'), self._counterUp);
            } else {
                $('.activity-lite__comments', self.$el).find('.activity-lite__new-form').show()
            }
        },

        _counterUp: function () {
            var self = this;
            var counter = self.$el.find('.activity-lite__comments_count');
            counter.text(parseInt(counter.text()) + 1);
        }
    });
})(App, Backbone, _);