(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VRiseBindingWidgetListItem = App.View.BaseView.extend({
		tagName: 'li',
		className: 'rise-binding-widget-list-item',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/RiseBindingWidgetListItem'))(args); },

        events: {
            'click': 'selectMe'
        },

        initialize: function(attrs) {
            this.model.on('change', this.render, this);
            this.m_$widget = attrs.widget;
        },

        render: function() {
            this.$el.html(this.template({
                description: this.model.get('description')
            }));
            return this;
        },

        deselectMe: function() {
            this.$el.removeClass('selected');
        },

        selectMe: function() {
            this.$el.addClass('selected');
            this.m_$widget.listItemSelected(this);
        }
	});
})(App, Backbone, _);