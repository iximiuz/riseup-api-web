(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VLiveHubItem = App.View.BaseView.extend({
		className: 'live-hub-item',

		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/LiveHubItem'))(args); },

        events: {
            'click [data-route]': '_navigateTo'
        },

        initialize: function(attrs) {
            attrs = attrs || {};

            App.M.installTo(this);
        },

        render: function() {
            this.$el.html(this.template({
                title: this._makeTitle(),
                time: this.model.get('createdAt').clone().format('dd, D MMMM H:mm')
            }));

            var vContent = this._makeContentView();
            if (vContent) {
                this._getContentHolder().html(vContent.render().el);
            }

            return this;
		},

        /**************************************************************************************************************/

        _getContentHolder: function() {
            return this.getElement('.live-hub-item__content');
        },

        _makeContentView: function() {
            if (this.model.isNewActivity() || this.model.isActivityCompleted()) {
                return new App.View.Mobile.VActivityLite({model: this.model.get('subject')});
            }

            App.assert(false);
            return null;
        },

        _makeTitle: function() {
            if (this.model.isNewActivity()) {
                return 'Участник ' + this._makeGeneratorLink() + ' добавил новую активность:'
            }

            if (this.model.isActivityCompleted()) {
                return 'Участник ' + this._makeGeneratorLink() + ' выполнил активность:'
            }

            App.assert(false);
            return 'Какое-то загадочное действие...'
        },

        _makeGeneratorLink: function() {
            if (this.model.isNewActivity() || this.model.isActivityCompleted()) {
                var mUser = this.model.get('generator');
                return '<a href="javascript: void 0" data-route="/riseupers/' + mUser.id + '/timeline">' + mUser.getPrintableName() + '</a>'
            }
        },

        _navigateTo: function(e) {
            App.navigate($(e.target).data('route'), true);
        }
	});
})(App, Backbone, _);