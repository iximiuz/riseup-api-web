(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.UsersListView = App.View.BaseView.extend({
		tagName: 'div',
		className: 'users-list',
		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/UsersList'))(args); },

        initialize: function(args) {
            App.M.installTo(this);

            this.m_sCh = 'users-list-ch';
            this.sub(this.m_sCh, this.renderUsers);
        },

        render: function() {
            this.$el.html(this.template());

            this.pub('models:find', {
                model: 'User',
                channel: this.m_sCh,
                criteria: {
                    sort: 'desc',
                    limit: 1000,
                    offset: 0
                }
            });

            return this;
        },

        renderUsers: function(aUsers) {
            var self = this, vUser;
            _.each(aUsers, function(mUser) {
                vUser = new App.View.Mobile.UsersListItemView({model: mUser});
                self.$el.append(vUser.render().el);
            });
        }
	});
})(App, Backbone, _);