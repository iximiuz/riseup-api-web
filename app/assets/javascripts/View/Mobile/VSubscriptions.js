(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.VSubscriptions = App.View.BaseView.extend({
        checked: [],
        tagName: 'div',
        className: 'subscriptions',
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/Subscriptions'))(args);
        },

        events: {
            'click .subscriptions__back-button': '_backToProfile',
            'click .subscriptions__save-button': '_save'
        },

        initialize: function (attrs) {
            this.m_mUser = attrs.user;

        },

        render: function () {
            this.$el.html(this.template({
                user: this.m_mUser.attributes
            }));
            this._loadChecked();
            return this;
        },

        _backToProfile: function () {
            App.navigate('profile', true, true);
        },

        // TODO: эти три метода должны быть в модели. Но что то я протупливаю и не догоняю как это красиво сделать.
        _loadChecked: function () {
            App.Server.defaultServer.GET('/subscriptions/', {token: new App.Authenticity().getAuthToken(), complete: this._loadComplete, dataType: 'json'})
        },

        _loadComplete: function (data) {
            var result = data.response;
            var inputs = $('input.subscriptions__content-checkbox');
            inputs.each(function () {
                var th = $(this);
                for (var i = 0; i < result.length; i++) {
                    if (result[i] == th.val()) {
                        th.attr('checked', true);
                    }
                }
            });
        },

        _save: function () {
            var result = {checked: []};
            var user_id = App.currentUser.id;
            var inputs = $('input.subscriptions__content-checkbox:checked');
            inputs.each(function () {
                var th = $(this);
                result.checked.push(th.val());
            });
            App.Server.defaultServer.PUT('/subscriptions/' + user_id, {data: result});
        }
    });
})(App, Backbone, _);
