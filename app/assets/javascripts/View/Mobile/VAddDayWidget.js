(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.VAddDayWidget = App.View.BaseView.extend({
        className: 'add-day-widget',
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/AddDayWidget'))(args);
        },

        events: {
//          'mousedown .add-day-widget': 'pickDate showAddHelp',
            'click .add-day-widget': 'pickDate',
            'click .add-day-widget__more-left': '_loadDays',
            'click .add-day-widget__more-right': '_loadDays'
        },

        initialize: function (attrs) {
            attrs = attrs || {};
            App.assert(attrs.from || attrs.to);

            this.m_tFrom = attrs.from;
            this.m_tTo = attrs.to;

            App.M.installTo(this);
        },

        render: function () {
            var sFrom = this.m_tFrom ? this.m_tFrom.format('YYYY-MM-DD') : null,
                sTo = this.m_tTo ? this.m_tTo.format('YYYY-MM-DD') : null,
                aMissedDays = this.m_mRegistryActivities ? this.m_mRegistryActivities.getDaysBetween(sFrom, sTo) : null,
                oMoreLeft = null,
                oMoreRight = null;

            if (aMissedDays && 0 < aMissedDays.length) {
                oMoreRight = {
                    from: aMissedDays[0].date,
                    to: aMissedDays[0].date,
                    total: aMissedDays[0].count
                };

                oMoreLeft = {
                    from: aMissedDays[aMissedDays.length - 1].date,
                    to: aMissedDays[aMissedDays.length - 1].date,
                    total: aMissedDays[aMissedDays.length - 1].count
                };

                for (var i = 1, l = aMissedDays.length - 1; i < l; i++) {
                    if (oMoreRight.total < 50) {
                        oMoreRight.total += aMissedDays[i].count;
                        oMoreRight.from = aMissedDays[i].date;
                    }

                    if (oMoreLeft.total < 50) {
                        oMoreLeft.total += aMissedDays[l - i].count;
                        oMoreLeft.to = aMissedDays[l - i].date;
                    }
                }
            }

            this.$el.html(this.template({
                canEdit: this.m_bCanEdit,
                moreLeft: oMoreLeft,
                moreRight: oMoreRight,
                from: sFrom,
                to: sTo
            }));
            return this;
        },

        setFrom: function (tFrom) {
            this.m_tFrom = tFrom;
        },

        setTo: function (tTo) {
            this.m_tTo = tTo;
        },

        pickDate: function () {
            this.pub('activity-stream:new-days', {from: this.m_tFrom, to: this.m_tTo});

            // stop event propagation
            return false;
        },

        showAddHelp: function() {
            var help = new App.Help();
            help.setText("Эта кнопка позволит Вам сделать ещё один шаг к достижению главной цели Вашей жизни!!!");
            help.show();
        },

        setCanEdit: function (bCanEdit) {
            this.m_bCanEdit = bCanEdit;
        },

        setMissedDays: function (mRegistryActivities) {
            this.m_mRegistryActivities = mRegistryActivities;
        },

        /**************************************************************************************************************/

        _loadDays: function (e) {
            var sRange = $(e.target).data('range'), aRange = sRange.split(' ');
            aRange.sort();
            this.pub('activity-stream:aux-days', aRange[0], aRange[1]);

            // stop event propagation
            return false;
        }
    }, {
        widgets: {},

        fillStream: function (oDays, mRegistryActivities, bCanEdit) {
            var dates = oDays.dates, days = oDays.days, curDate, curDay, prevDay;
            for (var i = 0, l = dates.length; i < l; i++) {
                curDate = dates[i];
                curDay = days[curDate];

                if (curDay.isEditable()) {
                    if (0 === i) {
                        App.View.Mobile.VAddDayWidget._renderBeforeIfNeeded(curDay, (void 0), mRegistryActivities, bCanEdit);
                        continue;
                    }

                    prevDay = days[dates[i - 1]];
                    App.View.Mobile.VAddDayWidget._deleteAfterIfNeeded(prevDay);
                    if (!App.View.Mobile.VAddDayWidget._renderBeforeIfNeeded(curDay, prevDay, mRegistryActivities, bCanEdit)) {
                        App.View.Mobile.VAddDayWidget._deleteBefore(curDay);
                    }
                }
            }

            if (0 !== oDays.dates.length) {
                App.View.Mobile.VAddDayWidget._renderAfterIfNeeded(oDays.days[oDays.dates[oDays.dates.length - 1]], bCanEdit);
            }

            App.View.Mobile.VAddDayWidget._removeWidgetsAroundRemovedDays(oDays.days);
        },

        resetWidgets: function () {
            App.View.Mobile.VAddDayWidget.widgets = {};
        },

        _deleteAfterIfNeeded: function (vPrevDay) {
            var widgets = App.View.Mobile.VAddDayWidget.widgets,
                sPrevDate = vPrevDay.getDateAsString();

            if (widgets[sPrevDate] && widgets[sPrevDate].after) {
                widgets[sPrevDate].after.remove();
                delete widgets[sPrevDate].after;
            }
        },

        _deleteBefore: function (vDay) {
            var widgets = App.View.Mobile.VAddDayWidget.widgets,
                sDate = vDay.getDateAsString();

            if (widgets[sDate] && widgets[sDate].before) {
                widgets[sDate].before.remove();
                delete widgets[sDate].before;
            }
        },

        _removeWidgetsAroundRemovedDays: function (assocExistingDays) {
            var widgets = App.View.Mobile.VAddDayWidget.widgets;
            _.each(_.keys(widgets), function (sDate) {
                if (!assocExistingDays[sDate]) {
                    if (widgets[sDate].after) {
                        widgets[sDate].after.remove();
                    }

                    if (widgets[sDate].before) {
                        widgets[sDate].before.remove();
                    }

                    delete widgets[sDate];
                }
            });
        },

        _renderAfterIfNeeded: function (vDay, bCanEdit) {
            var widgets = App.View.Mobile.VAddDayWidget.widgets,
                sDate = vDay.getDateAsString(),
                tFrom;

            if (!widgets[sDate] || !widgets[sDate].after) {
                tFrom = vDay.isEditable() ? vDay.getDate().add('days', 1) : App.getStartOfToday(true);
                var vAddWidget = new App.View.Mobile.VAddDayWidget({from: tFrom});
                vAddWidget.setCanEdit(bCanEdit);
                vAddWidget.render();
                vAddWidget.$el.insertBefore(vDay.$el);

                widgets[sDate] = widgets[sDate] || {};
                widgets[sDate]['after'] = vAddWidget;
            }
        },

        _renderBeforeIfNeeded: function (vDay, vPrevDay, mRegistryActivities, bCanEdit) {
            var tUntil = vDay.getDate(),
                tSince,
                sDate = vDay.getDateAsString(),
                widgets = App.View.Mobile.VAddDayWidget.widgets,
                nDistance,
                bAllowOneDayDistance = false,
                vAddWidget;

            if (vPrevDay && vPrevDay.isEditable()) {
                tSince = vPrevDay.getDate();
            } else {
                bAllowOneDayDistance = true;
                tSince = App.getStartOfToday(true);
            }

            nDistance = tUntil.diff(tSince, 'days');
            if (1 < nDistance || (1 === nDistance && bAllowOneDayDistance)) {
                widgets[sDate] = widgets[sDate] || {};

                if (!bAllowOneDayDistance) {
                    tSince = tSince.add('days', 1);
                }

                if (widgets[sDate]['before']) {
                    vAddWidget = widgets[sDate]['before'];
                    vAddWidget.setFrom(tSince);
                    vAddWidget.setTo(tUntil.add('days', -1));
                    vAddWidget.setMissedDays(mRegistryActivities);
                    vAddWidget.setCanEdit(bCanEdit);
                    vAddWidget.render();
                } else {
                    vAddWidget = new App.View.Mobile.VAddDayWidget({from: tSince, to: tUntil.add('days', -1)});
                    vAddWidget.setMissedDays(mRegistryActivities);
                    vAddWidget.setCanEdit(bCanEdit);
                    vAddWidget.render();
                    vAddWidget.$el.insertAfter(vDay.$el);
                    widgets[sDate]['before'] = vAddWidget;
                    vDay.setPrependDayWidget(vAddWidget);
                }

                return true;
            }

            return false;
        }
    });
})(App, Backbone, _);