(function (App, Backbone, _) {
    'use strict';

    App.View.Mobile.VCommentsList = App.View.BaseView.extend({
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/ActivityCommentsList'))(args);
        },

        events: {
            'click .activity-lite__new-form-cancel-button': 'hideAddCommentsForm',
            'click .activity-lite__new-form-send-button': 'sendComment'
        },

        initialize: function (args) {
            App.M.installTo(this);
            this.sub('comments-list-' + this.model.get('id'), this.renderList);
        },

        render: function () {
            var self = this;
//            if (!self.model.get('comments')) {
                self.$el.html(self.template(self.model.attributes));
//            }

            this.showAddCommentsForm();
//            if (!self.model.get('comments')) {
                var commentsStorage = new App.Storage.CommentStorage();
                commentsStorage.setUrl('/activities/' + self.model.get('id') + '/comments');
                commentsStorage.fetch({entity_type: 1, entity_id: self.model.get('id')}, function (data) {
                    self.model.set('comments', data);
                    self.pub('comments-list-' + self.model.get('id'), data)
                });
//            }
            return this;
        },

        renderList: function (aComments) {
            var self = this, vComments;
            _.each(aComments, function (mComment) {
                vComments = new App.View.Mobile.VCommentsListItemView({model: mComment, activityAuthor: (self.model.get('userId') == App.currentUser.id), activityId: self.model.get('id')});
                var appendForm = self.$el.find('#comments-field-' + self.model.get('id') + ' .activity-lite__comments-list');
                appendForm.append(vComments.render().el);
            });
        },

        renderOnlyComment: function (mComment) {
            var self = this, vComments;
            vComments = new App.View.Mobile.VCommentsListItemView({model: mComment, activityAuthor: (self.model.get('userId') == App.currentUser.id), activityId: self.model.get('id')});
            var appendForm = self.$el.find('#comments-field-' + self.model.get('id') + ' .activity-lite__comments-list');
            appendForm.append(vComments.render().el);
        },

        showAddCommentsForm: function () {
            var self = this;
            self.$el.find('.activity-lite__new-form').show();
        },

        /**
         * Скрыть форму добавления нового комментария
         */
        hideAddCommentsForm: function () {
            var self = this;
            self.$el.find('.activity-lite__new-form').hide();
        },

        sendComment: function () {
            var self = this;
            var commentsStorage = new App.Storage.CommentStorage();
            commentsStorage.setUrl('/activities/' + self.model.get('id') + '/comments');
            var msg = self._getMsgArea().val();
            if (msg) {
                commentsStorage.save({msg: msg, entity_id: self.model.get('id'), entity_type: 1}, function (responce) {
                    self._completeSendComment();
                    self.renderOnlyComment(responce);

                });
            } else {
                self._getErrorHint().append('<li>Сообщение не может быть пустым</li>');
            }
        },

        _getErrorHint: function () {
            var self = this;
            return self.$el.find('.activity-lite__new-form-error ul');
        },

        _getMsgArea: function () {
            var self = this;
            return self.$el.find('.activity-lite__new-form-msg');
        },

        _completeSendComment: function () {
            var self = this;
            self.hideAddCommentsForm();
            self._getErrorHint().empty();
            self._getMsgArea().val('');
            self.pub('countComments-' + self.model.get('id'));
        }
    });
})(App, Backbone, _);