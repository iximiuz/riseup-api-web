(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.AuthPageView = App.View.BaseView.extend({
		tagName: 'div',
		className: 'auth-page',
		template: function() { return _.template(TplHelper.getTplHtml('mobile/AuthPage')); },

        initialize: function(attrs) {
            attrs = attrs || {};

            this.m_sMode = attrs.mode;
        },

        render: function() {
			this.$el.html(this.template());

            if ('login' === this.m_sMode) {
                var vAuthForm = new App.View.Mobile.AuthFormView({
                    authenticity: new App.Authenticity(App.Server.defaultServer, '/auth_tokens')
                });

                this.getAuthFormHolder().prepend(vAuthForm.render().el);
            } else {
                var vRegForm = new App.View.Mobile.RegFormView({
                    authenticity: new App.Authenticity(App.Server.defaultServer, '/auth_tokens')
                });

                this.getRegFormHolder().prepend(vRegForm.render().el);
            }


			return this;
		},


        getAuthFormHolder: function() {
            return this.getElement('.auth-page__auth-form-holder');
        },

        getRegFormHolder: function() {
            return this.getElement('.auth-page__reg-form-holder');
        }
	});
})(App, Backbone, _);