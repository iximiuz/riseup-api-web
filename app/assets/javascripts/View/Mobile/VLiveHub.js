(function(App, Backbone, _) {
	'use strict';

    App.View.Mobile.VLiveHub = App.View.BaseView.extend({
		className: 'live-hub',

		template: function(args) { return _.template(TplHelper.getTplHtml('mobile/LiveHub'))(args); },

        initialize: function(attrs) {
            attrs = attrs || {};

            App.M.installTo(this);
            this.m_sCh = 'live-hub-events-ch';
            this.sub(this.m_sCh, this._renderEvents);
        },

        render: function() {
            this.$el.html(this.template());

            this._makeEventsRequest(App.now().unix());

            return this;
		},

        /**************************************************************************************************************/

        _makeEventsRequest: function(nBoundaryTimestamp) {
            this.pub('models:find', {
                model: 'LiveHubEvent',
                channel: this.m_sCh,
                criteria: {
                    boundaryTimestamp: nBoundaryTimestamp,
                    limit: 200
                }
            });
        },

        _renderEvents: function(aEvents) {
            var vEvent;

            _.each(aEvents, function(mEvent) {
                vEvent = new App.View.Mobile.VLiveHubItem({model: mEvent});
                this.$el.append(vEvent.render().el);
            }, this);
        }
	});
})(App, Backbone, _);