(function(App, Backbone, _) {
    'use strict';

    App.View.Common.VCellularDatePicker = App.View.BaseView.extend({
        className: 'cellular-date-picker',
        template: function(args) { return _.template(TplHelper.getTplHtml('common/CellularDatePicker'))(args); },

        events: {
            'click .cellular-date-picker__day': '_pickDay',
            'click .cellular-date-picker__to-the-future': '_monthUp',
            'click .cellular-date-picker__to-the-past': '_monthDown'
        },

        initialize: function(attrs) {
            App.assert(attrs);
            App.assert(!attrs.mode || {single: 1, multi: 1}[attrs.mode]);

            this.m_tInitDate = attrs.startDate;
            this.m_tMinAllowedDate = attrs.from;
            if (this.m_tMinAllowedDate) {
                this.m_sMinAllowedDate = this.m_tMinAllowedDate.clone().format('YYYY-MM-DD');
            }

            this.m_tMaxAllowedDate = attrs.to;
            if (this.m_tMaxAllowedDate) {
                this.m_sMaxAllowedDate = this.m_tMaxAllowedDate.clone().format('YYYY-MM-DD');
            }

            this.m_bIsSingleMode = ('single' === attrs.mode);

            this.m_aSelectedDays = {$days: {}, order: []};

            this.m_aListeners = {onSelect: {}, onDeselect: {}};

            App.M.installTo(this);
        },

        render: function() {
            this.$el.html(this.template());

            this.m_nCurMonth = parseInt(this.m_tInitDate.clone().format('M')) - 1;
            this.m_nCurYear = parseInt(this.m_tInitDate.clone().format('YYYY'));

            this._renderMonth(this.m_nCurMonth, this.m_nCurYear);

            return this;
        },

        addOnSelectListener: function(id, fnCallback, ctx) {
            ctx = ctx || window;
            this.m_aListeners.onSelect[id] = {fn: fnCallback, ctx: ctx};
        },

        addOnDeselectListener: function(id, fnCallback, ctx) {
            ctx = ctx || window;
            this.m_aListeners.onDeselect[id] = {fn: fnCallback, ctx: ctx};
        },

        afterInsertEl: function(nContainerWidth) {

        },

        deselectAll: function() {
            for (var i = this.m_aSelectedDays.order.length - 1; i >= 0; i--) {
                this._deselectDay(this.m_aSelectedDays.order[i]);
            }
        },

        getSelectedDates: function() {
            return this.m_aSelectedDays.order;
        },

        removeOnSelectListener: function(id) {
            delete this.m_aListeners.onSelect[id];
        },

        removeOnDeselctListener: function(id) {
            delete this.m_aListeners.onDeselect[id];
        },

        reset: function() {
            this.deselectAll();
        },

        /**************************************************************************************************************/

        _dayIsCurrent: function(sDate) {
            return (sDate === this.m_sCurDate);
        },

        _dayIsSelected: function(sDate) {
            return !!this.m_aSelectedDays.$days[sDate];
        },

        _deselectDay: function(sDate) {
            App.assert(this._dayIsSelected(sDate));

            var $day = this.m_aSelectedDays.$days[sDate];
            if ($day) {
                $day.removeClass('selected current');
                delete this.m_sCurDate;
                delete this.m_aSelectedDays.$days[sDate];
                this.m_aSelectedDays.order.splice(_.indexOf(this.m_aSelectedDays.order, sDate, 1), 1);

                this._notify(sDate, 'onDeselect');
            }
        },

        _getDay: function(sDate) {
            return this.getElement('.cellular-date-picker__day[data-date="' + sDate + '"]');
        },

        _getDaysHolder: function() {
            return this.getElement('.cellular-date-picker__days-holder');
        },

        _getMonthNameHolder: function() {
            return this.getElement('.cellular-date-picker__month-name');
        },

        _isSingleMode: function() {
            return this.m_bIsSingleMode;
        },

        _markDayAsCurrent: function(sDate) {
            App.assert(this._dayIsSelected(sDate));

            this._getDay(sDate).addClass('current');
            this.m_sCurDate = sDate;

            this._notify(sDate, 'onSelect');
        },

        _monthDown: function() {
            if (
                this.m_tMinAllowedDate
                && this.m_tMinAllowedDate.clone().startOf('month').format('YYYYMM') >= (this.m_nCurYear + '' + (this.m_nCurMonth + 1))) {
                return;
            }

            this.m_nCurMonth--;
            if (this.m_nCurMonth < 0) {
                this.m_nCurMonth = 11 + this.m_nCurMonth;
                this.m_nCurYear--;
            }

            this._renderMonth(this.m_nCurMonth, this.m_nCurYear);
        },

        _monthUp: function() {
            if (
                this.m_tMaxAllowedDate
                    && this.m_tMaxAllowedDate.clone().endOf('month').format('YYYYMM') <= (this.m_nCurYear + '' + (this.m_nCurMonth + 1))) {
                return;
            }

            this.m_nCurMonth++;
            if (this.m_nCurMonth > 11) {
                this.m_nCurMonth = this.m_nCurMonth % 12;
                this.m_nCurYear++;
            }

            this._renderMonth(this.m_nCurMonth, this.m_nCurYear);
        },

        _notify: function(sDate, sCase) {
            App.assert({onSelect: 1, onDeselect: 1}[sCase]);

            var listeners = this.m_aListeners[sCase];
            try {
                _.each(listeners, function(listener) {
                    listener.fn.apply(listener.ctx, [sDate]);
                });
            } catch (e) {
                console.log(e);
                App.assert(false);
            }
        },

        _pickDay: function(e) {
            var $day = $(e.currentTarget), sDate = $day.data('date');
            if (-1 !== $day.attr('class').indexOf('disabled')) {
                return;
            }

            if (this._dayIsSelected(sDate)) {
                if (this._dayIsCurrent(sDate)) {
                    this._deselectDay(sDate);
                } else {
                    this._unmarkCurrentDayIfNeeded();
                    this._markDayAsCurrent(sDate);
                }
            } else {
                this._unmarkCurrentDayIfNeeded();
                this._selectDay(sDate, $day);
                this._markDayAsCurrent(sDate);
            }
        },

        _produceDayHtml: function(sFullDate, sDate) {
            var sClass = (this.m_sMaxAllowedDate && this.m_sMaxAllowedDate < sFullDate)
                || (this.m_sMinAllowedDate && this.m_sMinAllowedDate > sFullDate) ? 'disabled' : '';

            return '<div class="cellular-date-picker__day ' + sClass + '" data-date="' + sFullDate + '">' +
                '<div class="cellular-date-picker__day-height-helper"></div>' +
                '<div class="cellular-date-picker__day-data" unselectable="on">' + sDate + '</div></div>';
        },

        _produceMonths: function(nMonth, nYear) {
            var tMonth, sDays = '', i, nDaysTotal;

            tMonth = moment(nYear + '', 'YYYY').month(nMonth).startOf('month');

            i = ((tMonth.clone().date(1).day() + 6) % 7) || 7;
            sDays += this._producePeriodPart(tMonth.clone().subtract('days', i), i - 1);
            nDaysTotal = i;

            i = tMonth.clone().endOf('month').date();
            sDays += this._producePeriodPart(tMonth.clone(), i - 1);
            nDaysTotal += i;

            return sDays + this._producePeriodPart(tMonth.endOf('month').add('days', 1), 42 - nDaysTotal - 1);
        },

        _producePeriodPart: function(tFromDate, nDaysCount) {
            var sDate,
                sDays = '',
                nYear = tFromDate.clone().format('YYYY'),
                sMonthNum = tFromDate.clone().format('MM');

            for (var j = tFromDate.date(), l = 0; l <= nDaysCount; l++, j++) {
                sDate = j;
                if (sDate < 10) {
                    sDate = '0' + sDate;
                }
                sDays += this._produceDayHtml(nYear + '-' + sMonthNum + '-' + sDate, j);
            }

            return sDays;
        },

        _renderMonth: function(nMonth, nYear) {
            this._getDaysHolder().html(this._produceMonths(nMonth, nYear));
            this._getMonthNameHolder().html(moment(nYear + ' ' + (nMonth + 1), 'YYYY M').format('MMMM YYYY'));
        },

        _selectDay: function(sDate, $day) {
            App.assert(!this._dayIsSelected(sDate));
            if (this._isSingleMode()) {
                App.assert(2 > this.m_aSelectedDays.order.length);
                if (1 === this.m_aSelectedDays.order.length) {
                    this._deselectDay(this.m_aSelectedDays.order[0]);
                }
            }

            $day.addClass('selected');
            this.m_aSelectedDays.$days[sDate] = $day;
            this.m_aSelectedDays.order.push(sDate);
        },

        _unmarkCurrentDayIfNeeded: function() {
            if (this.m_sCurDate) {
                this._getDay(this.m_sCurDate).removeClass('current');
            }
        }
    });
})(App, Backbone, _);