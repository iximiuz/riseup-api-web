(function (ns, Backbone) {
    'use strict';

    ns.View.BaseView = Backbone.View.extend({
        render: function () {
            this.$el.html(this.template());
            return this;
        },

        activate: function () {

        },

        deactivate: function () {

        },

        getElement: function (sSelector) {
            this.m_oCache = this.m_oCache || {};
            if (!this.m_oCache[sSelector]) {
                this.m_oCache[sSelector] = this.$(sSelector);
            }
            return this.m_oCache[sSelector];
        },

        resetCache: function () {
            this.m_oCache = {};
        },

        remove: function () {
            this.deactivate();
            Backbone.View.prototype.remove.apply(this, arguments);
        },

        stopMouseOverscrolling: function(e) {
            e.stopPropagation();
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        },

        subscriberId: function () {
            if (!this.m_sSubscriberId) {
                this.m_sSubscriberId = 'base-view:' + ++ns.View.BaseView.OBJ_COUNTER;
            }

            return this.m_sSubscriberId;
        },

        afterInsertEl: function () {

        }
    }, {
        OBJ_COUNTER: 0
    });
})(App, Backbone);