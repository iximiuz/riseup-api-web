//document ready
$(function () {
    'use strict';

    App.clientId = moment(new Date()).unix() + '.' + Math.floor(Math.random() * 1000000);
    App.Server.defaultServer = App.Server.RemoteServer.getInstance();

    App.Server.EPC = new App.Server.EventPollingController(App.Server.defaultServer);
    //App.Server.EPC.start('http://eq.riseup.dev/sub/' + App.currentUser.get('id'));

    App.DPD = new App.DataProviderDispatcher();
    App.DPD.registerDataProvider(
        'Activity',
        new App.ActivityDataProvider({server: App.Server.defaultServer, pollingProcessor: App.Server.EPC})
    );
    App.DPD.registerDataProvider(
        'Goal',
        new App.GoalDataProvider({server: App.Server.defaultServer, pollingProcessor: App.Server.EPC})
    );
    var usersDataProvider = new App.UsersDataProvider({server: App.Server.defaultServer, pollingProcessor: App.Server.EPC});
    App.DPD.registerDataProvider('User', usersDataProvider);
    App.DPD.registerDataProvider('Friend', usersDataProvider);
    App.DPD.registerDataProvider('Friendship', usersDataProvider);
    App.DPD.registerDataProvider('LiveHubEvent', new App.LiveHubEventDataProvider(
        {server: App.Server.defaultServer, pollingProcessor: App.Server.EPC}
    ));


    Backbone.ajaxSync = Backbone.sync;
    Backbone.getSyncMethod = function (model) {
        if (model.storage) {
            return App.Storage.RuntimeStorage.sync;
        }

        // fallback на стандартный механизм по сохранению моделей бекбона
        return Backbone.ajaxSync;
    };

    Backbone.sync = function (method, model, options) {
        return Backbone.getSyncMethod(model).apply(this, [method, model, options]);
    };


    App.initialize(new (Backbone.Router.extend({
        routes: {
            'admin': 'admin',
            'calendar': 'calendar',
            'live': 'liveHub',
            'login': 'login',
            'register': 'register',
            'profile': 'profile',
            'subscriptions': 'subscriptions',
            'search': 'search',
            'timeline': 'timeline',
            'feedback': 'feedback',
            'riseupers/:id/timeline(/)(?*query)': 'userTimeline',
            'riseupers': 'users',
            'rises(/:id)': 'rises',
            'friends(/)(:mode)(/)(:submode)': 'friends',
            'riseupers/:id/friends(/)(:mode)(/)(:submode)': 'userFriends',
            '*path': 'defaultRoute'
        },

        admin: function () {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.AdminPageController($('body')));
            App.getController().executeAdmin();
        },

        calendar: function() {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.CalendarPageController($('body')));
            App.getController().executeCalendar();
        },

        defaultRoute: function () {
            //App.assert(false);

            if (App.currentUser) {
                App.navigate('timeline', true, true);
            } else {
                App.navigate('login', true, true);
            }
        },

        friends: function (sMode, sSubMode) {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.navigate(
                'riseupers/' + App.currentUser.id + '/friends'
                    + (sMode ? ('/' + sMode) : '') + (sMode && sSubMode ? ('/' + sSubMode) : ''),
                true,
                true
            );
        },

        liveHub: function() {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.LiveHubPageController($('body')));
            App.getController().executeIndex();
        },

        login: function (args) {
            if (App.currentUser) {
                App.navigate('timeline', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.AuthPageController($('body')));
            App.getController().executeLogin(args);
        },

        register: function (args) {
            if (App.currentUser) {
                App.navigate('timeline', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.AuthPageController($('body')));
            App.getController().executeRegister(args);
        },

        feedback: function () {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.FeedbackPageController($('body')));
            App.getController().executeShowForm();
        },

        timeline: function (args) {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.navigate('riseupers/' + App.currentUser.id + '/timeline', true, true);
        },

        users: function (args) {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.UsersPageController($('body')));
            App.getController().executeList(args);
        },

        userTimeline: function (userId) {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            if (userId == App.currentUser.id) {
                var z = App.getURLParameter('z'),
                    riseBindingPattern =  'risebinding',
                    bindActivityId;

                App.setController(new App.PageController.Mobile.TimelinePageController($('body')));
                if (z && 'string' === typeof z) {
                    if (riseBindingPattern === z.substr(0, riseBindingPattern.length)) {
                        bindActivityId = z.substr(riseBindingPattern.length);
                    }
                }

                App.getController().executeTimeline(bindActivityId);
            } else {
                App.setController(new App.PageController.Mobile.UserTimelinePageController($('body')));
                App.getController().executeTimeline(userId);
            }
        },

        userFriends: function (userId, sMode, sSubMode) {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.FriendsPageController($('body')));
            App.getController().executeFriendsList(userId, sMode, sSubMode);
        },

        profile: function () {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.ProfilePageController($('body')));
            App.getController().executeProfile();
        },

        subscriptions: function () {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.SubscriptionsPageController($('body')));
            App.getController().executeSubscriptions();
        },

        rises: function (args) {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }

            App.setController(new App.PageController.Mobile.RisesPageController($('body')));
            App.getController().executeList(args);
        },

        search: function(args) {
            if (!App.currentUser) {
                App.navigate('login', true, true);
                return;
            }
            App.setController(new App.PageController.Mobile.SearchPageController($('body')));
            App.getController().executeSearch();

        }
    })));


    App.startHistory();

    // todo: hide address bar in safari
    /*setTimeout(function(){
        // Hide the address bar!
        window.scrollTo(0, 1);
    }, 10000);*/
});