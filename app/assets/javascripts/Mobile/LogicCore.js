(function (App) {
    App.LogicCore = new (App.Class.extend({
        initialize: function() {
            App.M.installTo(this);

            this.sub('rise-binding', this.startRiseBindingWidget);
        },

        startRiseBindingWidget: function(nActivityId) {
            var vRiseBindingWidget = new App.View.Mobile.VRiseBindingWidget({activityId: nActivityId});
            App.getController().m_vLayout.renderPopup(vRiseBindingWidget);
        }
    }));
})(App);