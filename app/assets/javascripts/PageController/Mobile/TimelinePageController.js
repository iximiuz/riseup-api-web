(function(App) {
    'use strict';

    App.PageController.Mobile.TimelinePageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.HeadlessLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeTimeline: function(bindActivityId) {
            App.initTimeline();

            var attrs = {}, vStreamPage;
            if (bindActivityId) {
                attrs.mode = 'rise-binding';
                attrs.bindActivityId = bindActivityId;
            } else {
                attrs.date = moment(new Date());
            }

            attrs.userId = App.currentUser.id;
            attrs.user = App.currentUser;
            attrs.editable = true;

            vStreamPage = new App.View.Mobile.VActivityStreamPage(attrs);
            this.m_vLayout.renderContent(vStreamPage);
        }
    });
})(App);