(function(App) {
    'use strict';

    App.PageController.Mobile.ProfilePageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeProfile: function() {
            var vProfile = new App.View.Mobile.VProfile({user: App.currentUser});
            this.m_vLayout.renderContent(vProfile, 'Профиль');
        }
    });
})(App);