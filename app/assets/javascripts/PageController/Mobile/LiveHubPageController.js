(function(App) {
    'use strict';

    App.PageController.Mobile.LiveHubPageController = App.PageController.BasePageController.extend({

        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeIndex: function() {
            var vLiveHub = new App.View.Mobile.VLiveHub();

            this.m_vLayout.renderContent(vLiveHub, 'Прямой эфир');
        }
    });
})(App);