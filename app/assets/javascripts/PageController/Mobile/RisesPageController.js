(function(App) {
    'use strict';

    App.PageController.Mobile.RisesPageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeList: function(args) {
            args = args || 'active';

            var attrs = {};
            if (args.match(/\d+_\d+/)) {
                attrs.goalId = args;
            } else {
                attrs.filter = args;
            }

            var vRisesPage = new App.View.Mobile.VRisesPage(attrs);
            this.m_vLayout.renderContent(vRisesPage, 'Райзы');

            if (attrs.filter) {
                App.navigate('rises/' + attrs.filter);
            }
        }
    });
})(App);