/**
 * Created with JetBrains RubyMine.
 * User: dimka3210
 * Date: 24.09.13
 * Time: 21:40
 */
(function (App) {
    'use strict';

    App.PageController.Mobile.FeedbackPageController = App.PageController.BasePageController.extend({
        initialize: function ($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeShowForm: function () {
            var vFeedbackForm = new App.View.Mobile.FeedbackView();
            this.m_vLayout.renderContent(vFeedbackForm, 'Обратная связь');
        }
    });
})(App);
