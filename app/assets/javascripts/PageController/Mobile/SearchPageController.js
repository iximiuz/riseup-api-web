/**
 * User: dimka3210
 * Date: 01.02.2014
 * Time: 21:40
 */
(function (App) {
    'use strict';

    App.PageController.Mobile.SearchPageController = App.PageController.BasePageController.extend({
        initialize: function ($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeSearch: function () {
            var vSearchForm = new App.View.Mobile.VSearch();
            this.m_vLayout.renderContent(vSearchForm, 'Поиск');
        }
    });
})(App);
