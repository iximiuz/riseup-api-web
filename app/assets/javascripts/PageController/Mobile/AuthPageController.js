(function(App) {
    'use strict';

    App.PageController.Mobile.AuthPageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.WelcomeLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeLogin: function() {
            var vAuthPage = new App.View.Mobile.AuthPageView({mode: 'login'});
            this.m_vLayout.renderContent(vAuthPage);
        },

        executeRegister: function() {
            var vAuthPage = new App.View.Mobile.AuthPageView({mode: 'register'});
            this.m_vLayout.renderContent(vAuthPage);
        }
    });
})(App);