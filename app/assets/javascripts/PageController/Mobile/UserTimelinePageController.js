(function(App) {
    'use strict';

    App.PageController.Mobile.UserTimelinePageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.HeadlessLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeTimeline: function(userId) {
            App.assert(userId != App.currentUser.id);

            var mUser = new App.Model.User({id: +userId}), self = this;
            mUser.fetch({
                success: function(model) {
                    self.doTimeline(model);
                }
            });
        },

        doTimeline: function(mUser) {
            App.initTimeline();

            var vUserStream, attrs = {};
            attrs.date = moment(new Date());
            attrs.user = mUser;
            attrs.userId = mUser.id;

            vUserStream = new App.View.Mobile.VActivityStreamPage(attrs);
            this.m_vLayout.renderContent(vUserStream);
        }
    });
})(App);