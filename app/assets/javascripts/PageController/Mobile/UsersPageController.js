(function(App) {
    'use strict';

    App.PageController.Mobile.UsersPageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeList: function(args) {
            var vUsersList = new App.View.Mobile.UsersListView();
            this.m_vLayout.renderContent(vUsersList, 'Участники');
        }
    });
})(App);