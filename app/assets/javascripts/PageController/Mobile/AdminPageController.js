(function(App) {
    'use strict';

    App.PageController.Mobile.AdminPageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeAdmin: function() {
            var vGenerator = new App.View.Mobile.Admin.VGeneratorActivities();
            this.m_vLayout.renderContent(vGenerator, 'Админка', {noHint: true});
        }
    });
})(App);