(function(App) {
    'use strict';

    App.PageController.Mobile.SubscriptionsPageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeSubscriptions: function() {
            var vSubscriptions = new App.View.Mobile.VSubscriptions({user: App.currentUser});
            this.m_vLayout.renderContent(vSubscriptions, 'Управление подписками');
        }
    });
})(App);