(function(App) {
    'use strict';

    App.PageController.Mobile.FriendsPageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeFriendsList: function(nUserId, sMode, sSubMode) {
            if (nUserId == App.currentUser.id) {
                this.doTimeline(App.currentUser, sMode, sSubMode);
            } else {
                var mUser = new App.Model.User({id: +nUserId}), self = this;
                mUser.fetch({
                    success: function(model) {
                        self.doTimeline(model, sMode, sSubMode);
                    }
                });
            }
        },

        doTimeline: function(mUser, sMode, sSubMode) {
            var vFriendsList = new App.View.Mobile.VFriendsList({user: mUser, mode: sMode, subMode: sSubMode});
            this.m_vLayout.renderContent(vFriendsList, 'Друзья');
        }
    });
})(App);