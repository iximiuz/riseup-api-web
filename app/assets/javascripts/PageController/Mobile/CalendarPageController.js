(function(App) {
    'use strict';

    App.PageController.Mobile.CalendarPageController = App.PageController.BasePageController.extend({
        initialize: function($page) {
            this.m_vLayout = new App.Layout.Mobile.MainLayout();
            $page.html(this.m_vLayout.render().el);
            this.m_vLayout.afterInsertEl();
        },

        executeCalendar: function() {
            var vPage = new App.View.Mobile.VCalendarPage();
            this.m_vLayout.renderContent(vPage, 'Календарь', {noHint: true});
        }
    });
})(App);