(function (App, _) {
    'use strict';

    App.Storage.SearchStorage = App.Storage.RuntimeStorage.extend({
        initialize: function (filter) {
            this.url = '/search/'+this._validFilter(filter);
        },

        fetch: function(oCriteria, fnOnSuccess) {
            var self = this;
            App.Server.defaultServer.GET(self.url, {
                data: oCriteria,
                token: new App.Authenticity().getAuthToken(),
                complete: fnOnSuccess,
                dataType: 'json'
            });
        },

        _validFilter: function (filter) {
            switch (filter) {
                case 'users':
                    return filter;
                case 'activities':
                    return filter;
                case 'goals':
                    return filter;
                default:
                    return 'all';
            }
        }
    });
})(App, _);