(function(App, _) {
	'use strict';

    App.Storage.GoalStorage = App.Storage.RuntimeStorage.extend({
		url: '/goals/',

		fetch: function(oCriteria, fnOnSuccess) {
			var self = this, mGoal, aGoals = [];

			//TODO: if (this.hasNotSyncedChanges() this.sync())

            App.Server.defaultServer.GET(this.url + '?' +  oCriteria.toQueryString(), {
				success: function(aGoalAttrs) {
					//TODO: goals = self.find(oCriteria); merge aGoals & goals. Отсутствующих в goals добавить, лишних из goals удалить,
					//пересекающиеся обновить (судя по state_counter`у ?).
					_.each(aGoalAttrs, function(oGoalAttrs) {
                        mGoal = self._get(oGoalAttrs.id);
                        if (mGoal) {
                            mGoal.set(mGoal.parse(oGoalAttrs));
                        } else {
                            mGoal = self._set(new App.Model.Goal(oGoalAttrs));
                        }

                        aGoals.push(mGoal);
					});

					fnOnSuccess(aGoals);
				},
				dataType: 'json'
			});
		}
	});
})(App, _);