(function(App, _) {
	'use strict';

    App.Storage.ActivityStorage = App.Storage.RuntimeStorage.extend({
		url: function(id) { return id ? ('/users/' + id + '/activities/') : '/activities/'; },

		fetch: function(oCriteria, fnOnSuccess, fnOnError) {
			var self = this,
                mActivity,
                aActivities = [],
                sUrl = this.url(App.currentUser.id == oCriteria.getUserId() ? void 0 : oCriteria.getUserId())
                    + '?' +  oCriteria.toQueryString();

			//TODO: if (this.hasNotSyncedChanges() this.sync())

            App.Server.defaultServer.GET(sUrl, {
				    success: function(response) {
                        if (response.error) {
                            fnOnError(response);
                            return;
                        }

					    //TODO: goals = self.find(oCriteria); merge aGoals & goals. Отсутствующих в goals добавить, лишних из goals удалить,
					    //пересекающиеся обновить (судя по state_counter`у ?).
					    _.each(response, function(oActivityAttrs) {
                            mActivity = self._get(oActivityAttrs.id);
                            if (mActivity) {
                                mActivity.set(mActivity.parse(oActivityAttrs));
                            } else {
                                mActivity = self._set(new App.Model.Activity(oActivityAttrs));
                            }

                            aActivities.push(mActivity);
					    });

					    fnOnSuccess(aActivities);
				    },
				    dataType: 'json'
			});
		}
	});
})(App, _);