(function (App, _) {
    'use strict';

    App.Storage.StatisticStorage = App.Storage.RuntimeStorage.extend({
        url: '/statistic/',

        save: function (params) {
            var self = this;
            App.Server.defaultServer.POST(self.url, {
                success: function (response) {
                    if (response.error) {
                        console.error(response.error);
                    }
                },
                dataType: 'json',
                data: params
            });
        }
    });
})(App, _);