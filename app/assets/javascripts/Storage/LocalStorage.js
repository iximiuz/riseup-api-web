(function(ns, $, _) {
	'use strict';

	ns.Storage = ns.Storage || {};

	var ModelHolder = ns.Class.extend({
		m_sStoreName    : null,
		m_oStore        : null,
		m_nInitTimestamp: 0,
		m_nCounter      : 0,

		initialize: function(fnModel, sStoreName) {
			this.m_sStoreName = sStoreName;
			this.m_oStore = localStorage.getItem(this.m_sStoreName);
			this.m_oStore = (this.m_oStore && $.parseJSON(this.m_oStore)) || this.createStore();
			this.m_nInitTimestamp = moment(new Date()).unix();
			this.m_fnModel = fnModel;
		},

		/**
		 * @param oBunch DiffStorageEntry
		 */
		save: function(oBunch) {
			var aDeleted = oBunch.getDeleted(),
				aSaved = oBunch.getSaved();

			_.each(aSaved, function(oModel) {
				if (!(oModel instanceof ns.Model.Base.DomainModel)) {
					throw new Error('Non-domain model can`t be saved.');
				}
			});

			this.doDelete(aDeleted, oBunch.getUserId());
			this.doSave(aSaved, oBunch.getUserId());

			localStorage.setItem(this.m_sStoreName, JSON.stringify(this.m_oStore));
		},

		genId: function() {
			return this.m_nInitTimestamp.toString() + '.' + ++this.m_nCounter;
		},

		createStore: function() {
			return {byId: {}, byUserId: {}};
		},

		doDelete: function(aDeleted, nUserId) {
			var self = this;
			_.each(aDeleted, function(nId) {
				delete self.m_oStore.byId[nId];
				delete self.m_oStore.byUserId[nUserId][nId];
			});
		},

		doSave: function(aSaved, nUserId) {
			var self = this;
			_.each(aSaved, function(oModel) {
				//Gen local storage ID for model if it has tmp id.
				if (!oModel.isPersistent()) {
					oModel.id = oModel.attributes.id = self.genId();
				}

				self.m_oStore.byId[oModel.id] = oModel.toJSON();
				self.m_oStore.byUserId[nUserId] = self.m_oStore.byUserId[nUserId] || {};
				self.m_oStore.byUserId[nUserId][oModel.id] = oModel.id;
			});
		}
	});

	var ActivityHolder = ModelHolder.extend({
		find: function(attributes) {
			if (attributes.from && attributes.to) {
				return this._findByPeriod(attributes.from, attributes.to);
			}

			throw new Error('Not implemented yet!');
		},

		/**************************************************************************************************************/
		_findByPeriod: function(from, to) {
			var self = this, result = [],
				nFromTimestamp = moment(from).unix(),
				nToTimestamp = moment(to).unix();


			_.each(this.m_oStore.byId, function(activityAttrs) {
				var mActivity = new self.m_fnModel(activityAttrs),
					date = mActivity.get('startDt'),
					nTimestamp;

				if (date) {
					nTimestamp = moment(date).unix();
					if (nTimestamp >= nFromTimestamp && nTimestamp <= nToTimestamp) {
						result.push(mActivity);
					}
				}
			});

			return result;
		}
	});

	var GoalHolder = ModelHolder.extend({
		find: function(attributes) {
			if (attributes.userId && (!attributes.filter || 'all' === attributes.filter)) {
				return this._findAll(attributes.userId);
			}

			throw new Error('Not implemented yet!');
		},

		/**************************************************************************************************************/
		_findAll: function(nUserId) {
			var self = this, result = {}, byUserId = this.m_oStore.byUserId[nUserId] || {}, byId = this.m_oStore.byId;

			_.each(byUserId, function(id) {
				result[id] = new self.m_fnModel(byId[id]);
			});

			return result;
		}
	});


	ns.Storage.LocalStorage = ns.Class.extend((function() {


		return {}
	}()));
})(App, $, _);