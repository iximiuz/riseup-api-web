(function(ns, _) {
	'use strict';

	ns.Storage.UserStorage = ns.Storage.RuntimeStorage.extend({
		url: function(oCriteria) { return {Friend: '/friends/', Friendship: '/friendship/'}[oCriteria.model] || '/users/'; },

		fetch: function(oCriteria, fnOnSuccess) {
			var self = this;

			//TODO: if (this.hasNotSyncedChanges() this.sync())

			ns.Server.defaultServer.GET(this.url(oCriteria) + '?' +  oCriteria.toQueryString(), {
				success: function(response) {
					//TODO: users = self.find(oCriteria); merge aUsers & users. Отсутствующих в users добавить, лишних из users удалить,
					//пересекающиеся обновить (судя по state_counter`у ?).

                    var result = null;
                    if (response.inbox) {
                        result = result || {};
                        result.inbox = self._parseResponseAttrsArray(response.inbox);
                    }

                    if (response.outbox) {
                        result = result || {};
                        result.outbox = self._parseResponseAttrsArray(response.outbox);
                    }

                    if (_.isArray(response)) {
                        result = self._parseResponseAttrsArray(response);
                    }

                    App.assert(null !== result);

					fnOnSuccess(result);
				},
				dataType: 'json'
			});
		},

        _parseResponseAttrsArray: function(aAttrs) {
            var aResult = [], mUser, self = this;

            _.each(aAttrs, function(oUsersAttrs) {
                mUser = self._get(oUsersAttrs.id);
                if (mUser) {
                    mUser.set(mUser.parse(oUsersAttrs));
                } else {
                    mUser = self._set(new ns.Model.User(oUsersAttrs));
                }

                aResult.push(mUser);
            });

            return aResult;
        }
	});
})(App, _);