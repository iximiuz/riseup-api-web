(function (App, _) {
    'use strict';

    App.Storage.AvatarStorage = App.Storage.RuntimeStorage.extend({
        initialize: function (userModel) {
            this.user = userModel;
            this.url = '/users/' + this.user.get('id') + '/avatar/';
        },
        save: function (file, callback) {
            var self = this;

            var xhr;
            try {
                xhr = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                try {
                    xhr = new ActiveXObject('Microsoft.XMLHTTP');
                } catch (E) {
                    xhr = false;
                }
            }

            if (!xhr && typeof XMLHttpRequest != 'undefined') {
                xhr = new XMLHttpRequest();
            }

            var token = new App.Authenticity().getAuthToken();

            var formData = new FormData();
            formData.append('thefile', file);
            xhr.open(
                'POST',
                App.Server.apiRootUrl + '/users/' + this.user.get('id') + '/avatar' + '?clientId=' + App.clientId + '&token=' + token,
                true
            );

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        var response = JSON.parse(xhr.response);
                        callback(response);
                    }
                }
            };
            xhr.send(formData);
        },
        edit: function (action, params, callback) {
            var self = this;
            App.Server.defaultServer.PUT(this.url + self.user.get('avatarUrl') + '?clientId=' + App.clientId + '&token=' + new App.Authenticity().getAuthToken(), {
                complete: callback,
                data: {
                    edit: {action: action,
                        actionParams: params
                    }
                },
                dataType: 'json'
            })
        },
        drop: function (callback) {
            var self = this;
            App.Server.defaultServer.DELETE(self.url + self.user.get('avatarUrl') + '?clientId=' + App.clientId + '&token=' + new App.Authenticity().getAuthToken(), {
                complete: callback,
                data: {},
                dataType: 'json'
            })
        }
    });
})(App, _);