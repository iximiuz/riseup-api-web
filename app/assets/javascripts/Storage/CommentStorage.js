(function(App, _) {
	'use strict';

    App.Storage.CommentStorage = App.Storage.RuntimeStorage.extend({
		url: '/comments',

		fetch: function(oCriteria, fnOnSuccess) {
			var self = this;
            App.Server.defaultServer.GET(self.url, {
                data: oCriteria,
                token: new App.Authenticity().getAuthToken(),
				success: fnOnSuccess,
				dataType: 'json'
			});
		},

        save: function(oData, fnOnSuccess) {
            var self = this;
            App.Server.defaultServer.POST(self.url, {
                data: oData,
                token: new App.Authenticity().getAuthToken(),
                success: fnOnSuccess,
                dataType: 'json'
            });
        },

        del: function(oData, fnOnSuccess) {
            var self = this;
            App.Server.defaultServer.DELETE(self.url, {
                data: oData,
                token: new App.Authenticity().getAuthToken(),
                success: fnOnSuccess,
                dataType: 'json'
            });
        },

        setUrl: function(url) {
            var self = this;
            self.url = url;
        }
	});
})(App, _);