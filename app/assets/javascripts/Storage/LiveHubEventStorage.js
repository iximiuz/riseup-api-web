(function(App, _) {
	'use strict';

    App.Storage.LiveHubEventStorage = App.Storage.RuntimeStorage.extend({
		url: function() { return '/live_hub_events/'; },

		fetch: function(oCriteria, fnOnSuccess, fnOnError) {
			var self = this,
                mEvent,
                aEvents = [],
                sUrl = this.url() + '?' +  oCriteria.toQueryString();

			App.Server.defaultServer.GET(sUrl, {
				    success: function(response) {
                        if (response.error) {
                            fnOnError(response);
                            return;
                        }

					    _.each(response, function(oEventAttrs) {
                            mEvent = self._get(oEventAttrs.id);
                            if (mEvent) {
                                mEvent.set(mEvent.parse(oEventAttrs));
                            } else {
                                mEvent = self._set(new App.Model.LiveHubEvent(oEventAttrs));
                            }

                            aEvents.push(mEvent);
					    });

					    fnOnSuccess(aEvents);
				    },
				    dataType: 'json'
			});
		}
	});
})(App, _);