/**
 * Created by dimka3210 on 13.02.14.
 */
(function (App, _) {
    'use strict';

    App.Storage.FileStorage = App.Storage.RuntimeStorage.extend({
        initialize: function (type) {
            this.type = (type) ? type : 'photo';
            this.url = /*App.Server.apiRootUrl + */'/upload/' + this.type;
        },
        save: function (file, callback) {
            var token = new App.Authenticity().getAuthToken();
            var formData = new FormData();
            formData.append(this.type, file);
            App.Server.defaultServer.POST(this.url + '?token=' + token, {
                data: formData,
                type: 'POST',
                contentType: false,
                processData: false,
                complete: callback
            });
        }
    });
})(App, _);