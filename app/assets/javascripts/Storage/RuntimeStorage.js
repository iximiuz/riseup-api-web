(function(ns, Backbone, _) {
	'use strict';

	ns.Storage = ns.Storage || {};

	ns.Storage.Criteria = ns.Class.extend({
		initialize: function() {
			this.m_aPredicates = [];
			this.m_oSort = [];
			this.m_nLimit = null;
			this.m_nOffset = null;
		},

		add: function(sAttrName, mixAttrVal, op) {
			if (mixAttrVal instanceof Date) {
				mixAttrVal = moment(mixAttrVal).format(ns.Storage.Criteria.DATE_FORMAT);
			}

			if (moment.isMoment(mixAttrVal)) {
				mixAttrVal = mixAttrVal.format(ns.Storage.Criteria.DATE_FORMAT);
			}

			this.m_aPredicates.push({attr: sAttrName, val: mixAttrVal, op: op});
		},

		setSort: function(sAttrName, order) {
			this.m_oSort.push({attr: sAttrName, order: order || ns.Storage.Criteria.SORT_ASC});
		},

		setLimit: function(nLimit) {
			this.m_nLimit = nLimit;
		},

		setOffset: function(nOffset) {
			this.m_nOffset = nOffset;
		},

		filtrate: function(oStorage) {
            return oStorage.findAll();
		},

		toQueryString: function() { throw new Error('Not implemented!'); }
	}, {
		DATE_FORMAT: 'YYYY-MM-DD HH:mm:ss',

		SORT_DESC: -1,
		SORT_ASC : 1,

		NON_EQUAL       : 0,
		EQUAL           : 1,
		GREATER         : 2,
		GREATER_OR_EQUAL: 3,
		LESS            : 4,
		LESS_OR_EQUAL   : 5,
		IN              : 6,

		m_oApplyOpSwitch: {
			0: function(attr, expected) { return attr != expected; },
			1: function(attr, expected) { return attr == expected; },
			2: function(attr, expected) {
				if (moment.isMoment(attr)) {
					if (!moment(expected).isValid()) {
						throw new Error('Invalid comparison.');
					}

					return attr.unix() > moment(expected).unix();
				}

				return attr > expected;
			},
			3: function(attr, expected) {
				if (moment.isMoment(attr)) {
					if (!moment(expected).isValid()) {
						throw new Error('Invalid comparison.');
					}

					return attr.unix() >= moment(expected).unix();
				}

				return attr >= expected;
			},
			4: function(attr, expected) {
				if (moment.isMoment(attr)) {
					if (!moment(expected).isValid()) {
						throw new Error('Invalid comparison.');
					}

					return attr.unix() < moment(expected).unix();
				}

				return attr < expected;
			},
			5: function(attr, expected) {
				if (moment.isMoment(attr)) {
					if (!moment(expected).isValid()) {
						throw new Error('Invalid comparison.');
					}

					return attr.unix() <= moment(expected).unix();
				}

				return attr <= expected;
			},
			6: function(attr, expected) { return _.contains(expected, attr); }
		}
	});


	ns.Storage.RuntimeStorage = ns.Class.extend((function() {

		var initialize = function(sModelName) {
			this.m_oById = {};
			this.m_sName = sModelName;

            var self = this;
			ns.M.installTo(this);
            this.sub('auth:logout', function() {
                self.m_oById = {};
            });
		};

		var create = function(oModel, oOptions) {
			var self = this, fnSuccess = oOptions.success, fnError = oOptions.error;


			_set.apply(this, [oModel]);

			oOptions.success = (function(oModel, oldId) {
				return function() {
					fnSuccess.apply(self, arguments);
					_set.apply(self, [oModel, oldId]);
				};
			})(oModel, oModel.cid);

			oOptions.error = function() {
				//TODO: если ошибка сети или 50x, то ничего не делаем, если же ошибка в бизнес-логике при
				//сохранении, то удаляем из runtime storage.
				debugger;
			};

			return Backbone.ajaxSync('create', oModel, oOptions);
		};

		var update = function(oModel, oOptions) {
			if (!oModel.id) {
				throw new Error('Model id can`t be empty when creating.');
			}

			var self = this, fnSuccess = oOptions.success, fnError = oOptions.error;

			oOptions.success = (function(oModel) {
				return function() {
					fnSuccess.apply(self, arguments);
					_set.apply(self, [oModel]);
				};
			})(oModel);

			oOptions.error = function() {
				//TODO: ...
				debugger;
			};

			return Backbone.ajaxSync('update', oModel, oOptions);
		};

		/**
		 * @param oCriteria ns.Storage.Criteria
		 * @return {*}
		 */
		var find = function(oCriteria) {
			return oCriteria.filtrate(this);
		};

		var findAll = function() {
			return this.m_oById;
		};

        var findOne = function(oModel, oOptions) {
            if (oModel.isNew()) {
                return false;
            }

            var self = this, fnSuccess = oOptions.success, fnError = oOptions.error;

            if (this.m_oById[oModel.id]) {
                setTimeout(function() {
                    fnSuccess.apply(self, [self.m_oById[oModel.id].toJSON()]);
                }, 0);
                return true;
            }

            oOptions.success = (function(oModel) {
                return function() {
                    fnSuccess.apply(self, arguments);
                    self.m_oById[oModel.id] = oModel;
                };
            })(oModel);

            oOptions.error = function() {
                //TODO: ...
                debugger;
            };

            return Backbone.ajaxSync('read', oModel, oOptions);
        };

		var destroy = function(oModel, oOptions) {
			if (oModel.isNew()) {
				return false;
			}

			var self = this, fnSuccess = oOptions.success, fnError = oOptions.error;

			oOptions.success = (function(oModel) {
				return function() {
					fnSuccess.apply(self, arguments);
					delete self.m_oById[oModel.id];
				};
			})(oModel);

			oOptions.error = function() {
				//TODO: ...
				debugger;
			};

			return Backbone.ajaxSync('delete', oModel, oOptions);
		};

		/**************************************************************************************************************/

        var _get = function(id) {
            return this.m_oById[id];
        };

		var _set = function(oModel, oldId) {
			if (oldId) {
				delete this.m_oById[oldId]; //TODO: _unset?
			}

			var id = oModel.id || oModel.cid;
			this.m_oById[id] = oModel;
            return oModel;
		};

		var _syncUpdate = function(aModelsAttrs) {
			var self = this, aResult = [];

			_.each(aModelsAttrs, function(oModelAttrs) {
				if (self.m_oById[oModelAttrs.id]) {
					self.m_oById[oModelAttrs.id].set(oModelAttrs);
				} else {
					self.m_oById[oModelAttrs.id] = new ns.Model[self.m_sName](oModelAttrs);
				}

				aResult.push(self.m_oById[oModelAttrs.id]);
			});

			return aResult;
		};
		/**************************************************************************************************************/

		return {
			initialize: initialize,
			create    : create,
			update    : update,
			find      : find,
            findOne   : findOne,
			findAll   : findAll,
			destroy   : destroy,

			_set      : _set,
            _get      : _get,
            __syncUpdate: _syncUpdate
		};
	})(), (function() {
		var oStorages = {};

		var produce = function(sModelName) {
			if (!oStorages[sModelName]) {
				//TODO: явное лучше неявного, добавить static-метод registerStorage(sModelName, oStorage).
				oStorages[sModelName] = ('function' === typeof ns.Storage[sModelName + 'Storage'])
					? new ns.Storage[sModelName + 'Storage'](sModelName)
					: new ns.Storage.RuntimeStorage(sModelName);
			}

			return oStorages[sModelName];
		};

		/**
		 * Сохранить DomainModel или коллекцию DomainModel`ей в RuntimeStorage.
		 *
		 * @param sMethod
		 * @param oModel
		 * @param oOptions - обертка Backbone, обновляющая модель после ответа сервера, вокруг клиентского success и error.
		 */
		var sync = function(sMethod, oModel, oOptions) {
			var oStoreEntry = oModel.storage || oModel.collection.storage,
				oSwitch = {
					read  : function() {
						return (oModel.id != undefined) ? oStoreEntry.findOne(oModel, oOptions) : undefined;
					},
					create: function() {
						return oStoreEntry.create(oModel, oOptions);
					},
					update: function() {
						return oStoreEntry.update(oModel, oOptions);
					},
					delete: function() {
						return oStoreEntry.destroy(oModel, oOptions);
					}
				};

			return oSwitch[sMethod]();
		};

		return {
			produce: produce,
			sync   : sync
		}
	})());
})(App, Backbone, _);