(function(App) {
    App.Model.LiveHubEvent = App.Model.Base.DomainModel.extend({
        urlRoot: function() { return App.Server.apiRootUrl + '/live_hub_events/'; },

        initialize: function(attrs) {
            attrs = attrs || {};

            if (1 == attrs.type || 2 == attrs.type) {
                this.attributes.generator = new App.Model.User(attrs.generator);
                this.attributes.subject = new App.Model.Activity(attrs.subject);
            }

            this.attributes.createdAt = moment(attrs.createdAt);
        },

        isNewActivity: function() {
            return App.Model.LiveHubEvent.TYPE_NEW_ACTIVITY == this.get('type');
        },

        isActivityCompleted: function() {
            return App.Model.LiveHubEvent.TYPE_ACTIVITY_COMPLETED == this.get('type');
        },

        parse: function(response, options) {
            if (1 == response.type || 2 == response.type) {
                response.generator = new App.Model.User(response.generator);
                response.subject = new App.Model.Activity(response.subject);
            }

            response.createdAt = moment(response.createdAt);

            return response;
        }
	}, {
        TYPE_NEW_ACTIVITY: 1,
        TYPE_ACTIVITY_COMPLETED: 2
    });
})(App);