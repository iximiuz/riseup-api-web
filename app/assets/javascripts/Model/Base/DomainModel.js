(function(ns, Backbone, _) {
	'use strict';

	ns.Model.Base = ns.Model.Base || {};

	ns.Model.Base.DomainModel = Backbone.Model.extend({
        changesSaved: function() {
            this.m_oPrevAttrs = {};
        },

        forceSetChangedAttr: function(sAttrName, val) {
            this.m_oPrevAttrs = this.m_oPrevAttrs || {};
            this.m_oPrevAttrs[sAttrName] = val;
        },

		resetChanges: function() {
            if (!this.m_oPrevAttrs) {
                return;
            }

            this.set(this.parse(this.m_oPrevAttrs), {silent: true});
            this.m_oPrevAttrs = {};
            this.changed = null;
            this.trigger('change', this);
        },

        /**
         * Хук для перехвата и сохранения предыдущего значения изменившихся атрибутов.
         *
         * @param args
         * @returns {*}
         */
        change: function(args) {
            this.m_oPrevAttrs = _.clone(this._previousAttributes);
            return Backbone.Model.prototype.change.apply(this, arguments);
        }
	});
})(App, Backbone, _);