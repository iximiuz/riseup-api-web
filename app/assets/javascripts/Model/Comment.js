(function (App) {
    'use strict';

    App.Model.Comment = App.Model.Base.DomainModel.extend({
        storage: App.Storage.RuntimeStorage.produce('Comment'),
        urlRoot: function () {
            return App.Server.apiRootUrl + '/activities/comments';
        }
    })
})(App);