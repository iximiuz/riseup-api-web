(function(ns) {
	'use strict';

	ns.Model.Goal = ns.Model.Base.DomainModel.extend({
		urlRoot: function() { return ns.Server.apiRootUrl + '/goals/'; },

		defaults: {
			completed: false
		},

		initialize: function(attributes, bToFetch) {
            if (bToFetch) {
                return;
            }

            attributes = attributes || {};
            if (!attributes.endDate) {
                throw new Error('Can`t create goal without endDate!')
            }
            if ('string' === typeof attributes.endDate || attributes.endDate instanceof Date) {
                this.attributes.endDate = moment(attributes.endDate);
            }

            if ('string' === typeof attributes.createdAt || attributes.createdAt instanceof Date) {
                this.attributes.createdAt = moment(attributes.createdAt);
            } else if (!attributes.createdAt) {
                this.attributes.createdAt = moment(new Date());
            }

            if (!(moment.isMoment(this.attributes.endDate))) {
                throw new Error(
                    'Unexpected type of endDate [' + typeof  attributes.endDate + ']. Only String or Date allowed.');
            }

            if (!attributes.state) {
                this.attributes.state = 0;
            }

            this.storage = ns.Storage.RuntimeStorage.produce('Goal'); //TODO: м.б. в defaults?
		},

        complete: function() {
            this.set('state', 1);
        },

        isActive: function() {
            return !this.isOverdued() && !this.isCompleted();
        },

        isCompleted: function() {
            return 1 == this.get('state');
        },

        isOverdued: function() {
            return !this.isCompleted() && this.get('endDate').unix() < moment(new Date()).unix();
        },

        toJSON: function() {
            var result = _.clone(this.attributes);
            result.endDate = result.endDate.format('YYYY-MM-DD');
            result.createdAt = result.createdAt.format('YYYY-MM-DD HH:mm:ss');
            return result;
        },

        parse: function(response, options) {
            response.endDate = moment(response.endDate);
            response.createdAt = moment(response.createdAt);
            return response;
        }
	});
})(App);