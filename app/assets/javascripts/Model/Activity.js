(function(App) {
	'use strict';

	App.Model.Activity = App.Model.Base.DomainModel.extend({
		storage: App.Storage.RuntimeStorage.produce('Activity'),
        urlRoot: function() { return App.Server.apiRootUrl + '/activities/'; },

		initialize: function(attributes) {
			if (!attributes.startDate) {
				throw new Error('Can`t create activity without startDate!')
			}

			if ('string' === typeof attributes.startDate || attributes.startDate instanceof Date) {
				this.attributes.startDate = moment(attributes.startDate);
			}

            if ('string' === typeof attributes.startTime || attributes.startTime instanceof Date) {
                this.attributes.startTime = moment(attributes.startTime, 'HH:mm:ss');
            }

            if ('string' === typeof attributes.cdate || attributes.cdate instanceof Date) {
                this.attributes.cdate = moment(attributes.cdate);
            }

			if (!(moment.isMoment(this.attributes.startDate))) {
				throw new Error(
					'Unexpected type of startDate [' + typeof  attributes.startDate + ']. Only String or Date allowed.');
			}

            if (undefined === attributes.state) {
                App.assert(!this.id);

                this.attributes.state = App.Model.Activity.STATE_NEW;
            }
		},

        toJSON: function() {
            var result = _.clone(this.attributes);
            result.startDate = result.startDate.format('YYYY-MM-DD');
            if (result.startTime && moment.isMoment(result.startTime)) {
                result.startTime = result.startTime.format('HH:mm:ss');
            }

            result.cdate = result.cdate.format('YYYY-MM-DD HH:mm:ss');
            return result;
        },

        isEditable: function() {
            var state = this.getState();
            return state !== App.Model.Activity.STATE_OVERDUE && state !== App.Model.Activity.STATE_COMPLETE;
        },

        canBeAppended: function() {
            return (this.get('startDate').unix() >= App.getStartOfToday());
        },

        canBePrepended: function() {
            return (this.get('startDate').unix() >= App.getStartOfToday());
        },

		complete: function() {
			this.set('state', 1);
			return this;
		},

        getState: function() {
            var state = this.get('state'), startDate = this.get('startDate').unix();
            if (App.Model.Activity.STATE_NEW == state || App.Model.Activity.STATE_COMPLETE == state) {
                return state;
            }

            if (startDate < App.getStartOfToday()) {
                return App.Model.Activity.STATE_OVERDUE;
            }

            if (startDate > App.getEndOfToday()) {
                return App.Model.Activity.STATE_IN_PLANS;
            }

            return App.Model.Activity.STATE_IN_PROGRESS;
        },

        hasRevertibleChanges: function() {
            if (_.isEmpty(this.m_oPrevAttrs)) {
                return false;
            }

            return !this.isStartedAt(this.m_oPrevAttrs.startTime)
                || this.get('description') != this.m_oPrevAttrs.description
                || (
                    this.get('goalId') != this.m_oPrevAttrs.goalId && !this.get('goalId') && !this.m_oPrevAttrs.goalId
                );
        },

        isCompleted: function() {
            return this.getState() === App.Model.Activity.STATE_COMPLETE;
        },

        isFailed: function() {
            return this.getState() === App.Model.Activity.STATE_OVERDUE;
        },

        isNewState: function() {
            return this.getState() === App.Model.Activity.STATE_NEW;
        },

        isStartedAt: function(time) {
            var startTime = this.get('startTime');

            if (!time && !startTime) {
                return true;
            }

            if (!time || !startTime) {
                return false;
            }

            if ('string' === typeof time && 'string' === typeof startTime) {
                return time === startTime;
            }

            return (0 === startTime.diff(moment(time, 'HH:mm:ss'), 'minutes'));
        },

		parse: function(response, options) {
            if (response.startDate) {
                response.startDate = moment(response.startDate);
            }

            if (response.startTime) {
                response.startTime = moment(response.startTime, 'HH:mm:ss');
            }

            if (response.cdate) {
                response.cdate = moment(response.cdate);
            }

			return response;
		}
	}, {
        STATE_NEW: -1,
        STATE_IN_PROGRESS: 0,
        STATE_COMPLETE: 1,
        STATE_IN_PLANS: 1000,
        STATE_OVERDUE: 2000
    });
})(App);