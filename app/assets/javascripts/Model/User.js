(function(App) {
    App.Model.User = App.Model.Base.DomainModel.extend({
        storage: App.Storage.RuntimeStorage.produce('User'),
        urlRoot: function() { return App.Server.apiRootUrl + '/users/'; },

        getAvatar: function() {
            return App.Server.picRootUrl + this.get('avatarUrl');
        },

        getPrintableName: function() {
            var sName = this.get('name'),
                sLastName = this.get('lastName'),
                sNick = this.get('nickname');


            return (sName ? (sName + ' ') : '') + (sLastName ? sLastName : '') || sNick;
        },

        isFriend: function() {
            App.assert(this.id != App.currentUser.id);

            return 3 === parseInt(this.get('friendshipStatus'));
        },

        wantToBeFriend: function() {
            App.assert(this.id != App.currentUser.id);

            return 2 === parseInt(this.get('friendshipStatus'));
        }
	});
})(App);