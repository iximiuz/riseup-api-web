(function(App) {
    App.Model.LikeDislike = App.Model.AbstractVote.extend({
        defaults: {
            type: 'likedislike'
        },

        dislike: function(options) {
            this.set('value', -1);
            this._save(null, options);
        },

        isDislike: function() {
            return this.get('value') == -1;
        },

        isLike: function() {
            return this.get('value') == 1;
        },

        like: function(options) {
            this.set('value', 1);
            this._save(null, options);
        }
	});
})(App);