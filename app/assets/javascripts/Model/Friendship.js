(function (App) {

    App.Model.Friendship = App.Model.Base.DomainModel.extend({
        initialize: function() {
            this.token = new App.Authenticity().getAuthToken();
        },

        action: function (status, toUser, appUser) {
            var _key = toUser + "_" + App.currentUser.get('id');
            if (status == 3 || status == 1) { // удаляем из друзей или отзываем свое предложение дружить, посланное ранее
                App.Server.defaultServer.DELETE("/friendship/" + _key + "?clientId=" + App.clientId + "&token=" + this.token, {
                    type: 'DELETE',
                    data: {
                        user_id: toUser
                    },
                    complete: function () {
                        appUser.setFriendshipStatus(0);
                    },
                    dataType: 'json'
                });
            } else if (2 == status) {  // принимаем предложение дружить
                App.Server.defaultServer.PUT("/friendship/"+ _key +"?clientId=" + App.clientId + "&token=" + this.token, {
                    data: {
                        user_id: toUser,
                        status: status
                    },
                    complete: function (data) {
                        var response = JSON.parse(data.response);
                        appUser.setFriendshipStatus(response.status);
                    },
                    dataType: 'json'
                });
            } else if (!status || status == 0) {  // отправляем запрос на добавление в друзья
                App.Server.defaultServer.POST("/friendship/?clientId=" + App.clientId + "&token=" + this.token, {
                    data: {
                        user_id: toUser,
                        status: status
                    },
                    complete: function (data) {
                        var response = JSON.parse(data.response);
                        appUser.setFriendshipStatus(response.status);
                    },
                    dataType: 'json'
                });
            } else {
                App.assert(false);
            }
        },

        declineIncommingOffer: function(nDstUserId, fnCallback, ctx) {
            ctx = ctx || this;

            App.Server.defaultServer.DELETE(
                '/friendship/'+ nDstUserId + '_' + App.currentUser.get('id') + '&token=' + this.token, {
                    data: {
                        user_id: nDstUserId,
                        status: 2
                    },
                    complete: function () {
                        fnCallback.apply(ctx, [0]);
                    },
                dataType: 'json'
            });
        }
    });
})(App);