(function (App, Backbone, _) {

    App.Model.RegistryActivities = App.Model.Base.DomainModel.extend({
        urlRoot: function () {
            return App.Server.apiRootUrl +
                (this.get('isCurUser')
                    ? '/activities/registry'
                    : ('/users/' + this.get('userId') + '/activities/registry'));
        },

        initialize: function(attrs) {
            this.m_aRecords = {};
        },

        dayHasActivities: function(sDate) {
            return !!this.m_aRecords[sDate];
        },

        getEarliestDayAfter: function(sDate, offset) {
            offset = offset || 1;

            var aRecords = this.get('records'),
                r = App.Utils.findOneOrNearest(aRecords, sDate, true, function(el) { return el.date; }, true);
            if (!r || !r.el) {
                return null;
            }

            return (r.idx + offset >= aRecords.length) ? aRecords[aRecords.length - 1] : aRecords[r.idx + offset];
        },

        /**
         * Возвращает день, на котором существуют активности,
         * с наиболее старой датой в промежутке между sDate (включительно)
         * и самым старым днем за всю историю использования сервиса,
         * удаленный от sDate на <= offset дней с активностями.
         *
         * @param sDate
         * @param offset
         * @returns {*}
         */
        getOldestDayFrom: function(sDate, offset) {
            offset = offset || 1;

            var aRecords = this.get('records'),
                r = App.Utils.findOneOrNearest(aRecords, sDate, true, function(el) { return el.date; });
            if (!r || !r.el) {
                return null;
            }

            return (r.idx - offset < 0) ? aRecords[0] : aRecords[r.idx - offset];
        },

        getDaysBetween: function(sFrom, sTo) {
            App.assert(sFrom <= sTo);
            if (sFrom >= sTo) {
                return [];
            }

            var aRecords = this.get('records'),
                oFrom = sFrom ? App.Utils.findOneOrNearest(aRecords, sFrom, true, function(el) { return el.date; }, true) : null,
                oTo = sTo ? App.Utils.findOneOrNearest(aRecords, sTo, true, function(el) { return el.date; }) : null,
                nStartIdx = oFrom ? oFrom.idx : 0,
                nEndIdx = oTo ? oTo.idx : aRecords.length - 1;

            if (!oFrom || !oTo || oFrom.el.date > oTo.el.date) {
                return [];
            }

            return aRecords.slice(nStartIdx, nEndIdx + 1);
        },

        hasDaysAfter: function(sDate) {
            var oEarliestDay = this.getEarliestDayAfter(sDate);
            return null !== oEarliestDay && sDate !== oEarliestDay.date;
        },

        hasDaysBefore: function(sDate) {
            var oOldestDay = this.getOldestDayFrom(sDate);
            return null !== oOldestDay && sDate !== oOldestDay.date;
        },

        parse: function(response, options) {
            _.each(response.records, function(record, idx) {
                this.m_aRecords[record.date] = {count: record.count, idx: idx}
            }, this);

            return response;
        }
    });
})(App, Backbone, _);