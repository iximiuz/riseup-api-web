(function(App) {
    App.Model.AbstractVote = App.Model.Base.DomainModel.extend({
        urlRoot: function() {
            return App.Server.apiRootUrl + '/voting/1/objects/'
                + this.get('objectType') + '/' + this.get('objectId') + '/votes';
        },

        initialize: function(attrs) {
            attrs = attrs || {};

            this._save = this.save;
            this.save = function() {
                throw new Error('You should not call save for vote model directly.');
            };
            this.set('createdAt', attrs.createdAt || new Date());
        }
	});
})(App);