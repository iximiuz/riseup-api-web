var TplHelper = TplHelper || {};
(function() {
	TplHelper = function() {
		var oTplCache = {};

		function loadTpl(sTplName, sTplDir) {
			if (!oTplCache[sTplName]) {
				sTplDir = sTplDir || 'assets';

				 var sTplUrl = App.Server.origin + '/' + sTplDir + '/' + sTplName + '.html'
                     + (App.debug ? '?z=' + new Date().getTime() : '');

				$.ajax({
					url    : sTplUrl,
					method : 'GET',
					async  : false,
					dataType: 'html',
					success: function(data) {
						oTplCache[sTplName] = data;
					}
				});
			}

			return oTplCache[sTplName];
		}


		return {
			getTplHtml: loadTpl
		}
	}();
})();