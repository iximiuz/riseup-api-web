(function(App) {
    'use strict';

    App.Layout.BaseLayout = App.View.BaseView.extend({
        renderPopup: function (vPopup) {
            this.$el.after(vPopup.render().el);
            vPopup.afterInsertEl();
        }
    });
})(App);