(function (App, _) {
    'use strict';

    App.Layout.Mobile.MainLayout = App.Layout.BaseLayout.extend({
        className: 'main-layout',
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/MainLayout'))(args);
        },

        events: {
            'click [data-action="logout"]': '_logout',
            'click [data-route="timeline"]': 'goToTimeline',
            'click [data-route="live-hub"]': 'goToLiveHub',
            'click [data-route="rises"]': 'goToRises',
            'click [data-route="friends"]': 'goToFriendsList',
            'click [data-route="riseupers"]': 'goToUsersList',
            'click [data-route="profile"]': 'goToProfile',
            'click [data-route="calendar"]': 'goToCalendar',
            'click [data-route="feedback"]': 'goToFeedbackForm',
            'click .main-layout__title': '_onClickTitle',
            'click .hint-area__clear': '_clearHintText'
        },

        hints: [
            "Just Frickin' Do It!",
            'Когда один глаз направлен на конечную цель, остается лишь один, чтобы направлять тебя в странствовании. Сконцентрируйся только на решении ближайшей задачи.',
            'Если у тебя есть мечта - у тебя есть и соответствующая способность реализовать ее.',
            'Либо ты контролируешь своё настроение, либо оно берёт власть над тобой.',
            'Герои - это люди, которые сделали что надо когда надо независимо от последствий.',
            'Иногда ты ожидаешь пинка в спину, а тебе протягивают руку помощи.',
            'Никогда нельзя говорить ребёнку, что его мечты не сбудутся - это жестоко и трагично, особенно если он поверит.'
        ],

        initialize: function() {
            App.M.installTo(this);
            this.sub('show-debug-hint', this.showHint);
            this.sub('window:resize', this._fitToScreen);
        },

        render: function () {
            this.$el.html(
                this.template({
                    title: 'Загрузка...',
                    authenticated: !!App.currentUser,
                    debug: App.debug
                })
            );

            return this;
        },

        afterInsertEl: function() {
            this._fitToScreen();
        },

        goToCalendar: function() {
            App.navigate('calendar', true);
        },
        
        goToFeedbackForm: function () {
            App.navigate('feedback', true);
        },

        goToFriendsList: function() {
            App.navigate('friends', true);
        },

        goToLiveHub: function() {
            App.navigate('live', true);
        },

        goToProfile: function() {
            App.navigate('profile', true);
        },

        goToTimeline: function() {
            App.navigate('timeline', true);
        },

        goToRises: function() {
            App.navigate('rises', true);
        },

        goToUsersList: function() {
            App.navigate('riseupers', true);
        },

        noBumpers: function() {
            this.$el.addClass('full-width');
        },

        renderContent: function (vContent, sTitle, options) {
            this.m_vContent = vContent;

            this._getTitle().text(sTitle);

            try {
                this._getContentHolder().html(vContent.render().el);
                vContent.afterInsertEl();
            } catch(e) {
                this._getContentHolder().html('Произошла ошибка. Попробуйте обновить страницу.');
                throw e;
            }

            this.$el.addClass('page-' + (vContent.className || 'default'));

            options = options || {};
            if (!options.noHint) {
                this.tryRenderHint();
            }
        },

        showHint: function(sMessage, options) {
            options = options || {};

            this._getHintTextHolder()[options.append ? 'append' : 'text'](sMessage);
            this._getHintArea().show();
        },

        tryRenderHint: function () {
            var hintShowedAt = parseFloat(localStorage.getItem('hint')), now = moment(new Date()).unix(),
                self = this;
            if (!hintShowedAt || hintShowedAt + 600 < now) {

                setTimeout(function() {
                    self._getHintTextHolder().text(self.hints[_.random(0, self.hints.length - 1)]);
                    self._getHintArea().show();
                    localStorage.setItem('hint', now);
                }, 10000);
            }
        },

        /**************************************************************************************************************/

        _clearHintText: function() {
            this._getHintTextHolder().text('');
        },

        _fitToScreen: function() {
            this.$el.css('min-height', App.getWindow().height())
        },

        _getContentHolder: function () {
            return this.getElement('.main-layout__content');
        },

        _getHintTextHolder: function () {
            return this.getElement('.hint-area_text');
        },

        _getHintArea: function () {
            return this.getElement('.hint-area');
        },

        _getTitle: function() {
            return this.getElement('.main-layout__title');
        },

        _onClickTitle: function() {
            this.pub('title-click:' + this.m_vContent.cid);
        },

        _logout: function() {
            (new App.Authenticity(App.Server.defaultServer, '/auth_tokens')).logout();
        }
    });
})(App, _);