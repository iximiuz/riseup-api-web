(function (App, _) {
    'use strict';

    App.Layout.Mobile.WelcomeLayout = App.Layout.BaseLayout.extend({
        className: 'welcome-layout',
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/WelcomeLayout'))(args);
        },

        render: function () {
            this.$el.html(
                this.template({
                    authenticated: !!App.currentUser,
                    debug: App.debug
                })
            );

            return this;
        },
        
        renderContent: function (vContent) {
            this.m_vContent = vContent;

            try {
                this._getContentHolder().html(vContent.render().el);
                vContent.afterInsertEl();
            } catch(e) {
                this._getContentHolder().html('Произошла ошибка. Попробуйте обновить страницу.');
                throw e;
            }

            this.$el.addClass('page-' + (vContent.className || 'default'));
        },

        /**************************************************************************************************************/

        _getContentHolder: function () {
            return this.getElement('.welcome-layout__content');
        }
    });
})(App, _);