(function (App, _) {
    'use strict';

    App.Layout.Mobile.HeadlessLayout = App.Layout.BaseLayout.extend({
        className: 'headless-layout',
        template: function (args) {
            return _.template(TplHelper.getTplHtml('mobile/HeadlessLayout'))(args);
        },

        events: {
            'click .headless-layout__side-bar': '_onClickSideBar'
        },

        initialize: function() {
            App.M.installTo(this);
            this.sub('window:resize', this._fitToScreen);
            this.sub('layout:hide-side-bar', this._hideSideBar);
            this.sub('layout:restore-side-bar', this._restoreSideBar);
        },

        render: function () {
            this.$el.html(
                this.template({
                    authenticated: !!App.currentUser,
                    debug: App.debug
                })
            );

            return this;
        },

        afterInsertEl: function() {
            this._fitToScreen();

            var self = this;
            Hammer(this.el).on('swipeleft', function() { self._onClickSideBar(); });
            Hammer(this.el).on('swiperight', function() { if (self.$el.hasClass('hide-side-bar')) { self._onClickSideBar(); } });
        },
        
        renderContent: function (vContent) {
            this.m_vContent = vContent;

            try {
                this._getContentHolder().html(vContent.render().el);
                vContent.afterInsertEl();
            } catch(e) {
                this._getContentHolder().html('Произошла ошибка. Попробуйте обновить страницу.');
                throw e;
            }

            this.$el.addClass('page-' + (vContent.className || 'default'));
        },

        /**************************************************************************************************************/

        _getContentHolder: function () {
            return this.getElement('.headless-layout__content');
        },

        _getSideBar: function () {
            return this.getElement('.headless-layout__side-bar');
        },

        _fitToScreen: function() {
            var nHeight = App.getWindow().height();
            this.$el.css('min-height', nHeight);
            this._getContentHolder().css('min-height', nHeight);
        },

        _hideSideBar: function() {
            this.$el.addClass('hide-side-bar');
        },

        _onClickSideBar: function() {
            this.pub('side-bar-click:' + this.m_vContent.cid);
        },

        _restoreSideBar: function() {
            this.$el.removeClass('hide-side-bar');
        }
    });
})(App, _);