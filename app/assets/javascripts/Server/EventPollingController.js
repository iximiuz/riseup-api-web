(function(ns, _) {
	'use strict';

	ns.Server.EventPollingController = ns.Class.extend(function() {
		var initialize = function(oServer) {
				ns.M.installTo(this);

				this.m_oServer = oServer;
				this.m_nEtag = 0;
				this.m_sLastModifiedTime = moment.utc(new Date());
				this.m_oHandlers = {};
			},

			processEvents = function(data) {
				var self = this, aHandlers;

				_.each(data, function(eventData, sEvent) {
					if (self.m_oHandlers[sEvent]) {
						aHandlers = self.m_oHandlers[sEvent];
						_.each(aHandlers, function(oEntry) {
							oEntry.handler.apply(oEntry.context, [eventData]);
						});
					}
				});
			},

			poll = function(sUrl) {
				var self = this;

				this.m_oServer.GET(sUrl, {
					headers : {
						'If-None-Match'    : this.m_nEtag,
						'If-Modified-Since': this.m_sLastModifiedTime
					},
					success : function(data, textStatus, jqXHR) {
						processEvents.apply(self, [data]);

						self.m_nEtag = jqXHR.getResponseHeader('Etag');
						self.m_sLastModifiedTime = jqXHR.getResponseHeader('Last-Modified');
						poll.apply(self, [sUrl]);
					},
					error   : function(jqXHR, textStatus, errorThrown) {
						if ('timeout' === textStatus) {
							poll.apply(self, [sUrl]);
							return;
						}

						//TODO: this.pub('failure:event-polling-controller', jqXHR, textStatus, errorThrown);
						console.log(arguments);
					},
					data: {
						v: (new Date()).getTime()
					},
					dataType: 'JSON'
				});
			},

			start = function(sUrl) {
				poll.apply(this, [sUrl]);
			},

			registerHandler = function(sEvent, fnHandler, context) {
				this.m_oHandlers[sEvent] = this.m_oHandlers[sEvent] || [];
				this.m_oHandlers[sEvent].push({handler: fnHandler, context: context});
			};
		return {
			initialize: initialize,
			start     : start,
			registerHandler: registerHandler
		};
	}());
})(App, _);