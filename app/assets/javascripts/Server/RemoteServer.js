(function(App, $) {
	'use strict';

    App.Server.RemoteServer = App.Class.extend(function() {
		return {
			GET: function(sUrl, oOptions) {
				oOptions = oOptions || {};
                oOptions.url = App.Server.apiRootUrl + sUrl;
				oOptions.type = 'GET';
				oOptions.data = oOptions.data || {};
				oOptions.data.clientId = App.clientId;

                if (App.currentUser) {
                    var authenticity = new App.Authenticity();
                    oOptions.data.token = authenticity.getAuthToken()
                }


                oOptions.crossDomain = true;

				$.ajax(oOptions);
			},

			POST: function(sUrl, oOptions) {
				oOptions = oOptions || {};
                oOptions.url = App.Server.apiRootUrl + sUrl;
				oOptions.type = 'POST';
				oOptions.data = oOptions.data || {};
				oOptions.data.clientId = App.clientId;

                if (App.currentUser) {
                    var authenticity = new App.Authenticity();
                    oOptions.data.token = authenticity.getAuthToken()
                }


                oOptions.crossDomain = true;

				$.ajax(oOptions)
			},

            DELETE: function(sUrl, oOptions) {
                oOptions = oOptions || {};
                oOptions.url = App.Server.apiRootUrl + sUrl;
                oOptions.type = 'DELETE';
                oOptions.data = oOptions.data || {};
                oOptions.data.clientId = App.clientId;

                if (App.currentUser) {
                    var authenticity = new App.Authenticity();
                    oOptions.data.token = authenticity.getAuthToken()
                }


                oOptions.crossDomain = true;

                $.ajax(oOptions)
            },

            PUT: function(sUrl, oOptions) {
                oOptions = oOptions || {};
                oOptions.url = App.Server.apiRootUrl + sUrl;
                oOptions.type = 'PUT';
                oOptions.data = oOptions.data || {};
                oOptions.data.clientId = App.clientId;

                if (App.currentUser) {
                    var authenticity = new App.Authenticity();
                    oOptions.data.token = authenticity.getAuthToken()
                }


                oOptions.crossDomain = true;

                $.ajax(oOptions)
            }
		};
	}(), (function() {
		var m_oInstance = null;

		var getInstance = function() {
			if (null === m_oInstance) {
				m_oInstance = new App.Server.RemoteServer();
			}

			return m_oInstance;
		};

		return {
			getInstance: getInstance
		};
	}()));
})(App, $);