(function (ns) {
    ns.DataProviderDispatcher = ns.Class.extend((function () {
        var oDataProviders = {};

        var init = function () {
                ns.M.installTo(this);

                this.sub('polling:start', dispatchStartPolling);
                this.sub('polling:stop', dispatchStopPolling);

                this.sub('models:find', dispatchFindModels);
            },

            dispatchStartPolling = function (args) {
                if (!args || !args.model || !oDataProviders[args.model]) {
                    throw new Error('Wrong polling:start arguments.');
                }

                oDataProviders[args.model].startPolling(args);
            },

            dispatchStopPolling = function (args) {
                console.log('dispatchStopPolling', args);
            },

            dispatchFindModels = function (args) {
                if (!args || !args.model || !oDataProviders[args.model]) {
                    throw new Error('Wrong models:get arguments.');
                }

                oDataProviders[args.model].findModels(args);
            },

            registerDataProvider = function (sModelName, oProvider) {
                oDataProviders[sModelName] = oProvider;
            };

        return {
            initialize: init,
            registerDataProvider: registerDataProvider
        };
    })());


    ns.DataProvider = ns.Class.extend({

        initialize: function (attributes) {
            ns.M.installTo(this);

            attributes = attributes || {};
            if (!attributes.server || !attributes.pollingProcessor) {
                throw new Error('Server or EventPollingProcessor not found.');
            }

            this.m_oServer = attributes.server;
            this.m_oPP = attributes.pollingProcessor;
        }
    });


    ns.ActivityDataProvider = ns.DataProvider.extend((function () {
        var sModelName = 'Activity';

        var findModels = function (args) {
                var params = args.criteria || {}, oStorage, self = this, oCriteria;

                if (!args.channel) {
                    throw new Error('Channel not defined.');
                }

                oCriteria = _makeCriteria.apply(this, [params]);
                if (!oCriteria) {
                    throw new Error('Not implemented!');
                }

                oStorage = ns.Storage.RuntimeStorage.produce(sModelName);
                oStorage.fetch(
                    oCriteria,
                    function (aActivities) { self.pub(args.channel, aActivities); },
                    function (response) { self.pub(args.channel, response); });
            },

            _makeCriteria = function (oParams) {
                if (!oParams.userId) {
                    return null;
                }

                return new (ns.Storage.Criteria.extend({
                    initialize: function (attrs) {
                        attrs = attrs || {};
                        this.m_nUserId = attrs.userId;
                        this.m_tDate = attrs.date;
                        this.m_tFrom = attrs.from;
                        this.m_tTo = attrs.to;
                        this.m_sActivityId = attrs.activityId;
                        this.m_nBefore = attrs.acount;
                        this.m_nAfter = attrs.bcount;
                        this.m_bDayAlign = attrs.dayAlign;
                    },

                    getUserId: function() {
                        return this.m_nUserId;
                    },

                    toQueryString: function () {
                        return (this.m_tFrom && this.m_tTo)
                            ? ('from=' + this.m_tFrom.format('YYYY-MM-DD') + '&to=' + this.m_tTo.format('YYYY-MM-DD'))
                            : ((this.m_sActivityId ? ('aid=' + this.m_sActivityId) : 'date=' + this.m_tDate.format('YYYY-MM-DD'))
                                + '&acount=' + this.m_nAfter + '&bcount=' + this.m_nBefore
                                + (this.m_bDayAlign ? '&day_align=true' : ''));
                    }
                }))(oParams);
            };

        return {
            findModels: findModels
        };
    })());


    ns.UsersDataProvider = ns.DataProvider.extend((function () {
        var sModelName = 'User';

        var findModels = function (args) {
                var params = args.criteria || {}, oStorage, self = this, oCriteria;

                if (!args.channel) {
                    throw new Error('Channel not defined.');
                }

                oCriteria = _makeCriteria.apply(this, [params]);
                oCriteria.model = args.model;

                if (!oCriteria) {
                    throw new Error('Not implemented!');
                }

                oStorage = ns.Storage.RuntimeStorage.produce(sModelName);
                oStorage.fetch(oCriteria, function (aUsers) {
                    self.pub(args.channel, aUsers);
                });
            },

            _makeCriteria = function (oParams) {
                oParams = oParams || {};

                return new (ns.Storage.Criteria.extend({
                    initialize: function (attrs) {
                        attrs = attrs || {};
                        this.m_sSort = attrs.sort;
                        this.m_nLimit = attrs.limit;
                        this.m_nOffset = attrs.offset;
                    },

                    toQueryString: function () {
                        var sResult = '';

                        if (undefined !== this.m_sSort) {
                            sResult += 'sort=' + this.m_sSort;
                        }

                        if (undefined !== this.m_nLimit) {
                            sResult += (sResult.length ? '&' : '') + 'limit=' + this.m_nLimit;
                        }

                        if (undefined !== this.m_nOffset) {
                            sResult += (sResult.length ? '&' : '') + 'offset=' + this.m_nOffset;
                        }

                        return sResult;
                    }
                }))(oParams);
            };

        return {
            findModels: findModels
        };
    })());
})(App);
