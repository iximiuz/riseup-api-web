(function (App, _) {
    'use strict';

    App.Help = App.Class.extend({
        m_oCache: false,

        initialize: function () {
            var help = this._getHelpBackground();
            help.on('click', function () {
                help.hide();
            });
        },

        getElement: function (sSelector) {
            this.m_oCache = this.m_oCache || {};
            if (!this.m_oCache[sSelector]) {
                this.m_oCache[sSelector] = $(sSelector);
            }
            return this.m_oCache[sSelector];
        },

        _getHelpBackground: function () {
            return this.getElement('.main-layout__helper-background');
        },

        _getHelpContent: function () {
            return this.getElement('.main-layout__helper-modal');
        },

        show: function () {
            var bgWidth = window.innerWidth;
            var bgHeight = window.innerHeight;
            var contentWidth = this.getElement('.main-layout').css('width');
            var contentHeight = this.getElement('.main-layout').css('height');

            console.log(this._getHelpContent());

            // Render
            this._getHelpContent().css({width: parseInt(contentWidth) + "px", height: (parseInt(contentHeight) - 40) + "px"});
            this._getHelpBackground().css({width: bgWidth + "px", height: bgHeight + "px"}).show();

        },

        setText: function (text) {
            this._getHelpContent().text(text);
        },

        setHtml: function (html) {
            this._getHelpContent().html(html);
        }

    });
})(App, _);