(function (App) {
    App.GoalDataProvider = App.DataProvider.extend((function () {
        var sModelName = 'Goal';

        var findModels = function (args) {
                var params = args.criteria || {}, oStorage, self = this, oCriteria;

                if (!args.channel) {
                    throw new Error('Channel not defined.');
                }

                oCriteria = _makeCriteria.apply(this, [params]);
                if (!oCriteria) {
                    throw new Error('Not implemented!');
                }

                oStorage = App.Storage.RuntimeStorage.produce(sModelName);
                oStorage.fetch(oCriteria, function (aGoals) {
                    self.pub(args.channel, aGoals);
                });
            },

            _makeCriteria = function (oParams) {
                oParams = oParams || {};

                if (undefined === oParams.sort || undefined === oParams.limit || undefined === oParams.offset) {
                    return null;
                }

                return new (App.Storage.Criteria.extend({
                    initialize: function (attributes) {
                        attributes = attributes || {};
                        this.m_sSort = attributes.sort;
                        this.m_nLimit = attributes.limit;
                        this.m_nOffset = attributes.offset;
                        this.m_sFilter = attributes.filter;
                    },

                    toQueryString: function () {
                        return 'sort=' + this.m_sSort + '&limit=' + this.m_nLimit + '&offset=' + this.m_nOffset + '&filter=' + this.m_sFilter;
                    }
                }))(oParams);
            };


        return {
            findModels: findModels
        };
    })());
})(App);
