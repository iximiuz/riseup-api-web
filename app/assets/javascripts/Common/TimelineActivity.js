(function(App) {
    App.TimelineActivity = App.Class.extend({
        initialize: function() {
            App.M.installTo(this);
            this.sub('acty-stream:load', this.load);
        },

        load: function(attrs) {
            App.assert(attrs);

            this.pub('models:find', {
                model   : 'Activity',
                channel : attrs.channel,
                criteria: {
                    activityId: attrs.activityId,
                    acount: attrs.acount,
                    bcount: attrs.bcount,
                    date: attrs.date,
                    from: attrs.from,
                    to: attrs.to,
                    userId : attrs.userId,
                    dayAlign: attrs.dayAlign
                }
            });
        }
    });
})(App);