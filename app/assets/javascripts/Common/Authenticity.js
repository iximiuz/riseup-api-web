(function(App, _) {
    App.Authenticity = App.Class.extend({
        initialize: function(oServer, sUrl) {
            App.M.installTo(this);

            this.m_oServer = oServer;
            this.m_sUrl = sUrl;
        },

        login: function(email, password) {
            var self = this;

            this.m_oServer.GET(this.m_sUrl + '/new', {
                data: {
                    email: email,
                    password: password
                },

                success: function(response) {
                    response = response || {};
                    if (response.user) {
                        App.setUser(new App.Model.User(response.user));
                        self.setAuthToken(response.token);
                        self.notifyOnAuth();
                    } else {
                        self.notifyOnAuthFail(response);
                    }
                },
                dataType: 'json'
            });
        },

        logout: function() {
            var self = this;

            this.m_oServer.DELETE(this.m_sUrl + '/' + this.getAuthToken(), {
                success: function(response) {
                    response = response || {};
                    if (!response.error) {
                        App.setUser(null);
                        self.removeAuthToken();
                        self.notifyOnLogout();
                    } else {
                        self.notifyOnAuthFail(response);
                    }
                },
                dataType: 'json'
            });
        },

        register: function(email, password) {
            var self = this;

            this.m_oServer.POST(this.m_sUrl, {
                data: {
                    email: email,
                    password: password
                },

                success: function(response) {
                    response = response || {};
                    if (response.user) {
                        App.setUser(new App.Model.User(response.user));
                        self.setAuthToken(response.token);
                        self.notifyOnAuth();
                    } else {
                        self.notifyOnAuthFail(response);
                    }
                },
                dataType: 'json'
            });
        },

        getAuthToken: function() {
            var token = $.fn.cookie('auth');
            return token;
        },

        setAuthToken: function(sToken) {
            $.fn.cookie('auth', sToken, {expires: 14});
        },

        removeAuthToken: function() {
            $.fn.cookie('auth', null);
        },

        notifyOnAuth: function() {
            this.pub('auth:login');
        },

        notifyOnLogout: function() {
            this.pub('auth:logout');
        },

        notifyOnAuthFail: function(response) {
            response = response || {};
            if (1014 == response.code) {
                response.error = 'Неверный формат email';
            }

            if (1003 == response.code) {
                response.error = 'Неверный пароль или пользователь';
            }

            if (1010 == response.code) {
                response.error = 'Такой пользователь не зарегистрирован';
            }

            if (1011 == response.code) {
                response.error = 'Такой пользователь уже зарегистрирован';
            }

            this.pub('auth:fail', response.error ? response.error : 'Неожиданная ошибка');
        }
    });
})(App, _);