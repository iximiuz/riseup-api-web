// main namespaces
var App = {
	View        : {
        Common: {},
        Mobile: {
            Admin: {}
        }
    },
	Model       : {},
	Collection  : {},
	Server      : {},
	DataProvider: {},
    Layout      : {
        Mobile: {}
    },
    PageController: {
        Mobile: {}
    },

    initialize: function(oRouter) {
        this.router = oRouter;

        var self = this;
        this.M.sub('auth:login', function() {
            self.navigate('timeline', true);
        });

        this.M.sub('auth:logout', function() {
            self.navigate('login', true);
        });

        $(window).on('resize', function() {
            //TODO: !improve! сделать подписывание на это событие только по требованию и отписывание,
            // когда ни остется ни одного подписчика с целью увеличения производительности
            self.M.pub('window:resize');

            this.m_nWindowHeight = null;
        });

        moment.lang('ru', MOMENT_RU);

        this.m_oMutexIntervals = {};

        $(window).on('scroll', function(e) {
            self.M.pub('window:scroll', e)
        });
    },

    assert: function (condition, message) {
        if (App.debug && !condition) {
            debugger;
            throw new Error(message || 'Assertion failed');
        }
    },

    startHistory: function() {
        Backbone.history.start({pushState: 'pushState' in window.history});
    },

    navigate: function(sRoute, triggerRouteEvent, withQueryString) {
        triggerRouteEvent = !!triggerRouteEvent;

        if (Backbone.history.fragment === sRoute && triggerRouteEvent) {
            // Хак для возможности нажимать дважды на один и тот же URL и вызывать при этом обработчик
            this.router.navigate('dummy' + window.location.search);
        }

        if (withQueryString) {
            sRoute += window.location.search;
        }

        this.router.navigate(sRoute, {trigger: triggerRouteEvent});
    },

    getBody: function() {
        if (!this.m_$body) {
            this.m_$body = $('body');
        }

        return this.m_$body;
    },

    getController: function() {
        return this.m_oController;
    },

    now: function() {
        return moment(new Date());
    },

    getEndOfToday: function(asMoment) {
        if (asMoment) {
            return moment(new Date()).endOf('day');
        }

        return moment(new Date()).endOf('day').unix();
    },

    getStartOfToday: function(asMoment) {
        if (asMoment) {
            return moment(new Date()).startOf('day');
        }

        return moment(new Date()).startOf('day').unix();
    },

    getWindow: function() {
        if (!this.m_$window) {
            this.m_$window = $(window);
        }

        return this.m_$window;
    },

    getWindowHeight: function() {
        if (!this.m_nWindowHeight) {
            this.m_nWindowHeight = this.getWindow().height();
        }

        return this.m_nWindowHeight;
    },

    getURLParameter: function(name) {
        return decodeURIComponent(
            (new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20')) || null;
    },

    setController: function(controller) {
        this.m_oController = controller;
    },

    initTimeline: function() {
        if (!this.m_oTimeline) {
            this.m_oTimeline = new App.TimelineActivity();
        }
    },

    setUser: function(mUser) {
        this.currentUser = mUser;
    },

    startMutexInterval: function(id, fnCallback, nPeriod) {
        this.stopMutexInterval(id);
        this.m_oMutexIntervals[id] = setInterval(fnCallback, nPeriod);
    },

    stopMutexInterval: function(id) {
        if (this.m_oMutexIntervals[id]) {
            clearInterval(this.m_oMutexIntervals[id]);
            this.m_oMutexIntervals[id] = null;
        }
    },

    isIphone: function() {
        if (void 0 === this.m_bIsIphone) {
            this.m_bIsIphone = (navigator.platform.indexOf("iPhone") != -1) ||
                (navigator.platform.indexOf("iPod") != -1);
        }

        return this.m_bIsIphone;
    },

    isIpad: function() {
        if (void 0 === this.m_bIsIpad) {
            this.m_bIsIpad = navigator.userAgent.match(/iPad/i) != null;
        }

        return this.m_bIsIpad;
    },

    saveWindowScrollPos: function() {
        this.m_nSavedScrollTop = window.scrollY;
        this.m_nSavedTotalHeight = document.documentElement.scrollHeight;
    },

    scrollWindowToAnimated: function(to, duration, fnCallback) {
        if (duration <= 0) {
            if ('function' === typeof fnCallback) {
                fnCallback();
            }

            return;
        }

        var difference = to - window.scrollY;
        var perTick = difference / duration * 10;

        setTimeout(function() {
            window.scrollTo(0, window.scrollY + perTick);
            App.scrollWindowToAnimated(to, duration - 10, fnCallback);
        }, 10);
    },

    restoreWindowScrollPos: function() {
        window.scrollTo(0, this.m_nSavedScrollTop + document.documentElement.scrollHeight - this.m_nSavedTotalHeight);
    }
};