(function(App) {
	var Mediator = App.Class.extend(function() {
		var oChannels = {}, nReservedCounter = 0, sReserveChannelPattern = 'channel:reserved';

		var subscribe = function(sChannel, fn) {
                App.assert(_.isFunction(this.subscriberId));

                //console.log('__SUB__: [ ' + sChannel + ' ]', {callback: fn}, {trace: (new Error).stack});

				if (!oChannels[sChannel]) {
					oChannels[sChannel] = {};
				}

				oChannels[sChannel][this.subscriberId()] = {context: this, callback: fn};
				return this;
			},

			publish = function(sChannel) {
				//console.log(
                //    '~~PUB~~: [ ' + sChannel + ' ]', Array.prototype.slice.call(arguments, 1), {trace: (new Error).stack}
                //);

				if (!oChannels[sChannel]) {
					return false;
				}

				var args = Array.prototype.slice.call(arguments, 1), subscription;
				for (var s in oChannels[sChannel]) {
                    if (oChannels[sChannel].hasOwnProperty(s)) {
                        subscription = oChannels[sChannel][s];
                        subscription.callback.apply(subscription.context, args);
                    }
				}
				return this;
			},

			reserveChannel = function(pattern) {
				var sResult = pattern || sReserveChannelPattern;
				return sResult + ':' + nReservedCounter++;
			};

		return {
			pub           : publish,
			sub           : subscribe,
			reserveChannel: reserveChannel,
			installTo     : function(obj) {
                App.assert(_.isFunction(obj.subscriberId));

				obj.sub = subscribe;
				obj.pub = publish;
				obj.reserveChannel = reserveChannel;
			}
		};
	}());

	App.M = new Mediator();
})(App);