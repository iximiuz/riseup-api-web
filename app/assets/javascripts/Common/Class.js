(function(ns, _) {
	'use strict';

	ns.Class = function() {
		this.initialize = this.initialize || function() {};
		this.initialize.apply(this, arguments);
	};

	//Backbone extend method
	ns.Class.extend = function(protoProps, staticProps) {
		var parent = this;
		var child;

        protoProps = protoProps || {};
        protoProps.subscriberId = function() {
            if (!this.m_sSubscriberId) {
                this.m_sSubscriberId = 'base-object:' + ++ns.Class.OBJ_COUNTER;
            }

            return this.m_sSubscriberId;
        };

		// The constructor function for the new subclass is either defined by you
		// (the "constructor" property in your `extend` definition), or defaulted
		// by us to simply call the parent's constructor.
		if (protoProps && _.has(protoProps, 'constructor')) {
			child = protoProps.constructor;
		} else {
			child = function() {
				parent.apply(this, arguments);
			};
		}

		// Add static properties to the constructor function, if supplied.
		_.extend(child, parent, staticProps);

		// Set the prototype chain to inherit from `parent`, without calling
		// `parent`'s constructor function.
		var Surrogate = function() {
			this.constructor = child;
		};
		Surrogate.prototype = parent.prototype;
		child.prototype = new Surrogate;

		// Add prototype properties (instance properties) to the subclass,
		// if supplied.
		if (protoProps) {
			_.extend(child.prototype, protoProps);
		}

		// Set a convenience property in case the parent's prototype is needed
		// later.
		child.__super__ = parent.prototype;

		return child;
	};

    ns.Class.OBJ_COUNTER = 0;
})(App, _);