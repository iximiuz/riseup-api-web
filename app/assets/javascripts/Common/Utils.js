(function(App, _) {
    'use strict';

    App.Utils = App.Utils || {};

    App.Utils.findOneOrNearest = function(mixCollection, mixEl, isSorted, comparator, greatest) {
        var isArray = _.isArray(mixCollection);
        if (!isArray && _.isObject(mixCollection)) {
            App.assert(false);
            return null;
        }

        var aCollection = !isArray ? _.toArray(mixCollection) : mixCollection;

        comparator = comparator || function(el) { return el };
        if (!_.isFunction(comparator)) {
            comparator = function(el) { return el[comparator]; }
        }

        if (!isArray || !isSorted) {
            aCollection = _.sortBy(!isArray ? aCollection : _.clone(aCollection), comparator)
        }

        var low = 0, high = aCollection.length;
        while (low < high) {
            var mid = (low + high) >>> 1;
            (comparator(aCollection[mid]) < mixEl) ? low = mid + 1 : high = mid;
        }

        if (0 <= low && low <= aCollection.length - 1 && comparator(aCollection[low]) === mixEl) {
            return {el: mixEl, idx: low, precise: true};
        }

        var idx = (void 0);
        if (greatest) {
            if (low < aCollection.length) {
                if (comparator(aCollection[low]) > mixEl) {
                    idx = low;
                } else if (low < aCollection.length - 1) {
                    idx = low + 1;
                }
            }
        } else {
            if (0 <= low) {
                if (low === aCollection.length) {
                    idx = low - 1;
                } else if (comparator(aCollection[low]) < mixEl) {
                    idx = low;
                } else if (0 < low) {
                    idx = low - 1;
                }
            }
        }

        return _.isUndefined(idx) ? null : {el: aCollection[idx], idx: idx};
    };

    App.Utils.DateIterator = function(mixFrom, mixTo) {
        this.m_tFrom = moment(mixFrom);
        this.m_tTo = moment(mixTo);
        this.m_tCurrent = this.m_tFrom;
    };

    App.Utils.DateIterator.prototype.next = function() {
        if (0 <= this.m_tTo.clone().diff(this.m_tCurrent, 'days')) {
            var result = this.m_tCurrent.clone();
            this.m_tCurrent.add('days', 1);

            return result;
        }

        return null;
    }
})(App, _);