(function (App, _) {
    App.LiveHubEventDataProvider = App.DataProvider.extend((function () {
        var sModelName = 'LiveHubEvent';

        var findModels = function (args) {
                var params = args.criteria || {}, oStorage, self = this, oCriteria;

                if (!args.channel) {
                    throw new Error('Channel not defined.');
                }

                oCriteria = _makeCriteria.apply(this, [params]);
                if (!oCriteria) {
                    throw new Error('Not implemented!');
                }

                oStorage = App.Storage.RuntimeStorage.produce(sModelName);
                oStorage.fetch(oCriteria, function (aActivities) {
                    self.pub(args.channel, aActivities);
                });
            },

            _makeCriteria = function (oParams) {
                oParams = oParams || {};

                if (void 0 === oParams.boundaryTimestamp || void 0 === oParams.limit) {
                    return null;
                }

                return new (App.Storage.Criteria.extend({
                    initialize: function(attrs) {
                        this.m_nBoundaryTimestamp = attrs.boundaryTimestamp;
                        this.m_nLimit = attrs.limit;
                    },

                    toQueryString: function () {
                        return '?bs=' + this.m_nBoundaryTimestamp + '&limit=' + this.m_nLimit;
                    }
                }))(oParams);
            };


        return {
            findModels: findModels
        };
    })());
})(App, _);
