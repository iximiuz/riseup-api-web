class MobileController < ApplicationController
  before_filter :authenticate

  layout 'mobile'

  def index
    @route = params.has_key?('r') ? params[:r] : ''
  end

  private

  def authenticate
    if cookies.has_key?('auth') && (token = UserRegistry.instance.authenticated?(cookies[:auth]))
      @user = User.find(token.user_id)
      @authenticated = true
    else
      @user = nil
      @authenticated = false
    end
  end

  protected

  def is_authenticated
    @authenticated && nil != @user
  end
end
