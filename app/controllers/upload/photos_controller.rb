class Upload::PhotosController < ApiController
  def create
    unless params.include?('photo') || params['photo'].include?('tempfile')
      render(json: {:status => 'error', :code => 1701, :sub => 'photo'}.to_json)
      return
    end
    file_name = ''
    photo_service = Riseup::Services::Upload::PhotosService.new(params['photo'].tempfile)
    begin
      file_name = photo_service.save(@user['nickname'])
    rescue
      render(json: {:status => 'error', :code => 1702}.to_json)
      return
    end
    render(json: {:status => 'ok', :path => file_name}.to_json)
  end
end
