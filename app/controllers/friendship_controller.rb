class FriendshipController < ApiController

  def index
    friendship_service = Riseup::Services::ServiceProvider.instance.get_service(:friendship)

    if 'inbox' == params[:mode]
      requests = friendship_service.inbox_request(@user.id)
      users = User.find(requests.map { |request| request[:user_id] })

      render(json: users.map { |user| Riseup::Rest::User.new(user).to_json })
      return
    end

    if 'outbox' == params[:mode]
      requests = friendship_service.outbox_request(@user.id)
      users = User.find(requests.map { |request| request[:user_id] })
      render(json: users.map { |user| Riseup::Rest::User.new(user).to_json })
      return
    end

    # todo: объединить в один запрос к базе!
    request_inbox = User.find(friendship_service.inbox_request(@user.id).map { |request| request[:user_id] })
    request_outbox = User.find(friendship_service.outbox_request(@user.id).map { |request| request[:user_id] })

    render(
        json: {
            :inbox => request_inbox.map { |user| Riseup::Rest::User.new(user).to_json },
            :outbox => request_outbox.map { |user| Riseup::Rest::User.new(user).to_json }
        }.to_json
    )
  end

  # /friendship
  def create
    friendship_service = Riseup::Services::ServiceProvider.instance.get_service(:friendship)
    status = friendship_service.send_invocation(@user.id, params[:user_id].to_i)

    render(json: {:status => status})
  end

  # /friendship/user1_user2
  def update
    friendship_service = Riseup::Services::ServiceProvider.instance.get_service(:friendship)
    status = friendship_service.accept_invocation(
        @user.id,
        Riseup::Rest::Id::FriendshipId.from_string(params[:id])
    )

    render(json: {:status => status})
  end

  # /friendship/user1_user2
  def destroy
    friendship_service = Riseup::Services::ServiceProvider.instance.get_service(:friendship)
    friendship_service.break_friendship(
        @user.id,
        Riseup::Rest::Id::FriendshipId.from_string(params[:id])
    )

    respond_to do |format|
      format.json { head :no_content }
    end
  end

end

