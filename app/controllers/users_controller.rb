class UsersController < ApiController

  # GET /users
  def index
    offset = params.has_key?('offset') ? params[:offset].to_i : 0
    limit = params.has_key?('limit') ? params[:limit].to_i : 20
    sort = params.has_key?('sort') && 'desc' == params[:sort] ? 'desc' : 'asc'

    users_storages = Riseup::Storages::UsersStorage.new(
        Riseup::Services::SP.i.get_service(:activities)
    )

    users = users_storages.find_all_with_stat(@user, offset, limit, sort)
    users_ids = users.map { |user| user.id }

    online_indicator_service = Riseup::Services::OnlineIndicatorService.new
    online_times = online_indicator_service.load_by_ids(users_ids)

    friendship_service = Riseup::Services::SP.i.get_service(:friendship)
    friendship_statuses = friendship_service.friendship_status(@user.id, users_ids)

    render(
        # !refactor: wtf code goes here!
        json: users.select { |user| user.id != @user.id }.map do |user|
          last_action_time = online_times[user.id]

          user.online = {
              :last_timestamp => last_action_time,
              :status => online_indicator_service.online?(last_action_time)
          }

          user.friendshipStatus = friendship_statuses[user.id]

          user.to_json
        end
    )
  end

  # GET /users/1
  # status of friendship:
  #       0 - not a friend / не в друзьях
  #       1 - not confirmed / не подтверждена другим пользователем
  #       2 - not accepted / прислал вам заявку и вы её не подтвердили
  #       3 - confirmed / в друзьях
  def show
    user = User.find_by_id(params[:id])
    unless user
      render(json: nil)
      return
    end

    friendship_service = Riseup::Services::ServiceProvider.instance.get_service(:friendship)
    friendship_status = friendship_service.friendship_status(@user.id, params[:id].to_i)

    render(json: Riseup::Rest::User.new(user, friendship_status).to_json)
  end

  def update
    if @user.id != params[:id].to_i
      render(json: {:error => 'Access denied.', :code => 1019})
      return
    end

    user = Riseup::Rest::User.new(JSON.parse(request.raw_post))

    @user.email = user.email if user.email
    @user.nickname = user.nickname if user.nickname
    @user.name = user.name if user.name
    @user.lastname = user.lastName if user.lastName

    if !user.sex.nil? && (0 == user.sex.to_i || 1 == user.sex.to_i)
      @user.sex = user.sex
    end

    if !user.credentials.nil? && (0 <= user.credentials.to_i && user.credentials.to_i <= 2)
      @user.credentials = user.credentials
    end

    @user.save()

    render(json: Riseup::Rest::User.new(@user).to_json)
  end
end
