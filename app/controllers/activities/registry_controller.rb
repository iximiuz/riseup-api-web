module Activities

  class RegistryController < ApiController
    # GET /activities/registry
    def index
      storage = Riseup::Storages::ActivitiesRegistryStorage.new
      render(json: storage.find(@user.id).to_json)
    end
  end

end