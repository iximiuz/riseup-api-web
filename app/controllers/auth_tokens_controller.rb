require 'riseup/registration/user_registry'

class AuthTokensController < ApiController
  skip_before_filter :check_token_presence

  # GET /auth_tokens/nickname
  def show
    unless params.has_key?('password')
      render(json: {:error => 'Missing param [password]', :code => 1002})
      return
    end

    user_registry = UserRegistry.instance
    users = User.find_by_sql ['SELECT * FROM users WHERE email = ?', params[:email]]
    if 1 == users.count
      if user_registry.can_authenticate?(users.first, params[:password])
        # @todo Переделать через Rest::AuthToken и setUser
        token = user_registry.create_token(users.first)
        token = JSON.parse(token.to_json)
        token[:user] = Riseup::Rest::User.new(users.first).to_json
        render(json: token)
      else
        render(json: {:error => 'Wrong password', code: 1003})
      end
    elsif 0 == users.count
      render(json: {:error => 'No such user', :code => 1010})
    else
      render(json: {:error => 'Too much users', code: 1004})
    end
  end

  # POST /auth_tokens
  def create
    if !params.has_key?('email') || !params.has_key?('password')
      render(json: {:error => 'Missing params [email] or [password]', :code => 1002})
      return
    end

    user_registry = UserRegistry.instance
    users = User.find_by_sql ['SELECT * FROM users WHERE email = ?', params[:email]]
    if 0 != users.count
      render(json: {:error => 'User already exists', code: 1011})
    else
      begin
        user = user_registry.register(params[:email], params[:password])
      rescue Riseup::Exceptions::BadEmailException
        render(json: {:error => 'Wrong email', code: 1014})
        return
      end

      # @todo Переделать через Rest::AuthToken и setUser
      token = user_registry.create_token(user)
      token = JSON.parse(token.to_json)
      token[:user] = Riseup::Rest::User.new(user).to_json
      render(json: token)
    end
  end

  def destroy
    user_registry = UserRegistry.instance
    token = user_registry.load_token(params[:id])

    unless token
      render(json: {:error => 'Wrong token', code: 1034})
      return
    end

    token.delete

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
