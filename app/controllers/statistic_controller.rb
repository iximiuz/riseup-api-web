class StatisticController < ApiController
  def create
    params = @_params
    Riseup::Services::StatisticService.save(params[:objectType], {:objectId => params[:objectId], :user_id => @user[:id], :dt => Riseup::Utils::TimeUtils.get_now_utc()}.to_json)
    render(json: 'ok'.to_json)
  end
end