class FriendsController < ApiController

  def index
    friendship_service = Riseup::Services::ServiceProvider.instance.get_service(:friendship)
    friends_ids = friendship_service.get_friends_for_user(@user.id).map { |friendship_data| friendship_data[:user_id] }

    friends = User.find(friends_ids)

    online_indicator_service = Riseup::Services::OnlineIndicatorService.new
    online_list = friends.empty? ? [] : online_indicator_service.load_by_ids(friends)

    render(json: friends.map { |user|
      user = Riseup::Rest::User.new(user)
      user.online = {
          :last_timestamp => online_list[user.id.to_s].to_i,
          :status => online_indicator_service.online?(online_list[user.id.to_s])
      }
      user.to_json }
    )
  end

end