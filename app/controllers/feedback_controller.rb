class FeedbackController < ApiController
  def create
    begin
      feedback = Feedback.new
      if params.include?(:attach)
        if params.include?(:attach) && params[:attach].tempfile
          dt = Date.today.to_s
          from = params[:attach].tempfile
          to = APP_CONFIG.files['feedback']['path'].to_s + '/' + Digest::MD5.hexdigest(params[:attach].original_filename + dt.to_s).to_s + '_' + dt.to_s + '_' + params[:attach].original_filename
          FileUtils.cp(from, to)
          feedback.attach = to
        end
      end

      feedback.cdate = feedback.udate = Time.now
      feedback.text = params['text']
      feedback.user_id = @user[:id]
      feedback.save
      render(json: {:status => 'ok', :code => 200}.to_json)
    rescue Exception => e
      puts e.backtrace
      render(json: {:error => e.message, :code => 500}.to_json)
    end
  end
end
