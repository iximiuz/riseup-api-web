class GoalsController < ApiController

  # GET /goals
  def index
    offset = params.has_key?('offset') ? params[:offset].to_i : 0
    limit = params.has_key?('limit') ? params[:limit].to_i : 20
    sort = params.has_key?('sort') && 'desc' == params[:sort] ? 'desc' : 'asc'
    filter = params.has_key?('filter') ? params[:filter] : 'all'

    storage = Riseup::Storages::GoalsStorage.new(Riseup::Services::SP.i.get_service(:activities))

    goals = storage.find_all_with_stat(@user, limit, offset, sort, filter)
    render(json: goals.map { |goal| goal.to_json })
  end

  # GET /goals/:id
  def show
    storage = Riseup::Storages::GoalsStorage.new(Riseup::Services::SP.i.get_service(:activities))
    goal = storage.find_with_stat(Riseup::Rest::Id::GoalId.new(params[:id]), @user)

    if goal
      render(json: goal.to_json)
    else
      render(json: nil)
    end
  end

  # POST /goals
  def create
    goal = Riseup::Rest::Goal.new(:attrs => JSON.parse(request.raw_post))
    if goal.userId != @user.id
      render(json: {:error => 'Access denied.', :code => 1009})
      return
    end

    storage = Riseup::Storages::GoalsStorage.new(Riseup::Services::SP.i.get_service(:activities))
    storage.save(goal)

    render(json: goal.to_json)
  end

  # PUT /goals/1
  def update
    goal = Riseup::Rest::Goal.new(:attrs => JSON.parse(request.raw_post))
    if goal.userId != @user.id
      render(json: {:error => 'Access denied.', :code => 1009})
      return
    end

    storage = Riseup::Storages::GoalsStorage.new(Riseup::Services::SP.i.get_service(:activities))
    storage.save(goal)

    render(json: goal.to_json)
  end

  # DELETE /goals/1
  def destroy
    storage = Riseup::Storages::GoalsStorage.new(Riseup::Services::SP.i.get_service(:activities))
    goal = storage.find(Riseup::Rest::Id::GoalId.new(params[:id]))
    if goal
      if goal.userId != @user.id
        render(json: {:error => 'Access denied.', :code => 1009})
        return
      end

      storage.delete(goal)
    end

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
