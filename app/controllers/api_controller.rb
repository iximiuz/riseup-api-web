class ApiController < ApplicationController
  before_filter :check_token_presence
  before_filter :set_access_control_headers
  after_filter :set_user_online
  skip_before_filter :check_token_presence, :only => [:options]

  def options
    head :ok
  end

  protected

  def set_user_online
    no_isset = true

    APP_CONFIG.no_online_controllers.map { |controller|
      if controller.to_s == params[:controller]
        no_isset = false
      end
    }

    if no_isset && !@user.nil?
      Riseup::Services::OnlineIndicatorService.new().up_online(@user.id)
    end
  end

  def check_token_presence
    render json: {:error => 'Auth token required', :code => 1001} and return unless params.has_key?('token')

    token = UserRegistry.instance.authenticated?(params[:token])
    unless token
      render json: {:error => 'Bad auth token', :code => 1005} and return
    end

    @user = User.find(token.user_id)
    unless @user
      render json: {:error => 'No such user', :code => 1006}
    end
  end

  private
  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    headers['Access-Control-Allow-Headers'] = '*,Content-Type'
  end
end
