module Voting

  class VotingController < ApiController
    before_filter :make_vote, :make_object

    private

    def make_object
      @object = Riseup::Storages::ResourcesStorage.find(
          params[:object], Riseup::Rest::Id::Id.create(params[:object], params[:obj_id])
      )

      if @object.respond_to?('belongs?') && @object.belongs?(@user.id)
        render(json: {:error => 'Voting for yourself objects not allowed.', :code => 100, :subcode => 101})
      end
    end

    def make_vote
      attrs = JSON.parse(request.raw_post)
      @vote = Riseup::Rest::Votes::AbstractVote.create(attrs)
    end
  end

end
