require File.dirname(__FILE__) + '/voting_controller'

module Voting

  class ObjectsVotesController < VotingController

    # show all votes for object defined by 'object', 'obj_id', 'tag' params
    def index

    end

    # show specific vote with id == param[:id] for object defined by 'object', 'obj_id', 'tag' params
    def show

    end

    # create vote for object defined by 'object', 'obj_id', 'tag' params
    #
    # http://dev.api.riseup.su/voting/:version/objects/:obj_type/:obj_id[/:tag]/votes + RAW POST BODY
    # http://dev.api.riseup.su/voting/1/objects/activity/106_844/xxx/votes
    #
    def create
      assert 1 == params[:version].to_i, params[:version]

      begin
        Riseup::Services::SP.i.get_service(:voting).vote(
            @vote,
            params[:tag]
        )
        render(json: @vote.to_json)
      rescue Riseup::Services::Voting::Exceptions::ReVoteBanException
        render(json: {:error => 'Re-vote not allowed.', :code => 100, :subcode => 102})
      end
    end

    # update vote for object defined by 'object', 'obj_id', 'tag' params
    def update
      a = 100
    end

    # delete vote for object defined by 'object', 'obj_id', 'tag' params
    def delete
      a = 100
    end
  end

end