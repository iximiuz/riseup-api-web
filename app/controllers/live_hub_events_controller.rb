class LiveHubEventsController < ApiController

  def index
    live_hub_service = Riseup::Services::SP.i.get_service(:live_hub)

    generators = live_hub_service.retrieve_generators_for_user(@user.id)
    privacy_manager = Riseup::Services::LiveHub::Privacy::PrivacyManager.new

    privileges = privacy_manager.calc_privileges(@user.id, generators)

    boundary_timestamp = params.key?(:bs) ? Time.at(params[:bs].to_i).utc : Time.now.utc
    limit = params.key?(:limit) ? [200, params[:limit].to_i].min : 20

    events = live_hub_service.retrieve_events_selection(privileges, limit, boundary_timestamp)

    render(json: events.map { |event| event.to_json })
  end

  def show

  end

end