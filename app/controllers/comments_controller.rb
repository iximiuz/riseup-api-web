class CommentsController < ApiController
  def show
    comments_service = Riseup::Services::CommentsService.new
    comments_storage = Riseup::Storages::CommentsStorage.new
    comments_storage.user = @user
    online_indicator_service = Riseup::Services::OnlineIndicatorService.new

    if params.include?('offset')
      offset= params['offset'].to_i
    else
      offset = 0
    end

    entity_id = comments_service.get_entity_id(params['entity_id'], params['entity_type'])
    comments = comments_storage.fetch(params['entity_type'], entity_id, offset)
    if comments.size.to_i == 0
      render(json: {}.to_json)
      return
    end
    users = Array.new
    comments.map { |comment|
      unless users.include?(comment.user_id.to_i)
        users.push(comment.user_id.to_i)
      end

      if comment.answer_user_id.to_i > 0
        unless users.include?(comment.answer_user_id.to_i)
          users.push(comment.answer_user_id.to_i)
        end
      end
    }
    online_times = online_indicator_service.load_by_ids(users)
    users = User.where('id IN('+users.join(',')+')')
    users_index_array = {}
    users.map { |user|
      users_index_array[user.id] = Riseup::Rest::UserMin.new(user)
      online_times.map { |k, v|
        if k.to_i == user.id.to_i
          users_index_array[user.id].online = {
              :last_timestamp => v,
              :status => online_indicator_service.online?(v)
          }
        end
      }
    }

    result = []

    comments.map { |comment|
      result.push(Riseup::Rest::Comment.new(comment, users_index_array[comment.user_id], users_index_array[comment.answer_user_id]).to_json)
    }

    render(json: result.to_json)
  end

  def create
    comments_service = Riseup::Services::CommentsService.new
    comments_storage = Riseup::Storages::CommentsStorage.new
    comments_storage.user = @user

    new_comment_param = {
        :entity_type => params['entity_type'].to_i,
        :entity_id => comments_service.get_entity_id(params['entity_id'], params['entity_type']),
        :msg => params['msg'],
        :user_id => @user[:id]
    }

    comment = comments_storage.save(new_comment_param)
    comments_service.counter_change(params['entity_type'].to_i, params['entity_id'])
    render(json: comment.to_json)
  end

  def destroy
    comments_service = Riseup::Services::CommentsService.new
    comments_storage = Riseup::Storages::CommentsStorage.new
    comments_storage.user = @user

    unless params[:comment_id]
      render(json: {:status => 'error', :code => 1501})
      return
    end

    comment = comments_storage.find_by_id(params[:comment_id].to_i)

    unless comment
      render(json: {:status => 'error', :code => 1502})
      return
    end

    entity = comments_service.get_entity(comment[:entity_type], comment[:entity_id])
    unless entity
      render(json: {:status => 'error', :code => 1503})
      # TODO: сделать оповещение администатору о ситуации. Комментарий не привязан к сущности.
      return
    end

    unless comment.user_id == @user[:id] || entity.user_id == @user[:id]
      render(json: {:status => 'error', :code => 1504})
      return
    end

    comments_storage.del(comment.id)
    comments_service.counter_change(Riseup::Services::CommentsService::ACTIVITIES_ENTITY, entity.id, 'down')
    result = {:status => 'ok'}

    render(json: result.to_json)
  end
end
