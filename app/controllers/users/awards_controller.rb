class Users::AwardsController < ApiController
  def create
  end

  def show
  end

  def index
    avards_service = Riseup::Services::AwardsService.new()
    render(json: avards_service.load_awards(@user[:id]))
  end

  def update
  end

  def destroy
  end
end
