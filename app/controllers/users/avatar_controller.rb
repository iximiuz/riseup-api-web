module Users
  class AvatarController < ApiController
    def index
      avatar_url = @user[:avatar_url]
      if avatar_url == ''
        avatar_url = APP_CONFIG.images['noimg']
      end

      if params[:with_domain] == true || params[:with_domain].to_i == 1
        avatar_url = 'http://' + APP_CONFIG.domains['app'] + avatar_url
      end

      render(json: {:avatar_url => avatar_url}.to_json)
    end

    def create
      avatar_storage = Riseup::Storages::AvatarStorage.new()
      temp_file = params['thefile']
      #tmp_file = temp_file.tempfile
      path = avatar_storage.save_avatar(temp_file)
      complect = avatar_storage.create_complect(temp_file)

      if path.empty?
        path = APP_CONFIG.images['noimg']
      end

      @user.avatar_url=path
      @user.save
      render(json: {:file => path, :complect => complect}.to_json)
    end

    def update
      if params[:user_id].to_i != @user[:id].to_i
        render(json: {:error => 'Access denied', :code => 500}.to_json)
        return
      elsif params[:id].to_s + '.' + params[:format] == APP_CONFIG.images['noimg']
        render(json: {:error => 'Unsupported method', :code => 500}.to_json)
        return
      end

      avatar_path = APP_CONFIG.images['path'] + params[:id].to_s + '.' + params[:format]
      avatar_storage = Riseup::Storages::AvatarStorage.new()
      image_service = Riseup::Services::ImgEditService.new(avatar_path)
      case params[:edit][:action]
        when 'rotate'
          image = image_service.rotate (params[:edit][:actionParams])
        else
          image = false
      end
      if image
        avatar_storage.save_img(image, avatar_path)
        render(json: {:file => params[:id].to_s + '.' + params[:format]}.to_json)
      else
        render(json: {:error => 'No valid params', :code => 500}.to_json)
      end
    end

    def destroy
      if params[:user_id].to_i != @user[:id].to_i
        render(json: {:error => 'Access denied', :code => 500}.to_json)
        return
      end

      @user.avatar_url=''
      @user.save
      render(json: {:file => APP_CONFIG.images['noimg']}.to_json)
    end

  end
end