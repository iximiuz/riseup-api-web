module Users
  module Activities

    class RegistryController < ApiController
      # GET /user/1/activities/registry
      def index
        storage = Riseup::Storages::ActivitiesRegistryStorage.new
        render(json: storage.find(params[:user_id]).to_json)
      end
    end

  end
end