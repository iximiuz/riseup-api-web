module Users
  class ActivitiesController < ApiController

    # GET /user/1/activities
    def index
      target_user = User.find_by_id(params[:user_id])
      unless target_user
        render(json: {:error => 'User not found.', :code => 1018})
        return
      end

      target_user = Riseup::Rest::User.new(target_user)
      # Проверка доступов в зависимости от пользовательских настроек видимости активностей
      access_privileges = _check_permissions(target_user, @user)

      activities_service = Riseup::Services::SP.i.get_service(:activities)
      goal_ids = params.has_key?('goal_ids') ? params[:goal_ids] : []

      if params.has_key?('aid') || params.has_key?('date')
        count_after = params.has_key?('acount') ? params[:acount].to_i : 0
        count_before = params.has_key?('bcount') ? params[:bcount].to_i : 0
        align_by_days = params[:day_align].present? && 'false' != params[:day_align]

        if params.has_key?('aid')
          activities = activities_service.find_by_activity(
              params[:user_id],
              params[:aid],
              count_after,
              count_before,
              align_by_days,
              goal_ids,
              access_privileges
          )
        else
          activities = activities_service.find_by_date(
              params[:user_id],
              params[:date],
              count_after,
              count_before,
              align_by_days,
              goal_ids,
              access_privileges
          )
        end
      elsif params.has_key?('from') && params.has_key?('to')
        from = params[:from]
        to = params[:to]

        if to < from
          render(json: {:error => 'Param [to] must be greater of equal [from].', :code => 1008})
          return
        end

        activities = activities_service.find_by_date_range(
            params[:user_id],
            from,
            to,
            goal_ids,
            access_privileges
        )
      else
        render(json: {:error => 'Wrong request params.', :code => 1008})
        return
      end

      if activities.empty? && [2] == access_privileges
        render(json: {:error => 'Access denied.', :code => 1028})
        return
      end

      begin
        Riseup::Services::SP.i.get_service(:voting).votize(
            activities,
            Riseup::Rest::BaseResource.get_type_by_resource_class(Riseup::Rest::Activity),
            [],
            @user.id
        )
      rescue
        # ignored
      end

      render(json: activities.map { |activity| activity.to_json })
    end

    # GET /user/1/activities/1
    def show
      target_user = User.find_by_id(params[:user_id])
      unless target_user
        render(json: {:error => 'User not found.', :code => 1018})
        return
      end

      target_user = Riseup::Rest::User.new(target_user)
      # Проверка доступов в зависимости от пользовательских настроек видимости активностей
      access_privileges = _check_permissions(target_user, @user)

      activities_service = Riseup::Services::SP.i.get_service(:activities)
      activity = activities_service.find(params[:id], access_privileges)

      unless activity && [2] == access_privileges
        render(json: {:error => 'Access denied.', :code => 1028})
        return
      end

      unless activity
        render(json: nil)
        return
      end

      begin
        Riseup::Services::SP.i.get_service(:voting).votize(
            activity,
            Riseup::Rest::BaseResource.get_type_by_resource_class(Riseup::Rest::Activity),
            [],
            @user.id
        )
      rescue
        # ignored
      end

      render(json: activity.to_json)
    end

    private ############################################################################################################

    def _check_permissions(target_user, current_user)
      if 2 == target_user.credentials
        return [0, 1, 2] # все, кроме тех, которые пользователь явно скрыл, так как он разрешил просмотр всем
      end

      if 1 == target_user.credentials.to_i && Riseup::Services::SP.i.get_service(:friendship).friends?(target_user.id, current_user.id)
          return [0, 1, 2] # все, кроме тех, что явно скрыты, так как мы друзья, а пользователь разрешил просмотр друзьям
      end

      [2] # только те, что явно открыты, так как пользователь скрыл по умолчанию все или мы с ним не дружим
    end
  end
end