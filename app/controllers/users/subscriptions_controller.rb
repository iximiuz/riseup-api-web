class Users::SubscriptionsController < ApiController
  ## Возвращаем массив с подписками текущего пользователя
  ## Пример: [1, 2]
  def index
    result = []
    rows = Subscription.select([:subscription_type]).where('user_id='+@user[:id].to_s)
    rows.map { |row|
      result.push(row[:subscription_type].to_i)
    }
    render(json: result.to_json)
  end

  def update
    unless params[:checked]
      Subscription.delete_all("user_id=#{@user[:id]}")
      render(json: {:status => 'ok'})
      return
    end

    unless access_values (params[:checked])
      render(json: {:status => 'error', :code => 1401})
      return
    end

    checked = params[:checked]
    rows_by_user = Subscription.where('user_id='+@user[:id].to_s)

    ## Сначала проверяем записи, которые есть в базе, которых нет - записываем.
    checked.map { |elem|
      isset = false
      rows_by_user.map { |row|
        if row.subscription_type.to_i == elem.to_i
          isset = true
        end
      }

      unless isset
        row = Subscription.new
        row.user_id = @user[:id]
        row.uniq_hash = get_uniq_hash
        row.subscription_type = elem.to_i
        row.create_dt = Time.now
        row.save
      end
    }

    ## Тут удаляем "лишние" строки
    types_checked = checked.join(',')
    Subscription.delete_all("user_id=#{@user[:id]} AND subscription_type NOT IN (#{types_checked})")

    render(json: {:status => 'ok'})
  end

  private ###############################################################################

  ## @return String
  def get_uniq_hash
    hash = Digest::MD5.hexdigest(@user[:id].to_s + Time.now.to_s + Random.rand.to_s)
    row = Subscription.where("uniq_hash='#{hash}'")

    if row.count > 0
      hash = get_uniq_hash
    end

    hash # return
  end

  ## @param array Array
  ## @return boolean
  def access_values(array)
    access = [1, 2]
    array.map { |item|
      unless access.include?(item.to_i)
        return false
      end
    }
    true # true
  end
end
