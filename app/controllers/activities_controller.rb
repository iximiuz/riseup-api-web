class ActivitiesController < ApiController

  # GET /activities
  def index
    activities_service = Riseup::Services::ServiceProvider.instance.get_service(:activities)
    goal_ids = params.has_key?('goal_ids') ? params[:goal_ids] : []

    if params.has_key?('aid') || params.has_key?('date')
      count_after = params.has_key?('acount') ? params[:acount].to_i : 0
      count_before = params.has_key?('bcount') ? params[:bcount].to_i : 0
      align_by_days = params[:day_align].present? && 'false' != params[:day_align]

      if params.has_key?('aid')
        activities = activities_service.find_by_activity(
            @user.id,
            params[:aid],
            count_after,
            count_before,
            align_by_days,
            goal_ids
        )
      else
        activities = activities_service.find_by_date(
            @user.id,
            params[:date],
            count_after,
            count_before,
            align_by_days,
            goal_ids
        )
      end
    elsif params.has_key?('from') && params.has_key?('to')
      from = params[:from]
      to = params[:to]

      if to < from
        render(json: {:error => 'Param [to] must be greater of equal [from].', :code => 1008})
        return
      end

      activities = activities_service.find_by_date_range(@user.id, from, to, goal_ids)
    else
      render(json: {:error => 'Wrong request params.', :code => 1008})
      return
    end

    begin
      Riseup::Services::SP.i.get_service(:voting).votize(
         activities,
         Riseup::Rest::BaseResource.get_type_by_resource_class(Riseup::Rest::Activity),
         []
      )
    rescue
      # ignored
    end

    render(json: activities.map { |activity| activity.to_json })
  end

  # GET /activities/1
  def show
    activities_service = Riseup::Services::ServiceProvider.instance.get_service(:activities)
    activity = activities_service.find(params[:id])

    ## activity Array
    if activity.count > 0 && activity.user_id != @user.id
      render(json: {:error => 'Access denied.', :code => 1009})
    else
      begin
        Riseup::Services::SP.i.get_service(:voting).votize(
            activity,
            Riseup::Rest::BaseResource.get_type_by_resource_class(Riseup::Rest::Activity),
            []
        )
      rescue
        # ignored
      end

      render(json: activity.to_json)
    end
  end

  # POST /activities
  def create
    begin
      activities_service = Riseup::Services::ServiceProvider.instance.get_service(:activities)
      activity = Riseup::Rest::Activity.new(JSON.parse(request.raw_post), nil, nil, @user)
      activities_service.create(@user, activity)
    rescue Riseup::Exceptions::PrivilegeException => e
      render(json: { :error => e.message, :code => e.code }, status: :forbidden)
      return
    rescue Riseup::Services::Activities::Exceptions::ActivitiesServiceException => e
      render(json: { :error => e.message, :code => e.code }, status: :conflict)
      return
    rescue => e
      logger.error(e.message + "\n " + e.backtrace.join("\n "))

      render(json: { :error => 'Unexpected error.' }, status: :internal_server_error)
      return
    end

    render(json: activity.to_json)
  end

  # PUT /activities/1
  def update
    activity = Riseup::Rest::Activity.new(JSON.parse(request.raw_post), nil, nil, @user)

    # move this logic to service and catch correspond access denied exception here
    if activity.userId != @user.id
      render(json: {:error => 'Access denied.', :code => 1009})
      return
    end

    activities_service = Riseup::Services::ServiceProvider.instance.get_service(:activities)
    activities_service.save(activity)

    render(json: activity.to_json)
  end

  # DELETE /activities/1
  def destroy
    activities_service = Riseup::Services::ServiceProvider.instance.get_service(:activities)

    # move this logic to service and catch correspond access denied exception here
    activity = activities_service.find(params[:id])
    if activity
      if activity.userId != @user.id
        render(json: {:error => 'Access denied.', :code => 1009})
        return
      end

      activity.user = @user
      activities_service.delete(activity)
    end

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
