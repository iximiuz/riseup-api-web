class SearchController < ApiController
  def show
    filter = params[:id]
    query = params[:q]

    if query.to_s == ''
      render(json: {:status => 'error', :code => 1601})
      return
    end

    # @var search_storage Riseup::Storages::SearchStorage
    search_storage = Riseup::Services::ServiceProvider.instance.get_service(:search)
    result = search_storage.find(query, filter)
    render(json: result.to_json)
  end
end
