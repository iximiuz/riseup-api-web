class ServiceRegistryMiddleware

  def initialize(app)
    @app = app
  end

  def call(env)
    business_observer_initializer = lambda do |service_provider|
      Riseup::Services::BusinessObserver.new(service_provider.get_service(:live_hub))
    end

    Riseup::Services::ServiceProvider.i.register_service(:business_observer, business_observer_initializer)


    friendship_initializer = lambda do |service_provider|
      service = Riseup::Services::Friendship::FriendshipService.new

      service.add_observer(service_provider.get_service(:business_observer))

      service
    end

    Riseup::Services::ServiceProvider.i.register_service(:friendship, friendship_initializer)


    live_hub_initializer = lambda do |service_provider|
      Riseup::Services::LiveHub::LiveHubService.new
    end

    Riseup::Services::ServiceProvider.i.register_service(:live_hub, live_hub_initializer)


    activities_initializer = lambda do |service_provider|
      activities_storage = Riseup::Storages::ActivitiesStorage.new
      activities_storage.add_observer(service_provider.get_service(:business_observer))

      activities_service = Riseup::Services::Activities::ActivitiesService.new(activities_storage)

      activities_service
    end

    Riseup::Services::ServiceProvider.i.register_service(:activities, activities_initializer)


    goals_initializer = lambda do |service_provider|
      activities_storage = Riseup::Storages::GoalsStorage.new(service_provider.get_service(:activities))

      activities_storage.add_observer(service_provider.get_service(:business_observer))

      activities_storage
    end

    Riseup::Services::ServiceProvider.i.register_service(:goals, goals_initializer)


    voting_initializer = lambda do |service_provider|
      Riseup::Services::Providers::VotingServiceProvider.get_service(1)
    end

    Riseup::Services::ServiceProvider.i.register_service(:voting, voting_initializer)

    comments_initializer = lambda do |service_provider|
      Riseup::Services::CommentsService.new
    end

    Riseup::Services::ServiceProvider.i.register_service(:comments, comments_initializer)

    search_initializer = lambda do |service_provider|
      Riseup::Storages::SearchStorage.new
    end
    Riseup::Services::ServiceProvider.i.register_service(:search, search_initializer)

    @app.call(env)
  end
end