require 'test_helper'
require 'lib/redis_test_helper'

class NestTest < ActiveSupport::TestCase
  include RedisTestHelper

  def setup
    super
    flush_redis_db
  end

  test 'ns' do
    ns = Riseup::Services::Nest.new('ns')
    ns.set('1')
    ns['bar']['baz'][42].set('2')

    ns = Riseup::Services::Nest.new('ns')
    assert_equal '1', ns.get()
    assert_equal '2', ns['bar']['baz'][42].get()
  end
end
