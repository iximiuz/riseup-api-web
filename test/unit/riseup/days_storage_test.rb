require 'test_helper'
require 'time'
require 'lib/redis_test_helper'

class DaysStorageTest < ActiveSupport::TestCase
  include RedisTestHelper

  def setup
    super

    flush_redis_db
  end

  test 'find' do
    day01011970 = Riseup::Domain::Activities::Day.new(1, DateTime.strptime('1970-01-01', '%Y-%m-%d').to_time.to_i, [1, 2, 3])
    day08011970 = Riseup::Domain::Activities::Day.new(1, DateTime.strptime('1970-01-08', '%Y-%m-%d').to_time.to_i, [4, 5, 6])
    day09011970 = Riseup::Domain::Activities::Day.new(1, DateTime.strptime('1970-01-09', '%Y-%m-%d').to_time.to_i, [7, 8])

    storage = Riseup::Storages::DaysStorage.new
    storage.save(day01011970)
    storage.save(day08011970)
    storage.save(day09011970)


    day = storage.find(1, DateTime.strptime('1970-01-05', '%Y-%m-%d').to_time.utc.to_i)
    assert_nil day


    day = storage.find(1, DateTime.strptime('1970-01-08', '%Y-%m-%d').to_time.utc.to_i)
    assert_equal '1970-01-08', day.get_date.to_s
    assert_equal 604800, day.get_date(true)
    assert_equal [4, 5, 6], day.get_activities
  end

  test 'find_next' do
    day01011970 = Riseup::Domain::Activities::Day.new(1, DateTime.strptime('1970-01-01', '%Y-%m-%d').to_time.to_i, [1, 2, 3])
    day08011970 = Riseup::Domain::Activities::Day.new(1, DateTime.strptime('1970-01-08', '%Y-%m-%d').to_time.to_i, [4, 5, 6])
    day09011970 = Riseup::Domain::Activities::Day.new(1, DateTime.strptime('1970-01-09', '%Y-%m-%d').to_time.to_i, [7, 8])

    storage = Riseup::Storages::DaysStorage.new
    storage.save(day01011970)
    storage.save(day08011970)
    storage.save(day09011970)


    nearest_right_day = storage.find_next(1, DateTime.strptime('1970-01-05', '%Y-%m-%d').to_time.utc.to_i)
    assert_equal '1970-01-08', nearest_right_day.get_date.to_s
    assert_equal 604800, nearest_right_day.get_date(true)
    assert_equal [4, 5, 6], nearest_right_day.get_activities


    nearest_right_day = storage.find_next(1, DateTime.strptime('1970-01-08', '%Y-%m-%d').to_time.utc.to_i)
    assert_equal '1970-01-09', nearest_right_day.get_date.to_s
    assert_equal 691200, nearest_right_day.get_date(true)
    assert_equal [7, 8], nearest_right_day.get_activities
  end
end
