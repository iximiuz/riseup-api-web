require 'test_helper'

class FooVoter
  include Voting::Voter

  can_vote :resource_name => 'FooVoter', :item_id_getter => :id

  attr_accessor :id

  def initialize(id)
    @id = id
  end
end

class BarVoter
  include Voting::Voter

  can_vote :resource_name => 'BarVoter', :item_id_getter => :id_getter

  def initialize(id)
    @id = id
  end

  def id_getter
    @id
  end
end

class ResMock
  include Voting::Votable

  can_be_voted :resource_name => 'ResMock', :item_id_getter => :id

  attr_accessor :id

  def initialize(id)
    @id = id
  end
end

class VoterTest < ActiveSupport::TestCase
  test 'test' do
    f = FooVoter.new(42)
    resource = ResMock.new(1)
    assert_equal 42, f.id
    vote = f.vote(resource)
    assert_equal 'FooVoter#42', vote.voter_id
    assert_equal 'ResMock#1', vote.votable_id

    ff = FooVoter.new(43)
    resource = ResMock.new(2)
    assert_equal 43, ff.id
    vote = ff.vote(resource)
    assert_equal 'FooVoter#43', vote.voter_id
    assert_equal 'ResMock#2', vote.votable_id

    b = BarVoter.new(9001)
    resource = ResMock.new(3)
    assert_equal 9001, b.id_getter
    vote = b.vote(resource)
    assert_equal 'BarVoter#9001', vote.voter_id
    assert_equal 'ResMock#3', vote.votable_id

    bb = BarVoter.new(9002)
    resource = ResMock.new(4)
    assert_equal 9002, bb.id_getter
    vote = bb.vote(resource)
    assert_equal 'BarVoter#9002', vote.voter_id
    assert_equal 'ResMock#4', vote.votable_id
  end
end
