require 'test_helper'

class FooRes
  include Voting::Votable

  can_be_voted :resource_name => 'FooRes', :item_id_getter => :id

  attr_accessor :id

  def initialize(id)
    @id = id
  end
end

class BarRes
  include Voting::Votable

  can_be_voted :resource_name => 'BarRes', :item_id_getter => :id_getter

  def initialize(id)
    @id = id
  end

  def id_getter
    @id
  end
end

class VoterMock
  include Voting::Voter

  can_vote :resource_name => 'VoterMock', :item_id_getter => :id

  attr_accessor :id

  def initialize(id)
    @id = id
  end
end

class VotableTest < ActiveSupport::TestCase
  test 'test' do
    f = FooRes.new(42)
    voter = VoterMock.new(1)
    assert_equal 42, f.id
    vote = f.vote(voter)
    assert_equal 'FooRes#42', vote.votable_id
    assert_equal 'VoterMock#1', vote.voter_id

    ff = FooRes.new(43)
    voter = VoterMock.new(2)
    assert_equal 43, ff.id
    vote = ff.vote(voter)
    assert_equal 'FooRes#43', vote.votable_id
    assert_equal 'VoterMock#2', vote.voter_id

    b = BarRes.new(9001)
    voter = VoterMock.new(3)
    assert_equal 9001, b.id_getter
    vote = b.vote(voter)
    assert_equal 'BarRes#9001', vote.votable_id
    assert_equal 'VoterMock#3', vote.voter_id

    bb = BarRes.new(9002)
    voter = VoterMock.new(4)
    assert_equal 9002, bb.id_getter
    vote = bb.vote(voter)
    assert_equal 'BarRes#9002', vote.votable_id
    assert_equal 'VoterMock#4', vote.voter_id
  end
end
