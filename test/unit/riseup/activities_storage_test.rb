require 'test_helper'
require 'lib/redis_test_helper'

class ActivitiesStorageTest < ActiveSupport::TestCase
  include RedisTestHelper

  fixtures :activities

  def setup
    super

    flush_redis_db
    load_redis_fixtures(:activities)
  end

  #
  # delete one
  #
  test 'delete existing' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity2 = storage.find(2)
    storage.delete(activity2)

    activity2 = storage.find(2)
    assert_nil activity2
  end

  #
  # delete one
  #
  test 'delete all daily activities and then add another one to this day' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity5 = storage.find(5)
    activity6 = storage.find(6)
    activity7 = storage.find(7)

    storage.delete(activity5)
    storage.delete(activity6)
    storage.delete(activity7)

    new_activity_attrs = {
        'nextId' => 8,
        'prevId' => 4,
        'userId' => 1,
        'goalId' => nil,
        'description' => 'user1_new_activity',
        'state' => 0,
        'startDate' => '2013-04-28',
        'cdate' => Time.now.strftime('%Y-%m-%d %H:%M:%S')
    }

    new_activity = Rest::Activity.new(new_activity_attrs)
    new_activity = storage.save(new_activity)
    assert new_activity.id
  end

  #
  # find one
  #
  test 'find existing in day middle' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(2)
    assert_equal 2, activity.id
    assert_equal 1, activity.prevId
    assert_equal 3, activity.nextId
    assert_equal 1, activity.userId
    assert_equal 'user1_activity2', activity.description
  end

  #
  # find one
  #
  test 'find existing without prev id' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(1)
    assert_equal 1, activity.id
    assert_nil activity.prevId
    assert_equal 2, activity.nextId
    assert_equal 1, activity.userId
    assert_equal 'user1_activity1', activity.description
  end

  #
  # find one
  #
  test 'find existing without next id' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(13)
    assert_equal 13, activity.id
    assert_equal 12, activity.prevId
    assert_nil activity.nextId
    assert_equal 1, activity.userId
    assert_equal 'user1_activity13', activity.description
  end

  #
  # find one
  #
  test 'find existing on days left boundary' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(5)
    assert_equal 5, activity.id
    assert_equal 4, activity.prevId
    assert_equal 6, activity.nextId
    assert_equal 1, activity.userId
    assert_equal 'user1_activity5', activity.description
  end

  #
  # find one
  #
  test 'find existing on days right boundary' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(7)
    assert_equal 7, activity.id
    assert_equal 6, activity.prevId
    assert_equal 8, activity.nextId
    assert_equal 1, activity.userId
    assert_equal 'user1_activity7', activity.description
  end

  #
  # find many
  #
  test 'find by central date' do
    storage = Riseup::Storages::ActivitiesStorage.new
    activities = storage.find_by_date_range(1, '2013-03-27', 5, 6)

    assert_equal 5, activities.count

    assert_equal 1, activities.first.id
    assert_nil activities.first.prevId
    assert_equal 2, activities.first.nextId

    assert_equal 2, activities.second.id
    assert_equal 1, activities.second.prevId
    assert_equal 3, activities.second.nextId

    assert_equal 3, activities.third.id
    assert_equal 2, activities.third.prevId
    assert_equal 4, activities.third.nextId

    assert_equal 4, activities.fourth.id
    assert_equal 3, activities.fourth.prevId
    assert_equal 5, activities.fourth.nextId

    assert_equal 5, activities.fifth.id
    assert_equal 4, activities.fifth.prevId
    assert_equal 6, activities.fifth.nextId
  end

  #
  # find many
  #
  test 'find by central activity id' do
    storage = Riseup::Storages::ActivitiesStorage.new
    activities = storage.find_by_activity(3, 5, 6)

    assert_equal 7, activities.count

    assert_equal 1, activities.first.id
    assert_nil activities.first.prevId
    assert_equal 2, activities.first.nextId

    assert_equal 2, activities.second.id
    assert_equal 1, activities.second.prevId
    assert_equal 3, activities.second.nextId

    assert_equal 3, activities.third.id
    assert_equal 2, activities.third.prevId
    assert_equal 4, activities.third.nextId

    assert_equal 4, activities.fourth.id
    assert_equal 3, activities.fourth.prevId
    assert_equal 5, activities.fourth.nextId

    assert_equal 5, activities.fifth.id
    assert_equal 4, activities.fifth.prevId
    assert_equal 6, activities.fifth.nextId

    assert_equal 6, activities[5].id
    assert_equal 5, activities[5].prevId
    assert_equal 7, activities[5].nextId

    assert_equal 7, activities[6].id
    assert_equal 6, activities[6].prevId
    assert_equal 8, activities[6].nextId
  end

  #
  # find many
  #
  test 'find by central activity id with single acount and bcount' do
    storage = Riseup::Storages::ActivitiesStorage.new
    activities = storage.find_by_activity(3, 1, 1)

    assert_equal 2, activities.count

    assert_equal 2, activities.first.id
    assert_equal 1, activities.first.prevId
    assert_equal 3, activities.first.nextId

    assert_equal 3, activities.second.id
    assert_equal 2, activities.second.prevId
    assert_equal 4, activities.second.nextId
  end

  #
  # find many
  #
  test 'find by central activity id without prev id' do
    storage = Riseup::Storages::ActivitiesStorage.new
    activities = storage.find_by_activity(1, 4, 2)

    assert_equal 4, activities.count

    assert_equal 1, activities.first.id
    assert_equal nil, activities.first.prevId
    assert_equal 2, activities.first.nextId

    assert_equal 2, activities.second.id
    assert_equal 1, activities.second.prevId
    assert_equal 3, activities.second.nextId

    assert_equal 3, activities.third.id
    assert_equal 2, activities.third.prevId
    assert_equal 4, activities.third.nextId

    assert_equal 4, activities.fourth.id
    assert_equal 3, activities.fourth.prevId
    assert_equal 5, activities.fourth.nextId
  end

  #
  # find many
  #
  test 'find by central activity id without next id' do
    storage = Riseup::Storages::ActivitiesStorage.new
    activities = storage.find_by_activity(13, 4, 1)

    assert_equal 2, activities.count

    assert_equal 12, activities.first.id
    assert_equal 11, activities.first.prevId
    assert_equal 13, activities.first.nextId

    assert_equal 13, activities.second.id
    assert_equal 12, activities.second.prevId
    assert_equal nil, activities.second.nextId
  end

  #
  # find many
  #
  test 'find by non existing activity id' do
    storage = Riseup::Storages::ActivitiesStorage.new
    activities = storage.find_by_activity(30, 1, 1)

    assert_equal 0, activities.count
  end

  test 'save new in day middle' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = Rest::Activity.new({
                                      'nextId' => 2,
                                      'prevId' => 1,
                                      'userId' => 1,
                                      'goalId' => nil,
                                      'description' => 'user1_new_activity',
                                      'state' => 0,
                                      'startDate' => '2013-04-21',
                                      'startTime' => '12:00:00',
                                      'cdate' => Time.now.strftime('%Y-%m-%d %H:%M:%S')
                                  })

    saved_activity = storage.save(activity)
    assert nil != saved_activity.id

    find_activity = storage.find(saved_activity.id)
    assert_equal find_activity.id, saved_activity.id
    assert_equal 1, find_activity.prevId
    assert_equal 2, find_activity.nextId
    assert_equal 1, find_activity.userId
    assert_equal 'user1_new_activity', find_activity.description

    activity1 = storage.find(1)
    assert_equal activity1.nextId, find_activity.id

    activity2 = storage.find(2)
    assert_equal activity2.prevId, find_activity.id
  end

  #
  # При добавлении новой активности без prevId и nextId, она добавляется в конец дня startDate. prevId и nextId
  # высчитываются автоматически и возвращаются в сохранненом объекте активности.
  #
  test 'save new without prev and next ids to existing day without prev day and with next day' do
    storage = Riseup::Storages::ActivitiesStorage.new

    date = '2013-04-21'
    activity = Rest::Activity.new({
                                      'userId' => 1,
                                      'goalId' => nil,
                                      'description' => 'user1_new_activity',
                                      'state' => 0,
                                      'startDate' => date,
                                      'startTime' => '12:00:00',
                                      'cdate' => Time.now.strftime('%Y-%m-%d %H:%M:%S')
                                  })

    saved_activity = storage.save(activity)
    assert nil != saved_activity.id

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    prev_day = storage.get_days_storage.find_prev(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(saved_activity.id)
    assert_equal cur_day.get_activities.last, saved_activity.id
    assert_equal cur_day.get_activities[-2], saved_activity.nextId
    assert_equal nil, prev_day
    assert_equal nil, saved_activity.prevId
  end

  test 'save new without prev and next ids to existing day with prev and next days' do
    storage = Riseup::Storages::ActivitiesStorage.new

    date = '2013-04-28'
    activity = Rest::Activity.new({
                                      'userId' => 1,
                                      'goalId' => nil,
                                      'description' => 'user1_new_activity',
                                      'state' => 0,
                                      'startDate' => date,
                                      'startTime' => '12:00:00',
                                      'cdate' => Time.now.strftime('%Y-%m-%d %H:%M:%S')
                                  })

    saved_activity = storage.save(activity)
    assert nil != saved_activity.id

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    prev_day = storage.get_days_storage.find_prev(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(saved_activity.id)
    assert_equal cur_day.get_activities.last, saved_activity.id
    assert_equal cur_day.get_activities[-2], saved_activity.nextId
    assert_equal prev_day.get_activities.first, saved_activity.prevId
  end

  test 'save new without prev and next to new day before latest existing day' do
    storage = Riseup::Storages::ActivitiesStorage.new

    date = '2013-04-10'
    activity = Rest::Activity.new({
                                      'userId' => 1,
                                      'goalId' => nil,
                                      'description' => 'user1_new_activity',
                                      'state' => 0,
                                      'startDate' => date,
                                      'startTime' => '12:00:00',
                                      'cdate' => Time.now.strftime('%Y-%m-%d %H:%M:%S')
                                  })

    saved_activity = storage.save(activity)
    assert nil != saved_activity.id

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    next_day = storage.get_days_storage.find_next(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    prev_day = storage.get_days_storage.find_prev(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(saved_activity.id)
    assert_equal cur_day.get_activities.last, saved_activity.id
    assert_equal cur_day.get_activities.first, saved_activity.id

    assert_equal nil, prev_day
    assert_equal nil, saved_activity.prevId
    assert_equal next_day.get_activities.last, saved_activity.nextId
  end

  test 'save new without prev and next to new day after newest existing day' do
    storage = Riseup::Storages::ActivitiesStorage.new

    date = '2013-05-29'
    activity = Rest::Activity.new({
                                      'userId' => 1,
                                      'goalId' => nil,
                                      'description' => 'user1_new_activity',
                                      'state' => 0,
                                      'startDate' => date,
                                      'startTime' => '12:00:00',
                                      'cdate' => Time.now.strftime('%Y-%m-%d %H:%M:%S')
                                  })

    saved_activity = storage.save(activity)
    assert nil != saved_activity.id

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    next_day = storage.get_days_storage.find_next(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    prev_day = storage.get_days_storage.find_prev(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(saved_activity.id)
    assert_equal cur_day.get_activities.last, saved_activity.id
    assert_equal cur_day.get_activities.first, saved_activity.id

    assert_equal nil, next_day
    assert_equal nil, saved_activity.nextId
    assert_equal prev_day.get_activities.first, saved_activity.prevId
  end

  test 'save new without prev_id' do
    omit
  end

  test 'save new without next_id' do
    omit
  end

  test 'save new on days right boundary' do
    omit
  end

  test 'save new on days left boundary' do
    omit
  end

  test 'save new on new day' do
    omit
  end

  test 'save existing without reordering' do
    storage = Riseup::Storages::ActivitiesStorage.new
    activity = storage.find(2)

    assert_equal 2, activity.id
    assert_equal 'user1_activity2', activity.description

    activity.description = 'modified description'
    storage.save(activity)

    activity = storage.find(2)
    assert_equal 2, activity.id
    assert_equal 'modified description', activity.description
  end

  #
  # Меняем 3ю и 2ю активности местами за счет изменения параметров activity с id = 2
  #
  test 'save existing with reordering in day middle' do
    storage = Riseup::Storages::ActivitiesStorage.new

    # given
    activity1 = storage.find(1)
    assert_equal 1, activity1.id
    assert_equal nil, activity1.prevId
    assert_equal 2, activity1.nextId

    activity2 = storage.find(2)
    assert_equal 2, activity2.id
    assert_equal 1, activity2.prevId
    assert_equal 3, activity2.nextId

    activity3 = storage.find(3)
    assert_equal 3, activity3.id
    assert_equal 2, activity3.prevId
    assert_equal 4, activity3.nextId

    activity4 = storage.find(4)
    assert_equal 4, activity4.id
    assert_equal 3, activity4.prevId
    assert_equal 5, activity4.nextId

    # when
    activity2.prevId = 3
    activity2.nextId = 4
    storage.save(activity2)

    # than
    activity1 = storage.find(1)
    assert_equal 1, activity1.id
    assert_equal nil, activity1.prevId
    assert_equal 3, activity1.nextId

    activity2 = storage.find(2)
    assert_equal 2, activity2.id
    assert_equal 3, activity2.prevId
    assert_equal 4, activity2.nextId

    activity3 = storage.find(3)
    assert_equal 3, activity3.id
    assert_equal 1, activity3.prevId
    assert_equal 2, activity3.nextId

    activity4 = storage.find(4)
    assert_equal 4, activity4.id
    assert_equal 2, activity4.prevId
    assert_equal 5, activity4.nextId
  end

  #
  # Меняем первую и вторую активности местами
  #
  test 'save existing with reordering without prev_id' do
    storage = Riseup::Storages::ActivitiesStorage.new

    # given
    activity1 = storage.find(1)
    assert_equal 1, activity1.id
    assert_equal nil, activity1.prevId
    assert_equal 2, activity1.nextId

    activity2 = storage.find(2)
    assert_equal 2, activity2.id
    assert_equal 1, activity2.prevId
    assert_equal 3, activity2.nextId

    activity3 = storage.find(3)
    assert_equal 3, activity3.id
    assert_equal 2, activity3.prevId
    assert_equal 4, activity3.nextId

    # when
    activity1.prevId = 2
    activity1.nextId = 3
    storage.save(activity1)

    # than
    activity1 = storage.find(1)
    assert_equal 1, activity1.id
    assert_equal 2, activity1.prevId
    assert_equal 3, activity1.nextId

    activity2 = storage.find(2)
    assert_equal 2, activity2.id
    assert_equal nil, activity2.prevId
    assert_equal 1, activity2.nextId

    activity3 = storage.find(3)
    assert_equal 3, activity3.id
    assert_equal 1, activity3.prevId
    assert_equal 4, activity3.nextId
  end

  #
  # Ставим 13ю после 6ой
  #
  test 'save existing with reordering without next_id' do
    storage = Riseup::Storages::ActivitiesStorage.new

    # given
    activity6 = storage.find(6)
    assert_equal 6, activity6.id
    assert_equal 5, activity6.prevId
    assert_equal 7, activity6.nextId

    activity7 = storage.find(7)
    assert_equal 7, activity7.id
    assert_equal 6, activity7.prevId
    assert_equal 8, activity7.nextId

    activity12 = storage.find(12)
    assert_equal 12, activity12.id
    assert_equal 11, activity12.prevId
    assert_equal 13, activity12.nextId

    activity13 = storage.find(13)
    assert_equal 13, activity13.id
    assert_equal 12, activity13.prevId
    assert_equal nil, activity13.nextId

    # when
    activity13.startDt = '2013-04-28 13:25:50'
    activity13.prevId = 6
    activity13.nextId = 7
    storage.save(activity13)

    # than
    activity6 = storage.find(6)
    assert_equal 6, activity6.id
    assert_equal 5, activity6.prevId
    assert_equal 13, activity6.nextId

    activity7 = storage.find(7)
    assert_equal 7, activity7.id
    assert_equal 13, activity7.prevId
    assert_equal 8, activity7.nextId

    activity12 = storage.find(12)
    assert_equal 12, activity12.id
    assert_equal 11, activity12.prevId
    assert_equal nil, activity12.nextId

    activity13 = storage.find(13)
    assert_equal 13, activity13.id
    assert_equal 6, activity13.prevId
    assert_equal 7, activity13.nextId
  end

  test 'save existing with reordering on days right boundary' do
    omit
  end

  test 'save existing with reordering on days left boundary' do
    omit
  end

  test 'save existing with reordering on new day' do
    omit
  end

  test 'save existing with reordering from day with one activity' do
    omit
  end

  test 'save existing without prev and next ids to existing day from first existing day' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(2)
    assert activity

    old_day = activity.get_day

    # Перемещаем активность на другой день без явного указания позиции в нем
    date = '2013-04-28'
    activity.startDate = date
    activity.prevId = activity.nextId = nil

    activity = storage.save(activity)

    #TODO: !defect! Убрать reload old_day после внедения Identity Map в DaysStorage
    old_day = storage.get_days_storage.find(old_day.user_id, old_day.get_date(true))

    assert !old_day.has_activity(activity.id)

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    prev_day = storage.get_days_storage.find_prev(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(activity.id)
    assert_equal cur_day.get_activities.last, activity.id
    assert_equal cur_day.get_activities[-2], activity.nextId

    assert_equal prev_day.get_activities.first, activity.prevId
  end

  test 'save existing without prev and next ids to existing day in middle' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(6)
    assert activity

    old_day = activity.get_day

    # Перемещаем активность на другой день без явного указания позиции в нем
    date = '2013-05-09'
    activity.startDate = date
    activity.prevId = activity.nextId = nil

    activity = storage.save(activity)

    #TODO: !defect! Убрать reload old_day после внедения Identity Map в DaysStorage
    old_day = storage.get_days_storage.find(old_day.user_id, old_day.get_date(true))

    assert !old_day.has_activity(activity.id)

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    prev_day = storage.get_days_storage.find_prev(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(activity.id)
    assert_equal cur_day.get_activities.last, activity.id
    assert_equal cur_day.get_activities[-2], activity.nextId

    assert_equal prev_day.get_activities.first, activity.prevId
  end

  test 'save existing without prev and next ids to new day with prev and next days' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(6)
    assert activity

    old_day = activity.get_day

    # Перемещаем активность на другой день без явного указания позиции в нем
    date = '2013-05-08'
    activity.startDate = date
    activity.prevId = activity.nextId = nil

    activity = storage.save(activity)

    #TODO: !defect! Убрать reload old_day после внедения Identity Map в DaysStorage
    old_day = storage.get_days_storage.find(old_day.user_id, old_day.get_date(true))

    assert !old_day.has_activity(activity.id)

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    next_day = storage.get_days_storage.find_next(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    prev_day = storage.get_days_storage.find_prev(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(activity.id)
    assert_equal cur_day.get_activities.first, activity.id
    assert_equal cur_day.get_activities.last, activity.id

    assert_equal next_day.get_activities.last, activity.nextId
    assert_equal prev_day.get_activities.first, activity.prevId
  end

  test 'save existing without prev and next ids to new day with prev day and without next day' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(6)
    assert activity

    old_day = activity.get_day

    # Перемещаем активность на другой день без явного указания позиции в нем
    date = '2013-05-29'
    activity.startDate = date
    activity.prevId = activity.nextId = nil

    activity = storage.save(activity)

    #TODO: !defect! Убрать reload old_day после внедения Identity Map в DaysStorage
    old_day = storage.get_days_storage.find(old_day.user_id, old_day.get_date(true))

    assert !old_day.has_activity(activity.id)

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    prev_day = storage.get_days_storage.find_prev(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(activity.id)
    assert_equal cur_day.get_activities.first, activity.id
    assert_equal cur_day.get_activities.last, activity.id

    assert_equal nil, activity.nextId
    assert_equal prev_day.get_activities.first, activity.prevId
  end

  test 'save existing without prev and next ids to new day with next day and without prev day' do
    storage = Riseup::Storages::ActivitiesStorage.new

    activity = storage.find(6)
    assert activity

    old_day = activity.get_day

    # Перемещаем активность на другой день без явного указания позиции в нем
    date = '2013-03-29'
    activity.startDate = date
    activity.prevId = activity.nextId = nil

    activity = storage.save(activity)

    #TODO: !defect! Убрать reload old_day после внедения Identity Map в DaysStorage
    old_day = storage.get_days_storage.find(old_day.user_id, old_day.get_date(true))

    assert !old_day.has_activity(activity.id)

    cur_day = storage.get_days_storage.find(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))
    next_day = storage.get_days_storage.find_next(1, Riseup::Utils::TimeUtils.date_to_timestamp(date))

    assert cur_day.has_activity(activity.id)
    assert_equal cur_day.get_activities.first, activity.id
    assert_equal cur_day.get_activities.last, activity.id

    assert_equal next_day.get_activities.last, activity.nextId
    assert_equal nil, activity.prevId
  end

  test 'save existing with modified invariant field' do
    storage = Riseup::Storages::ActivitiesStorage.new
    activity = storage.find(2)

    assert_equal 2, activity.id
    assert_equal 'user1_activity2', activity.description

    activity.userId = 9001
    assert_raise(RuntimeError.new('Invariant field(s) changed.')) do
      storage.save(activity)
    end
  end

  test 'find goal statistic' do
    storage = Riseup::Storages::ActivitiesStorage.new
    stat = storage.find_goal_stat(1, DateTime.new(2013, 5, 22, 22, 35, 0).to_s(:db))

    assert_equal 1, stat.get_goal_id
    assert_equal 2, stat.get_completed_count
    assert_equal 2, stat.get_failed_count
    assert_equal 6, stat.get_total_count

    stat = storage.find_goal_stat(1, DateTime.new(2013, 5, 29, 22, 35, 0).to_s(:db))
    assert_equal 1, stat.get_goal_id
    assert_equal 2, stat.get_completed_count
    assert_equal 4, stat.get_failed_count
    assert_equal 6, stat.get_total_count
  end
end
