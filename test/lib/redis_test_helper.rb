module RedisTestHelper
  def flush_redis_db
    Riseup::Services::Providers::RedisProvider.get_service.flushdb
  end

  def load_redis_fixtures(key)
    if :activities == key
      storage = Riseup::Storages::DaysStorage.new
      storage.save(Riseup::Domain::Activities::Day.new(1, DateTime.strptime('2013-04-21', '%Y-%m-%d').to_time.to_i, [1, 2, 3, 4]))
      storage.save(Riseup::Domain::Activities::Day.new(1, DateTime.strptime('2013-04-28', '%Y-%m-%d').to_time.to_i, [5, 6, 7]))
      storage.save(Riseup::Domain::Activities::Day.new(1, DateTime.strptime('2013-05-09', '%Y-%m-%d').to_time.to_i, [8, 9, 10]))
      storage.save(Riseup::Domain::Activities::Day.new(1, DateTime.strptime('2013-05-10', '%Y-%m-%d').to_time.to_i, [11, 12, 13]))

      storage.save(Riseup::Domain::Activities::Day.new(2, DateTime.strptime('2013-04-21', '%Y-%m-%d').to_time.to_i, [14]))
      storage.save(Riseup::Domain::Activities::Day.new(2, DateTime.strptime('2013-05-07', '%Y-%m-%d').to_time.to_i, [15]))
    end
  end
end
