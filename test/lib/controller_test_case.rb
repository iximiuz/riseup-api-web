require 'test_helper'

class ControllerTestCase < ActionController::TestCase
  def raw_post(action, params, body, session = nil, flash = nil)
    do_raw_request(:post, action, params, body, session, flash)
  end

  def raw_put(action, params, body, session = nil, flash = nil)
    do_raw_request(:put, action, params, body, session, flash)
  end

  private
  def do_raw_request(method, action, params, body, session, flash)
    @request.env['RAW_POST_DATA'] = body
    response = self.send(method, action, params, session, flash)
    @request.env.delete('RAW_POST_DATA')
    response
  end
end
