require 'lib/redis_test_helper'
require 'lib/controller_test_case'

class ActivitiesControllerTest < ControllerTestCase
  include RedisTestHelper

  def setup
    super

    flush_redis_db
    load_redis_fixtures(:activities)
  end

  # GET list
  test 'get activities with missed params' do
    get(:index, {:date => '2013-05-05', :token => 'd17369bc12a54db84464ed4354fe8ce4'})   #user_id == 1
    assert_response :success
    assert_equal '{"error":"Missed required params [acount] and [bcount]","code":1008}', @response.body
  end

  # GET list
  test 'get activities by date with only after count' do
    get(:index, {:date => '2013-05-05', :acount => 3, :token => 'd17369bc12a54db84464ed4354fe8ce4'})   #user_id == 1
    assert_response :success
    assert_equal '[{"id":8,"nextId":9,"prevId":7,"userId":1,"goalId":null,"description":"user1_activity8","startDate":"2013-05-09","startTime":"01:00:00","state":1,"cdate":"2013-04-28T12:18:17Z"},{"id":9,"nextId":10,"prevId":8,"userId":1,"goalId":null,"description":"user1_activity9","startDate":"2013-05-09","startTime":"05:00:00","state":1,"cdate":"2013-04-28T12:18:17Z"},{"id":10,"nextId":11,"prevId":9,"userId":1,"goalId":null,"description":"user1_activity10","startDate":"2013-05-09","startTime":"01:00:00","state":1,"cdate":"2013-04-28T12:18:17Z"}]', @response.body
  end

  # GET list
  test 'get activities by date with only before count' do
    get(:index, {:date => '2013-05-05', :bcount => 2, :token => 'd17369bc12a54db84464ed4354fe8ce4'})   #user_id == 1
    assert_response :success
    assert_equal '[{"id":6,"nextId":7,"prevId":5,"userId":1,"goalId":null,"description":"user1_activity6","startDt":"2013-04-28T12:00:00Z","cdate":"2013-04-28T12:18:17Z"},{"id":7,"nextId":8,"prevId":6,"userId":1,"goalId":null,"description":"user1_activity7","startDt":"2013-04-28T00:40:00Z","cdate":"2013-04-28T12:18:17Z"}]', @response.body
  end

  # GET list
  test 'get activities by date with after count and before count' do
    get(:index, {:date => '2013-05-05', :acount => 3, :bcount => 2, :token => 'd17369bc12a54db84464ed4354fe8ce4'})   #user_id == 1
    assert_response :success
    assert_equal '[{"id":6,"nextId":7,"prevId":5,"userId":1,"goalId":null,"description":"user1_activity6","startDt":"2013-04-28T12:00:00Z","cdate":"2013-04-28T12:18:17Z"},{"id":7,"nextId":8,"prevId":6,"userId":1,"goalId":null,"description":"user1_activity7","startDt":"2013-04-28T00:40:00Z","cdate":"2013-04-28T12:18:17Z"},{"id":8,"nextId":9,"prevId":7,"userId":1,"goalId":null,"description":"user1_activity8","startDt":"2013-05-09T01:00:00Z","cdate":"2013-04-28T12:18:17Z"},{"id":9,"nextId":10,"prevId":8,"userId":1,"goalId":null,"description":"user1_activity9","startDt":"2013-05-09T05:00:00Z","cdate":"2013-04-28T12:18:17Z"},{"id":10,"nextId":11,"prevId":9,"userId":1,"goalId":null,"description":"user1_activity10","startDt":"2013-05-09T01:00:00Z","cdate":"2013-04-28T12:18:17Z"}]', @response.body
  end

  # GET list
  test 'get activities by central activity id' do
    get(:index, {:aid => 4, :acount => 7, :bcount => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'})   #user_id == 1
    assert_response :success

    expected_activity3 = {
        id: 3,
        nextId: 4,
        prevId: 2,
        userId: 1,
        goalId: nil,
        description: 'user1_activity3',
        state: 1,
        startDate: '2013-04-21',
        startTime: '10:00:00',
        cdate: '2013-04-28T12:18:17Z'
    }


    assert_equal "[{\"id\":3,\"nextId\":4,\"prevId\":2,\"userId\":1,\"goalId\":null,\"description\":\"user1_activity3\",\"startDate\":\"2013-04-21\",\"startTime\":\"10:00:00\",\"state\":1,\"cdate\":\"2013-04-28T12:18:17Z\"},{\"id\":4,\"nextId\":5,\"prevId\":3,\"userId\":1,\"goalId\":null,\"description\":\"user1_activity4\",\"startDt\":\"2013-04-21T05:00:00Z\",\"cdate\":\"2013-04-28T12:18:17Z\"},{\"id\":5,\"nextId\":6,\"prevId\":4,\"userId\":1,\"goalId\":null,\"description\":\"user1_activity5\",\"startDt\":\"2013-04-28T00:00:00Z\",\"cdate\":\"2013-04-28T12:18:17Z\"},{\"id\":6,\"nextId\":7,\"prevId\":5,\"userId\":1,\"goalId\":null,\"description\":\"user1_activity6\",\"startDt\":\"2013-04-28T12:00:00Z\",\"cdate\":\"2013-04-28T12:18:17Z\"},{\"id\":7,\"nextId\":8,\"prevId\":6,\"userId\":1,\"goalId\":null,\"description\":\"user1_activity7\",\"startDt\":\"2013-04-28T00:40:00Z\",\"cdate\":\"2013-04-28T12:18:17Z\"},{\"id\":8,\"nextId\":9,\"prevId\":7,\"userId\":1,\"goalId\":null,\"description\":\"user1_activity8\",\"startDt\":\"2013-05-09T01:00:00Z\",\"cdate\":\"2013-04-28T12:18:17Z\"},{\"id\":9,\"nextId\":10,\"prevId\":8,\"userId\":1,\"goalId\":null,\"description\":\"user1_activity9\",\"startDt\":\"2013-05-09T05:00:00Z\",\"cdate\":\"2013-04-28T12:18:17Z\"},{\"id\":10,\"nextId\":11,\"prevId\":9,\"userId\":1,\"goalId\":null,\"description\":\"user1_activity10\",\"startDt\":\"2013-05-09T01:00:00Z\",\"cdate\":\"2013-04-28T12:18:17Z\"}]", @response.body
  end

  # POST one
  test 'post first activity' do
    new_activity = {
            nextId: nil,
            prevId: nil,
            userId: 3,
            goalId: nil,
            description: 'user3_new_activity',
            state: 0,
            startDate: '2013-5-28',
            startTime: '12:00:00',
            cdate: Time.now.strftime('%Y-%m-%d %H:%M:%S')
        }

    raw_post(    #user_id == 3
        :create,
        {:token => '0a36f0d3822ee82894b7dcb01dca1b44'},
        new_activity.to_json
    )

    assert_response :success

    created_activity = JSON.parse(@response.body)
    assert created_activity.has_key?('id')
    assert_equal new_activity[:nextId], created_activity['nextId']
    assert_equal new_activity[:prevId], created_activity['prevId']
    assert_equal new_activity[:userId], created_activity['userId']
    assert_equal new_activity[:goalId], created_activity['goalId']
    assert_equal new_activity[:description], created_activity['description']
    assert_equal new_activity[:startDate], created_activity['startDate']
    assert_equal new_activity[:startTime], created_activity['startTime']
    assert_equal new_activity[:cdate], created_activity['cdate']
  end

  # POST one
  test 'post two activities sequentially' do
    #
    # first request
    #
    new_activity1 = {
        nextId: nil,
        prevId: nil,
        userId: 3,
        goalId: nil,
        description: 'user3_first_new_activity',
        state: 0,
        startDate: '2013-5-28',
        startTime: '12:00:00',
        cdate: Time.now.strftime('%Y-%m-%d %H:%M:%S')
    }

    raw_post(    #user_id == 3
        :create,
        {:token => '0a36f0d3822ee82894b7dcb01dca1b44'},
        new_activity1.to_json
    )

    assert_response :success

    created_activity1 = JSON.parse(@response.body)
    assert created_activity1.has_key?('id')
    assert_equal new_activity1[:nextId], created_activity1['nextId']
    assert_equal new_activity1[:prevId], created_activity1['prevId']
    assert_equal new_activity1[:userId], created_activity1['userId']
    assert_equal new_activity1[:goalId], created_activity1['goalId']
    assert_equal new_activity1[:description], created_activity1['description']
    assert_equal new_activity1[:state], created_activity1['state']
    assert_equal new_activity1[:startDate], created_activity1['startDate']
    assert_equal new_activity1[:startTime], created_activity1['startTime']
    assert_equal new_activity1[:cdate], created_activity1['cdate']


    #
    # second request
    #
    new_activity2 = {
        nextId: nil,
        prevId: created_activity1['id'],
        userId: 3,
        goalId: nil,
        description: 'user3_second_new_activity',
        state: 1,
        startDate: '2013-5-28',
        startTime: '12:00:00',
        cdate: Time.now.strftime('%Y-%m-%d %H:%M:%S')
    }

    raw_post(    #user_id == 3
        :create,
        {:token => '0a36f0d3822ee82894b7dcb01dca1b44'},
        new_activity2.to_json
    )

    assert_response :success

    created_activity2 = JSON.parse(@response.body)
    assert created_activity2.has_key?('id')
    assert_equal new_activity2[:nextId], created_activity2['nextId']
    assert_equal new_activity2[:prevId], created_activity2['prevId']
    assert_equal new_activity2[:userId], created_activity2['userId']
    assert_equal new_activity2[:goalId], created_activity2['goalId']
    assert_equal new_activity2[:description], created_activity2['description']
    assert_equal new_activity2[:state], created_activity2['state']
    assert_equal new_activity2[:startDate], created_activity2['startDate']
    assert_equal new_activity2[:startTime], created_activity2['startTime']
    assert_equal new_activity2[:cdate], created_activity2['cdate']
  end

  # PUT one
  test 'put activity and update attrs without reordering' do
    activity_to_update = {
        id: 6,
        nextId: 7,
        prevId: 5,
        userId: 1,
        goalId: nil,
        description: 'new_description',  # old descr was 'user1_activity6'
        startDate: '2013-4-28',
        startTime: '12:00:00',
        cdate: '2013-04-28T12:18:17Z'
    }

    raw_put(    #user_id == 1
        :update,
        {:id => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'},
        activity_to_update.to_json
    )

    assert_response :success

    updated_activity = JSON.parse(@response.body)

    assert_equal activity_to_update[:id], updated_activity['id']
    assert_equal activity_to_update[:nextId], updated_activity['nextId']
    assert_equal activity_to_update[:prevId], updated_activity['prevId']
    assert_equal activity_to_update[:userId], updated_activity['userId']
    assert_equal activity_to_update[:goalId], updated_activity['goalId']
    assert_equal activity_to_update[:description], updated_activity['description']
    assert_equal activity_to_update[:startDate], updated_activity['startDate']
    assert_equal activity_to_update[:startTime], updated_activity['startTime']
    assert_equal activity_to_update[:cdate], updated_activity['cdate']
  end

  # PUT one
  test 'put activity and update attrs with reordering' do
    activity_to_update = {
        id: 6,
        nextId: 5,
        prevId: 4,
        userId: 1,
        goalId: nil,
        description: 'new_description',  # old descr was 'user1_activity6'
        startDate: '2013-4-28',
        startTime: '12:00:00',
        cdate: '2013-04-28T12:18:17Z'
    }

    raw_put(    #user_id == 1
        :update,
        {:id => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'},
        activity_to_update.to_json
    )

    assert_response :success

    updated_activity = JSON.parse(@response.body)

    assert_equal activity_to_update[:id], updated_activity['id']
    assert_equal activity_to_update[:nextId], updated_activity['nextId']
    assert_equal activity_to_update[:prevId], updated_activity['prevId']
    assert_equal activity_to_update[:userId], updated_activity['userId']
    assert_equal activity_to_update[:goalId], updated_activity['goalId']
    assert_equal activity_to_update[:description], updated_activity['description']
    assert_equal activity_to_update[:startDate], updated_activity['startDate']
    assert_equal activity_to_update[:startTime], updated_activity['startTime']
    assert_equal activity_to_update[:cdate], updated_activity['cdate']
  end

  # DELETE one
  test 'delete activity' do
    get(:show, {:id => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'})  #user_id == 1
    assert_equal 1, JSON::parse(@response.body)['id']

    delete(:destroy, {:id => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'})  #user_id == 1
    assert_response 406

    get(:show, {:id => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'})  #user_id == 1
    assert_equal 'null', @response.body
  end

  # DELETE one
  test 'delete idempotence' do
    delete(:destroy, {:id => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'})  #user_id == 1
    assert_response 406

    delete(:destroy, {:id => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'})  #user_id == 1
    assert_response 406

    delete(:destroy, {:id => 1, :token => 'd17369bc12a54db84464ed4354fe8ce4'})  #user_id == 1
    assert_response 406
  end
end
