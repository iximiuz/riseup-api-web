require 'lib/controller_test_case'

class AuthTokensControllerTest < ControllerTestCase
  test 'register new' do
    post(:create, {:user => 'new_user', :password => '1234'})
    assert_response :success

    token = JSON::parse(@response.body)
    assert token.has_key?('token')
  end

  test 'register when user already exists with same password' do
    post(:create, {:user => 'spartacus@riseup.su', :password => '1234'})
    assert_response :success

    response = JSON::parse(@response.body)
    assert response.has_key?('error')
    assert response.has_key?('code')
    assert_equal 1011, response['code']
  end

  test 'register when user already exists with different password' do
    post(:create, {:user => 'spartacus@riseup.su', :password => '5678'})
    assert_response :success

    error = JSON::parse(@response.body)
    assert error.has_key?('error')
    assert error.has_key?('code')
    assert_equal 1011, error['code']
  end

  test 'login' do
    get(:show, {:id => 'spartacus@riseup.su', :password => '1234'})
    assert_response :success

    token = JSON::parse(@response.body)
    assert token.has_key?('token')
  end

  test 'login non existing user' do
    get(:show, {:id => 'batiatus@riseup.su', :password => '1234'})
    assert_response :success

    error = JSON::parse(@response.body)
    assert error.has_key?('error')
    assert error.has_key?('code')
    assert_equal 1010, error['code']
  end

  test 'login with wrong password' do
    get(:show, {:id => 'spartacus@riseup.su', :password => '12345678'})
    assert_response :success

    error = JSON::parse(@response.body)
    assert error.has_key?('error')
    assert error.has_key?('code')
    assert_equal 1003, error['code']
  end
end
