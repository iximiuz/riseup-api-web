# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 0) do

  create_table "activities", :force => true do |t|
    t.integer  "user_id",       :limit => 8
    t.integer  "goal_id",       :limit => 8
    t.string   "description",   :limit => 32768
    t.integer  "state",         :limit => 2,     :default => 0, :null => false
    t.datetime "start_dt",                                      :null => false
    t.datetime "cdate",                                         :null => false
    t.datetime "udate",                                         :null => false
    t.integer  "privacy_level", :limit => 2,     :default => 0, :null => false
  end

  add_index "activities", ["goal_id"], :name => "goal_id"

  create_table "activities_registries", :force => true do |t|
    t.integer "user_id", :limit => 8
    t.date    "date",                                :null => false
    t.integer "count",                :default => 0, :null => false
  end

  add_index "activities_registries", ["user_id", "date"], :name => "activities_registries_unique", :unique => true

  create_table "auth_tokens", :force => true do |t|
    t.integer  "user_id",   :limit => 8
    t.string   "client_id", :limit => 16
    t.string   "token",     :limit => 32
    t.datetime "cdate",                   :null => false
  end

  create_table "goals", :force => true do |t|
    t.integer  "user_id",     :limit => 8,                     :null => false
    t.string   "description", :limit => 32768, :default => "", :null => false
    t.integer  "state",       :limit => 2,     :default => 0,  :null => false
    t.datetime "end_dt"
    t.datetime "created_at",                                   :null => false
  end

  add_index "goals", ["user_id"], :name => "user_id"

  create_table "users", :force => true do |t|
    t.string   "nickname",    :limit => 32
    t.string   "name",        :limit => 64
    t.string   "lastname",    :limit => 64
    t.string   "sex",         :limit => 1
    t.integer  "credentials",                :default => 2,  :null => false
    t.string   "email"
    t.string   "pwd_hash"
    t.integer  "timezone",                   :default => 0,  :null => false
    t.datetime "reg_dt",                                     :null => false
    t.string   "avatar_url",  :limit => 256, :default => ""
  end

  add_index "users", ["email"], :name => "email", :unique => true

end
