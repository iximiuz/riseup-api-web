-- Table: comments

-- DROP TABLE comments;

CREATE TABLE comments
(
  id bigserial NOT NULL,
  entity_type integer NOT NULL DEFAULT 1,
  entity_id bigint NOT NULL,
  msg text NOT NULL,
  user_id bigint NOT NULL,
  answer_user_id bigint,
  create_dt timestamp with time zone DEFAULT now(),
  is_moderated integer DEFAULT 0,
  is_deleted integer DEFAULT 0
)
WITH (
  OIDS=FALSE
);
ALTER TABLE comments
  OWNER TO riseup;


ALTER TABLE activities ADD COLUMN comments_count integer;
ALTER TABLE activities ALTER COLUMN comments_count SET NOT NULL;
ALTER TABLE activities ALTER COLUMN comments_count SET DEFAULT 0;