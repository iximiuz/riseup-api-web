ALTER TABLE activities DROP CONSTRAINT activities_goal_id_fkey;
ALTER TABLE activities ADD CONSTRAINT activities_goal_id_fkey FOREIGN KEY (goal_id)
      REFERENCES goals (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE SET NULL;