CREATE TABLE subscriptions
(
  id serial NOT NULL,
  uniq_hash character(40),
  user_id bigint,
  subscription_type integer,
  create_dt timestamp without time zone,
  update_dt timestamp without time zone DEFAULT now(),
  CONSTRAINT pk PRIMARY KEY (id),
  CONSTRAINT uniq_hash UNIQUE (uniq_hash)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE subscriptions
  OWNER TO riseup;