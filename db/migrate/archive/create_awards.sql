CREATE TABLE `awards` (
	`id` INT(11) NOT NULL,
	`is_active` TINYINT(1) NOT NULL DEFAULT '0',
	`name` VARCHAR(128) NOT NULL,
	`img_filename` VARCHAR(128) NOT NULL
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;

ALTER TABLE `awards`
	ADD PRIMARY KEY (`id`);

CREATE TABLE `users_awards` (
	`id` INT(11) NOT NULL,
	`user_id` INT(11) NULL DEFAULT NULL,
	`award_id` INT(11) NULL DEFAULT NULL,
	`issue_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `FK__users` (`user_id`),
	INDEX `FK__awards` (`award_id`),
	CONSTRAINT `FK__users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `FK__awards` FOREIGN KEY (`award_id`) REFERENCES `awards` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
