alter table users add column `name` varchar(64) after nickname;
alter table users add column `lastname` varchar(64) after `name`;
alter table users add column `sex` char(1) after `lastname`;
alter table users add column `credentials` INT(11) after `sex`;