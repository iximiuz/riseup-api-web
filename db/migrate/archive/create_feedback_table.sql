CREATE TABLE `feedback` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`text` TEXT NULL COLLATE 'utf8_unicode_ci',
	`user_id` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
