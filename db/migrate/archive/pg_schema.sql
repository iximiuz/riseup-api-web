--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: fn_tr_ad_activities_registry_fill(); Type: FUNCTION; Schema: public; Owner: riseup
--

CREATE FUNCTION fn_tr_ad_activities_registry_fill() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE activities_registries SET "count" = GREATEST(0, "count" - 1) 
    WHERE user_id = OLD.user_id AND "date" = OLD.start_dt::DATE;
    
    RETURN NEW;
END $$;


ALTER FUNCTION public.fn_tr_ad_activities_registry_fill() OWNER TO riseup;

--
-- Name: fn_tr_ai_activities_registry_fill(); Type: FUNCTION; Schema: public; Owner: riseup
--

CREATE FUNCTION fn_tr_ai_activities_registry_fill() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    BEGIN
        INSERT INTO activities_registries(user_id, "date", "count") VALUES (NEW.user_id, NEW.start_dt::DATE, 1);
    EXCEPTION WHEN unique_violation THEN
        UPDATE activities_registries SET "count" = "count" + 1 WHERE user_id = NEW.user_id AND "date" = NEW.start_dt::DATE;
    END;

    RETURN NEW;
END $$;


ALTER FUNCTION public.fn_tr_ai_activities_registry_fill() OWNER TO riseup;

--
-- Name: fn_tr_au_activities_registry_fill(); Type: FUNCTION; Schema: public; Owner: riseup
--

CREATE FUNCTION fn_tr_au_activities_registry_fill() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF NEW.start_dt::DATE != OLD.start_dt::DATE THEN
        BEGIN
            INSERT INTO activities_registries (user_id, "date", "count") VALUES (NEW.user_id, NEW.start_dt::DATE, 1);
        EXCEPTION WHEN unique_violation THEN
            UPDATE activities_registries SET "count" = "count" + 1 WHERE user_id = NEW.user_id AND "date" = NEW.start_dt::DATE;
        END;

        UPDATE activities_registries SET "count" = GREATEST(0, "count" - 1) WHERE user_id = OLD.user_id AND "date" = OLD.start_dt::DATE;
    END IF;
    
    RETURN NEW;
END $$;


ALTER FUNCTION public.fn_tr_au_activities_registry_fill() OWNER TO riseup;

--
-- Name: activities_id_seq; Type: SEQUENCE; Schema: public; Owner: riseup
--

CREATE SEQUENCE activities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activities_id_seq OWNER TO riseup;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activities; Type: TABLE; Schema: public; Owner: riseup; Tablespace: 
--

CREATE TABLE activities (
    id bigint DEFAULT nextval('activities_id_seq'::regclass) NOT NULL,
    user_id bigint,
    goal_id bigint,
    description character varying(32768),
    state smallint DEFAULT 0 NOT NULL,
    start_dt timestamp without time zone NOT NULL,
    cdate timestamp with time zone DEFAULT now() NOT NULL,
    udate timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.activities OWNER TO riseup;

--
-- Name: activities_registries_id_seq; Type: SEQUENCE; Schema: public; Owner: riseup
--

CREATE SEQUENCE activities_registries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activities_registries_id_seq OWNER TO riseup;

--
-- Name: activities_registries; Type: TABLE; Schema: public; Owner: riseup; Tablespace: 
--

CREATE TABLE activities_registries (
    id bigint DEFAULT nextval('activities_registries_id_seq'::regclass) NOT NULL,
    user_id bigint,
    date date NOT NULL,
    count integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.activities_registries OWNER TO riseup;

--
-- Name: auth_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: riseup
--

CREATE SEQUENCE auth_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_tokens_id_seq OWNER TO riseup;

--
-- Name: auth_tokens; Type: TABLE; Schema: public; Owner: riseup; Tablespace: 
--

CREATE TABLE auth_tokens (
    id bigint DEFAULT nextval('auth_tokens_id_seq'::regclass) NOT NULL,
    user_id bigint,
    client_id character varying(16),
    token character varying(32),
    cdate timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.auth_tokens OWNER TO riseup;

--
-- Name: goals_id_seq; Type: SEQUENCE; Schema: public; Owner: riseup
--

CREATE SEQUENCE goals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.goals_id_seq OWNER TO riseup;

--
-- Name: goals; Type: TABLE; Schema: public; Owner: riseup; Tablespace: 
--

CREATE TABLE goals (
    id bigint DEFAULT nextval('goals_id_seq'::regclass) NOT NULL,
    user_id bigint NOT NULL,
    description character varying(32768) DEFAULT ''::character varying NOT NULL,
    state smallint DEFAULT 0 NOT NULL,
    end_dt timestamp without time zone,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.goals OWNER TO riseup;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: riseup
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO riseup;

--
-- Name: users; Type: TABLE; Schema: public; Owner: riseup; Tablespace: 
--

CREATE TABLE users (
    id bigint DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    nickname character varying(32),
    name character varying(64),
    lastname character varying(64),
    sex character(1) DEFAULT NULL::bpchar,
    credentials integer DEFAULT 2 NOT NULL,
    email character varying(255),
    pwd_hash character varying(255),
    timezone integer DEFAULT 0 NOT NULL,
    reg_dt timestamp with time zone NOT NULL,
    avatar_url character varying(256) DEFAULT ''::character varying
);


ALTER TABLE public.users OWNER TO riseup;

--
-- Name: activities_pkey; Type: CONSTRAINT; Schema: public; Owner: riseup; Tablespace: 
--

ALTER TABLE ONLY activities
    ADD CONSTRAINT activities_pkey PRIMARY KEY (id);


--
-- Name: activities_registries_pkey; Type: CONSTRAINT; Schema: public; Owner: riseup; Tablespace: 
--

ALTER TABLE ONLY activities_registries
    ADD CONSTRAINT activities_registries_pkey PRIMARY KEY (id);


--
-- Name: activities_registries_unique; Type: CONSTRAINT; Schema: public; Owner: riseup; Tablespace: 
--

ALTER TABLE ONLY activities_registries
    ADD CONSTRAINT activities_registries_unique UNIQUE (user_id, date);


--
-- Name: auth_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: riseup; Tablespace: 
--

ALTER TABLE ONLY auth_tokens
    ADD CONSTRAINT auth_tokens_pkey PRIMARY KEY (id);


--
-- Name: goals_pkey; Type: CONSTRAINT; Schema: public; Owner: riseup; Tablespace: 
--

ALTER TABLE ONLY goals
    ADD CONSTRAINT goals_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: riseup; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: email; Type: INDEX; Schema: public; Owner: riseup; Tablespace: 
--

CREATE UNIQUE INDEX email ON users USING btree (email);


--
-- Name: goal_id; Type: INDEX; Schema: public; Owner: riseup; Tablespace: 
--

CREATE INDEX goal_id ON activities USING btree (goal_id);


--
-- Name: user_id; Type: INDEX; Schema: public; Owner: riseup; Tablespace: 
--

CREATE INDEX user_id ON goals USING btree (user_id);


--
-- Name: tr_ad_activities_registry_fill; Type: TRIGGER; Schema: public; Owner: riseup
--

CREATE TRIGGER tr_ad_activities_registry_fill AFTER DELETE ON activities FOR EACH ROW EXECUTE PROCEDURE fn_tr_ad_activities_registry_fill();


--
-- Name: tr_ai_activities_registry_fill; Type: TRIGGER; Schema: public; Owner: riseup
--

CREATE TRIGGER tr_ai_activities_registry_fill AFTER INSERT ON activities FOR EACH ROW EXECUTE PROCEDURE fn_tr_ai_activities_registry_fill();


--
-- Name: tr_au_activities_registry_fill; Type: TRIGGER; Schema: public; Owner: riseup
--

CREATE TRIGGER tr_au_activities_registry_fill AFTER UPDATE ON activities FOR EACH ROW EXECUTE PROCEDURE fn_tr_au_activities_registry_fill();


--
-- Name: activities_goal_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: riseup
--

ALTER TABLE ONLY activities
    ADD CONSTRAINT activities_goal_id_fkey FOREIGN KEY (goal_id) REFERENCES goals(id);


--
-- Name: activities_registries_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: riseup
--

ALTER TABLE ONLY activities_registries
    ADD CONSTRAINT activities_registries_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: activities_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: riseup
--

ALTER TABLE ONLY activities
    ADD CONSTRAINT activities_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: auth_tokens_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: riseup
--

ALTER TABLE ONLY auth_tokens
    ADD CONSTRAINT auth_tokens_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: goals_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: riseup
--

ALTER TABLE ONLY goals
    ADD CONSTRAINT goals_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;


--
-- PostgreSQL database dump complete
--


CREATE SEQUENCE feedback_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER TABLE public.feedback_id_seq OWNER TO riseup;

CREATE TABLE feedback (
  id bigint DEFAULT nextval('feedback_id_seq'::regclass) NOT NULL,
  user_id bigint default null,
  text varchar,
  cdate timestamp with time zone DEFAULT now() NOT NULL,
  udate timestamp with time zone DEFAULT now() NOT NULL
);

ALTER TABLE public.feedback OWNER TO riseup;

ALTER TABLE ONLY feedback
ADD CONSTRAINT feedback_pkey PRIMARY KEY (id);

ALTER TABLE ONLY feedback
ADD CONSTRAINT feeback_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);