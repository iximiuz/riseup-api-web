CREATE SCHEMA live_hub;
REVOKE ALL ON SCHEMA live_hub FROM PUBLIC;
GRANT ALL ON SCHEMA live_hub TO riseup;

CREATE TABLE live_hub.events
(
    id BIGSERIAL PRIMARY KEY,
    event_type SMALLINT NOT NULL,
    privacy_level SMALLINT NOT NULL,
    generator_id BIGINT NOT NULL,
    generator_type SMALLINT NOT NULL,
    subject_id VARCHAR(24) NOT NULL,
    subject_type SMALLINT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
)
WITH (
  OIDS=FALSE
);

ALTER TABLE live_hub.events OWNER TO riseup;

CREATE INDEX event_created_generator_idx ON live_hub.events USING btree (created_at, generator_id);



CREATE TABLE live_hub.subscriptions
(
  id bigserial NOT NULL,
  user_id bigint NOT NULL,
  generator_id bigint NOT NULL,
  generator_type smallint NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  CONSTRAINT subscriptions_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE live_hub.subscriptions
  OWNER TO riseup;

CREATE INDEX subscriptions_user_id_idx
  ON live_hub.subscriptions
  USING btree
  (user_id);

ALTER TABLE live_hub.subscriptions
  ADD CONSTRAINT user_subscriber_uniquiness UNIQUE (user_id, generator_id, generator_type);