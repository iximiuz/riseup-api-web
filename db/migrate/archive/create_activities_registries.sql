USE riseupdb_v2;

DROP TABLE IF EXISTS `activities_registries`;
CREATE TABLE `activities_registries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `count` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`date`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TRIGGER IF EXISTS `riseupdb_v2`.`tr_ad_registry_fill`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` TRIGGER `riseupdb_v2`.`tr_ad_registry_fill` AFTER DELETE ON `riseupdb_v2`.`activities`
FOR EACH ROW BEGIN
  UPDATE activities_registries SET activities_registries.`count` = GREATEST(0, activities_registries.`count` - 1) WHERE user_id = OLD.user_id AND activities_registries.`date` = DATE(OLD.start_dt);
END $$
DELIMITER ;

DROP TRIGGER IF EXISTS `riseupdb_v2`.`tr_ai_registry_fill`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` TRIGGER `riseupdb_v2`.`tr_ai_registry_fill` AFTER INSERT ON `riseupdb_v2`.`activities`
FOR EACH ROW BEGIN
  INSERT INTO activities_registries (user_id, `date`, `count`) VALUES (NEW.user_id, DATE(NEW.start_dt), 1)
  ON DUPLICATE KEY UPDATE activities_registries.`count` = activities_registries.`count` + 1;
END $$
DELIMITER ;

DROP TRIGGER IF EXISTS `riseupdb_v2`.`tr_au_registry_fill`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` TRIGGER `riseupdb_v2`.`tr_au_registry_fill` AFTER UPDATE ON `riseupdb_v2`.`activities`
FOR EACH ROW BEGIN
  IF DATE(NEW.start_dt) != DATE(OLD.start_dt) THEN
    INSERT INTO activities_registries (user_id, `date`, `count`) VALUES (NEW.user_id, DATE(NEW.start_dt), 1)
    ON DUPLICATE KEY UPDATE activities_registries.`count` = activities_registries.`count` + 1;

    UPDATE activities_registries SET activities_registries.`count` = GREATEST(0, activities_registries.`count` - 1) WHERE user_id = OLD.user_id AND activities_registries.`date` = DATE(OLD.start_dt);
  END IF;
END $$

DELIMITER ;
